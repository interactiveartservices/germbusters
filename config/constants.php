<?php
/**
* PUT YOUR CUSTOM CONFIGURATION HERE ON YOUR CONTROLLERS, YOU MAY USE
* Config::get(App::environment() . 'constants.ARRAY_KEY')
*/
return [
	'iasadmin_url' => 'http://padawan-dev-php.interactiveartservices.com:8888',

	// zoho crm url
	'zohocrmurl' => 'https://crm.zoho.com/crm/private',
	// zoho authentication token
	'authtoken' => '2a3daabfa8dc4fddb4702fe32095aca9',

	'default_localhost_ip_list' => [
		'127.0.0.1', '10.0.2.2'
	],

	//for login
	'enc_key' => 'Klet9Yg,!RiAfQqOhgF%ghClle_a68',

	/** Data Encrypt Standard **/
	// 32 * 8 = 256 bit key
	'des_key' => 'mkirwd897+22#bbtrm8814z5qx=498j9',
	// 32 * 8 = 256 bit iv
	'des_iv' => '141352hhbeye66#cs!9hjv887mxx7@2y',
	'client_testmonial_jid' => 12314,
	
];
