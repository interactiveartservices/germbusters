<?php
/**
* PUT YOUR CUSTOM CONFIGURATION HERE ON YOUR CONTROLLERS, YOU MAY USE
* Config::get(App::environment() . 'constants.ARRAY_KEY')
*/
return [
	//'iasadmin_url' => 'http://iasadmin.local.dev',
	'iasadmin_url' => 'http://padawan-dev-php.interactiveartservices.com:8888',

	// zoho crm url
	'zohocrmurl' => 'https://crm.zoho.com/crm/private',
	// zoho authentication token
	'authtoken' => '2a3daabfa8dc4fddb4702fe32095aca9',
	
	'default_localhost_ip_list' => [
		'127.0.0.1', '10.0.2.2'
	],

	'login_page' => 'login',
	'login_mobile' => 'login',	
];
