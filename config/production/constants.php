<?php
/**
* PUT YOUR CUSTOM CONFIGURATION HERE ON YOUR CONTROLLERS, YOU MAY USE
* Config::get(App::environment() . 'constants.ARRAY_KEY')
*/
return [
	'iasadmin_url' => 'http://admin.storyboards.com',

	// zoho crm url
	'zohocrmurl' => 'https://crm.zoho.com/crm/private',
	// zoho authentication token
	'authtoken' => '2a3daabfa8dc4fddb4702fe32095aca9',

	'default_localhost_ip_list' => [
		'127.0.0.1', '10.0.2.2'
	],
	
	/*
	*	'login_page' => 'http://admin.storyboards.com/login',
	*	'login_mobile' => 'http://admin.storyboards.com/login',
	*/

	'login_page' => 'login',
	'login_mobile' => 'login',
	
	/** Data Encrypt Standard **/
	// 32 * 8 = 256 bit key
	'des_key' => 'mkirwd897+22#bbtrm8814z5qx=498j9',
	// 32 * 8 = 256 bit iv
	'des_iv' => '141352hhbeye66#cs!9hjv887mxx7@2y',

	'client_testmonial_jid' => 12314,

	'stripe' => [ 
					// switch to test/live mode
					'mode'=>'live',
					// Test API Keys
					'test'=>[
								'pk'=>'pk_test_51H9vvwG8gn6lkaz9NmLanvMAJK4PM35uUG56HLuCmQycpz11BkXnx2Bt22TINnCxFSJn22hMbZYnB7ABVEpAYKWq00r0chCS2b', // publishable key
								'sk'=>'sk_test_51H9vvwG8gn6lkaz9Ait1lTuor0Oww3fkg2IDs6Gw1SlLJJqb7tLK2OqccqZ2rcaDuqGBNSc6EJnaf2bZKJApXufA00t5rtAdZy'  // secret key

							],
					// Live API Keys
					'live'=>[
								'pk'=>'pk_live_51H9vvwG8gn6lkaz996t6KWbi0GZEGIQfiwkmELWFmj0KEXMdTCYUTiqwwMZofgkursMft1BCFkuitIPsYbmhJsUC00LtuX3OcQ', // publishable key
								'sk'=>'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'  // secret key
							]
				],


	// PAYPAL
	'paypal' => [ 
					// switch to test/live mode
					'mode'=>'live',
					// Test Client ID
					'test'=>[
								'env'=>'sandbox',
								// client id for sandbox
								'sandbox'=>'AY1JjNN95N-_SK8ZDchNsPZ87WGTvQXqnZmo9WpMomNPhEclueS9LrZOL4XAjQoqfCgoFOk4nDihDO6j',
								// client id for production
								'production'=>'AS5D4coKV9sQR3YiZtIr1QwZd8NrcDfrLpZYOpxG9gyNzWWs6PUgqlPdMkW6ygAwqQp1-tV5xbm9Inxn'
							],
					// Live Client ID
					'live'=>[
								'env'=>'production',
								// client id for sandbox
								'sandbox'=>'AUQcJWLvqcw44_2rjWh92s0KlC6l_Hw3iZS3MHPcWxrTqgxbBW-ubwDG7baBTZtbrpGtaYk2np7gnpI_',
								// client id for production
								'production'=>'AS5D4coKV9sQR3YiZtIr1QwZd8NrcDfrLpZYOpxG9gyNzWWs6PUgqlPdMkW6ygAwqQp1-tV5xbm9Inxn'
							]
				],	
];
