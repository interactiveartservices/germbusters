<?php 

function SendEmail($mail, $data)
{
    Mail::send($mail->body, $data, function($message) use ($mail) {
        if(isset($mail->to)) {
	        foreach($mail->to as $key => $value) {
	            $message->to($value->address, $value->name);
	        }
	    }

        if(isset($mail->replyTo)) {
            foreach($mail->replyTo as $key => $value) {
                $message->replyTo($value->address, $value->name);
            }
        }

        if(isset($mail->bcc)) {
            foreach($mail->bcc as $key => $value) {
                $message->bcc($value->address, $value->name);
            }                  
        }

        $message->from($mail->sender->address, $mail->sender->name);
        $message->subject($mail->subject);
    });  
} 

class MailAddress {
    public $name;
    public $address;

    function __construct($m_name, $m_address)
    {
        $this->name    = $m_name;
        $this->address = $m_address;
    }
}

class MailAttachment  {
    public $path;
    public $name;

    function __construct($m_path, $m_name)
    {
        $this->path = $m_path;
        $this->name = $m_name;
    }
}