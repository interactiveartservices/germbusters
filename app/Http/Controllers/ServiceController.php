<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use View,Config,DB, Mail, Response;
use App\Models\UserLog, Auth, App\Models\Services;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Geocoder\StatefulGeocoder;
use GeoIp2\Database\Reader;
use Http\Adapter\Guzzle6\Client;

use Session, Cookie;

class ServiceController extends Controller
{

    public function __construct(Services $services)
    {
        /**
         * For body id and class
         */
        $this->body_id = '';
        $this->body_class = '';
        $this->sitelang = (!empty($_GET['sitelang'])) ? $_GET['sitelang'] : '';
        $this->lang = (!empty($_GET['lang'])) ? $_GET['lang'] : '';
        $this->is_survey = false;
        $this->redirectTo = Config::get(App::environment() . '.constants.iasadmin_url');

        /**
         * Set IP
         * Note: To be secure for AWS, please use HTTP_X_FORWARDED_FOR
         * return array
         */

        // $this->showlocation = \App::make('App\Http\Libraries\Geoip\Geoip')->getLocationByIp($ip,$this->lang);

        $this->services = $services;
           
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function commercial_services()
    {

        $this->body_id = 'commercial_services';

        $is_chrome = 'ua-not-chrome';
        if(preg_match('/(Chrome|CriOS)\//i',$_SERVER['HTTP_USER_AGENT'])  && !preg_match('/(Aviator|ChromePlus|coc_|Dragon|Edge|Flock|Iron|Kinza|Maxthon|MxNitro|Nichrome|OPR|Perk|Rockmelt|Seznam|Sleipnir|Spark|UBrowser|Vivaldi|WebExplorer|YaBrowser)/i',$_SERVER['HTTP_USER_AGENT'])){
            // Browser might be Google Chrome
            $is_chrome = 'ua-yes-chrome';
        } else {
            $is_chrome = 'ua-not-chrome';
        }
         
        return View::make('services.commercial_services',[])
            ->with('body_id' , $this->body_id)
            ->with('body_class', 'commercial')
            ->with('head_title', 'Commercial | Cleaning & Disinfection Service | Germbusters ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Commercial');
    }

    public function modify_services(request $request)
    {
        $this->body_id = 'modify_services';
        $this->body_class = 'gb-services';

        $shoppingcart = 
            App::make('App\Http\Controllers\ShoppingCartController');

        $cookie_name = $shoppingcart::COOKIE_NAME;

        if(!Cookie::has($cookie_name)) {
           Cookie::queue($cookie_name, uniqid('germbusters', true), 518400);
        } 

        return View::make('services.modify_services',[])
            ->with('body_id' , $this->body_id)
            ->with('body_class', $this->body_class)
            ->with('items', $shoppingcart->GetCartItems());
    }

    public function scheduling(request $request)
    {    
        $this->body_id = 'schedule_services';
        $this->body_class = 'gb-services';

        $shoppingcart = 
            App::make('App\Http\Controllers\ShoppingCartController');
  
        if(!$shoppingcart->IsCart()) {
            return redirect("/cleaning-quote/modify-services");
        }

        return View::make('services.schedule_services',[])
            ->with('body_id' , $this->body_id)
            ->with('body_class', $this->body_class)
            ->with('items', $shoppingcart->GetCartItems());
    }

    public function checkout(request $request)
    {
        $shoppingcart = 
            App::make('App\Http\Controllers\ShoppingCartController');

        if(!$shoppingcart->IsCart()) {
            return redirect("/cleaning-quote/modify-services");
        }

        $addresses = null;

        if(Auth::check()) {
            $addresses = App::make("\App\Models\Account")->GetAddresses(Auth::id());
        }

        return View::make('services.checkout_services',[])
            ->with('body_id' , 'checkout_services')
            ->with('body_class', 'gb-services')
            ->with('items', $shoppingcart->GetCartItems())
            ->with('addresses', $addresses);
    }

    public function review_order(request $request)
    {
        $shoppingcart = 
            App::make('App\Http\Controllers\ShoppingCartController');

        if(!$shoppingcart->IsCart()) {
            return redirect("/cleaning-quote/modify-services");
        }

        return View::make('services.review_order',[])
            ->with('body_id' , 'review_order')
            ->with('body_class', 'gb-services')
            ->with('items', $shoppingcart->GetCartItems());
    }

    public function update_order(request $request)
    {
        $shoppingcart = 
            App::make('App\Http\Controllers\ShoppingCartController');

        if($request->action == 'your-information') {
            $address = App::make("\App\Models\Account")->GetAddressById($request->address);

            if(Auth::check()) {
                $shoppingcart->UpdateDetails(Auth::check() ? Auth::user()->firstname : '', 
                                             Auth::check() ? Auth::user()->lastname  : '', 
                                             $address->address1,
                                             $address->address2, 
                                             $address->city, 
                                             $address->state, 
                                             $address->zipcode, 
                                             Auth::check() ? Auth::user()->email  : '', 
                                             $address->phone,
                                             $request->special_instructions,
                                             $request->offers,
                                             $address->id,
                                             $request->type);
            }

            return  redirect('/cleaning-quote/review-order');
        } else if($request->action == 'guest-information') {
            $shoppingcart->UpdateDetails($request->fname, 
                                         $request->lname, 
                                         $request->address1,
                                         $request->address2, 
                                         $request->city, 
                                         $request->state, 
                                         $request->zipcode, 
                                         $request->email, 
                                         $request->phone,
                                         $request->special_instructions,
                                         $request->offers);

            return  redirect('/cleaning-quote/review-order');
        } else if($request->action == 'addr-information') {
            $items = $shoppingcart->GetCartItems();

            if(Auth::check()) {
                $addr = App::make("\App\Models\Account");

                $addr_id = $addr->Address(Auth::id(), 
                                          $request->address1, 
                                          $request->address2, 
                                          $request->zipcode, 
                                          $request->city, 
                                          $request->state,
                                          $request->phone,
                                          $request->altphone,
                                          '',
                                          0);

                $shoppingcart->UpdateDetails(Auth::check() ? Auth::user()->firstname : '', 
                                             Auth::check() ? Auth::user()->lastname  : '', 
                                             $request->address1,
                                             $request->address2, 
                                             $request->city, 
                                             $request->state, 
                                             $request->zipcode, 
                                             Auth::check() ? Auth::user()->email  : '', 
                                             $request->phone,
                                             $request->special_instructions,
                                             $items['buyer']['offers'],
                                             $addr_id);
            }

            return  redirect('/cleaning-quote/review-order');
        } else if($request->action == 'save-information') {
            $order = App::make("\App\Models\Order")
                        ->SaveOrder(Auth::check() ? Auth::id() : 0, $shoppingcart->GetCartItems());

            $mail = new \stdClass();

            $mail->sender = new \MailAddress('Germbusters', 'administrative@storyboards.com');

            if(strtolower(App::environment()) == 'local') {
                $mail->to[] = new \MailAddress('catherine', 'catherine@storyboards.com');
            } else {
                $mail->to[] = new \MailAddress('hello', 'hello@germbusters911.com');
            }

            $mail->subject = 'New Booking';

            $mail->body = 'services.templates._order';

            SendEmail($mail, [ 'order'=> $shoppingcart->GetCartItems() ]);

            return redirect('/cleaning-quote/residential/success/'. $order);
        } else {
            return "Page Not Found.";
        }  
    }

    public function residential_success($uid = 0)
    {
        $shoppingcart = 
            App::make('App\Http\Controllers\ShoppingCartController');

        if(!$shoppingcart->IsCart()) {
            return redirect("/cleaning-quote/modify-services");
        }

        $cookie_name = $shoppingcart::COOKIE_NAME;

        if(Cookie::has($cookie_name)) {
           Cookie::queue(Cookie::forget($cookie_name));
        }

        $order = App::make("\App\Models\Order")->GetOrderByGenID($uid);

        if(isset($order)) {
            $items = App::make("\App\Models\Order")->GetOrderItemsByGenID($uid);

            return View::make('services.residential_success',[])
                ->with('body_id' , 'residential_success')
                ->with('body_class','success')
                ->with('order', $order)
                ->with('items', $items);
        } else {
            return "Invalid ID";
        }
   }

    public function commercial_success($gen_id = 0)
    {

        
        $user = $this->services->GetRequest($gen_id);  

        return View::make('services.commercial_success',[])
            ->with('body_id' , 'commercial_success')
            ->with('body_class', 'success')
             ->with('user', $user);
            // ->with('requests', $this->services->GetRequest($request->id));
   }
    


    public function save_commercial_services(Request $request)
    {

        $is_deep_disinfection = $request->is_deep_disinfection;
        $is_wiping_service = $request->is_wiping_service;
        $is_uv_disinfection = $request->is_uv_disinfection;
        $is_weekly_maintenance = $request->is_weekly_maintenance;
        $is_monthly_maintenance = $request->is_monthly_maintenance;
        $square_footage = $request->square_footage;
        $promo_code =  $request->promo_code;
        $how_did_you_hear = $request->how_did_you_hear;
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $company = $request->company;
        $location_number_name = $request->location_number_name;
        $address = $request->address;
        $address2 = $request->address2;
        $zipcode = $request->zipcode;
        $city = $request->city;
        $state = $request->state;
        $no_of_locations = $request->no_of_locations;
        $phone = $request->phone;
        $alt_phone = $request->alt_phone;
        $email = $request->email;
        $special_instructions = $request->special_instructions;
       
        $request_service = new Services();
        $gen_id = $this->services->Commercial_Service($is_deep_disinfection, $is_wiping_service, $is_uv_disinfection, $is_weekly_maintenance, $is_monthly_maintenance, $square_footage, $promo_code,  $how_did_you_hear, $firstname, $lastname, $company, $location_number_name, $address, $address2, $zipcode, $city, $state, $no_of_locations, $phone, $alt_phone, $email, $special_instructions);

        // for admin
        $bodymsg =  "Square Footage:  " . $square_footage . "\r\n";
        $bodymsg .= "Promo Code:  " . $promo_code . "\r\n";
        $bodymsg .= "How did you hear about us:  " . $how_did_you_hear . "\r\n";
        $bodymsg .= "Name:  " . $firstname . " " . $lastname . "\r\n";
        $bodymsg .= "Company:  " . $company . "\r\n";
        $bodymsg .= "Locations Name/Number:  " . $location_number_name . "\r\n";
        $bodymsg .= "Address1:  " . $address .  "\r\n";  
        $bodymsg .= "Address2:  " . $address2 . "\r\n";
        $bodymsg .= "Zip Code:  " . $zipcode .  "\r\n";
        $bodymsg .= "City:  " . $city .  "\r\n";
        $bodymsg .= "State:  " . $state . "\r\n";
        $bodymsg .= "No. of Locations:  " . $no_of_locations . "\r\n";
        $bodymsg .= "Phone:  " . $request->phone . "\r\n";
        $bodymsg .= "Alt Phone:  " . $request->alt_phone . "\r\n";
        $bodymsg .= "Email Address:  " . $request->email . "\r\n";
        $bodymsg .= "Special Instructions:  " . $request->special_instructions . "\r\n";
        $bodymsg .= "Service: " . (($is_deep_disinfection == "1") ? "Electrostatic Deep Cleaning Disinfection" : "") . (($is_wiping_service == "1") ? ", Hand wiping service" : "") . (($is_uv_disinfection == "1") ? ", UV-C Disinfection" : "") . (($is_weekly_maintenance == "1") ? ", Weekly maintenance plans" : "") . (($is_monthly_maintenance == "1") ? ", Monthly maintenance plans" : "");

        Mail::raw($bodymsg, function ($message) use ($firstname,$lastname,$email){
            $message->to('hello@germbusters911.com','hello')->from('administrative@storyboards.com', $firstname . " " . $lastname)->subject("GermBusters - New Commercial Cleaning Estimate")->replyto($email, $firstname . " " . $lastname);
        });

             // check for failures
          if (Mail::failures()) {
            // return response showing failed emails
            return json_encode(array("success"=>false));
         } else {
              //return json_encode(array("success"=>true,"id"=>$gen_id));
           return $gen_id;
         }


  }

      public function send_contact_form(Request $request)
    {
        $frmpage = $request->frmpage;
        $industry = $request->industry;
        $product = $request->product;
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $jobtitle = $request->jobtitle;
        $phone = $request->phone;
        $email = $request->email;
        $message = $request->message;
        $notify = $request->notify;

        $request_service = new Services();
        $gen_id = $this->services->Save_Quote($frmpage,$industry, $firstname, $lastname, $jobtitle, $email, $phone, $message, $notify);
      

        // for admin
        $bodymsg =  "Page:  " . $frmpage. "\r\n\r\n";
        $bodymsg .=  "Industry:  " . $industry. "\r\n";
        $bodymsg .= "Product:  " . $product. "\r\n";
        $bodymsg .= "Name:  " . $firstname . " " . $lastname . "\r\n";
        $bodymsg .= "Job Title:  " . $jobtitle .  "\r\n";  
        $bodymsg .= "Phone:  " . $phone . "\r\n";
        $bodymsg .= "Email:  " . $email .  "\r\n";
        $bodymsg .= "Message:  " . $message .  "\r\n";
  
        Mail::raw($bodymsg, function ($message) use ($firstname,$lastname,$email){
            $message->to('hello@germbusters911.com','hello')->from('administrative@storyboards.com', $firstname . " " . $lastname)->subject("Germbusters - Get Quote")->replyto($email, $firstname . " " . $lastname);
        });

        // check for failures
          if (Mail::failures()) {
            // return response showing failed emails
            return json_encode(array("success"=>false));
         } else {
             return json_encode(array("success"=>true,"id"=>""));
         }

  }


   public function get_quote_list()
    {
         $quote = App::make("\App\Models\Services");

            return View::make('get_quote_list',[])
                    ->with('body_id' , 'get_quote_list')
                    ->with('body_class', 'admin-list')
                    ->with('head_title', 'List - Get Quote | Germbusters ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Estimates | Invoice')
                    ->with('quotes', $quote->Get_Quotes());
   
    }

  public function add_note(Request $request)
    {
        $message = "";

        if(Auth::check()) {
            $result = 
                App::make("\App\Models\Services")->AddNote($request->id, 
                                                          $request->notes);
            if(!$result) {
                $message = "Saved.";
            }

        }

         return json_encode(array("success"=>true,"id"=>""));
    }



  public function getLocationByZip(string $query)
  {
      $client = new Client();

      $provider = new GoogleMaps($client, 'us', 'AIzaSyC2DQNn31o4aKsciGI9IZEE11X7EyRmlyQ');
      $geocoder = new StatefulGeocoder($provider, 'en');
      $dumper = new \Geocoder\Dumper\GeoArray();

      $result = $geocoder->geocodeQuery(GeocodeQuery::create($query));
      $str = $dumper->dump($result->first());        

    //   $fl = $geocoder->geocodeQuery(GeocodeQuery::create('1905 NW 32 street, Pompano Beach, FL 33064'));          
    //   $dumper = new \Geocoder\Dumper\GeoArray();
    //   $fl_str = $dumper->dump($fl->first());

      $fl = [-80.14940800000001,26.2700565]; // coordinates for fl office | 1905 NW 32 street, Pompano Beach, FL 33064
      $charlotte = [-80.9724505,35.3443217]; //coordinates for charloote office | 2807 Mt Isle Harbor Dr Charlotte , N.C 28214

      //var_export($result);
      //return $str['geometry'];
      return response()->json([             
            'o' => $fl, 
            'c' => $charlotte,
            'q' => $str['geometry']['coordinates'] 
         ], 200);
      //return $str['geometry']['coordinates'];
  }

  public function getLocationFromCoords($lat, $lng)
  {
    $fl = [-80.14940800000001,26.2700565]; // coordinates for fl office | 1905 NW 32 street, Pompano Beach, FL 33064
    $charlotte = [-80.9724505,35.3443217]; //coordinates for charloote office | 2807 Mt Isle Harbor Dr Charlotte , N.C 28214
    
    return response()->json([             
        'o' => $fl, 
        'c' => $charlotte,
        'q' => [$lat, $lng]
     ], 200);
  }


 
}
