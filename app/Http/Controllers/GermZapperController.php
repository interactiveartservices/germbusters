<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\App;
use View, Config, Blade, Response, Auth, Session, Cookie;

class GermZapperController extends Controller 
{
  const COOKIE_NAME = 'germzapper';
  const CASE_TYPE   = 'case-type';

	public function __construct(\App\Models\ShoppingCart $shoppingcart) 
	{
		$this->middleware(function ($request, $next) {
            $stripe = Config::get(App::environment(). '.constants.stripe');

             if(strtolower($stripe['mode']) == 'live') {
                $this->stripe = $stripe['live'];
             } else {
                $this->stripe = $stripe['test'];
             }                                    

              return $next($request);
        }); 

        $this->shoppingcart = $shoppingcart;
  	}

    public function InitializeItems($casetype)
    {
        $items = [
                    'list'=>[
                                 [
                                             'id'=>'5ece4797eaf5e',
                                    'description'=>'*Ozone free',
                                           'type'=>'bulb',
                                      'case-type'=>'handheld-case',
                                          'price'=>2295.00,
                                       'selected'=>$casetype == 'handheld-case' ? 1 : 0
                                  ],
                                  [
                                             'id'=>'5f3a352595d32',
                                    'description'=>'+Ozone',
                                           'type'=>'bulb',
                                      'case-type'=>'handheld-case',
                                          'price'=> 2205.00,
                                       'selected'=>0  
                                  ],
                                  [
                                             'id'=>'5f40ea9551b84',
                                    'description'=>'*Ozone free',
                                           'type'=>'bulb',
                                      'case-type'=>'multipurpose-case',
                                          'price'=>2395.00,
                                       'selected'=>$casetype == 'multipurpose-case' ? 1 : 0
                                  ],
                                  [
                                             'id'=>'5f40eaa2c8bcf',
                                    'description'=>'+Ozone',
                                           'type'=>'bulb',
                                      'case-type'=>'multipurpose-case',
                                          'price'=> 2305.00,
                                       'selected'=>0  
                                  ],
                                  [
                                             'id'=>'5f3792cd65134',
                                    'description'=>'Stand Kit',
                                           'type'=>'case',
                                      'case-type'=>'',
                                          'price'=>589.00,
                                       'selected'=>0  
                                  ],
                                  [
                                             'id'=>'5f379728eef24',
                                    'description'=>'Carry Case',
                                           'type'=>'case',
                                      'case-type'=>'',
                                          'price'=>325.00,
                                       'selected'=>0  
                                ],
                            ],
                   'total'=>0.00
                ];          

        $this::Calculate($items);
        return $items;
    }

    public function germzappercart(Request $request)
    {
        $items = [];
        $ckeid = $this::GetCookieID();

        switch(strtolower($request->action)) {
            case 'update':
                $data = $this->shoppingcart->GetShoppingCart($ckeid);

                if((int)$request->reset == 0) {
                    if(isset($data)) {
                        $items = $data;
                    } else {
                        $items = $this::InitializeItems($request->casetype); 
                    }
                } else {
                    $items = $this::InitializeItems($request->casetype); 
                }

                if(strtolower($request->type) == 'bulb') {
                    foreach($items['list'] as $key => &$list) {
                        if(strtolower($list['type']) == strtolower($request->type)) {
                            $list['selected'] = 0;
                        }
                    }
                }

                foreach($items['list'] as $key => &$list) {
                    if($list['id'] == $request->id) {
                        $list['selected'] = (int)$request->isselected;
                    }
                }

                $this::Calculate($items);
                break;
        }

        
        $this->shoppingcart->SaveShoppingCart($ckeid, json_encode($items));

        return $items;
    }

    private function Calculate(&$items)
    {
        $total = 0.00;

        foreach($items['list'] as $key => &$list) {
            if((int)$list['selected'] == 1) {
                $total = $total + (float)$list['price'];
            }
        }

        $items['total'] = $total;
    }

    public function uv_disinfection_device($slug)
    {
        $cookie_name = $this::COOKIE_NAME;

        if(Cookie::has($cookie_name)) {
           Cookie::queue(Cookie::forget($cookie_name));
        }

        return View::make('uv_disinfection_device', [])
                ->with('body_id' , 'services-page')
                ->with('body_class', 'services')
                ->with('head_title', 'UV Light Disinfection Service | Germbusters911 ')
                ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business')
                ->with('meta_img', 'https://www.germbusters911.com/images/germzapper/banner1-home@1x.jpg');
    }

    public function gz_step1($case)
    {
        $cookie_name = $this::COOKIE_NAME;

        if(Cookie::has($cookie_name)) {
           Cookie::queue(Cookie::forget($cookie_name));
        }

        Cookie::queue($cookie_name, uniqid($cookie_name, true), 518400);
        
        $items = $this->InitializeItems($case);

        $head_title = $case == 'multipurpose-case' ? 'Germzapper UV light Multipurpose' : 'Germzapper UV light Handheld';
        $meta_desc = $case == 'multipurpose-case' ? 'GermZapper 300® multi-function case is portable enough to hold in your hands, yet powerful enough to disinfect a room. Just turn it on, shine & sanitize' : 'GermZapper 300® handheld case kills 99.99% of all pathogens on surfaces without the use of harsh chemicals. Just turn it on, shine & sanitize';

        return View::make('germzapper.bulb', [])
	            ->with('body_id' , 'germzapper')
	            ->with('body_class', 'germzapper scroll-off')
	            ->with('items', $items)
              ->with('case', strtolower($case))
              ->with('head_title', $head_title)
              ->with('meta_desc', $meta_desc);
    }

    public function gz_step2($case)
    {
        $items = $this->GetItems($case);

        $hasCase = false;
        foreach($items['list'] as $key => $data) {
            if((strtolower($data['type']) == 'case') && ((int)$data['selected']) == 1) {
                $hasCase = true;
                break;
            }
        }

        if(!$hasCase) {
            foreach($items['list'] as $key => &$data) {
                if((strtolower($data['type']) == 'case')) {
                    $data['selected'] = 1;
                    break;
                }
            }

            $this::Calculate($items);
            $this->shoppingcart->SaveShoppingCart($this::GetCookieID(), json_encode($items));

            $items = $this->GetItems($case);
        }

        return View::make('germzapper.accessories', [])
	            ->with('body_id' , 'germzapper')
	            ->with('body_class', 'germzapper')
	            ->with('items', $items)
              ->with('case', strtolower($case));
    }


    public function gz_step3($case)
    {
        $items = $this->GetItems($case);

        return View::make('germzapper.payment', [])
	            ->with('body_id' , 'germzapper')
	            ->with('body_class', 'germzapper')
              ->with('stripe', $this->stripe)
              ->with('items', $items)
              ->with('case', strtolower($case));
    }

    public function gz_step4()
    {
        if(!$this->IsCart()) {
            return redirect("/uv-light-disinfection-device");
        }

        $cookie_name = $this::COOKIE_NAME;

        if(Cookie::has($cookie_name)) {
           Cookie::queue(Cookie::forget($cookie_name));
        }

        return View::make('germzapper.success', [])
              ->with('body_id' , 'germzapper')
              ->with('body_class', 'germzapper');
    }

    public function IsCart()
    {
        return (Cookie::has($this::COOKIE_NAME));
    }

    public function process(Request $request)
    {
        $items  = $this->GetItems($request->casetype);
        $method = 'stripe';

        $bulb = '';
        $case = '';
        foreach($items['list'] as $key => $data) {
            if((strtolower($data['type']) == 'bulb') && ((int)$data['selected']) == 1) {
                $bulb = $data['description'];
                $case = $data['case-type'];
                break;
            }
        }

        $isstandkit = 0;
        $iscarycase = 0;

        foreach($items['list'] as $key => $data) {
            if((strtolower($data['id']) == '5f3792cd65134') && ((int)$data['selected']) == 1) {
                $isstandkit = 1;
            }

            if((strtolower($data['id']) == '5f379728eef24') && ((int)$data['selected']) == 1) {
                $iscarycase = 1;
            }
        }

        $data = (object)[ 
                             'name'=>$request->cardname,
                            'token'=>$request->stripetoken, 
                           'charge'=>floatval($items['total']), 
                            'email'=>$request->email
                        ];

        $response = $this->CreditCard($data, false);

        $id = App::make("\App\Models\GermZapper")
                ->SaveOrder($bulb, $case, $isstandkit, $iscarycase, $request->fname, $request->lname, $request->phone, $request->email, $request->zipcode, $items['total'], $response);

        if($id > 0) {
            $mail = new \stdClass();

            $mail->sender = new \MailAddress('Germbusters', 'administrative@storyboards.com');

            if(strtolower(App::environment()) == 'local') {
                $mail->to[] = new \MailAddress('aries', 'aries@storyboards.com');
                $mail->to[] = new \MailAddress('catherine', 'catherine@storyboards.com');
            } else {
                $mail->to[] = new \MailAddress('hello', 'hello@germbusters911.com');
            }

            $mail->subject = 'New Gemzapper Order';
            $mail->body = 'germzapper.templates._notification';

            SendEmail($mail, [ 
                                'order'=>App::make("\App\Models\GermZapper")->GetOrder($id),
                                'items'=>$items
                             ]);

            return redirect("/germzapper-uv-light-disinfection-device/success/" . $request->casetype);
        } else {
            return "Error: Unable to process order.";
        }
    }

   private function CreditCard($data, $usestorecard)
   {
        \Stripe\Stripe::setApiKey($this->stripe['sk']);

        try {
             if($usestorecard) {
                $customer = App::make("\App\Models\Order")->GetCustomerStoreCard(0);
            } else {
                $customer = \Stripe\Customer::create(array(
                                                             'description'=>'Customer for '. $data->name,
                                                                  'source'=>$data->token,
                                                                   'email'=>$data->email
                                                          ));
         }
         
         $stripe = \Stripe\Charge::create(array(
                                                         'customer'=>is_object($customer) ? $customer->id : $customer,
                                                           'amount'=>floatval($data->charge) * 100,
                                                         'currency'=>'usd',
                                                      'description'=>'Cardholder Name - '. $data->name,
                                                    'receipt_email'=>$data->email
                                               ));

            return (object)[ 'success'=>true,  'response'=>$stripe ];
        } catch(\Stripe\Error\Card $e) {
             return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            return (object)['success'=>false, 'response'=>$e ];
        }      
   }

	public function GetItems($case)
    {   
        $items = 
            $this->shoppingcart->GetShoppingCart($this::GetCookieID());

        return isset($items) ? $items : $this::InitializeItems($case);
    }

    private function GetCookieID()
    {
        if(Cookie::has($this::COOKIE_NAME)) {
            return Cookie::get($this::COOKIE_NAME);
        } else {
            return '';
        }
    }
}