<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use View,Config,DB, Mail, Response;
use App\Models\UserLog, Auth, Account, Exception;
use GeoIp2\Database\Reader;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;

class IndexController extends Controller
{

    public function __construct()
    {
        /**
         * For body id and class
         */
        $this->body_id = '';
        $this->body_class = '';
        $this->sitelang = (!empty($_GET['sitelang'])) ? $_GET['sitelang'] : '';
        $this->lang = (!empty($_GET['lang'])) ? $_GET['lang'] : '';
        $this->is_survey = false;
        $this->redirectTo = Config::get(App::environment() . '.constants.iasadmin_url');

        /**
         * Set IP
         * Note: To be secure for AWS, please use HTTP_X_FORWARDED_FOR
         * return array
         */
        $ip = !empty(\Request::server('HTTP_X_FORWARDED_FOR')) ? \Request::server('HTTP_X_FORWARDED_FOR') : \Request::server('REMOTE_ADDR');
        $this->showlocation = \App::make('App\Http\Libraries\Geoip\Geoip')->getLocationByIp($ip,$this->lang);
        
        $this->getGermanText = false;
        if(strtolower($this->showlocation['countryCode']) == 'de') {
            if(strtolower($this->sitelang) == 'en') {
                $this->getGermanText = false;
            } else {
                $this->getGermanText = true;
            }
        }
    }

    public function getIpLocation() {                            
        $reader =  new Reader(app_path() . '/Http/Misc/GeoLite2-City.mmdb');    
        
        $ip = !empty(\Request::server('HTTP_X_FORWARDED_FOR')) ? \Request::server('HTTP_X_FORWARDED_FOR') : \Request::server('REMOTE_ADDR');
        $ip = (empty($ip) || in_array($ip, Config::get('constants.default_localhost_ip_list'))) ? '122.3.255.90' : $ip;        
        $record = $reader->city($ip);

        $fl = [-80.14940800000001,26.2700565]; // coordinates for fl office | 1905 NW 32 street, Pompano Beach, FL 33064
        $charlotte = [-80.9724505,35.3443217]; //coordinates for charloote office | 2807 Mt Isle Harbor Dr Charlotte , N.C 28214
    
        return response()->json([             
            'o' => $fl, 
            'c' => $charlotte,
            'q' => [$record->location->latitude, $record->location->longitude]
        ], 200);        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->body_id = 'home';

        $is_chrome = 'ua-not-chrome';
        if(preg_match('/(Chrome|CriOS)\//i',$_SERVER['HTTP_USER_AGENT'])  && !preg_match('/(Aviator|ChromePlus|coc_|Dragon|Edge|Flock|Iron|Kinza|Maxthon|MxNitro|Nichrome|OPR|Perk|Rockmelt|Seznam|Sleipnir|Spark|UBrowser|Vivaldi|WebExplorer|YaBrowser)/i',$_SERVER['HTTP_USER_AGENT'])){
            // Browser might be Google Chrome
            $is_chrome = 'ua-yes-chrome';
        } else {
            $is_chrome = 'ua-not-chrome';
        }
        
        $addresses = null;

        if(Auth::check()) {
            $addresses = App::make("\App\Models\Account")->GetAddress(Auth::id());
        }

        return View::make('home',['location' => $this->showlocation])
            ->with('body_id' , $this->body_id)
            ->with('body_class', $this->body_class . ' ' . $is_chrome)
            ->with('addresses', $addresses)
            ->with('head_title', 'UVC light | Cleaning-Disinfection Services | Germbusters911')
            ->with('meta_desc', 'Discover the innovative world of GermBusters911 and disinfect your home or office with UVC light, UV Robots, Electrostatic Spray, plus get expert support & breath cleaner air with UVC- HVAC.');;
    }

    public function showUserAccount()
    {
        $account = App::make("\App\Models\Account");

        $orders = null;
        $addresses = null;

        if(Auth::check()) {
            $orders = 
                App::make("\App\Models\Order")->GetOrderByUserID(Auth::id());
            $addresses = 
                App::make("\App\Models\Account")->GetAddresses(Auth::id());
        }

        return View::make('account', [])
                    ->with('body_id' , 'account')
                    ->with('body_class', '')
                    ->with('orders', $orders)
                    ->with('addresses', $addresses);
    }    

    public function uvc_light()
    {
        return View::make('uvc_light', [])
            ->with('body_id' , 'services-page')
            ->with('body_class', 'services')
            ->with('head_title', 'UVC Light Cleaning- Disinfectant Robot & Ultraviolet Machine')
            ->with('meta_desc', 'GermBusters uses hospital-trusted UVC lamps in mobile ultraviolet machines & disinfectant robots both scientifically verified and backed with a no-risk guarantee.');
    }

        public function uv_disinfection_robot()
    {
        return View::make('uv_disinfection_robot', [])
            ->with('body_id' , 'services-page')
            ->with('body_class', 'services')
            ->with('head_title', 'UV Disinfectant Robot | Germbusters911 ')
            ->with('meta_desc', 'Germinator® is an autonomous robot that disinfects 6 rooms per hour and provides 24/7 protection from pathogens. Available for purchase or lease')
            ->with('meta_img', 'https://www.germbusters911.com/images/services-banner@1x.png');
    }


    public function uv_disinfection_cart()
    {
        return View::make('uv_disinfection_cart', [])
            ->with('body_id' , 'services-page')
            ->with('body_class', 'services')
            ->with('head_title', 'UV Light Disinfection Cart | Germbusters911 ')
            ->with('meta_img', 'https://www.germbusters911.com//images/services-banner-cart.png');
    }

     public function uvc_hvac()
    {
        return View::make('uvc_hvac', [])
            ->with('body_id' , 'services-page')
            ->with('body_class', 'services')
            ->with('head_title', 'UV-C HVAC | Germbusters911 ')
            ->with('meta_desc', 'GermBusters offers germicidal UVC HVAC solutions with our proprietary line of UV air purification systems effectively and efficiently eliminate biofilm');
    }


    public function disinfection_services()
    {
        return View::make('disinfection_services', [])
            ->with('body_id' , '')
            ->with('body_class', 'services')
            ->with('head_title', 'Cleaning & Disinfection Service | Germbusters911 ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business protecting against viruses, bacteria & fungi.');
    }  

    public function applications()
    {
        return View::make('applications', [])
            ->with('body_id' , 'applications')
            ->with('body_class', 'privacy')
            ->with('head_title', 'Applications | Cleaning & Disinfection Service | Germbusters911 ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Applications');
    }

    public function about_us()
    {
       
        return View::make('about', [])
            ->with('body_id' , 'about')
            ->with('body_class', 'privacy')
            ->with('head_title', 'About Us | Cleaning & Disinfection Service | Germbusters911 ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | About');
    }

    public function news_index()
    {
       
        return View::make('news.index', [])
            ->with('body_id' , 'news')
            ->with('body_class', 'news')
            ->with('head_title', 'Germbusters911 News & Updates ')
            ->with('meta_desc', 'Germbusters News focuses on News and actionable strategies using UV light technology for disinfection. ');
    }

    public function uv_light_kills()
    {
       
        return View::make('news.uv_light_kills', [])
            ->with('body_id' , 'news')
            ->with('body_class', 'news')
            ->with('head_title', 'UVC Light Disinfects Covid19')
            ->with('meta_desc', 'GermBusters GermZappers™ 300 is a UV light device that destroys Coronavirus in 3 seconds- chemical free')
            ->with('meta_img', 'https://www.germbusters911.com/images/news-thumb1.jpg');
    }

    public function steps_disinfecting_groceries()
    {
       
        return View::make('news.steps_disinfecting_groceries', [])
            ->with('body_id' , 'news')
            ->with('body_class', 'news')
            ->with('head_title', '10 steps Disinfecting groceries UV light ')
            ->with('meta_desc', 'In this quick guide you’ll learn 10 steps to use UV Light to disinfect your groceries using the GermZapper 300')
            ->with('meta_img', 'https://www.germbusters911.com/images/news-germzapper.png');
    }

    public function reasons_uv_light()
    {
       
        return View::make('news.reasons_uv_light', [])
            ->with('body_id' , 'news')
            ->with('body_class', 'news')
            ->with('head_title', '13 benefits of UV light disinfection')
            ->with('meta_desc', '13 reasons to use UV light in your disinfection service and help stop the spread of coronavirus')
            ->with('meta_img', 'https://www.germbusters911.com/images/news-pushcart.jpg');
    }

     public function contact_us()
    {
       
        return View::make('contact', ['location' => $this->showlocation])
            ->with('body_id' , 'contact')
            ->with('body_class', 'services')
            ->with('head_title', 'Contact Us | Cleaning & Disinfection Service | Germbusters911 ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Contact');
    }

    public function privacy_policy()
    {
       
        return View::make('privacy_policy', [])
            ->with('body_id' , 'privacy_policy')
            ->with('body_class', 'privacy')
            ->with('head_title', 'Privacy Policy | Germbusters911 ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Privacy Policy');
    }

    public function terms_conditions()
    {
        return View::make('terms_conditions', [])
            ->with('body_id' , 'terms_conditions')
            ->with('body_class', 'privacy')
            ->with('head_title', 'Terms & Conditions | Germbusters911 ')
            ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Terms');
    }

    public function affiliate_review()
    {
        return View::make('affiliate_review')
        ->with('body_id' , 'affiliate_review')
        ->with('body_class', 'affiliate')
        ->with('head_title', 'Affiliate_review | Germbusters911 ')
        ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Terms');
    }

    public function Address(Request $request)
    {
        if((int)$request->id == 0) {
            if(Auth::check()) {
                $addr = App::make("\App\Models\Account");
                $addr->Address(Auth::id(), 
                               $request->address1, 
                               $request->address2, 
                               $request->zipcode, 
                               $request->city, 
                               $request->state,
                               $request->phone,
                               $request->altphone,
                               $request->sms,
                               $request->isprimary == "1" ? 1 : 0);
            }
        } else {
            if(Auth::check()) {
                $addr = App::make("\App\Models\Account");
                $addr->UpdateAddress($request->id,
                                     Auth::id(), 
                                     $request->address1, 
                                     $request->address2, 
                                     $request->zipcode, 
                                     $request->city, 
                                     $request->state,
                                     $request->phone,
                                     $request->altphone,
                                     $request->sms,
                                     $request->isprimary == "1" ? 1 : 0);
            }            
        }       

        return redirect("/my-account");
    }

    public function update_email(Request $request)
    {
        if(Auth::check()) {
            $email = App::make("\App\Models\Account");
            $email->UpdateEmail(Auth::id(), 
                           $request->email);
        }

        return json_encode(array("success"=>true));   
    }

    public function forgot_password(Request $request)
    {

         $pwd = App::make("\App\Models\Account");
         $user = $pwd->GetPasswordByEmail($request->email);

        $bodymsg =  "Hello!  \r\n\r\n";
        $bodymsg .= "Here's your password for your email " . $request->email . "\r\n\r\n";  
        $bodymsg .= "Password: ".  $user->password_text ."\r\n";

        $email = $request->email;

        Mail::raw($bodymsg, function ($message) use ($email){
            $message->to($email,'hello')->from('administrative@storyboards.com', 'hello')->subject("Germbusters - Forgot Password");
        });


        return json_encode(array("success"=>true));   
    }

    public function unsubscribe(Request $request)
    {
        if(Auth::check()) {
            $offers = App::make("\App\Models\Account");
            $offers->Unsubscribe(Auth::id());
        }

        return json_encode(array("success"=>true));   
    }  


    public function GetAddresses()
    {
        $addresses = null;

        if(Auth::check()) {
            $addresses = App::make("\App\Models\Account")->GetAddresses(Auth::id());
        }

        return json_encode($addresses);        
    }

    public function GetAddress(Request $request)
    {
        $addresses = null;

        if(Auth::check()) {
            $addresses = App::make("\App\Models\Account")->GetAddressById($request->id);
        }

        return json_encode($addresses);        
    }

    public function UpdatePassword(Request $request)
    {
        $message = "";

        if(Auth::check()) {
            $result = 
                App::make("\App\Models\Account")->UpdatePassword(Auth::id(), 
                                                                 $request->oldpass, 
                                                                 $request->newpass);
            if(!$result) {
                $message = "The old password that you have entered is wrong.";
            }

        }

        return $message;
    }

    public function GetOrderDetails(Request $request)
    {
        $order = 
            App::make("\App\Models\Order")->GetOrderByID($request->id);
        $items = 
            App::make("\App\Models\Order")->GetOrderItemsByID($request->id);

        $order->id = str_pad($order->id, 6, "0", STR_PAD_LEFT); 

        return json_encode([ 'order'=>$order, 'items'=>$items ]);        
    }

    public function verify_captcha(Request $request)
    {

        $client = new Client();
        
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => ['secret' => '6LeAfrMZAAAAADu9DKcQafi9wWrLNXXhx56MC9oO', 'response' => $request->code] 
        ]);

        if($response->getStatusCode() == 200) {
            $output = $response->getBody()->getContents();
        }

        return response()->json(json_decode($output), 200);


    }

}