<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use View,Config,DB, Mail, Response;
use App\Models\UserLog, Auth;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Geocoder\StatefulGeocoder;
use GeoIp2\Database\Reader;
use Http\Adapter\Guzzle6\Client;
use Illuminate\Support\Facades\Storage;
use Session, Cookie, URL;
use SnappyPDF;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['review', 'invoice', 'invoice_details', 'promocode', 'invoice_payment', 'process_payment', 'save_details', 'invoice_paid', 'placeorder', 'saveSig', 'getSig', 'print', 'success']]);        

        $this->middleware(function ($request, $next) {
            $stripe = Config::get(App::environment(). '.constants.stripe');
            $paypal = Config::get(App::environment(). '.constants.paypal');

            if(Auth::check()) {
                if(Auth::user()->live == 0) {
                   // stripe settings for dev
                   $this->stripe = $stripe['test'];
                   // paypal settings for dev
                   $this->paypal = $paypal['test'];
                } else {
                   if(strtolower($stripe['mode']) == 'live') {
                      $this->stripe = $stripe['live'];
                   } else {
                      $this->stripe = $stripe['test'];
                   }              

                   if(strtolower($paypal['mode']) == 'live') {
                      $this->paypal = $paypal['live'];
                   } else {
                      $this->paypal = $paypal['test'];
                   }                                                   
                }  
             } else {
                 if(strtolower($stripe['mode']) == 'live') {
                    $this->stripe = $stripe['live'];
                 } else {
                    $this->stripe = $stripe['test'];
                 }              

                 if(strtolower($paypal['mode']) == 'live') {
                    $this->paypal = $paypal['live'];
                 } else {
                    $this->paypal = $paypal['test'];
                 }                         
              }

              return $next($request);
        }); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function estimates_invoices_list($type)
    {
        if(Auth::user()->email == 'affiliateprogram@germbusters911.com') {
            $invoice = App::make("\App\Models\Order");

            return View::make('estimates.list',[])
                    ->with('body_id' , 'estimate_list')
                    ->with('body_class', 'admin-list')
                    ->with('head_title', 'List | Estimates - Invoices | Germbusters911 ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Estimates | Invoice')
                    ->with('invoices', $invoice->GetEstimateList($type))
                    ->with('type', $type);
        } else {
            return "You Don't Have Permission to Access";
        }
    }

    public function create($type, $id)     
    {
        if(Auth::user()->email == 'affiliateprogram@germbusters911.com') {
            $details = null;
            if($type == 'residential') {
                $details = App::make("\App\Models\Order")->GetResidentialInvoiceByID($id);
            } else {
                $details = App::make("\App\Models\Order")->GetCommercialInvoiceByID($id);
            }

            $invoice_items = null;

            if(strtolower($type)=='commercial') {
                if(isset($details->invoice_id)) {
                    $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
                }
            }

            $workorder = '';

            if(isset($details->workorder)) {
                $workorder = trim($details->workorder);
            } 

            if($workorder == '') {
                $workorder = 
                    date("mdyhi"). '-'. str_pad(App::make("\App\Models\Order")->GetCountInvoice()+1, 5, '0', STR_PAD_LEFT);
            }

            return View::make('estimates.create',[])
                    ->with('body_id' , 'estimate')
                    ->with('body_class', 'commercial')
                    ->with('head_title', 'Estimate | Invoice |Germbusters ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Estimates | Invoice')
                    ->with('type', $type)
                    ->with('details', $details)
                    ->with('invoice_items',$invoice_items)
                    ->with('workorder', $workorder)
                    ->with('id', $id);
        } else {
            return "You Don't Have Permission to Access";
        }
    }

    public function review($type, $id, $eid)
    {
        $details = null;
        if($type == 'residential') {
            $details = App::make("\App\Models\Order")->GetResidentialInvoiceByID($id);
        } else {
            $details = App::make("\App\Models\Order")->GetCommercialInvoiceByID($id);
        }

        $invoice_items = null;
        if(isset($details->invoice_id)) {
            if((int)$details->invoice_id > 0) {
                $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
            }
        }

        if(isset($details)) {
            return View::make('estimates.review')
                    ->with('body_id' , 'affiliate_review')
                    ->with('body_class', 'affiliate')
                    ->with('head_title', 'Review Estimate | Germbusters911 ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Review Estimate')
                    ->with('details', $details)
                    ->with('invoice_items',$invoice_items)
                    ->with('type', $type)
                    ->with('id', $id);
        } else {
            return "Invalid ID - Review";
        }
    }    

    public function print($invoice_type, $type, $id, $eid)
    {
        $details = null;

        if($type == 'residential') {
            $details = App::make("\App\Models\Order")->GetResidentialInvoiceByID($id);
        } else {
            $details = App::make("\App\Models\Order")->GetCommercialInvoiceByID($id);
        }

        $invoice_items = null;
        if(isset($details->invoice_id)) {
            if((int)$details->invoice_id > 0) {
                $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
            }
        }

        if(isset($details)) {
            
            if(Storage::exists($id . '.dat')) {
                $file = Storage::get($id . '.dat');
                $sig = decrypt($file);
            } else {
                $sig = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';
            }
            
            $data = [ 
                'invoice_title' => $invoice_type,
                'details'=> $details,
                'invoice_items'=>$invoice_items,
                'type'=>$type,
                'id'=>$id,
                'sig'=>$sig
            ];

            //return View::make('pdf.print', $data);
            $pdf = SnappyPDF::loadView('pdf.print', $data);
            return $pdf->inline($invoice_type . '-' . $details->invoice_id .'.pdf');
        } else {
            return "Invalid ID";
        }

    }

     public function print_paid($id, $eid)
    {
        $details = App::make("\App\Models\Order")->GetPaymentInvoiceByID($id);
    
        $invoice_items = null;
        if(isset($details->invoice_id)) {
            if((int)$details->invoice_id > 0) {
                $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
            }
        }

       if(isset($details)) {
            
            if(Storage::exists($id . '.dat')) {
                $file = Storage::get($id . '.dat');
                $sig = decrypt($file);
            } else {
                $sig = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';
            }
            
            $data = [ 
                'invoice_title' => "Invoice",
                'details'=> $details,
                'invoice_items'=>$invoice_items,
                'type'=>"paid",
                'id'=>$id,
                'sig'=>$sig
            ];

            //return View::make('pdf.print', $data);
            $pdf = SnappyPDF::loadView('pdf.print', $data);
            return $pdf->inline('invoice-paid-' . $details->invoice_id .'.pdf');
        } else {
            return "Invalid ID";
        }

    }       



    public function invoice($type, $id, $eid)
    {
        $details = null;
        if($type == 'residential') {
            $details = App::make("\App\Models\Order")->GetResidentialInvoiceByID($id);
        } else {
            $details = App::make("\App\Models\Order")->GetCommercialInvoiceByID($id);
        }

        $invoice_items = null;
        if(isset($details->invoice_id)) {
            if((int)$details->invoice_id > 0) {
                $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
            }
        }

        if(isset($details)) {
            return View::make('estimates.invoice')
                    ->with('body_id' , 'invoice')
                    ->with('body_class', 'affiliate')
                    ->with('head_title', 'Invoice | Germbusters911 ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Invoice')
                    ->with('details', $details)
                    ->with('invoice_items', $invoice_items)
                    ->with('type', $type)
                    ->with('id', $id);
        } else {
            return "Invalid ID - Invoicess";
        }    
    }

    public function process(request $request)
    {
        $invoice  = App::make("\App\Models\Order");

        $id = $invoice->SaveInvoice($request);

        if($id > 0) {
   
            if(!request("promo_code") == '') {
                return redirect("/estimate/review/". strtolower($request->type). '/'. $id . '/'.  urlencode(base64_encode($id)) . '/?afmc=' . request("promo_code"));
            }else{
                return redirect("/estimate/review/". strtolower($request->type). '/'. $id . '/'.  urlencode(base64_encode($id)));
            }

        }
    }

    public function placeorder(request $request)
    {
        App::make("\App\Models\Order")->SetInvoiceStatus($request->id, $request->type);

        return redirect("invoice/". strtolower($request->type). '/'. $request->id . '/'.  urlencode(base64_encode($request->id)));
    }


    public function invoice_details($type, $id, $eid)
    {
        $details = null;
        if($type == 'residential') {
            $details = App::make("\App\Models\Order")->GetResidentialInvoiceByID($id);
        } else {
            $details = App::make("\App\Models\Order")->GetCommercialInvoiceByID($id);
        }

        $invoice_items = null;
        if(isset($details->invoice_id)) {
            if((int)$details->invoice_id > 0) {
                $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
            }
        }

        if(isset($details)) {
            return View::make('invoice.details')
                    ->with('body_id' , 'estimate')
                    ->with('body_class', 'commercial affiliate')
                    ->with('head_title', 'Invoice Details | Germbusters911 ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Invoice Details')
                    ->with('details', $details)
                    ->with('invoice_items', $invoice_items)
                    ->with('type', $type)
                    ->with('id', $id);
        } else {
            return "Invalid ID - Details";
        }    

    }

    public function success($id, $eid)
    {
      
            return View::make('invoice.success')
                        ->with('body_id' , 'success')
                        ->with('body_class', 'affiliate')
                        ->with('head_title', 'Payment Success | Germbusters911 ')
                        ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Success')
                        ->with('id', $id);
            
    }       

    public function invoice_paid($id, $eid)
    {
        $details = App::make("\App\Models\Order")->GetPaymentInvoiceByID($id);
    
        $invoice_items = null;
        if(isset($details->invoice_id)) {
            if((int)$details->invoice_id > 0) {
                $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
            }
        }

        if(isset($details)) {
            return View::make('invoice.paid')
                        ->with('body_id' , 'estimate')
                        ->with('body_class', 'affiliate')
                        ->with('head_title', 'Paid Invoice | Germbusters911 ')
                        ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Details | Invoice')
                        ->with('details', $details)
                        ->with('invoice_items', $invoice_items)
                        ->with('id', $id);
         } else {
            return "Invalid ID - Paid";
        }        
    }       


    public function send_estimate(Request $request)
    {

        $firstname = $request->fname;
        $lastname = $request->lname;
        $email = $request->email;
        $subject = $request->subject;
        $message = $request->message;
      
        $bodymsg =  $message . "\r\n";
 
        Mail::raw($bodymsg, function ($message) use ($firstname,$lastname,$email,$subject){
            $message->to($email, $firstname . " " . $lastname)->from('administrative@storyboards.com', "Germbusters911")->subject($subject);
        });

        // check for failures
          if (Mail::failures()) {
            // return response showing failed emails
            return json_encode(array("success"=>false));
         } else {
             return json_encode(array("success"=>true,"id"=>""));
         }


  }
    public function saveSig(Request $request)
    {
        $enc_file = encrypt($request->data);
        Storage::put($request->invoice_id . '.dat', $enc_file);
        return response()->json('saved', 200);        
    }

    public function getSig($invoice_id)
    {
        if(Storage::exists($invoice_id . '.dat')) {
            $file = Storage::get($invoice_id . '.dat');
            $dec_file = decrypt($file);
        } else {
            $dec_file = '';
        }
        

        return $dec_file;
    }

    public function invoice_payment($id, $eid)
    {
        $details = App::make("\App\Models\Order")->GetPaymentInvoiceByID($id);

        if(isset($details)) {
            $invoice_items = null;

            if(isset($details->invoice_id)) {
                if((int)$details->invoice_id > 0) {
                    $invoice_items = App::make("\App\Models\Order")->GetInvoiceItemsByID($id, $details->invoice_id);
                }
            }

            return View::make('invoice.payment')
                        ->with('body_id' , 'estimate')
                        ->with('body_class', 'affilliate')
                        ->with('head_title', 'Payment | Germbusters911 ')
                        ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Details | Invoice')
                        ->with('stripe', $this->stripe)
                        ->with('paypal', $this->paypal)
                        ->with('id', $id)
                        ->with('details', $details)
                        ->with('invoice_items', $invoice_items);
        } else {
            return "Invalid ID - Payment";
        }    
    } 

    public function process_payment(Request $request)
    {
        $details = App::make("\App\Models\Order")->GetPaymentInvoiceByID($request->id);

        if(isset($details)) {
            $method  = strtolower($request->paymethod);
            $paymethod  = "";

            if($method == 'creditcard') {
                $data = (object)[ 
                             'name'=>$request->cardname,
                            'token'=>$request->stripetoken, 
                           'charge'=>floatval($details->total), 
                            'email'=>$details->email
                          ];

                $payment = $this->CreditCard($data, false);
                $paymethod = $payment->response->payment_method_details->card->brand;
            } else {
                $payment = (object)[ 'success'=>true, 'response'=>json_decode(base64_decode($request->paypal)) ];
                $paymethod = "paypal"; 
            }

            App::make("\App\Models\Order")
                ->SavePayment($request->id, floatval($details->total), ucwords($paymethod), $request->shipaddr, $method,  ($payment->success ? 'SUCCESS' : 'FAILED'), $payment->response);
            App::make("\App\Models\Order")->UpdateServices($details->invoice_id, floatval($details->total), ($payment->success ? 'Paid' : 'Not Paid'));

            return redirect("invoice/checkout/success/".  $request->id . '/'.  urlencode(base64_encode($request->id)));
        } else {
            return "Invalid ID - Payment Process";
        }
    }

   private function CreditCard($data, $usestorecard)
   {
        \Stripe\Stripe::setApiKey($this->stripe['sk']);

        try {
             if($usestorecard) {
                $customer = App::make("\App\Models\Order")->GetCustomerStoreCard(0);
            } else {
                $customer = \Stripe\Customer::create(array(
                                                             'description'=>'Customer for '. $data->name,
                                                                  'source'=>$data->token,
                                                                   'email'=>$data->email
                                                          ));
         }
         
         $stripe = \Stripe\Charge::create(array(
                                                         'customer'=>is_object($customer) ? $customer->id : $customer,
                                                           'amount'=>floatval($data->charge) * 100,
                                                         'currency'=>'usd',
                                                      'description'=>'Cardholder Name - '. $data->name,
                                                    'receipt_email'=>$data->email
                                               ));

            return (object)[ 'success'=>true,  'response'=>$stripe ];
        } catch(\Stripe\Error\Card $e) {
             return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send yourself an email
            return (object)[ 'success'=>false, 'response'=>$e ];
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            return (object)['success'=>false, 'response'=>$e ];
        }      
   }

    public function save_details(Request $request)
    {
        $id = App::make("\App\Models\Order")->SaveDetails($request);

        if($id > 0) {
            if(!request("promo_code") == '') {
                return redirect("/invoice/checkout/payment/". $id . '/'.  urlencode(base64_encode($id)) . '/?afmc=' . request("promo_code"));
             }else{
                return redirect("/invoice/checkout/payment/". $id . '/'.  urlencode(base64_encode($id)));
            }
        }
    }

    public function promocode(Request $request)
    {
        $promo = App::make("\App\Models\Order")->GetPromoCode(trim($request->promo));

        if(isset($promo)) {
            return $request->promo;
        } else {
            return 0;
        }
    }

    public function delete_service($type, $id)
    {
        App::make("\App\Models\Order")->DeleteService($id, $type);

        return redirect()->back();
    }
}
