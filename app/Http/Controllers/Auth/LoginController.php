<?php

/** 

NOTE: to revert login process to LARAVEL admin follow the steps bellow
      1. Update view page file of login from resources\views\auth\login.blade.php
         Uncomment the form tag with action attribute and remove the existing
      2. Go to public\js\default.js
         On line 1623, remove this code e.preventDefault();

**/

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

use View, Config, App, Auth, Redirect, URL, Socialite, Session;


abstract class SSO
{
    const GOOGLE   = 'google';
    const FACEBOOK = 'facebook';
    const TWITTER  = 'twitter';
}

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Override the traditional login form
     *
     * @return \Illuminate\Http\Response
    */
   
    public function showLoginForm(Request $request)
    {
        $domain  = strtolower($_SERVER['SERVER_NAME']);
        $backurl = \Request::root();

        if(preg_match("/127.0.0.1/i", $domain) || preg_match("/germbusters.local.test/i", $domain) || preg_match("/germbusters911/i", $domain)) {
            $previous = URL::previous();

            if(!preg_match("/login/i", $previous)) {
                $backurl = $previous;
            }
        }

        Session::put('url.intended', $backurl);

        return view::make('auth.login', [])
                    ->with('body_id' , 'signin')
                    ->with('body_class', '')
                    ->with('head_title', 'Login | Germbusters911 ')
                    ->with('meta_desc', 'Login & book or modify an appointment. GermBusters911 and disinfect your home or office with innovative UVC products, spray & wipe.');
    }

    public function showCreateAccount()
    {
        return View::make('auth.signup', [])
                    ->with('body_id' , 'signin')
                    ->with('body_class', '')
                    ->with('head_title', 'Create An Account | Germbusters911 ')
                    ->with('meta_desc', 'Germbusters cleaning & disinfection service uses advanced technology to promote a healthy environment at your home or business | Account');
    }   

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    
    protected function authenticated(Request $request, $user)
    {
        $iscart = 0;

        if(isset($request->cart)) {
            if((int)$request->cart == 1) {
                $iscart = 1;
            } 
        }

        if($iscart == 1) {
            Session::put('url.intended', "/cleaning-quote/checkout");
        }

        $auth = 
            User::where('email', $request->email)->first();

        if($auth) {
            Auth::login($auth, true);  

            if($iscart == 1) {
                $shoppingcart = 
                    App::make('App\Http\Controllers\ShoppingCartController');

                $shoppingcart->UpdateDetails($auth->firstname, 
                                             $auth->lastname, 
                                             $auth ->address1,
                                             $auth ->address2, 
                                             $auth ->city, 
                                             $auth ->state, 
                                             $auth ->zipcode, 
                                             $auth ->email, 
                                             $auth ->phone,
                                             '',
                                             $auth->offers);
            }
        } else {
            return redirect('/login');
        }

        if(Auth::user()->email == 'affiliateprogram@germbusters911.com') {
            return redirect('/list/estimates');
        } else {
            if(Session::has('url.intended')) {
                return Redirect::intended(Session::get('url.intended'));
            } else {
                return redirect("/");
            }
        }
    }

    public function redirectToProvider($provider, $cart = 0)
    {
        if((int)$cart == 1) {
            if(!Session::has('url.cart')) {
                Session::put('url.cart', '1');
            }

            Session::put('url.intended', "/cleaning-quote/checkout");
        } 

        return Socialite::driver($provider)->redirect();
    }   

    public function handleProviderCallback($provider)
    {
        $socialite = null;

        if(strtolower($provider)==SSO::GOOGLE) {
            $socialite = Socialite::driver($provider)->stateless()->user();
        } else if(strtolower($provider)==SSO::FACEBOOK) {
            $socialite = Socialite::driver($provider)->stateless()->user();
        } else if(strtolower($provider)==SSO::TWITTER) {
            $socialite = Socialite::driver($provider)->user();
        }

        if($socialite) {
            $auth = null;

            if(strtolower($provider)==SSO::GOOGLE) {
                $auth = User::where('provider_id', $socialite->user['id'])->first();
            } else if(strtolower($provider)==SSO::FACEBOOK) {
                $auth = User::where('provider_id', $socialite->id)->first();
            } else if(strtolower($provider)==SSO::TWITTER) {
                $auth = User::where('provider_id', $socialite->id)->first();
            }

            if($auth) {
                Auth::login($auth, true);  
            } else {  
                if(strtolower($provider)==SSO::GOOGLE) {
                    $isemail = User::where('email', $socialite->email)->first() ? true : false;

                    if($isemail) {
                        $provider = User::where('email', $socialite->email)->first()->provider;

                        if(!isset($provider) || ($provider!=SSO::GOOGLE))  {
                            $user = User::where('email', $socialite->email)
                                        ->update([
                                                        'provider_id'=>$socialite->user['id'],
                                                           'provider'=>$provider, 
                                                           'password'=>'',
                                                              'email'=>$socialite->email,           
                                                          'firstname'=>$socialite->user['given_name'],
                                                           'lastname'=>$socialite->user['family_name'],
                                                             'avatar'=>$socialite->avatar,
                                                    'avatar_original'=>$socialite->avatar_original,
                                                         'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')                        
                                                 ]);
                        }
                    } else {
                        $user = User::create([
                                                    'provider_id'=>$socialite->user['id'],
                                                       'provider'=>SSO::GOOGLE,
                                                       'password'=>'',
                                                          'email'=>$socialite->email,      
                                                      'firstname'=>$socialite->user['given_name'],
                                                       'lastname'=>$socialite->user['family_name'],
                                                         'avatar'=>$socialite->avatar,
                                                'avatar_original'=>$socialite->avatar_original,       
                                                           'live'=>1,
                                                     'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')   
                                            ]);
                    }

                    $auth = User::where('email', $socialite->email)
                                  ->where('provider', SSO::GOOGLE)
                                  ->first();

                    if($auth) {
                        Auth::login($auth, true);  
                    }    
                } else if(strtolower($provider)==SSO::FACEBOOK) {  
                    $isuser = User::where('provider_id', $socialite->id)
                                    ->where('provider', SSO::FACEBOOK)
                                    ->first() ? true : false;

                    if($isuser)
                    {
                        $provider = User::where('provider_id', $socialite->id)->first()->provider;

                        if(!isset($provider) || ($provider!=SSO::FACEBOOK))  {
                            $user = User::where('provider_id', $socialite->provider_id)
                                        ->update([
                                                        'provider_id'=>$socialite->id,
                                                           'provider'=>$provider, 
                                                           'password'=>'',
                                                              'email'=>$socialite->email,           
                                                          'firstname'=>$socialite->name,
                                                           'lastname'=>'',
                                                             'avatar'=>$socialite->avatar,
                                                    'avatar_original'=>$socialite->avatar_original,
                                                         'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')                      
                                                 ]);
                        }
                    } else {
                        $user = User::create([
                                  'provider_id'=>$socialite->id,
                                     'provider'=>SSO::FACEBOOK,
                                     'password'=>'',
                                        'email'=>$socialite->email,      
                                    'firstname'=>$socialite->name,
                                     'lastname'=>'',
                                       'avatar'=>$socialite->avatar,
                              'avatar_original'=>$socialite->avatar_original,    
                                         'live'=>1,
                                   'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')      
                          ]);
                    }

                    $auth = User::where('provider_id', $socialite->id)
                                  ->where('provider', SSO::FACEBOOK)
                                  ->first();

                    if($auth) {
                        Auth::login($auth, true);  
                    }                 
                } else if(strtolower($provider)==SSO::TWITTER) {
                    $isuser = User::where('provider_id', $socialite->id)
                                    ->where('provider', SSO::TWITTER)
                                    ->first() ? true : false;

                    if($isuser)
                    {
                        $provider = User::where('provider_id', $socialite->id)->first()->provider;

                        if(!isset($provider) || ($provider!=SSO::TWITTER))  {
                            $user = User::where('provider_id', $socialite->provider_id)
                                        ->update([
                                                        'provider_id'=>$socialite->id,
                                                           'provider'=>$provider, 
                                                           'password'=>'',
                                                              'email'=>$socialite->email,           
                                                          'firstname'=>$socialite->name,
                                                           'lastname'=>'',
                                                             'avatar'=>$socialite->avatar,
                                                    'avatar_original'=>$socialite->avatar_original,
                                                         'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')                        
                                                 ]);
                        }
                    } else {
                        $user = User::create([
                                  'provider_id'=>$socialite->id,
                                     'provider'=>SSO::TWITTER,
                                     'password'=>'',
                                        'email'=>$socialite->email,      
                                    'firstname'=>$socialite->name,
                                     'lastname'=>'',
                                       'avatar'=>$socialite->avatar,
                              'avatar_original'=>$socialite->avatar_original, 
                                         'live'=>1,
                                   'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')         
                          ]);
                    }

                    $auth = User::where('provider_id', $socialite->id)
                                  ->where('provider', SSO::TWITTER)
                                  ->first();

                    if($auth) {
                        Auth::login($auth, true);  
                    }    
                }
            }

            if(Session::has('url.cart')) {
                $shoppingcart = 
                    App::make('App\Http\Controllers\ShoppingCartController');

                $shoppingcart->UpdateDetails($auth->firstname, 
                                             $auth->lastname, 
                                             $auth->address1,
                                             $auth->address2, 
                                             $auth->city, 
                                             $auth->state, 
                                             $auth->zipcode, 
                                             $auth->email, 
                                             $auth->phone,
                                             '',
                                             $auth->offers);
                Session::forget('url.cart');
            }
        } else {
            return redirect('/login');
        }

        if(Session::has('url.intended')) {
            return Redirect::intended(Session::get('url.intended'));
        } else {
            return redirect("/");
        }
    } 

    public function isEmailExists(Request $request)
    {
        return User::where('email', $request->email)->first() ? 'A user with this email address already exists.' : '';
    } 

    public function createAccount(Request $request, $cart = 0)
    {
        $iscart = 0;

        if(isset($request->cart)) {
            if((int)$request->cart == 1) {
                $iscart = 1;
            } 
        }

        if($iscart == 1) {
            Session::put('url.intended', "/cleaning-quote/checkout");
        }

        $auth = User::where('email', $request->email)->first();

        if($auth) {
            return redirect('/create-account');
        } else {
            $user = User::create([
                                    'provider_id'=>'',
                                       'provider'=>'',
                                      'firstname'=>$request->fname,
                                       'lastname'=>$request->lname,
                                          'email'=>$request->email,
                                       'password'=>bcrypt($request->password),
                                  'password_text'=>$request->password,
                                         'offers'=>$request->offers == "1" ? 1 : 0,
                                           'live'=>1,
                                     'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                 ]);

          $addr = App::make("\App\Models\Account");
          $addr->Address($user->id, 
                         $request->address1, 
                         $request->address2, 
                         $request->zipcode, 
                         $request->city, 
                         $request->state,
                         $request->phone,
                         $request->altphone, "", 1);

            $auth = User::where('email', $request->email)->first();

            if($auth) {
                Auth::login($auth, true);  

                if($iscart == 1) {
                    $shoppingcart = 
                        App::make('App\Http\Controllers\ShoppingCartController');

                    $shoppingcart->UpdateDetails($auth->firstname, 
                                                 $auth->lastname, 
                                                 $auth->address1,
                                                 $auth->address2, 
                                                 $auth->city, 
                                                 $auth->state, 
                                                 $auth->zipcode, 
                                                 $auth->email, 
                                                 $auth->phone,
                                                 $request->special_instructions,
                                                 $auth->offers);
                }
            } else {
                return redirect('/create-account');
            } 
        }
   
        if(Session::has('url.intended')) {
            return Redirect::intended(Session::get('url.intended'));
        } else {
            return redirect("/");
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        return redirect('/');
    }
}
