<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\App;
use View, Config, Blade, Response, Auth, Session, Cookie;

class ShoppingCartController extends Controller 
{
      const COOKIE_NAME = 'gbshopid';

	public function __construct(\App\Models\ShoppingCart $shoppingcart) 
	{
		  $this->shoppingcart = $shoppingcart;
  }
    
	public function ShoppingCart(Request $request)
  {
        $items = [];
        $ckeid = $this::GetCookieID();

        switch(strtolower($request->action)) {
          	case 'update':
            		$data = $this->shoppingcart->GetShoppingCart($ckeid);

            		if(isset($data)) {
                    $items = $data;
            		} else {
                    $items = $this::InitializeItems(); 
            		}

                if($request->type == 'items') {
                    foreach($items['list'] as $key => &$list) {
                        if($list['id'] == $request->id) {
                            if($request->field == 'quantity') {
                                $list['quantity'] = (int)$request->value;

                                if((int)$request->value > 0) {
                                    $list['disinfect'] = 1;
                                } else {
                                    $list['disinfect'] = 0;
                                }
                            } else if($request->field == 'uvc') {
                                $list['uvc']['isuvc'] = (int)$request->value;

                                if((int)$request->value > 0) {
                                    $list['uvc']['disinfect'] = 1;
                                } else {
                                    $list['uvc']['disinfect'] = 0;
                                }                                
                            } else if($request->field == 'wipe') {
                                $list['wipe']['iswipe'] = (int)$request->value;

                                if((int)$request->value > 0) {
                                    $list['wipe']['disinfect'] = 1;
                                } else {
                                    $list['wipe']['disinfect'] = 0;
                                }
                            } else if($request->field == 'apttest') {
                                $list['apttest']['isapttest'] = (int)$request->value;

                                if((int)$request->value > 0) {
                                    $list['apttest']['disinfect'] = 1;
                                } else {
                                    $list['apttest']['disinfect'] = 0;
                                }
                            }   

                            break;                        
                        }
                    }
                } else if($request->type == 'parking') {
                    foreach($items['parking'] as $key => &$parking) {
                        if($parking['id'] == $request->id) {
                            if($request->field == 'parking') {
                                $parking['ischeck'] = (int)$request->value;
                            } else if($request->field == 'area') {
                                $parking['ischeck'] = (int)$request->value;
                            } else if($request->field == 'driveway') {
                                $parking['ischeck'] = (int)$request->value;
                            }   

                            break;                        
                        }
                    }      
                } else if($request->type == 'promocode') {  
                    $promocode = 
                      App::make("\App\Models\Order")->GetPromoCode(trim($request->value));

                    if(isset($promocode) || (trim($request->value)) === '') {
                        $items['total']['promocode'] = trim($request->value);   
                    } else {
                        $items['total']['promocode'] = '';
                        return 0;
                    }
                }

                $this::Calculate($items);

            		break;
            case 'clear':
                $items = $this::InitializeItems(); 
                break;
        }
    
        $this->shoppingcart->SaveShoppingCart($ckeid, json_encode($items));

        return $items;
    }

    private function GetCookieID()
    {
        if(Cookie::has($this::COOKIE_NAME)) {
            return Cookie::get($this::COOKIE_NAME);
        } else {
            return '';
        }
    }

    private function InitializeItems()
    {
        $items = [
                   'buyer'=>[
                                  'firstname'=>'',
                                   'lastname'=>'',
                                   'address1'=>'',
                                   'address2'=>'',
                                       'city'=>'',
                                      'state'=>'',
                                    'zipcode'=>'',
                                      'email'=>'',
                                      'phone'=>'',
                                   'altphone'=>'',
                                 'specialins'=>'',
                                     'offers'=>0,
                                     'addrid'=>0,
                                   'addrtype'=>1
                            ],
                    'list'=>[
                                [
                                          'id'=>'5ed2127b36daa', 
                                        'name'=>'Room(s)', 
                                       'price'=>35.00, 
                                    'quantity'=>0, 
                                   'disinfect'=>0,
                                        'uvc'=>[
                                                       'isuvc'=>0,
                                                       'price'=>35.00,
                                                   'disinfect'=>0
                                                ],
                                        'wipe'=>[
                                                      'iswipe'=>0,
                                                       'price'=>12.00,
                                                   'disinfect'=>0
                                                ],
                                     'apttest'=>[
                                                   'isapttest'=>0,
                                                       'price'=>14.85,
                                                   'disinfect'=>0
                                                ] 
                               ], 
                               [
                                          'id'=>'5ed2129736200', 
                                        'name'=>'Staircase', 
                                       'price'=>25.00, 
                                    'quantity'=>0, 
                                   'disinfect'=>0,
                                        'uvc'=>[
                                                       'isuvc'=>0,
                                                       'price'=>25.00,
                                                   'disinfect'=>0
                                                ],                                   
                                        'wipe'=>[
                                                      'iswipe'=>0,
                                                       'price'=>12.00,
                                                   'disinfect'=>0,
                                                ],
                                     'apttest'=>[
                                                   'isapttest'=>0,
                                                       'price'=>14.85,
                                                   'disinfect'=>0
                                                ]   
                               ], 
                               [
                                          'id'=>'5ed212c91b4a3', 
                                        'name'=>'Bath/Laundry', 
                                       'price'=>25.00, 
                                    'quantity'=>0,
                                   'disinfect'=>0, 
                                        'uvc'=>[
                                                       'isuvc'=>0,
                                                       'price'=>25.00,
                                                   'disinfect'=>0
                                                ],                                    
                                        'wipe'=>[
                                                      'iswipe'=>0,
                                                       'price'=>12.00,
                                                   'disinfect'=>0
                                                ],
                                     'apttest'=>[
                                                   'isapttest'=>0,
                                                       'price'=>14.85,
                                                   'disinfect'=>0
                                                ]   
                               ], 
                               [
                                          'id'=>'5ed212db3da86', 
                                        'name'=>'Entry/Hall', 
                                       'price'=>25.00, 
                                    'quantity'=>0, 
                                   'disinfect'=>0,
                                         'uvc'=>[
                                                       'isuvc'=>0,
                                                       'price'=>25.00,
                                                   'disinfect'=>0
                                                ],                                   
                                        'wipe'=>[
                                                      'iswipe'=>0,
                                                       'price'=>12.00,
                                                   'disinfect'=>0
                                                ],
                                     'apttest'=>[
                                                   'isapttest'=>0,
                                                       'price'=>14.85,
                                                   'disinfect'=>0
                                                ]   
                                ]
                            ],
                 'parking'=>[
                                [
                                          'id'=>'5ece4797eaf5e',
                                        'name'=>'I do not have parking nearby.',
                                     'ischeck'=>0,
                                       'price'=>35.00,
                                       'count'=>0      
                                ],
                                [
                                          'id'=>'5ed4ea1f9eeb1',
                                        'name'=>'Area is on 3rd floor or higher.',
                                     'ischeck'=>0,
                                       'price'=>35.00,
                                       'count'=>0      
                                ],
                                [
                                          'id'=>'5ed4e71823c17',    
                                        'name'=>'I have guaranteed parking.',                                
                                     'ischeck'=>0,
                                       'price'=>0.00,
                                       'count'=>0      
                                ],                                                
                            ],
                  'jobmin'=>25.00,
                   'total'=>[
                                  'count'=>[
                                              'disinfect'=>0,
                                                    'uvc'=>0,
                                                   'wipe'=>0,
                                                'apttest'=>0
                                           ],
                                  'price'=>[
                                              'disinfect'=>0.00,
                                                    'uvc'=>0.00,
                                                   'wipe'=>0.00,
                                                'apttest'=>0.00                                  
                                           ],
                               'subtotal'=>0.00,
                               'discount'=>0.00,
                              'estimated'=>0.00,
                              'promocode'=>''
                            ],
                    'step'=>1
                ];

        return $items;
    }

    public function UpdateDetails($fname, $lname, $addr1, $addr2, $city, $state, $zipcode, $email, $phone, $specialinstruction, $offers, $addrid = 0, $addrtype = 1)
    {
        $items = $this::GetCartItems();

        $items['buyer']['firstname'] = $fname;
        $items['buyer']['lastname'] = $lname;
        $items['buyer']['address1'] = $addr1;
        $items['buyer']['address2'] = $addr2;
        $items['buyer']['city'] = $city;
        $items['buyer']['state'] = $state;
        $items['buyer']['zipcode'] = $zipcode;
        $items['buyer']['email'] = $email;
        $items['buyer']['phone'] = $phone;
        $items['buyer']['phone'] = $phone;
        $items['buyer']['phone'] = $phone;
        $items['buyer']['specialins'] = $specialinstruction;
        $items['buyer']['offers'] = $offers;
        $items['buyer']['addrid'] = $addrid;
        $items['buyer']['addrtype'] = $addrtype;

        $this->shoppingcart
             ->SaveShoppingCart($this::GetCookieID(), json_encode($items));

        return $items;
    }

    private function Calculate(&$items)
    {
        $ttl_qty = 0;
        $ttl_uvc = 0;
        $ttl_wpe = 0;
        $ttl_apt = 0;

        $ttl_dis_prc = 0.00;
        $ttl_uvc_prc = 0.00;
        $ttl_wpe_prc = 0.00;
        $ttl_apt_prc = 0.00;
        $ttl_par_prc = 0.00;

        $sub_prc = 0.00;
        $est_prc = 0.00;

        foreach($items['list'] as $key => &$list) {
            $ttl_qty = $ttl_qty + $list['quantity'];

            if((int)$list['uvc']['disinfect'] == 1) {
                $ttl_uvc = $ttl_uvc + 1;
            }

            if((int)$list['wipe']['disinfect'] == 1) {
                $ttl_wpe = $ttl_wpe + 1;
            }

            if((int)$list['apttest']['isapttest'] == 1) {
                $ttl_apt = $ttl_apt + 1;
            }

            $ttl_dis_prc = $ttl_dis_prc + (float)$list['price'] * (int)$list['quantity'];
            $ttl_uvc_prc = $ttl_uvc_prc + (float)$list['uvc']['price'] * (int)$list['uvc']['disinfect']; 
            $ttl_wpe_prc = $ttl_wpe_prc + (float)$list['wipe']['price'] * (int)$list['wipe']['disinfect'];   
            $ttl_apt_prc = $ttl_apt_prc + (float)$list['apttest']['price'] * (int)$list['apttest']['disinfect'];                  
        }

        foreach($items['parking'] as $key => &$parking) {
            if((int)$parking['ischeck'] == 1) {
                $ttl_par_prc = $ttl_par_prc + (float)$parking['price'];
            }
        }

        $sub_prc  = $ttl_dis_prc + $ttl_uvc_prc + $ttl_wpe_prc + $ttl_apt_prc + $ttl_par_prc;
        
        if($sub_prc > 0.00) {
            $est_prc = $sub_prc; // + (float)$items['jobmin'];
        }

        $items['total']['count']['disinfect'] = $ttl_qty;
        $items['total']['count']['uvc']       = $ttl_uvc;
        $items['total']['count']['wipe']      = $ttl_wpe;
        $items['total']['count']['apttest']   = $ttl_apt;

        $items['total']['price']['disinfect'] = $ttl_dis_prc;
        $items['total']['price']['uvc']       = $ttl_uvc_prc;
        $items['total']['price']['wipe']      = $ttl_wpe_prc;
        $items['total']['price']['apttest']   = $ttl_apt_prc;

        $items['total']['subtotal']  = $sub_prc;
        
        if($items['total']['promocode'] != '') {
            $items['total']['discount']  = floatval($est_prc) * 0.05; 
            $items['total']['estimated'] = floatval($est_prc) - floatval($est_prc) * 0.05; 
        } else {
            $items['total']['discount']  = 0.00;
            $items['total']['estimated'] = $est_prc;
        }
    }

    public function GetCartItems()
    {   
        $items = 
            $this->shoppingcart->GetShoppingCart($this::GetCookieID());

        return isset($items) ? $items : $this::InitializeItems();
    }

    public function IsCart()
    {
        if(Cookie::has($this::COOKIE_NAME)) {
            $items = $this::GetCartItems();

            return (float)$items['total']['estimated'] > 0.00;
        } else {
            return false;
        }
    }
}