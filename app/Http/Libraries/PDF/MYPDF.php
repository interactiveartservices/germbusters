<?php

namespace App\Http\Libraries\PDF;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use App\Http\Requests;
use PDF;

class myPDF extends PDF 
{
	public $selection_title_global;
	public $name;

	public function setMyPDF($case, $value)
	{
		switch ($case) {
			case 1:
				$this->selection_title_global = $value;
				break;
			
			case 2:
				$this->name = $value;
				break;
		}
	}

	public function returnMyPDF($case)
	{
		switch ($case) {
			case 1:
				return $this->selection_title_global;
				break;
			
			case 2:
				return $this->name;
				break;
		}
	}

    public function createPage($content, $filename, $selection_title, $name = NULL, $print = NULL, $perpage = NULL)
    {
        $pdf = new myPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        myPDF::setMyPDF(1, $selection_title); // set title
        myPDF::setMyPDF(2, $name); // set name

        // set document information
        PDF::SetCreator('');
        PDF::SetAuthor('');
        PDF::SetTitle('');
        PDF::SetSubject('');
        PDF::SetKeywords('');

        // set default monospaced font
        PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, 65, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(15);
        PDF::SetFooterMargin(15);
		
		// set image scale factor
		PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

		// custom header
		PDF::setHeaderCallback(function($pdf) {
    		$style2 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 199, 44));
            PDF::SetFont('helvetica', 'B', 14);
            // Title
            PDF::Cell(0, 0, str_replace('-',' ',myPDF::returnMyPDF(1)), 0, 1, 'C', 0, '', 0);
			PDF::SetFont('helvetica', '', 10);
			$html = '<br /><br /><span style="text-align:center;"><i>Images selected by:</i> <b>' . myPDF::returnMyPDF(2) . '</b></span>';
			PDF::writeHTMLCell(0, 0, '', '', myPDF::returnMyPDF(2) == NULL ? '' : $html, 0, 0, false, "L", true);
			PDF::Line(96, 38, 114, 38, $style2);
		});

		// custom footer
		PDF::setFooterCallback(function($pdf) {
			PDF::SetFont('helvetica', '', 9);
			$html = '
				<span style="text-align:center;color:#999;">
				<b>Storyboards Online</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; storyboards.com &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; hello@storyboards.com
				</span>
			';
			PDF::writeHTMLCell(0, 0, '', '', $html, 0, 0, false, "L", true);
			$image_file = 'http://www.storyboards.com/img/sbo_pdf.jpg';
			PDF::Image($image_file, 0, 250, 0, '', 'JPG', '', 'T', false, 0, 'C', false, false, 0, false, false, false);
		});

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                PDF::setLanguageArray($l);
        }
		
		// set font
        PDF::SetFont('helvetica', '', 9);

		foreach($content as $value)
		{
			// add a page
			PDF::AddPage();
			
			// output the HTML content
			if ($perpage == 1 || $perpage == 2)
			{
				PDF::writeHTML($value, true, 0, true, 0, 'C');
			}
			else
			{
				PDF::writeHTML($value, true, 0, true, 0);
			}
		}
		
		// reset pointer to the last page
        PDF::lastPage();
		
		// write some JavaScript code
$js = <<<EOD
print(true);
EOD;
if (isset($print))
{
	PDF::IncludeJS($js);
}

        //Close and output PDF document
        //$pdf->Output($filename, 'I'); //For Viewing
        PDF::Output(str_replace('-',' ',$filename), $print == NULL ? 'D' : 'I'); //Force Download
    }

    public function pdfAttach($content, $filename, $selection_title, $name = NULL, $print = NULL, $perpage = NULL)
    {
        $pdf = new myPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		myPDF::setMyPDF(1, $selection_title); // set title
        myPDF::setMyPDF(2, $name); // set name
        // set document information
        PDF::SetCreator('');
        PDF::SetAuthor('');
        PDF::SetTitle('');
        PDF::SetSubject('');
        PDF::SetKeywords('');

        // set default monospaced font
        PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        PDF::SetMargins(PDF_MARGIN_LEFT, 65, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(15);
        PDF::SetFooterMargin(15);
		
		// set image scale factor
		PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

		// custom header
		PDF::setHeaderCallback(function($pdf) {
    		$style2 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 199, 44));
            PDF::SetFont('helvetica', 'B', 14);
            // Title
            PDF::Cell(0, 0, str_replace('-',' ',myPDF::returnMyPDF(1)), 0, 1, 'C', 0, '', 0);
			PDF::SetFont('helvetica', '', 10);
			$html = '<br /><br /><span style="text-align:center;"><i>Images selected by:</i> <b>' . myPDF::returnMyPDF(2) . '</b></span>';
			PDF::writeHTMLCell(0, 0, '', '', myPDF::returnMyPDF(2) == NULL ? '' : $html, 0, 0, false, "L", true);
			PDF::Line(96, 38, 114, 38, $style2);
		});

		// custom footer
		PDF::setFooterCallback(function($pdf) {
			PDF::SetFont('helvetica', '', 9);
			$html = '
				<span style="text-align:center;color:#999;">
				<b>Storyboards Online</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; storyboards.com &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; hello@storyboards.com
				</span>
			';
			PDF::writeHTMLCell(0, 0, '', '', $html, 0, 0, false, "L", true);
			$image_file = 'http://www.storyboards.com/img/sbo_pdf.jpg';
			PDF::Image($image_file, 0, 250, 0, '', 'JPG', '', 'T', false, 0, 'C', false, false, 0, false, false, false);
		});

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                PDF::setLanguageArray($l);
        }
		
		// set font
        PDF::SetFont('helvetica', '', 9);

		foreach($content as $value)
		{
			// add a page
			PDF::AddPage();
			
			// output the HTML content
			if ($perpage == 1 || $perpage == 2)
			{
				PDF::writeHTML($value, true, 0, true, 0, 'C');
			}
			else
			{
				PDF::writeHTML($value, true, 0, true, 0);
			}
		}
		
		// reset pointer to the last page
        PDF::lastPage();

        //Close and output PDF document
        //$pdf->Output($filename, 'I'); //For Viewing
        $g_drv = (App::environment() == 'production') ? '/g_drive' : '';
        return PDF::Output($g_drv . "/storyboards/public/temp/" . str_replace('-',' ',$filename), 'F'); 
    }
}