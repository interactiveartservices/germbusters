<?php

namespace App\Http\Libraries\Image;

use App\Http\Libraries\Image\Resize;

class Image {
    // creates a thumbnail
    //    $orig_file  - name of the original image
    //    $thumb_file - name of the file to which to save this image.
    //    $width      - width of the thumbnail
    //    $height     - height of the thumbnail
    //    $quality    - image quality (0 to 100%)
    //    $option     - 'auto', 'exact', 'portrait', 'landscape', 'crop'
    public function Thumbnail($orig_file, $thumb_file, $width, $height, $quality, $option = 'auto') 
    {
        // creates an image from the specified file.
        $img = new resize($orig_file);
        
        // reize width and height - based on $option
        $img->resizeImage($width, $height, $option);
        
        // save the image with N quality level compression
	    $img->saveImage($thumb_file, $quality);
    }
	
	public function GetThumbnailImage($orig_file, $width, $height, $option = 'auto') 
    {
        // creates an image from the specified file.
        $img = new resize($orig_file);
        
        // reize width and height - based on $option
        $img->resizeImage($width, $height, $option);
        
        $thumb = $img->getImage();
        $img->destroyImage();
        
        return $thumb;
    }

    public function Sharpen($image)
    {
        $matrix = array(
          array(-1, -1, -1),
          array(-1, 16, -1),
          array(-1, -1, -1),
        );

        $divisor = array_sum(array_map('array_sum', $matrix));
        imageconvolution($image, $matrix, $divisor, 0);

        return $image;
   }        
}        