<?php

namespace App\Http\Libraries\Encryption;

use Config, App;

class Encryption 
{
	private $des_key = '';
	private $des_iv = '';

	function __construct() {
        $this->env = (App::environment() != 'production') ? App::environment() . '.' : '';
		$this->des_key = Config::get($this->env . 'constants.des_key');
		$this->des_iv = Config::get($this->env . 'constants.des_iv');		
	}

    public function encrypt($string_to_encrypt)
    {
        /*
            namespace:

                use App\Http\Libraries\Encryption\Encryption;

            example:
                $encryption = new Encryption();

                // encrypt
                $encrypt = $encryption->encrypt('Hello World!');
                echo $encrypt;

                // decrypt
                $decrypt =  $encryption->decrypt($encrypt);
                echo $decrypt;

            return: encrypted string
        */

        $rtn = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->des_key, $string_to_encrypt, MCRYPT_MODE_CBC, $this->des_iv);
        $rtn = base64_encode($rtn);
        
        return $rtn;
    }    

    public function decrypt($string_to_decrypt)
    {
        /*
            namespace:

                use App\Http\Libraries\Encryption\Encryption;

            example:
                $encryption = new Encryption();

                // encrypt
                $encrypt = $encryption->encrypt('Hello World!');
                echo $encrypt;

                // decrypt
                $decrypt =  $encryption->decrypt($encrypt);
                echo $decrypt;

            return: decrypted string
        */

        $string_to_decrypt = base64_decode($string_to_decrypt);
        $rtn = mcrypt_decrypt(MCRYPT_RIJNDAEL_256,  $this->des_key, $string_to_decrypt, MCRYPT_MODE_CBC, $this->des_iv);
        $rtn = rtrim($rtn, "\0\4");
        
        return $rtn;
    }

    function encryptRJ256($string_to_encrypt)
    {
        $rtn = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->des_key, $string_to_encrypt, MCRYPT_MODE_CBC, $this->des_iv);
        $rtn = base64_encode($rtn);

        return urlencode($rtn);
    }

    function decryptRJ256($string_to_decrypt)
    {
        $string_to_decrypt = base64_decode($string_to_decrypt);
        $rtn = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->des_key, $string_to_decrypt, MCRYPT_MODE_CBC, $this->des_iv);
        $rtn = rtrim($rtn, "\0\4");

        return $rtn;
    }
}