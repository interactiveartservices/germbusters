<?php

namespace App\Http\Libraries\Database;

use PDO, DB, Config;

class Database 
{
    private $connection  = null;

    function __construct()
    {
        $this->Connect();
    }

    function __destruct() {
        unset($this->connection);
    }

    public function Connect()
    {
        $constr = sprintf('mysql:host=%s;dbname=%s;port=%s;charset=utf8', config('database.connections.mysql.host'),
                                                                          config('database.connections.mysql.database'),
                                                                          config('database.connections.mysql.port'));

        try
        {
            $this->connection = new PDO($constr, config('database.connections.mysql.username'),
                                                 config('database.connections.mysql.password'));
        } catch(PDOException $ex) {
            die('Unable to connect');
        }
    }

    public function ExecuteStoredProc($query, $bindings = [], &...$output)
    {
        /*
            namespace:
                use App\Http\Libraries\Database\Database;

            example:
                // MySQL:

                        CREATE DEFINER=`vagrant`@`10.0.2.2` PROCEDURE `TEST`
                        (
                            IN `id` INT
                        )
                        BEGIN
                            SELECT * FROM IAS_USERS LIMIT 2;
                            SELECT * FROM IAS_COMPANIES LIMIT 4;
                        END  

                        CREATE DEFINER=`vagrant`@`10.0.2.2` PROCEDURE `TestOutput`
                        (
                            IN `p_x` INT, 
                            OUT `p_mul` INT, 
                            IN `p_y` INT, 
                            OUT `p_add` INT, 
                            OUT `p_sub` INT
                        )
                        BEGIN
                            SET p_add = p_x + p_y;
                            SET p_sub = p_x - p_y;
                            SET p_mul = p_x * p_y;
                            
                            SELECT p_x, p_y;
                        END

                // PHP:

                        $database = new Database();

                        $result = $database->ExecuteStoredProc("Call Test(:id)", array('id' => 5));

                        // FIRST RESULT -> table1
                        // 1st record
                        if(isset($result->table1)) {
                            echo 'ID:'. $result->table1[0]->IUS_ID. '<br />';
                            echo 'Username:'. $result->table1[0]->username. '<br />';
                            echo '<br />';
                            // 2nd record
                            echo 'ID:'. $result->table1[1]->IUS_ID. '<br />';
                            echo 'Username:'. $result->table1[1]->username. '<br />';
                            echo '<br />';
                        }

                        // SECOND RESULT -> table2
                        if(isset($result->table2)) {
                            foreach($result->table2 as $value) {
                                echo 'Company Name:'. $value->CompanyName. '<br />';
                                echo 'Street'. $value->Street. '<br />';
                                echo 'City'. $value->City. '<br />';
                                echo '<br />';
                            }
                        }

                        // NEXT RESULT -> table(Number)
                        // ...

                        // show all results
                        // echo json_encode($result);


                        // with output parameter
                        $add = '';
                        $sub = '';
                        $mul = '';
                        $database = new Database();

                        $query = $database->ExecuteStoredProc('Call TestOutput(:x, @mul, :y, @add, @sub)', 
                                                                array(
                                                                        'x' => 5,
                                                                        'y' => 8
                                                                     ), 
                                                                $mul, $add, $sub
                                                              );

                        echo "mul: $mul </br>";
                        echo "add: $add </br>";
                        echo "sub: $sub </br>";

                        echo "table: ". json_encode($query);    

            return: { object }
        */

        $temp  = [];

        if($query != '')
        {
            if($this->connection === null) {
                $this->Connect();
            }

            $stmt = $this->connection->prepare($query);
            $stmt->execute($bindings);

            $cntr = 1;

            do {
                $rowset = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $temp["table$cntr"] = $rowset;

                $cntr += 1;
            } while ($stmt->nextRowset());

            array_pop($temp);

            if(count($temp) == 1) {
                if(isset($temp['table1'])) {
                    $temp = $temp['table1'];
                }
            }

            $stmt->closeCursor();

            if(count($output) > 0) {
                $outparam = $this->connection
                                 ->query('SELECT '. $this->GetOutputParameter($query))
                                 ->fetch(PDO::FETCH_ASSOC);
                
                $cntr = 0;
                foreach($outparam as $key => $value) {
                    $output[$cntr] = $value;
                    $cntr += 1;
                }
            }
        }

        return json_decode(json_encode($temp));
    }

    private function GetOutputParameter($query)
    {
        $spos_paren = strrpos($query, '(',  1);
        $epos_paren = strrpos($query, ')', -1);
        
        $array = array_map('trim', explode(',', substr($query, $spos_paren+1, 
                                                       $epos_paren-$spos_paren-1))); 

        $param = array();

        foreach($array as $value) {
            if(strrpos($value, '@') !== false) {
                array_push($param, $value);
            }
        }
        return join(', ', $param);
    }
}