<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;


class GermZapper extends Model
{
    public function GetOrder($id)
    {
        $query = DB::Table('germzapper_cart')
                  ->where('id', '=', $id)
                  ->first();

        return $query;
    }

  	public function SaveOrder($bulb, $case, $isstandkit, $iscarycase, $fname, $lname, $phone, $email, $zipcode, $total, $payment)
  	{
          $id = DB::Table('germzapper_cart')
                  ->insertGetId([
                                                  'bulb'=>$bulb,
                                                  'case'=>$case,
                                             'stand_kit'=>$isstandkit,
                                            'carry_case'=>$iscarycase,
                                             'firstname'=>$fname,
                                              'lastname'=>$lname,
                                                 'phone'=>$phone,
                                                 'email'=>$email,
                                       'billing_zipcode'=>$zipcode,  
                                                 'total'=>$total,
                                            'identifier'=>$payment->success ? $payment->response->id : '',
                                            'card_brand'=>$payment->success ? $payment->response->payment_method_details->card->brand : '',
                                      'payment_response'=>json_encode($payment),
                                        'payment_status'=>$payment->success ? 'SUCCESS' : 'FAILED',
                                            'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                ]);

          return $id;
  	}  
}