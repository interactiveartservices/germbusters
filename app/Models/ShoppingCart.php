<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;


class ShoppingCart extends Model
{  
	public function GetShoppingCart($cookieid)
    {
        $query = DB::Table('shoppingcart')
                    ->where('cookie_id', '=', $cookieid)
                    ->first();

        if(!empty($query)) {
            return json_decode(json_decode($query->items, true), true);
        }                    

        return null;
    }

    public function DeleteShoppingCart($cookieid)
    {
        $query = DB::Table('shoppingcart')
                    ->where('cookie_id', '=', $cookieid)
                    ->delete();

        return null;
    }

	public function SaveShoppingCart($cookieid, $data)
	{
        $query = DB::Table('shoppingcart')
                    ->where('cookie_id', '=', $cookieid)
                    ->first();

        if(!empty($query)) {
            return DB::Table('shoppingcart')
                        ->where('cookie_id', '=', $cookieid)
                        ->update([ 
                                    'items'=>json_encode($data),
                                 ]);
        } else {
            return DB::Table('shoppingcart')
                        ->insertGetId([ 
                                          'cookie_id'=>$cookieid,
                                              'items'=>json_encode($data),
                                         'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                     ]);
        }
	}
}