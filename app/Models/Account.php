<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;


class Account extends Model
{  
    public function Address($userid, $addr1, $addr2, $zipcode, $city, $state, $phone, $altphone, $sms, $isprimary)
    {
        if($isprimary == "1"){

             DB::Table('users_addresses')
                ->where('user_id', '=', $userid)
                ->update([ 
                            'primary'=>0                               
                         ]); 
        } 

        return DB::Table('users_addresses')
                    ->insertGetId([ 
                                      'user_id'=>$userid,
                                     'address1'=>$addr1,
                                     'address2'=>$addr2,
                                      'zipcode'=>$zipcode,
                                         'city'=>$city,
                                        'state'=>$state,
                                        'phone'=>$phone,
                                    'alt_phone'=>$altphone,
                                    'sms_phone'=>$sms,
                                      'primary'=>$isprimary,
                                   'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                 ]);   
    }

    public function UpdateAddress($id, $userid, $addr1, $addr2, $zipcode, $city, $state, $phone, $altphone, $sms, $isprimary)
    {
        if($isprimary == "1"){

             DB::Table('users_addresses')
                ->where('user_id', '=', $userid)
                ->update([ 
                            'primary'=>0                               
                         ]); 
        } 

        return DB::Table('users_addresses')
                    ->where('id', '=', $id)
                    ->update([ 
                                   'address1'=>$addr1,
                                   'address2'=>$addr2,
                                    'zipcode'=>$zipcode,
                                       'city'=>$city,
                                      'state'=>$state,
                                      'phone'=>$phone,
                                  'alt_phone'=>$altphone,
                                  'sms_phone'=>$sms,
                                    'primary'=>$isprimary
                            ]);   
    }

    public function UpdateEmail($userid, $email)
    {

       DB::Table('users')
          ->where('id', '=', $userid)
          ->update([ 
                      'email'=>$email                               
                   ]); 
    }

    public function Unsubscribe($userid)
    {

       DB::Table('users')
          ->where('id', '=', $userid)
          ->update([ 
                      'offers'=>0                               
                   ]); 
    }


    public function GetPasswordByEmail($email)
    {
        $query = DB::Table('users')
                    ->where('email', '=', $email)
                    ->first();

        return $query;     
    }  

    public function GetAddress($userid)
    {
        $query = DB::Table('users_addresses')
                    ->where('user_id', '=', $userid)
                    ->where('primary', '=', 1)
                    ->first();

        return $query;     
    }

    public function GetAddressById($id)
    {
        $query = DB::Table('users_addresses')
                    ->where('id', '=', $id)
                    ->first();

        return $query;     
    }


    public function GetAddresses($userid)
    {
        $query = DB::Table('users')
                    ->join('users_addresses', 'users_addresses.user_id', '=', 'users.id')
                    ->where('user_id', '=', $userid)
                    ->select([ 
                                'users.firstname', 
                                'users.lastname', 
                                'users_addresses.id', 
                                'users_addresses.zipcode',
                                'users_addresses.city',
                                'users_addresses.state',
                                'users_addresses.phone',    
                                'users_addresses.alt_phone', 
                                'users_addresses.address1', 
                                'users_addresses.address2',
                                'users_addresses.primary'             
                            ])
                    ->get();

        return $query;     
    }

    public function GetPrimaryAddress($userid)
    {
        $query = DB::Table('users_addresses')
                    ->where('user_id', '=', $userid)
                    ->where('primary', '=', 1)
                    ->first();

        return $query;     
    }

    public function GetOtherAddresses($userid)
    {
        $query = DB::Table('users_addresses')
                    ->where('user_id', '=', $userid)
                    ->where('primary', '=', 0)
                    ->get();

        return $query;     
    }

    public function UpdatePassword($userid, $oldpass, $newpass)
    {
        $query = DB::Table('users')
                  ->where('id', '=', $userid)
                  ->where('password_text', '=', $oldpass)
                  ->first();

        if(isset($query)) {
            DB::Table('users')
                ->where('id', '=', $userid)
                ->update([ 
                                 'password'=>bcrypt($newpass),
                            'password_text'=>$newpass                               
                         ]);

            return true;
        }

        return false;
    }
}