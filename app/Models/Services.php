<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Services extends Model
{
  

    public function Commercial_Service($is_deep_disinfection, $is_wiping_service, $is_uv_disinfection, $is_weekly_maintenance, $is_monthly_maintenance, $square_footage, $promo_code,  $how_did_you_hear, $firstname, $lastname, $company, $location_number_name, $address, $address2, $zipcode, $city, $state, $no_of_locations, $phone, $alt_phone, $email, $special_instructions) 
    {

        $gen_id = uniqid(true);

         $id = DB::Table('services_requests_commercial')
                    ->insertGetId([ 
                                        'is_deep_disinfection'=>trim($is_deep_disinfection),
                                        'is_wiping_service'=>trim($is_wiping_service),
                                        'is_uv_disinfection'=>$is_uv_disinfection,                        
                                        'is_weekly_maintenance'=>$is_weekly_maintenance,
                                        'is_monthly_maintenance'=>$is_monthly_maintenance,
                                        'square_footage'=>$square_footage,
                                        'promo_code'=>$promo_code,
                                        'how_did_you_hear'=>$how_did_you_hear,
                                        'firstname'=>$firstname,
                                        'lastname'=>$lastname,
                                        'company'=>$company,
                                        'location_number_name'=>$location_number_name,
                                        'gen_id'=>$gen_id,
                                        'address1'=>$address,
                                        'address2'=>$address2,
                                        'zipcode'=>$zipcode,
                                        'city'=>$city,
                                        'state'=>$state,
                                        'no_of_locations'=>$no_of_locations,
                                        'phone'=>$phone,
                                        'alt_phone'=>$alt_phone,
                                        'email'=>$email,
                                        'special_instructions'=>$special_instructions,
                                        'status'=>'Inquiry',
                                        'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                  ]);

         return $gen_id;             

    }

    public function Save_Quote($frmpage, $industry, $firstname, $lastname, $jobtitle, $email, $phone, $message, $notify) 
    {


         $id = DB::Table('get_quote')
                    ->insertGetId([
                                        'industry'=>$industry,
                                        'firstname'=>$firstname,
                                        'lastname'=>$lastname,                        
                                        'jobtitle'=>$jobtitle,
                                        'jobtitle'=>$jobtitle,
                                        'email'=>$email,
                                        'phone'=>$phone,
                                        'message'=>$message,
                                        'notify'=>$notify,
                                        'page'=>$frmpage,
                                        'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                  ]);

         return $id;             

    }

     public function Get_Quotes() 
    {

        $query =  DB::table('get_quote');


        if(empty($query)) {
                return null;                  
        } else {
            $query = $query->get();
        }

       return $query; 

    }

    public function AddNote($id, $notes) 
    {

    
        $query = DB::Table('get_quote')
                ->where('id', '=', $id)
                ->update([
                       'notes'=>$notes
                     ]);  

        return $id;        

    }


    
    public function GetRequest($gen_id = null) 
    {
        $query =  DB::table('services_requests_commercial');

        if(isset($gen_id)) {
            $query = $query->where('gen_id', '=',  $gen_id)
                        ->first();             

            if(empty($query)) {
                return null;
            }                    
        } else {
            $query = $query->get();
        }

       return $query; 
    }
  

}