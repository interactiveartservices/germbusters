<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Order extends Model
{  
    public function GetResidentialByID($id)
    {
        $query = DB::Table('services_requests_residential')
                    ->where('id', '=', $id)
                    ->first();

        return $query;      
    }

    public function GetCommercialByID($id)
    {
        $query = DB::Table('services_requests_commercial')
                    ->where('id', '=', $id)
                    ->first();

        return $query;      
    }

    public function GetOrderByGenID($id)
    {
        $query = DB::Table('services_requests_residential')
                    ->where('gen_id', '=', $id)
                    ->first();

        return $query;     	
    }

    public function GetOrderByID($id)
    {
        $query = DB::Table('services_requests_residential')
                    ->where('id', '=', $id)
                    ->first();

        return $query;      
    }

    public function GetOrderItemsByID($id)
    {
        $query = DB::Table('services_requests_items')
                    ->where('services_requests_id', '=', $id)
                    ->get();

        return $query;      
    }

    public function GetOrderByUserID($id)
    {
        $query = DB::Table('services_requests_residential')
                    ->where('user_id', '=', $id)
                    ->get();

        return $query;      
    }

    public function GetOrderItemsByGenID($id)
    {
        $id = DB::Table('services_requests_residential')
                    ->where('gen_id', '=', $id)
                    ->first()
                    ->id;

        $query = DB::Table('services_requests_items')
                    ->where('services_requests_id', '=', $id)
                    ->get();

        return $query;      
    }

    public function SaveOrder($userid, $order)
    {
    	  $gen_id = uniqid(true);

        $id = DB::Table('services_requests_residential')
                ->insertGetId([ 
                				          	 'gen_id'=>$gen_id,
                				            'user_id'=>$userid,
                                  'firstname'=>$order['buyer']['firstname'],
                                   'lastname'=>$order['buyer']['lastname'],
                                   'address1'=>$order['buyer']['address1'],
                                   'address2'=>$order['buyer']['address2'],
                                       'city'=>$order['buyer']['city'],
                                      'state'=>$order['buyer']['state'],
                                    'zipcode'=>$order['buyer']['zipcode'],
                                      'phone'=>$order['buyer']['phone'],
                                  'alt_phone'=>$order['buyer']['altphone'],
                                      'email'=>$order['buyer']['email'],
                       'special_instructions'=>$order['buyer']['specialins'],
                                     'offers'=>$order['buyer']['offers'],
                                   'addrtype'=>$order['buyer']['addrtype'],
                                     'jobmin'=>$order['jobmin'],
                      'total_count_disinfect'=>$order['total']['count']['disinfect'],
                            'total_count_uvc'=>$order['total']['count']['uvc'],
                           'total_count_wipe'=>$order['total']['count']['wipe'],
                        'total_count_apttest'=>$order['total']['count']['apttest'],
                      'total_price_disinfect'=>$order['total']['price']['disinfect'],
                            'total_price_uvc'=>$order['total']['price']['uvc'],
                           'total_price_wipe'=>$order['total']['price']['wipe'],
                        'total_price_apttest'=>$order['total']['price']['apttest'],
                                   'subtotal'=>$order['total']['subtotal'],
                                   'discount'=>$order['total']['discount'],
                                      'total'=>$order['total']['estimated'],
                                 'promo_code'=>$order['total']['promocode'],   
                                     'status'=>'Inquiry',
                                 'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                             ]);

        foreach($order['list'] as $key => &$list) {
         	DB::Table('services_requests_items')
                ->insert([ 
                              'services_requests_id'=>$id,
                                		        'itemid'=>$list['id'],
                               			          'name'=>$list['name'],
                                             'price'=>$list['price'],
                                          'quantity'=>$list['quantity'],
                                         'disinfect'=>$list['disinfect'],
                                           'is_wipe'=>$list['wipe']['iswipe'],
                                        'wipe_price'=>$list['wipe']['price'],
                                    'wipe_disinfect'=>$list['wipe']['disinfect'],
                                        'is_apttest'=>$list['apttest']['isapttest'],
                                     'apttest_price'=>$list['apttest']['price'],
                                 'apttest_disinfect'=>$list['apttest']['disinfect'],
                                          'is_check'=>0,
                                        'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                        ]);
    	}

        foreach($order['parking'] as $key => &$list) {
         	DB::Table('services_requests_items')
                ->insert([ 
                              'services_requests_id'=>$id,
                                		        'itemid'=>$list['id'],
                               			          'name'=>$list['name'],
                                             'price'=>$list['price'],
                                          'quantity'=>0,
                                         'disinfect'=>0,
                                           'is_wipe'=>0,
                                        'wipe_price'=>0,
                                    'wipe_disinfect'=>0,
                                        'is_apttest'=>0,
                                     'apttest_price'=>0,
                                 'apttest_disinfect'=>0,
                                          'is_check'=>$list['ischeck'],
                                        'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                        ]);
        }

      	return $gen_id;;
    }

    public function GetEstimateList($type)
    {
        $res = DB::Table('services_requests_residential AS a')
                ->join('services_invoice AS b', 'b.id', '=', 'a.invoice_id', 'left outer')
                ->join('services_payment AS c', 'c.invoice_id', '=', 'b.id', 'left outer')
                ->selectRaw("a.id")
                ->selectRaw("CONCAT(a.firstname, ' ', a.lastname) AS name")
                ->selectRaw("'' AS company")
                ->selectRaw("DATE_FORMAT(a.date_added, '%m/%d/%Y') AS date_added")
                ->selectRaw("'Residential' AS type")
                ->selectRaw("a.promo_code")
                ->selectRaw("IF(a.addrtype=1, a.address1, a.address2) AS address ")
                ->selectRaw("a.total")
                ->selectRaw("a.status AS status")
                ->selectRaw("a.generalestimate")
                ->selectRaw("a.invoice_id")
                ->selectRaw("b.payment_terms AS payment_terms")
                ->selectRaw("b.total AS invoice_total")
                ->selectRaw("b.status AS invoice_status")
                ->selectRaw("b.scheduled_date")
                ->selectRaw("c.id as payment_id");

        if(strtolower($type) == 'invoices') {     
            $res = $res->where('a.status', '=', 'Invoice');
        } else {
            $res = $res->where('a.status', '!=', 'Invoice');
        }

        $com = DB::Table('services_requests_commercial AS a')
                ->join('services_invoice AS b', 'b.id', '=', 'a.invoice_id', 'left outer')
                ->join('services_payment AS c', 'c.invoice_id', '=', 'b.id', 'left outer')
                ->selectRaw("a.id")
                ->selectRaw("CONCAT(a.firstname, ' ', a.lastname) AS name")
                ->selectRaw("a.company")
                ->selectRaw("DATE_FORMAT(a.date_added, '%m/%d/%Y') AS date_added")
                ->selectRaw("'Commercial' AS type")
                ->selectRaw("a.promo_code")
                ->selectRaw("a.address1")
                ->selectRaw("'0.00' AS total")
                ->selectRaw("a.status AS status")
                ->selectRaw("a.generalestimate")
                ->selectRaw("a.invoice_id")
                ->selectRaw("b.payment_terms AS payment_terms")
                ->selectRaw("b.total AS invoice_total")
                ->selectRaw("b.status AS invoice_status")
                ->selectRaw("b.scheduled_date")
                ->selectRaw("c.id as payment_id");

        if(strtolower($type)== 'invoices') {     
            $com = $com->where('a.status', '=', 'Invoice');
        } else {
            $com = $com->where('a.status', '!=', 'Invoice');
        }

        $query = $res->union($com)->get();

        return $query;      
    }

    public function GetResidentialInvoiceByID($id)
    {
        $query = DB::Table('services_requests_residential AS a')
                    ->join('services_invoice AS b', 'b.id', '=', 'a.invoice_id', 'left outer')
                    ->where('a.id', '=', $id)
                    ->select([
                                DB::raw('a.id AS id'),
                                'a.firstname',
                                'a.lastname',
                                'a.address1',
                                'a.address2',
                                'a.city',
                                'a.state',
                                'a.zipcode',
                                'a.phone',
                                'a.alt_phone',
                                'a.email',
                                'a.square_footage', 
                                'a.promo_code',                          
                                'a.how_did_you_hear',
                                'a.invoice_id',
                                'a.date_added',

                                'b.acct_manager',
                                'b.payment_terms',
                                'b.scheduled_date',
                                'b.scheduled_time',
                                'b.general_desc',
                                'b.general_price',
                                'b.description_disclaimer',
                                'b.terms_conditions',
                                'b.subtotal',
                                'b.discount',
                                'b.promo',
                                'b.total',
                                'b.paid',
                                'b.due',
                                DB::raw("IFNULL(b.workorder, '') AS workorder"),
                                'b.start_date',
                                'b.end_date'
                            ])
                    ->first();

        return $query;      
    }

    public function GetCommercialInvoiceByID($id)
    {
        $query = DB::Table('services_requests_commercial AS a')
                    ->join('services_invoice AS b', 'b.id', '=', 'a.invoice_id', 'left outer')
                    ->where('a.id', '=', $id)
                    ->select([
                                DB::raw('a.id AS id'),
                                'a.is_deep_disinfection',
                                'a.is_wiping_service',
                                'a.is_uv_disinfection',
                                'a.is_one_time',
                                'a.is_biweekly',
                                'a.is_annual',
                                'a.is_weekly_maintenance',
                                'a.is_monthly_maintenance',
                                'a.square_footage',
                                'a.promo_code',
                                'a.how_did_you_hear',
                                'a.firstname',
                                'a.lastname',
                                'a.company',
                                'a.location_number_name',
                                'a.address1',
                                'a.address2',
                                'a.city',
                                'a.state',
                                'a.zipcode',
                                'a.phone',
                                'a.alt_phone',
                                'a.email',
                                'a.invoice_id',
                                'a.date_added',

                                'b.acct_manager',
                                'b.payment_terms',
                                'b.scheduled_date',
                                'b.scheduled_time',
                                'b.general_desc',
                                'b.general_price',
                                'b.description_disclaimer',
                                'b.terms_conditions',
                                'b.subtotal',
                                'b.discount',
                                'b.promo',
                                'b.total',
                                'b.paid',
                                'b.due',
                                DB::raw("IFNULL(b.workorder, '') AS workorder"),
                                'b.start_date',
                                'b.end_date'
                            ])
                    ->first();

        return $query;      
    }

    public function GetPaymentInvoiceByID($id)
    {
        $query = DB::Table('services_payment AS a')
                    ->join('services_invoice AS b', 'b.id', '=', 'a.invoice_id', 'left outer')
                    ->where('a.id', '=', $id)
                    ->select([
                                DB::raw('a.id AS id'),
                                'a.firstname',
                                'a.lastname',
                                'a.address1',
                                'a.address2',
                                'a.city',
                                'a.state',
                                'a.zipcode',
                                'a.phone',
                                'a.email',
                                'a.invoice_id',
                                'a.promo_code',
                                'a.brand',
                                'a.amount',
                                'a.date_paid',
                                'a.status',

                                'b.acct_manager',
                                'b.payment_terms',
                                'b.scheduled_date',
                                'b.scheduled_time',
                                'b.general_desc',
                                'b.general_price',
                                'b.description_disclaimer',
                                'b.terms_conditions',
                                'b.subtotal',
                                'b.discount',
                                'b.promo',
                                'b.total',
                                'b.paid',
                                'b.due',
                                DB::raw("IFNULL(b.workorder, '') AS workorder"),
                                'b.start_date',
                                'b.end_date'
                            ])
                    ->first();

        return $query;      
    }

    public function GetCountInvoice()
    {
        return DB::Table('services_invoice')
                ->count();
    }

    public function GetInvoiceItemsByID($id, $invoiceid)
    {
        if((int)$invoiceid > 0) {
            $query = DB::Table('services_invoice_items')
                      ->where('invoice_id', '=', $invoiceid)
                      ->get();
        } else {
            $query = DB::Table('services_requests_commercial')
                        ->where('id', '=', $id)
                        ->first();

            $arr = null;

            if((int)$query->is_deep_disinfection == 1) {
                $arr[] = [
                            
                                     'id'=>0,
                             'invoice_id'=>0,
                                  'title'=>'Electrostatic Deep Cleaning Disinfection',
                            'description'=>'Electrostatic spray uses a specialized EPA Registered Disinfectant Cleaner that is combined with air and atomized by an electrode inside the sprayer to kill a broad spectrum of microorganisms in order to provide clean, sanitary and healthy environment.',
                                  'price'=>'0.00'
                           ];
            }

            if((int)$query->is_wiping_service == 1) {
                $arr[] = [
                            
                                     'id'=>0,
                             'invoice_id'=>0,
                                  'title'=>'Hand wiping service',
                            'description'=>'',
                                  'price'=>'0.00'
                           ];
            }

            if((int)$query->is_uv_disinfection == 1) {
                $arr[] = [
                            
                                     'id'=>0,
                             'invoice_id'=>0,
                                  'title'=>'UV-C Disinfection',
                            'description'=>'',
                                  'price'=>'0.00'
                           ];
            }

            /*
                if((int)$query->is_one_time == 1) {
                    $arr[] = [
                                
                                         'id'=>0,
                                 'invoice_id'=>0,
                                      'title'=>'One time',
                                'description'=>'',
                                      'price'=>'0.00'
                               ];
                }

                if((int)$query->is_weekly_maintenance == 1) {
                    $arr[] = [
                                
                                         'id'=>0,
                                 'invoice_id'=>0,
                                      'title'=>'Weekly maintainance plans',
                                'description'=>'',
                                      'price'=>'0.00'
                               ];
                }

                if((int)$query->is_biweekly == 1) {
                    $arr[] = [
                                
                                         'id'=>0,
                                 'invoice_id'=>0,
                                      'title'=>'Biweekly',
                                'description'=>'',
                                      'price'=>'0.00'
                               ];
                }

                if((int)$query->is_monthly_maintenance == 1) {
                    $arr[] = [
                                
                                         'id'=>0,
                                 'invoice_id'=>0,
                                      'title'=>'Monthly maintenance plans',
                                'description'=>'',
                                      'price'=>'0.00'
                               ];
                }

                if((int)$query->is_annual == 1) {
                    $arr[] = [
                                
                                         'id'=>0,
                                 'invoice_id'=>0,
                                      'title'=>'Annual',
                                'description'=>'',
                                      'price'=>'0.00'
                               ];
                }
            */
                
            $query = json_decode(json_encode($arr));
        }

        return $query;
    }

     public function GetPaymentId($invoiceid)
    {
        $query = DB::Table('services_payment')
                    ->where('invoice_id', '=', $invoiceid)
                    ->first();

        return $query;
    }

    public function SaveInvoice($data)
    {
        /*
            $dt = explode('-', $data->daterange);
            $d1 = date_format(date_create(trim($dt[0])), 'Y-m-d');
            $d2 = date_format(date_create(trim($dt[1])), 'Y-m-d');
        */

        $id = $data->id;

        $invoice_id = 0;

        if($data->id > 0) {
            $query = DB::Table('services_requests_commercial')
                        ->where('id', '=', $data->id)
                        ->first();

            if(isset($query)) {
                $invoice_id = (int)$query->invoice_id;
            }
        }

        if($data->general_price == '') {
            $data->general_price = '0.00';
        }else{
            $data->general_price = str_replace("$","",trim($data->general_price));
        }

        if($data->discount == '') {
            $data->discount = '0.00';
        }else{
            $data->discount = str_replace("$","",trim($data->discount));
        }

        //$myfile = fopen("/g_drive/germbusters/tests/logs.txt", "a+") or die("Unable to open file!");
        //$txt = 'id: '. $data->id. ' - invoice id:'.  $invoice_id. "\n";
        //fwrite($myfile, $txt);


        if($invoice_id == 0) {
        //    fwrite($myfile, "ADD INVOICE". "\n");
            $invoice_id = DB::Table('services_invoice')
                          ->insertGetId([ 
                                                      'service_id'=>$data->id,
                                                            'type'=>strtolower($data->type),
                                                      'start_date'=>date_format(date_create($data->startdate), 'Y-m-d'),
                                                        'end_date'=>date_format(date_create($data->enddate), 'Y-m-d'),  
                                                    'acct_manager'=>$data->acct_manager,
                                                   'payment_terms'=>$data->payment_terms,
                                                  'scheduled_date'=>$data->scheduled_date,
                                                  'scheduled_time'=>$data->scheduled_time,

                                                    'general_desc'=>$data->general_desc,
                                                   'general_price'=>floatval($data->general_price),
                                          'description_disclaimer'=>$data->description_disclaimer,
                                                'terms_conditions'=>$data->terms_conditions,
                                                        'discount'=>$data->discount,
                                                       'workorder'=>$data->workorder,
                                                      'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                       ]);
        } else {
            //fwrite($myfile, "EDIT INVOICE". "\n");
            DB::Table('services_invoice')
                ->where('id', '=', $invoice_id)
                ->update([ 
                                            'start_date'=>date_format(date_create($data->startdate), 'Y-m-d'),
                                              'end_date'=>date_format(date_create($data->enddate), 'Y-m-d'),
                                          'acct_manager'=>$data->acct_manager,
                                         'payment_terms'=>$data->payment_terms,
                                        'scheduled_date'=>$data->scheduled_date,
                                        'scheduled_time'=>$data->scheduled_time,

                                          'general_desc'=>$data->general_desc,
                                         'general_price'=>floatval($data->general_price),
                                'description_disclaimer'=>$data->description_disclaimer,
                                      'terms_conditions'=>$data->terms_conditions,
                                              'discount'=>$data->discount,
                                             'workorder'=>$data->workorder,
                                            'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                             ]);
        }

        //fclose($myfile);

        $sub = 0.00;
        $ttl = 0.00;

        if(isset($data->service_title)) {
            $srvc_c = array();
            foreach($data->service_count as $value) {
                $srvc_c[] = $value;
            }

            $srvc_t = array();
            foreach($data->service_title as $value) {
                $srvc_t[] = $value;
            }

            $srvc_d = array();
            foreach($data->service_desc as $value) {
                $srvc_d[] = $value;
            }

            $srvc_p = array();
            foreach($data->service_price as $value) {
                $srvc_p[] = str_replace("$","",trim($value));
            }

            DB::Table('services_invoice_items')
                ->where('invoice_id', '=', $invoice_id)
                ->delete();

            for($i=0; $i<count($srvc_t); $i++) {
                DB::Table('services_invoice_items')
                          ->insertGetId([ 
                                                'invoice_id'=>$invoice_id,
                                                     'title'=>$srvc_t[$i],
                                               'description'=>$srvc_d[$i],
                                                     'price'=>$srvc_p[$i] == '' ? '0.00' : str_replace("$","",trim($srvc_p[$i]))
                                       ]);

                $sub = $sub + floatval($srvc_p[$i]);
            }
        }
        $sub = $sub + floatval($data->general_price);
        $ttl = $sub - floatval($data->discount);

        if($data->promo_code == '') {
            $pmc = 0.00;
        } else {
            $pmc = $sub * 0.05;
            $ttl = $ttl - $pmc;
        }

        DB::Table('services_invoice')
            ->where('id', '=', $invoice_id)
            ->update([
                          'subtotal'=>$sub,
                             'promo'=>$pmc,
                             'total'=>$ttl,
                               'due'=>$ttl
                    ]);

        if(strtolower($data->type) == "residential") {
            DB::Table('services_requests_residential')
                ->where('id', '=', $data->id)
                ->update([
                                        'invoice_id'=>$invoice_id,
                                        'promo_code'=>$data->promo_code,
                                    'square_footage'=>$data->square_footage,
                                  'how_did_you_hear'=>$data->how_did_you_hear,

                                         'firstname'=>$data->firstname,
                                          'lastname'=>$data->lastname,
                              'location_number_name'=>$data->location_number_name,
                                          'address1'=>$data->address1, 
                                          'address2'=>$data->address2, 
                                           'zipcode'=>$data->zipcode, 
                                              'city'=>$data->city, 
                                             'state'=>$data->state,      
                                             'email'=>$data->email,      
                                             'phone'=>$data->phone,      
                                         'alt_phone'=>$data->alt_phone,      
                                             'state'=>$data->state   
                         ]);
        } else {
            if((int)$data->id == 0) {
                $id = DB::Table('services_requests_commercial')
                        ->insertGetId([
                                                'invoice_id'=>$invoice_id,
                                      'is_deep_disinfection'=>$data->is_deep_disinfection,
                                         'is_wiping_service'=>$data->is_wiping_service,
                                        'is_uv_disinfection'=>$data->is_uv_disinfection,

                                               'is_one_time'=>$data->is_one_time,
                                     'is_weekly_maintenance'=>$data->is_weekly_maintenance,
                                               'is_biweekly'=>$data->is_biweekly,
                                    'is_monthly_maintenance'=>$data->is_monthly_maintenance,
                                                 'is_annual'=>$data->is_annual,

                                            'square_footage'=>$data->square_footage,
                                                'promo_code'=>$data->promo_code,
                                          'how_did_you_hear'=>$data->how_did_you_hear,

                                                 'firstname'=>$data->firstname,
                                                  'lastname'=>$data->lastname,
                                                   'company'=>$data->company,
                                      'location_number_name'=>$data->location_number_name,
                                                  'address1'=>$data->address1, 
                                                  'address2'=>$data->address2, 
                                                   'zipcode'=>$data->zipcode, 
                                                      'city'=>$data->city, 
                                                     'state'=>$data->state,      
                                                     'email'=>$data->email,      
                                                     'phone'=>$data->phone,      
                                                 'alt_phone'=>$data->alt_phone,      
                                                     'state'=>$data->state,
                                                    'status'=>'Estimate',
                                                'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                                 ]);

                DB::Table('services_invoice')
                    ->where('id', '=', $invoice_id)
                    ->update([
                               'service_id'=>$id
                            ]);
            } else {
                 DB::Table('services_requests_commercial')
                      ->where('id', '=', $data->id)
                      ->update([
                                              'invoice_id'=>$invoice_id,
                                    'is_deep_disinfection'=>$data->is_deep_disinfection,
                                       'is_wiping_service'=>$data->is_wiping_service,
                                      'is_uv_disinfection'=>$data->is_uv_disinfection,

                                             'is_one_time'=>$data->is_one_time,
                                   'is_weekly_maintenance'=>$data->is_weekly_maintenance,
                                             'is_biweekly'=>$data->is_biweekly,
                                  'is_monthly_maintenance'=>$data->is_monthly_maintenance,
                                               'is_annual'=>$data->is_annual,

                                          'square_footage'=>$data->square_footage,
                                              'promo_code'=>$data->promo_code,
                                        'how_did_you_hear'=>$data->how_did_you_hear,

                                               'firstname'=>$data->firstname,
                                                'lastname'=>$data->lastname,
                                                 'company'=>$data->company,
                                    'location_number_name'=>$data->location_number_name,
                                                'address1'=>$data->address1, 
                                                'address2'=>$data->address2, 
                                                 'zipcode'=>$data->zipcode, 
                                                    'city'=>$data->city, 
                                                   'state'=>$data->state,      
                                                   'email'=>$data->email,      
                                                   'phone'=>$data->phone,      
                                               'alt_phone'=>$data->alt_phone,      
                                                   'state'=>$data->state   
                               ]);
            }       
        }

        return (int)$id;
    }

    public function SetInvoiceStatus($id, $type)
    {
        if(strtolower($type) == "residential") { 
            $query = DB::Table('services_requests_residential')
                        ->where('id', '=', $id)
                       ->update([
                                'status'=>'Invoice'
                             ]);
        } else {
            $query = DB::Table('services_requests_commercial')
                        ->where('id', '=', $id)
                       ->update([
                               'status'=>'Invoice'
                             ]);         
        }
    }

    public function SetInvoicePaidStatus($id, $type)
    {
        if(strtolower($type) == "residential") { 
            $query = DB::Table('services_requests_residential')
                        ->where('id', '=', $id)
                        ->first();

            if(isset($query)) {
                 DB::Table('services_requests_residential')
                     ->where('id', '=', $query->invoice_id)
                     ->update([
                                'status'=>'Not Paid'
                             ]);
             }
        } else {
            $query = DB::Table('services_requests_commercial')
                        ->where('id', '=', $id)
                        ->first();
                       
            if(isset($query)) {
                 DB::Table('services_requests_commercial')
                     ->where('id', '=', $query->invoice_id)
                     ->update([
                                'status'=>'Not Paid'
                             ]);
             }            
        }
    }

    public function GetPromoCode($promocode)
    {
        $query = DB::Table('affiliates')
                    ->where('promocode', '=', $promocode)
                    ->first();

        return $query;
    }

    public function GetCustomerStoreCard($userid)
    {
        $query = DB::Table('services_payment')
                    ->first();

        if(!empty($query)) {
            return null;
        }
    }

    public function SavePayment($id, $amount, $paymentmethod, $shipaddr, $method, $status, $response)
    {
        $identifier = '';

        $method = strtolower($method);
        if($method == 'creditcard') {
            if(isset($response->id)) {
                $identifier = $response->id;
            }
        } else if($method == 'paypal') {
            if(isset($response->paymentID)) {
                $identifier = $response->paymentID;
            }
        }

        return DB::Table('services_payment')
              ->where('id', '=', $id)
              ->update([ 
                                    'identifier'=>$identifier,
                                'payment_method'=>strtoupper($method), 
                              'payment_response'=>json_encode($response),
                                        'amount'=>$amount,
                                        'status'=>$status,
                                      'shipaddr'=>$shipaddr,
                                         'brand'=>$paymentmethod,
                                     'date_paid'=>(new \DateTime())->format('Y-m-d H:i:s')
                            ]);
    }

    public function SaveDetails($data)
    {
        if($data->type == 'residential') {
          $invoice_id = DB::Table('services_requests_residential')
                          ->where('id', '=', $data->id)
                          ->first()
                          ->invoice_id;
        } else {
          $invoice_id = DB::Table('services_requests_commercial')
                          ->where('id', '=', $data->id)
                          ->first()
                          ->invoice_id;
        }

        $query = DB::Table('services_invoice')
                    ->where('id', '=', $invoice_id)
                    ->first();

        if(isset($query)) {
            $promo = floatval($query->promo);
            $total = floatval($query->total);

            if((floatval($query->promo) == 0.00) && ($data->promo_code != '')) {
                $promo = floatval($query->subtotal) * 0.05;
                $total = floatval($query->subtotal) - floatval($query->discount) -  $promo;
            } else if((floatval($query->promo) > 0.00) && ($data->promo_code == '')) {
                $promo = 0.00;
                $total = floatval($query->subtotal) - floatval($query->discount) + floatval($query->promo);
            }

            DB::Table('services_invoice')
              ->where('id', '=', $invoice_id)
              ->update([ 
                            'promo'=>$promo,
                            'total'=>$total

                      ]); 
        }

        return DB::Table('services_payment')
                ->insertGetId([ 
                                      'firstname'=>$data->fname,
                                       'lastname'=>$data->lname, 
                                       'address1'=>$data->address1,
                                       'address2'=>$data->address2,
                                   'iscommercial'=>$data->iscommercial == 'on' ? 1 : 0,
                                           'city'=>$data->city,
                                          'state'=>$data->state,
                                        'zipcode'=>$data->zipcode,
                                          'email'=>$data->email,
                                     'promo_code'=>$data->promo_code,
                                     'invoice_id'=>$invoice_id,
                                     'date_added'=>(new \DateTime())->format('Y-m-d H:i:s')
                              ]);      
    }

    public function UpdateServices($invoice_id, $paid, $status)
    {
        if(strtolower($status) == 'paid') {
            DB::Table('services_invoice')
              ->where('id', '=', $invoice_id)
              ->update([ 
                            'paid'=>$paid,
                          'status'=>$status
                      ]); 
        }
    }

    public function DeleteService($id, $type) 
    {
        $invoice_id = 0;

        if(strtolower($type) == 'residential') {
          $invoice_id = DB::Table('services_requests_residential')
                          ->where('id', '=', $id)
                          ->first()
                          ->invoice_id;
            DB::Table('services_requests_residential')
                          ->where('id', '=', $id)
                          ->delete();

            DB::Table('services_requests_items')
                          ->where('services_requests_id', '=', $id)
                          ->delete();
                          
        } else {
            $invoice_id = DB::Table('services_requests_commercial')
                          ->where('id', '=', $id)
                          ->first()
                          ->invoice_id;        
            DB::Table('services_requests_commercial')
                          ->where('id', '=', $id)
                          ->delete();
        }        

        DB::Table('services_invoice')
              ->where('id', '=', $invoice_id)
              ->delete();

        DB::Table('services_invoice_items')
              ->where('invoice_id', '=', $invoice_id)
              ->delete();

        DB::Table('services_payment')
              ->where('invoice_id', '=', $invoice_id)
              ->delete();
    }
}