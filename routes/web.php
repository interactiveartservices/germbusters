<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::resource('/','IndexController');
Route::get('my-account','IndexController@showUserAccount');
Route::get('uvc-light', 'IndexController@uvc_light');
Route::get('uv-disinfection-robot', 'IndexController@uv_disinfection_robot');
Route::get('uv-light-disinfection-cart', 'IndexController@uv_disinfection_cart');
Route::get('uv-light-disinfection-device-work', 'IndexController@uv_disinfection_device')->defaults('slug', 'uv-light-disinfection-device-work');
Route::get('uvc-hvac', 'IndexController@uvc_hvac');
Route::get('disinfection-services', 'IndexController@disinfection_services');
Route::get('applications', 'IndexController@applications');
Route::get('about-us', 'IndexController@about_us');
Route::get('contact-us', 'IndexController@contact_us');
Route::get('privacy-policy', 'IndexController@privacy_policy');
Route::get('terms-conditions', 'IndexController@terms_conditions');
Route::get('affiliate-review', 'IndexController@affiliate_review');

Route::get('news', 'IndexController@news_index');
Route::get('uv-light-kills-coronavirus', 'IndexController@uv_light_kills');
Route::get('13-reasons-uv-light-disinfection', 'IndexController@reasons_uv_light');
Route::get('10-steps-disinfecting-groceries-uv-light', 'IndexController@steps_disinfecting_groceries');

Route::get('uv-light-disinfection-device', 'GermZapperController@uv_disinfection_device')->defaults('slug', 'uv-light-disinfection-device');

Route::get('germzapper-uv-light-disinfection-device/{case}', 'GermZapperController@gz_step1');
Route::get('germzapper-uv-light-disinfection-device/accessories/{case}', 'GermZapperController@gz_step2');
Route::get('germzapper-uv-light-disinfection-device/payment/{case}', 'GermZapperController@gz_step3');
Route::get('germzapper-uv-light-disinfection-device/success/{case}', 'GermZapperController@gz_step4');

Route::post('uv-light-disinfection-device/process', 'GermZapperController@process');
Route::post('uv-light-disinfection-device/germzappercart', 'GermZapperController@germzappercart');

Route::get('account/addresses','IndexController@GetAddresses');
Route::post('account/address','IndexController@Address');
Route::post('account/getaddress','IndexController@GetAddress');
Route::post('account/getorder','IndexController@GetOrderDetails');
Route::post('account/update_email','IndexController@update_email');
Route::post('account/unsubscribe','IndexController@unsubscribe');
Route::post('account/forgot_password','IndexController@forgot_password');
Route::post('account/updatepassword','IndexController@updatepassword');

Route::get('logout', 'Auth\LoginController@logout');
Route::get('create-account', 'Auth\LoginController@showCreateAccount');

Route::post('login', 'Auth\LoginController@login');

Route::post('create-account/email','Auth\LoginController@isEmailExists');
Route::post('signup-account','Auth\LoginController@createAccount');


/** AUTHENTICATION FOR SOCIALITE **/
Route::get('auth/{provider}/{cart?}', 'Auth\LoginController@redirectToProvider')->where('cart', '[0-9]+');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::post('shop/product', 'ShoppingCartController@ShoppingCart');

Route::get('cleaning-quote/commercial','ServiceController@commercial_services');
Route::get('cleaning-quote/commercial/success/{id?}','ServiceController@commercial_success');
Route::get('cleaning-quote/modify-services','ServiceController@modify_services');
Route::get('cleaning-quote/scheduling','ServiceController@scheduling');
Route::get('cleaning-quote/checkout','ServiceController@checkout');
Route::get('cleaning-quote/review-order','ServiceController@review_order');
Route::get('cleaning-quote/residential/success/{id?}','ServiceController@residential_success');

Route::post('cleaning-quote/update_order','ServiceController@update_order');

Route::post('cleaning-quote/commercial','ServiceController@save_commercial_services');
Route::post('services/get-quote','ServiceController@send_contact_form');


Route::get('list/{type}', 'InvoiceController@estimates_invoices_list');
Route::get('list/delete/{type}/{id}', 'InvoiceController@delete_service');
Route::get('estimate/create/{type}/{id}', 'InvoiceController@create');
Route::get('estimate/review/{type}/{id}/{eid}', 'InvoiceController@review');
Route::get('invoice/{type}/{id}/{eid}', 'InvoiceController@invoice');

Route::get('print/{invoice_type}/{type}/{id}/{eid}','InvoiceController@print');
Route::get('print/invoice/checkout/paid/{id}/{eid}','InvoiceController@print_paid');

Route::post('estimate/send','InvoiceController@send_estimate');

Route::post('estimates-invoices/process', 'InvoiceController@process');
Route::post('estimates-invoices/placeorder', 'InvoiceController@placeorder');

Route::get('invoice/details/{type}/{id}/{eid}','InvoiceController@invoice_details');
Route::get('invoice/checkout/paid/{id}/{eid}','InvoiceController@invoice_paid');
Route::get('invoice/checkout/payment/{id}/{eid}','InvoiceController@invoice_payment');
// Route::get('invoice/{type}/{id}', 'InvoiceController@invoice');
Route::get('invoice/checkout/success/{id}/{eid}', 'InvoiceController@success');

Route::post('invoice/process','InvoiceController@process_payment');
Route::post('invoice/details','InvoiceController@save_details');
Route::post('invoice/promocode','InvoiceController@promocode');

Route::get('get-quote-list', 'ServiceController@get_quote_list');
Route::post('get-quote-list/addnote','ServiceController@add_note');