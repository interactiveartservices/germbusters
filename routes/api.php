<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');


Route::get('/getlocationbyzip/{zip}','ServiceController@getLocationByZip');
Route::get('/getlocationbycoords/{lat}/{lang}','ServiceController@getLocationFromCoords');
Route::get('/getiplocation','IndexController@getIpLocation');
Route::post('/verify_captcha','IndexController@verify_captcha');
Route::post('/save_sig','InvoiceController@saveSig');
Route::get('/show_sig/{invoice_id}','InvoiceController@getSig');

