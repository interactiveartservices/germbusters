
jQuery(document).ready(function($) {

    var loc_url = '';

    $('#step1 #txtZip').on('input', function() {

        if($('#step1 #txtZip').val() != ""){
            $('#step1 #zipcheck').removeClass("btn-gray");
            $('#step1 #zipcheck').addClass("btn-yellow");
        }else{
            $('#step1 #zipcheck').removeClass("btn-yellow");
            $('#step1 #zipcheck').addClass("btn-gray");

        }

    });

    $('#step3 #txtAddress').on('input', function() {

        if($('#step3 #txtAddress').val() != ""){
            $('#step3 #zipfind').removeClass("btn-gray");
            $('#step3 #zipfind').addClass("btn-yellow");
        }else{
            $('#step3 #zipfind').removeClass("btn-yellow");
            $('#step3 #zipfind').addClass("btn-gray");

        }

    });

    $(".quote-residential-btn").click(function(e) {
        e.preventDefault();
        $(this).removeClass('deselected').addClass('selected');
        $(".quote-commercial-btn").removeClass('selected').addClass('deselected');
    });
    $(".quote-commercial-btn").click(function(e) {
        e.preventDefault();
        $(this).removeClass('deselected').addClass('selected');
        $(".quote-residential-btn").removeClass('selected').addClass('deselected');
    });

    $(".schedule").click(function(e) {
        if(gq_is_cart) {
            var type = $(".quote-residential-btn").hasClass("selected") ? 'residential' : 'commercial';

            if(type==='residential') {
                window.location = "/cleaning-quote/modify-services";
            } else {
                $('#modalGetQuote').modal("show");
            }
        } else {
            $('#modalGetQuote').modal("show");
        }        
    });

    $("#step-next").click(function(e) {
        e.preventDefault();
        window.location = loc_url;
        // $('#step1').removeClass('hide');
        // $('#step2').addClass('hide');
    });

    //  $("#add_address").click(function(e) {
    //     e.preventDefault();
    //     $('#step_address').removeClass('hide');
    //     $('#step2').addClass('hide');
    // });


    $("#my-location").click(function(e) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            alert("Geolocation is not supported by this browser.");
        }
    });

    function showPosition(position) {
        jQuery.get('/api/getlocationbycoords/' + position.coords.latitude + '/' + position.coords.longitude, function(data) {
            check_location(data, 'zip');                
        });
        // x.innerHTML="Latitude: " + position.coords.latitude + 
        //     "<br>Longitude: " + position.coords.longitude;
    }

    
    $('#check-zip').on('submit', function(e) { 
        e.preventDefault();  
        var zc = $("#txtZip").val();

        var regex = /^\d{5}(?:[-\s]\d{4})?$/

        var ok = regex.test(zc);

        if(ok) {
            get_zip(zc, 'zip');
        } else {
            alert('Invalid zip code');
        }        
    });

    $("#zipcheck").click(function(e) {
        e.preventDefault();
        $('#check-zip').submit();
    });

    $("#findmyzip").click(function(e) {
        e.preventDefault();
        $('#step3').removeClass('hide');
        $('#step1').addClass('hide');
    });

    $("#zipfind").click(function(e) {
        e.preventDefault();
        $('#find-zip').submit();
    })

    $('#find-zip').on('submit', function(e) { 
        e.preventDefault();  

        var addr = $("#txtAddress").val();
        var city = $("#txtCity").val();
        var state = $("#txtState").val();

        
        if(addr.trim() == '' && city.trim() == '' && state.trim() == '') {
            alert('Invalid address');            
        } else {
            var complete_address = encodeURIComponent(addr + ' ' + city + ' ' + state);        
            get_zip(complete_address, 'address');
        }            
    });

    $('#modalGetQuote').on('hidden.bs.modal', function () {
        $(".steps").addClass("hide");
        $("#step1").removeClass("hide");
    })

    function check_location(data, step) {
        fl_coords = new google.maps.LatLng(data.o[0], data.o[1]);
        ch_coords = new google.maps.LatLng(data.c[0], data.c[1]);
        q_coords = new google.maps.LatLng(data.q[0], data.q[1]);

        fl_distance = google.maps.geometry.spherical.computeDistanceBetween(fl_coords, q_coords);
        fl_distance_km = fl_distance/1000; //1km = 1000m

        ch_distance = google.maps.geometry.spherical.computeDistanceBetween(ch_coords, q_coords);
        ch_distance_km = ch_distance/1000; //1km = 1000m
    
        var type = $("#quote-residential-btn").hasClass("selected") ? 'residential' : 'commercial';



        //60mi = 96.5km 
        if(fl_distance_km <= 96.5) {
            $('#step2').removeClass('hide');
            $('#step1').addClass('hide');
            $('#step3').addClass('hide');
            loc_url = (type == 'residential') ? "/cleaning-quote/modify-services" : "/cleaning-quote/commercial";
            //window.location = (type == 'residential') ? "/cleaning-quote/modify-services" : "/cleaning-quote/commercial";
        } else if(ch_distance_km <= 96.5) {                    
            $('#step2').removeClass('hide');
            $('#step1').addClass('hide');
            $('#step3').addClass('hide');
            loc_url =(type == 'residential') ? "/cleaning-quote/modify-services" : "/cleaning-quote/commercial";
            //window.location = (type == 'residential') ? "/cleaning-quote/modify-services" : "/cleaning-quote/commercial";
        } else {                    
            $('#step4').removeClass('hide');
            if(step == 'zip') {                        
                $('#step1').addClass('hide');
            } else if(step == 'address') {
                $('#step3').addClass('hide');
            }                    
        }
    }

    function get_zip(address, step) {
        jQuery.get('/api/getlocationbyzip/' + address, function(data) {
            //console.log(data);
            check_location(data, step);                
        });
    }     

    // jQuery('.tp-banner').show().revolution(
    // {
    //     dottedOverlay:"none",
    //     delay:16000,
    //     startwidth:1170,
    //     startheight:750,
    //     hideThumbs:200,
        
    //     thumbWidth:100,
    //     thumbHeight:50,
    //     thumbAmount:5,
        
    //     navigationType:"bullet",
    //     navigationArrows:"solo",
    //     navigationStyle:"preview4",
        
    //     touchenabled:"on",
    //     onHoverStop:"off",
        
    //     swipe_velocity: 0.7,
    //     swipe_min_touches: 1,
    //     swipe_max_touches: 1,
    //     drag_block_vertical: false,
                                
    //                             parallax:"mouse",
    //     parallaxBgFreeze:"on",
    //     parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                                
    //     keyboardNavigation:"off",
        
    //     navigationHAlign:"center",
    //     navigationVAlign:"bottom",
    //     navigationHOffset:0,
    //     navigationVOffset:20,

    //     soloArrowLeftHalign:"left",
    //     soloArrowLeftValign:"center",
    //     soloArrowLeftHOffset:20,
    //     soloArrowLeftVOffset:0,

    //     soloArrowRightHalign:"right",
    //     soloArrowRightValign:"center",
    //     soloArrowRightHOffset:20,
    //     soloArrowRightVOffset:0,
                
    //     shadow:0,
    //     fullWidth:"on",
    //     fullScreen:"off",

    //     spinner:"spinner4",
        
    //     stopLoop:"off",
    //     stopAfterLoops:-1,
    //     stopAtSlide:-1,

    //     shuffle:"off",
        
    //     autoHeight:"off",						
    //     forceFullWidth:"off",						
                                
                                
                                
    //     hideThumbsOnMobile:"off",
    //     hideNavDelayOnMobile:1500,						
    //     hideBulletsOnMobile:"off",
    //     hideArrowsOnMobile:"off",
    //     hideThumbsUnderResolution:0,
        
    //     hideSliderAtLimit:0,
    //     hideCaptionAtLimit:0,
    //     hideAllCaptionAtLilmit:0,
    //     startWithSlide:0,
    //     videoJsPath:"rs-plugin/videojs/",
    //     fullScreenOffsetContainer: ""	
    // });

    
            
                            
});	//ready
