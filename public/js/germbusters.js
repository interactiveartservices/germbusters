$(document).ready(function() {

     window.smoothScroll = function(target) {
        var scrollContainer = target;
        do { //find scroll container
            scrollContainer = scrollContainer.parentNode;
            if (!scrollContainer) return;
            scrollContainer.scrollTop += 1;
        } while (scrollContainer.scrollTop == 0);
        
        var targetY = 0;
        do { //find the top of target relatively to the container
            if (target == scrollContainer) break;
            targetY += target.offsetTop;
        } while (target = target.offsetParent);
        
        scroll = function(c, a, b, i) {
            i++; if (i > 30) return;
            c.scrollTop = a + (b - a) / 30 * i;
            setTimeout(function(){ scroll(c, a, b, i); }, 20);
        }
        // start scrolling
        scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
    }

    geocodeIp();

    function geocodeIp() {
        jQuery.get('/api/getiplocation', function(data) {
            fl_coords = new google.maps.LatLng(data.o[0], data.o[1]);
            ch_coords = new google.maps.LatLng(data.c[0], data.c[1]);
            q_coords = new google.maps.LatLng(data.q[0], data.q[1]);

            // fl_distance = google.maps.geometry.spherical.computeDistanceBetween(fl_coords, q_coords);
            // fl_distance_km = fl_distance/1000; //1km = 1000m

            ch_distance = google.maps.geometry.spherical.computeDistanceBetween(ch_coords, q_coords);
            ch_distance_km = ch_distance/1000; //1km = 1000m
            
            if(ch_distance_km <= 96.5) {
                jQuery("#ip_dyn_free_quote").html('For a free quote <b>704-877-2455</b>');
                jQuery("#ip_dyn_serving").html('Serving Charlotte');
                jQuery("#ip_dyn_locations").html('Serving Charlotte, Matthews, Gastonia and Concord');
                jQuery("#ip_dyn_phone").html('704-877-2455');

                jQuery("#ip_loc").html('GermBusters - Charlotte, NC');
                jQuery("#ip_add").html('2807 Mount Isle Harbor Dr, Charlotte');
                jQuery("#ip_phone").html('704-877-2455');

                document.getElementById("news_link").href="/uvc-light-kills-covid19-charlotte"; 

            } else{
                jQuery("#ip_dyn_free_quote").html('For a free quote <b>954-761-2480</b>');
                jQuery("#ip_dyn_serving").html('Serving South Florida');
                jQuery("#ip_dyn_locations").html('Serving Fort Lauderdale, Boca Raton, Miami and West Palm Beach');
                jQuery("#ip_dyn_phone").html('954-761-2480');

                jQuery("#ip_loc").html('GermBusters - Pompano Beach, FL');
                jQuery("#ip_add").html('1905 NW 32 St, Building 5, Pompano Beach');
                jQuery("#ip_phone").html('954-761-2480');
            }
        });
    }



    //Contact form 
      $("#btnGetQuote").click(function(e) {
        
        e.preventDefault();

        $('#btnGetQuote').html('Sending <i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending
     
        var frmpage = $('#frmPage').val();
     	var industry = $('#industry').val();
        var product = $('#product').val();
        var firstname = $('#firstname').val();
        var lastname  = $('#lastname').val();
        var jobtitle  = $('#jobtitle').val();
        var phone     = $('#phone').val();
        var email     = $('#email').val();
        var message   = $('#message').val();
        var notify    = (document.getElementById("notify").checked) ? "1" : "0";
        var CSRF_TOKEN   = $('meta[name="csrf-token"]').attr('content');


        var $elem = null;
        var iserr = false;

        var $frmContact = $("#frmContact");

        $frmContact.removeClass("form-error");
        // clear all error messages
        $frmContact.find(".error").html("&nbsp;").removeClass("error");
        // clear all borderlines except for email 
        $frmContact.find(".input-error").removeClass("input-error");


        /* first name */
        $elem = $frmContact.find("input[name='firstname']");
        if(isRequired($elem, "Please provide your first name")) {
            iserr = !iserr ? true : iserr;
        }
        
        /* last name */
        $elem = $frmContact.find("input[name='lastname']");
        if(isRequired($elem, "Please provide your your last name")) {
            iserr = !iserr ? true : iserr;   
        }           

         /* phone */
        $elem = $frmContact.find("input[name='phone']");
        if(isRequired($elem, "Please enter a valid phone number")) {
            iserr = !iserr ? true : iserr;   
        }  

 		/* email */
        $elem = $frmContact.find("input[name='email']");
        if(isRequired($elem, "Please enter a valid email address.")) {
            iserr = !iserr ? true : iserr;   
        }  


        if(!isEmail($elem , "Please enter a valid email address.")) {
            iserr = !iserr ? true : iserr;            
        }

        if($elem.hasClass("email-error")) {
            if(isEmailExists($elem, "A user with this email address already exists.")) {
                iserr = !iserr ? true : iserr;            
            }
        } else {
            $elem.find(".email-error").removeClass("email-error");
        }
       
        if(iserr) {

            $('#btnGetQuote').html('Submit'); 
        	return false;
            // if(!$frmcommercial.hasClass("form-error")) {
            //     $frmcommercial.addClass("form-error");
            // } 
        } 

        $.post('/api/verify_captcha', { code: $(".g-recaptcha-response").val() })
        .done(function(response) {
            
            if(response.success) {
                $.post('/services/get-quote', {'frmpage':frmpage, 'industry':industry, 'product':product, 'firstname':firstname, 'lastname':lastname, 'jobtitle':jobtitle, 'phone':phone, 'email':email, 'message':message, 'notify':notify, '_token': CSRF_TOKEN}, 

                    function(response){  
                
                    // Load json data from server and output message    
                    if(response.type == 'error') {

                    } else {
                        $('#frmContact input').val('');
                        $('#frmContact select').val('');
                        $('#frmContact textarea').val('');
                        $('#btnGetQuote').html('Sent!');
                        window.location =  window.location.href + '/?success';
                    }
            

                }, 'json');
            } else {
                alert('captcha error');
                $('#btnGetQuote').html('Submit'); 
            }
        });
        
       
        // Ajax post data to server
        

    });
   
});


