$(function() {
    /*
         $('.styledCheckbox').on('click',function() {
            if($(this).hasClass("sel")){
                $(this).removeClass('sel');
                $("#offers").val("0");
            }else{
                $(this).addClass('sel');
                $("#offers").val("1");
            }
        });
    */
    
     function isPassword($elem) {
        var password = $elem.val().trim();

        if(password.length < 8) {
            $elem.addClass("input-error");
            $elem.parent().find(".message").addClass("error").html("Password must be 8 characters.");
            return false;                        
        }

        regex = /[0-9]/;
        if(!regex.test(password)) {
            $elem.addClass("input-error");
            $elem.parent().find(".message").addClass("error").html("Password must contain at least one number.");
            return false;                                            
        }
        
        return true;
    }

    function isPasswordMatch($pass, $conf, message) {
        if($pass.val().trim() != $conf.val().trim()) {
            $conf.addClass("input-error");
            $conf.parent().find(".message").addClass("error").html(message);
            return false;                        
        }

        return true;
    }
   
    /* USER ACCOUNT */
    var $frmsignup = $("#signup-form");

    $frmsignup.bind("submit", function(e) {
        e.preventDefault();

        var $elem = null;
        var iserr = false;

        $frmsignup.removeClass("form-error");
        // clear all error messages
        $frmsignup.find(".error").html("&nbsp;").removeClass("error");
        // clear all borderlines except for email 
        $frmsignup.find(".input-error").removeClass("input-error");

        /* first name */
        $elem = $frmsignup.find("input[name='fname']");
        if(isRequired($elem, "Please provide your first name")) {
            iserr = !iserr ? true : iserr;
        }
        
        /* last name */
        $elem = $frmsignup.find("input[name='lname']");
        if(isRequired($elem, "Please provide your your last name")) {
            iserr = !iserr ? true : iserr;   
        }           

        /* phone */
        $elem = $frmsignup.find("input[name='phone']");
        if(isRequired($elem, "Please enter a valid phone number")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* zip code */
        $elem = $frmsignup.find("input[name='zipcode']");
        if(isRequired($elem, "Please enter a valid zip code")) {
            iserr = !iserr ? true : iserr;   
        }  

        $elem = $frmsignup.find("input[name='zipcode']");
        if(isZipCode($elem)) {
            iserr = !iserr ? true : iserr;   
        }  

        /* city */
        $elem = $frmsignup.find("input[name='city']");
        if(isRequired($elem, "Please provide your city")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* state */
        $elem = $frmsignup.find("input[name='state']");
        if(isRequired($elem, "Please provide your state")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* address 1 */
        $elem = $frmsignup.find("input[name='address1']");
        if(isRequired($elem, "Please provide your address")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* email */
        $elem = $frmsignup.find("input[name='email']");
        if(isRequired($elem, "Please provide your email address")) {
            iserr = !iserr ? true : iserr;         
        }

        if(!isEmail($elem , "Please enter a valid email address.")) {
            iserr = !iserr ? true : iserr;            
        }

        if($elem.hasClass("email-error")) {
            if(isEmailExists($elem, "A user with this email address already exists.")) {
                iserr = !iserr ? true : iserr;            
            }
        } else {
            $elem.find(".email-error").removeClass("email-error");
        }

        /* password */
        $pass = $frmsignup.find("input[name='password']");
        if(isRequired($pass, "Please enter a password.")) {
            iserr = !iserr ? true : iserr;         
        }                

        if(!isPassword($pass)) {
            iserr = !iserr ? true : iserr;            
        }                

        /* confirm password */
        $conf = $frmsignup.find("input[name='confirm']");
        if(isRequired($conf, "Please confirm the password.")) {
            iserr = !iserr ? true : iserr;         
        }                

        if(!isPasswordMatch($pass, $conf, "Please make sure your passwords match.")) {
            iserr = !iserr ? true : iserr;         
        }

        if(iserr) {
            if(!$frmsignup.hasClass("form-error")) {
                $frmsignup.addClass("form-error");
            } 
        }
    });

    $frmsignup.on("submit", function(e) { 
        e.preventDefault();

        if(!$frmsignup.hasClass("form-error")) {
            $frmsignup.get(0).submit();
        }
    });

    $frmsignup.find("input[name='email']").focusout(function() {
        if($(this).val().trim() != "") {
            //$frmsignup.find(".email-loader").show();

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");  
            var fd  = new FormData();      

            fd.append("email", $(this).val().trim());

            $.ajax({
                            url: "/create-account/email",
                           type: "POST",
                        headers: {
                                    'X-CSRF-Token': CSRF_TOKEN
                                 },
                           data: fd,
                          cache: false,
                    contentType: false,
                    processData: false,
                      forceSync: false,
                        success: function (data, message, xhr) {
                                    if(isEmailExists($frmsignup.find("input[name='email']"), data)) {
                                        // do nothing
                                    }

                                    //$frmsignup.find(".email-loader").hide();
                                 }
            });                         
        }
    }); 

    $frmsignup.find("input[name='email']").trigger("focusout");
});