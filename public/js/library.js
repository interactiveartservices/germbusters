 function isPassword($elem) {
    var password = $elem.val().trim();

    if(password.length < 8) {
        $elem.addClass("input-error");
        $elem.parent().find(".message").addClass("error").html("Password must be 8 characters.");
        return false;                        
    }

    regex = /[0-9]/;
    if(!regex.test(password)) {
        $elem.addClass("input-error");
        $elem.parent().find(".message").addClass("error").html("Password must contain at least one number.");
        return false;                                            
    }
    
    return true;
}

function isPasswordMatch($pass, $conf, message) {
    if($pass.val().trim() != $conf.val().trim()) {
        $conf.addClass("input-error");
        $conf.parent().find(".message").addClass("error").html(message);
        return false;                        
    }

    return true;
}
    
function checkEmail(email) {
    var pattern = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    return pattern.test(email);
}

var $elem = null;

function isRequired($elem, message) {
    if($elem.parent().hasClass("required")) {
        if($elem.val().trim()==="") {
            $elem.addClass("input-error");
            $elem.parent().find(".message").addClass("error").html(message);
            return true;
        }
    }

    return false;
}

function isEmail($elem, message) {
    if(!checkEmail($elem.val().trim())) {
        $elem.addClass("input-error");
        $elem.parent().find(".message").addClass("error").html(message);
        return false;                        
    }

    return true;
}

function isEmailExists($elem, message) {
    if(message!=="") {
        $elem.addClass("email-error");
        $elem.parent().find(".message").addClass("error").html(message);
        return true;
    } else {
        $elem.removeClass("email-error");
        $elem.parent().find(".message").html("&nbsp;").removeClass("error");
        return false;
    }
}