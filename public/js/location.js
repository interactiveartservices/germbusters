function isZipCode($elem, message) {
    if($elem.val().trim()!=="") {
        var zip = get_zip($elem.val().trim());

        if(!zip.success) {
            $elem.addClass("input-error");
            $elem.parent().find(".message").addClass("error").html(zip.message);
            alert(zip.message);
            return true;
        }
    }

    return false;
}

function check_location(data) 
{        
    fl_coords = new google.maps.LatLng(data.o[0], data.o[1]);
    ch_coords = new google.maps.LatLng(data.c[0], data.c[1]);
    q_coords = new google.maps.LatLng(data.q[0], data.q[1]);

    fl_distance = google.maps.geometry.spherical.computeDistanceBetween(fl_coords, q_coords);
    fl_distance_km = fl_distance/1000; // 1km = 1000m

    ch_distance = google.maps.geometry.spherical.computeDistanceBetween(ch_coords, q_coords);
    ch_distance_km = ch_distance/1000; // 1km = 1000m
            
    // 60mi = 96.5km 
    if(fl_distance_km <= 96.5) {     
        return true;
    } else if(ch_distance_km <= 96.5) {
        return true;
    } else {                    
        return false;           
    }        
 }

function get_zip(zipcode)
{
    var result = {
                     success: true,
                     message: ""
                 }

    var objResponse = $.ajax({
                                type:'GET',
                                 url:'/api/getlocationbyzip/'.concat(zipcode),
                               async: false,
                             success: function(data) {
                                             result = {
                                                         success: true,
                                                         message: ""
                                                      }
                                      },
                               error: function (xhr, status, errMsg) {
                                             result = {
                                                         success: false,
                                                         message: "Your zip code is invalid."
                                                      }
                                      }
                            });

    if(result.success) {
        result.success = check_location(JSON.parse(objResponse.responseText));

        if(!result.success) {
            result.message = "Sorry your outside our service area.";
        }
    }

    return result;
}


