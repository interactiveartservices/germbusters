$(function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");  

	$germzapper = $(".germzapper");

    $('#menu-step1').on('click', function() {
        location.href = '/germzapper-uv-light-disinfection-device/'.concat($("#casetype").val());
    });

    $('#menu-step2').on('click', function() {
        location.href = '/germzapper-uv-light-disinfection-device/accessories/'.concat($("#casetype").val());
    });
	
	$('#btn-step1').on('click', function() {
        location.href = '/germzapper-uv-light-disinfection-device/accessories/'.concat($("#casetype").val());
    });

	$('#btn-step2').on('click', function() {
        location.href = '/germzapper-uv-light-disinfection-device/payment/'.concat($("#casetype").val());
    });

	$germzapper.find('.accessories').click(function(event) {
        var $this = $(this);

        if($this.hasClass("sel")) {
            $this.removeClass("sel");
        } else {
            $this.addClass("sel");
        }

		EnableInput(false);
        $.post('/uv-light-disinfection-device/germzappercart',{     _token: CSRF_TOKEN,
                                                                      'id': $(this).data("id"),
                                                                  'action': 'update',
                                                                    'type': $(this).data("type"),
                                                              'isselected': $this.hasClass("sel") ? 1 : 0,
                                                                'casetype': '',
                                                                   'reset': 0
                                                              }, function(result) {
            $("#total_cart").text(accounting.formatMoney(result['total'], "$", 2, ",", "."));
            EnableInput(true);
        }); 
    })

    $germzapper.find('.bulb').click(function(event) {
        var $this = $(this);

        $germzapper.find(".bulb").removeClass("sel");
        $this.addClass("sel");

        EnableInput(false);
        $.post('/uv-light-disinfection-device/germzappercart',{     _token: CSRF_TOKEN,
                                                                      'id': $(this).data("id"),
                                                                  'action': 'update',
                                                                    'type': $(this).data("type"),
                                                              'isselected': $this.hasClass("sel") ? 1 : 0,
                                                                'casetype': $("#casetype").val(),
                                                                   'reset': 1
                                                              }, function(result) {
            $("#total_cart").text(accounting.formatMoney(result['total'], "$", 2, ",", "."));
            EnableInput(true);
        }); 

        if($("#casetype").val() === 'handheld-case') {
            if($(this).data("id") == "5ece4797eaf5e") {
                $(".tds-switch_toggle-container").removeClass("switch_ozone").addClass("switch_ozone_free");
                $("#ozone_txt").addClass("hide");
                $("#ozone_free_txt").removeClass("hide");
            }else{
                $(".tds-switch_toggle-container").removeClass("switch_ozone_free").addClass("switch_ozone");
                $("#ozone_txt").removeClass("hide");
                $("#ozone_free_txt").addClass("hide");
            }
        } else {
             if($(this).data("id") == "5f40ea9551b84") {
                $(".tds-switch_toggle-container").removeClass("switch_ozone").addClass("switch_ozone_free");
                $("#ozone_txt").addClass("hide");
                $("#ozone_free_txt").removeClass("hide");
            }else{
                $(".tds-switch_toggle-container").removeClass("switch_ozone_free").addClass("switch_ozone");
                $("#ozone_txt").removeClass("hide");
                $("#ozone_free_txt").addClass("hide");
            }
        }
     
        // $("#savingsToggle").change(function() {
        //         if($(".tds-switch_toggle-container").hasClass("switch_ozone_free")) {
        //             $(".tds-switch_toggle-container").removeClass("switch_ozone_free").addClass("switch_ozone");
        //         } else {
        //             $(".tds-switch_toggle-container").removeClass("switch_ozone").addClass("switch_ozone_free");
        //         }

        //         //console.log(this.checked);
        //     })
            

    })

    function EnableInput(b) {
        if(b) {
            $("#btn-step1").removeAttr("disabled");
            $("#btn-step2").removeAttr("disabled");
        } else {
            $("#btn-step1").attr("disabled", true);
            $("#btn-step2").attr("disabled", true);
        }
    }
});