$(function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");  

    var $gbsvc = $(".gb-services");

    $gbsvc.find('.other-services').prop('checked', false);

    $("#modify_services").on('click', '.quantity-btn .add', function(event){
        updateQty("add", this);
    })

    $("#modify_services").on('click', '.quantity-btn .sub', function(event){
        updateQty("sub", this);
    })

    $("#modify_services").on('click', '#clear_cart', function(event){
        clearUserSelection();
    })

    $('#progresstracker_select_services').on('click', function() { 
        location.href = '/cleaning-quote/modify-services';
    });

    $('#progresstracker_scheduling').on('click', function() {
        location.href = '/cleaning-quote/scheduling';
    });

    $('#progresstracker_guest_information').on('click', function() {
        location.href = '/cleaning-quote/checkout';
    });

    $('#modify_services').on('click', '.btn-alt', function() {
        location.href = '/cleaning-quote/scheduling';
    });

    $('#schedule_services').on('click', '.btn-alt', function() {
        location.href = '/cleaning-quote/checkout';
    });

    $gbsvc.find('.other-services.styledCheckbox').on('click',function() {
        if($(this).hasClass("sel")) {
            $(this).removeClass('sel');
        }else {
            $(this).addClass('sel');
        }
    });
    
    $gbsvc.on('click', '.other-services', function() {
        var field = '';

        if($(this).hasClass("other-services-uvc")) {
            field = 'uvc';
        } else if($(this).hasClass("other-services-wipe")) {
            field = 'wipe';
        } else if($(this).hasClass("other-services-apt-test")) {
            field = 'apttest';
        } else if($(this).hasClass("other-services-parking")) {
            field = 'parking';
        } else if($(this).hasClass("other-services-area")) {
            field = 'area';
        } else if($(this).hasClass("other-services-driveway")) {
            field = 'driveway';
        }

        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                            'id': $(this).data("id"),
                                        'action': 'update',
                                         'field': field,
                                          'type': $(this).data("type"),
                                         'value': $(this).hasClass('sel') ? 1 : 0
                                    }, function(result) {
            estimatedTotal(result);
            EnableInput(true);
        }); 
    })

    $gbsvc.on('click', '#apply_promo_code', function() {
        $('#apply_promo_code').attr("disabled", true);
        $('#apply_promo_code').html('Apply <i class="fa fa-spinner fa-pulse fa-fw"></i>'); 

        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                        'action': 'update',
                                          'type': 'promocode',
                                         'value': $("#inputPromoCodeDesktop").val(),
                                    }, function(result) {
            if(isNaN(result)) {
                estimatedTotal(result);
            } else {
                alert('The code you have entered is either not valid, expired, or not accepted by this operation.');
            }

            EnableInput(true);

            $('#apply_promo_code').removeAttr("disabled");
            $('#apply_promo_code').html('Apply'); 
        }); 
    });
    
    // footer mobile
    $('.cart__openclose').click(function(){
        $("#bottom-bar .cart--quote--estimated").toggle();

        if ($(this).hasClass("cart__open")){
            $(this).removeClass("cart__open");
            $(this).addClass("cart__close");

            $(this).parent().parent().removeClass("cart__open");
            $(this).parent().parent().addClass("cart__close");

        }else{
            $(this).removeClass("cart__close");
            $(this).addClass("cart__open");

            $(this).parent().parent().removeClass("cart__close");
            $(this).parent().parent().addClass("cart__open");
        }
                 
    });

    function updateQty(actionType, thisObj) {
        var iQty = 
            parseInt($(thisObj).parents('.quantity-btn').find('.qty').attr('data-qty'));

        if(actionType == "add") {
            uQty = iQty + 1;
        } else {
            uQty = iQty - 1;
            uQty = uQty < 1 ? 0 : uQty;
        }

        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                            'id': $(thisObj).data("id"),
                                        'action': 'update',
                                         'field': 'quantity',
                                          'type': $(thisObj).data("type"),
                                         'value': uQty
                                    }, function(result) {
            estimatedTotal(result);
            EnableInput(true);

            $(thisObj).parents('.quantity-btn').find('.qty').attr('data-qty',uQty);
            $(thisObj).parents('.quantity-btn').find('.qty').text(uQty);
        });  
    }

    function estimatedTotal(result)
    {
        $gbsvc.find('.disinfect__total')
              .text(accounting.formatMoney(result['total']['price']['disinfect'], "$", 2, ",", "."));
        $gbsvc.find('.uvc__total')
              .text(accounting.formatMoney(result['total']['price']['uvc'], "$", 2, ",", "."));
        $gbsvc.find('.wipe__total')
              .text(accounting.formatMoney(result['total']['price']['wipe'], "$", 2, ",", "."));
        $gbsvc.find('.apt__total')
              .text(accounting.formatMoney(result['total']['price']['apttest'], "$", 2, ",", "."));

        $gbsvc.find('.disinfect__quantity')
              .text(result['total']['count']['disinfect']);
        $gbsvc.find('.uvc__quantity')
              .text(result['total']['count']['uvc']);
        $gbsvc.find('.wipe__quantity')
              .text(result['total']['count']['wipe']);
        $gbsvc.find('.apt__test__quantity')
              .text(result['total']['count']['apttest']);

        $gbsvc.find('.cart__order__total')
              .text(accounting.formatMoney(result['total']['subtotal'], "$", 2, ",", "."));
        $gbsvc.find('.cart__subtotal .cart__price')
              .text(accounting.formatMoney(result['total']['subtotal'], "$", 2, ",", "."));
        $gbsvc.find('.cart__total')
              .text(accounting.formatMoney(result['total']['estimated'], "$", 2, ",", "."));
        $gbsvc.find('.cart_final__total')
              .text(accounting.formatMoney(result['total']['estimated'], "$", 2, ",", "."));

        $gbsvc.find('.cart--quote--estimated').addClass('hide');
        $gbsvc.find('.cart--quote--default').addClass('hide');

        var subTotal = parseFloat(result['total']['subtotal']);

    
        if(subTotal > 0) {
            $gbsvc.find('.cart--quote--estimated').removeClass('hide');

            if(subTotal >= 399){
                $gbsvc.find('.cart__alert').hide();
                $gbsvc.find('#txtjobminimum').hide();
                $gbsvc.find("#next_step").removeAttr("disabled");
            } else {
                $gbsvc.find('.cart__alert').show();
                $gbsvc.find('#txtjobminimum').show();
                $gbsvc.find("#next_step").attr("disabled", true);
            }            
        } else {
            $gbsvc.find("#next_step").attr("disabled", true);
            $gbsvc.find('.cart--quote--default').removeClass('hide');
        }

        $gbsvc.find(".cart__discount")
            .text(accounting.formatMoney(result['total']['discount'], "- $", 2, ",", "."));

        if(parseFloat(result['total']['discount']) > 0.00) {
            $gbsvc.find('.cart__promo').removeClass("hide");
        } else {
            $gbsvc.find('.cart__promo').addClass("hide");
        }
    }

    function clearUserSelection() 
    {
        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                   'action': 'clear'
                               }, function(result) {

            estimatedTotal(result);
            EnableInput(true);

            $gbsvc.find('.cart--quote--estimated').addClass('hide');
            $gbsvc.find('.cart--quote--default').removeClass('hide');
            $gbsvc.find('.styledCheckbox').removeClass('sel');

            $gbsvc.find('.quantity-btn .qty').each(function() {
                $(this).attr('data-qty', 0);
                $(this).text(0);
            })

            $gbsvc.find("#next_step").attr("disable", true);
        }); 
    }

    function EnableInput($isenable)
    {
        if($isenable) {
            $gbsvc.find("#loader").hide();

            $gbsvc.find(".table .quantity").removeAttr("disabled");
            $gbsvc.find(".table .other-services").css("pointer-events", "auto");
            $gbsvc.find(".form__field .other-services").css("pointer-events", "auto");
        } else {
            $gbsvc.find("#loader").show();

            $gbsvc.find(".table .quantity").attr("disabled", true);
            $gbsvc.find(".table .other-services").css("pointer-events", "none");
            $gbsvc.find(".form__field .other-services").css("pointer-events", "none");
        }
    }
})