$(function() {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");  

    var isUserSelected = false;
    $('.other-services').prop('checked', false);
    $("#modify_services").on('click', '.quantity-btn .add', function(event){
        updateQty("add", this);
    })
    $("#modify_services").on('click', '.quantity-btn .sub', function(event){
        updateQty("sub", this);
    })
    $("#modify_services").on('click', '#clear_cart', function(event){
        clearUserSelection();
    })

    $('#progresstracker_select_services').on('click',function(){
        location.href = '/cleaning-quote/modify-services';
    });

    $('#progresstracker_scheduling').on('click',function(){
        location.href = '/cleaning-quote/scheduling';
    });

    $('#progresstracker_guest_information').on('click',function(){
        location.href = '/cleaning-quote/checkout';
    });

    $('#modify_services').on('click', '.btn-alt',function(){
        location.href = '/cleaning-quote/scheduling';
    });

    $('#schedule_services').on('click', '.btn-alt',function(){
        location.href = '/cleaning-quote/checkout';
    });

    $('#checkout_services').on('click', '.btn-alt',function(){
        location.href = '/cleaning-quote/review-order';
    });

     $('.styledCheckbox').on('click',function(){
        if($(this).hasClass("sel")){
            $(this).removeClass('sel');
        }else{
            $(this).addClass('sel');
        }
    });

    

    $('#modify_services').on('click', '.other-services',function(){
        document.getElementById("next_step").disabled = false;

        var iPrc = parseFloat($(this).attr('data-prc'));
        var wipeTotal = parseFloat($('.wipe__total').attr('data-wipetotal'));
        var aptTotal = parseFloat($('.apt__total').attr('data-apttotal'));
        var subTotal = parseFloat($('.cart__subtotal').attr('data-subtotal'));
        //if($(this).prop('checked')){
        if($(this).hasClass('sel')){
            subTotal = parseFloat(subTotal+iPrc);
            $(this).attr('data-disinfect', 1);

            if($(this).hasClass("other-services-wipe")){
                wipeTotal = parseFloat(wipeTotal+iPrc);
                $('.wipe__total').attr('data-wipetotal', wipeTotal);
                $('.wipe__total').text(accounting.formatMoney(wipeTotal, "$", 2, ",", "."));
            }else if($(this).hasClass("other-services-apt-test")){
                aptTotal = parseFloat(aptTotal+iPrc);
                $('.apt__total').attr('data-apttotal', aptTotal);
                $('.apt__total').text(accounting.formatMoney(aptTotal, "$", 2, ",", "."));
            }

        }else{
            subTotal = parseFloat(subTotal-iPrc);
            $(this).attr('data-disinfect', 0);

            if($(this).hasClass("other-services-wipe")){
                wipeTotal = parseFloat(wipeTotal-iPrc);
                $('.wipe__total').attr('data-wipetotal', wipeTotal);
                $('.wipe__total').text(accounting.formatMoney(wipeTotal, "$", 2, ",", "."));
            }else if($(this).hasClass("other-services-apt-test")){
                aptTotal = parseFloat(aptTotal-iPrc);
                $('.apt__total').attr('data-apttotal', aptTotal);
                $('.apt__total').text(accounting.formatMoney(aptTotal, "$", 2, ",", "."));
            }
        }

        estimatedTotal(subTotal);

        var field = '';

        if($(this).hasClass("other-services-wipe")) {
            field = 'wipe';
        } else if($(this).hasClass("other-services-apt-test")) {
            field = 'apttest';
        } else if($(this).hasClass("other-services-parking")) {
            field = 'parking';
        } else if($(this).hasClass("other-services-area")) {
            field = 'area';
        } else if($(this).hasClass("other-services-driveway")) {
            field = 'driveway';
        }

        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                            'id': $(this).data("id"),
                                        'action': 'update',
                                         'field': field,
                                          'type': $(this).data("type"),
                                         'value': $(this).hasClass('sel') ? 1 : 0
                                    }, function(result) {
            EnableInput(true);
        }); 
    })

    function updateQty(actionType, thisObj) {
        isUserSelected = true;

        var iQty = $(thisObj).parents('.quantity-btn').find('.qty').attr('data-qty');
        var iPrc = parseFloat($(thisObj).parents('.quantity-btn').find('.qty').attr('data-prc'));
        var disinfectTotal = parseFloat($('.disinfect__total').attr('data-disinfecttotal'));
        var subTotal = parseFloat($('.cart__subtotal').attr('data-subtotal'));
        var uQty;

        if(actionType == "add"){
            uQty = parseInt(iQty) + 1;

            disinfectTotal = parseFloat(disinfectTotal+iPrc);
            $('.disinfect__total').attr('data-disinfecttotal', disinfectTotal);
            $('.disinfect__total').text(accounting.formatMoney(disinfectTotal, "$", 2, ",", "."));
          
            subTotal = parseFloat(subTotal+iPrc);
            $(thisObj).parents('.quantity-btn').find('.qty').attr('data-disinfect', 1);
        }else{
            uQty = parseInt(iQty) - 1;

            disinfectTotal = parseFloat(disinfectTotal-iPrc);
            $('.disinfect__total').attr('data-disinfecttotal', disinfectTotal);
            $('.disinfect__total').text(accounting.formatMoney(disinfectTotal, "$", 2, ",", "."));

            subTotal = uQty >= 0 ? parseFloat(subTotal-iPrc) : parseFloat(subTotal); 
            uQty = uQty < 0 ? 0 : uQty;

            if(uQty == 0){
                $(thisObj).parents('.quantity-btn').find('.qty').attr('data-disinfect', 0);
            }
        }

        $(thisObj).parents('.quantity-btn').find('.qty').attr('data-qty',uQty);
        $(thisObj).parents('.quantity-btn').find('.qty').text(uQty);
        
        estimatedTotal(subTotal);

        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                            'id': $(thisObj).data("id"),
                                        'action': 'update',
                                         'field': 'quantity',
                                          'type': $(thisObj).data("type"),
                                         'value': uQty
                                    }, function(result) {
            EnableInput(true);
        });  
    }

    function EnableInput($isenable)
    {
        if($isenable) {
            $("#loader").hide();

            $(".table .quantity").removeAttr("disabled");
            $(".table .other-services").css("pointer-events", "auto");
            $(".form__field .other-services").css("pointer-events", "auto");
        } else {
            $("#loader").show();

            $(".table .quantity").attr("disabled", true);
            $(".table .other-services").css("pointer-events", "none");
            $(".form__field .other-services").css("pointer-events", "none");
        }
    }

    function estimatedTotal(subTotal){
        var disinfectQty = 0;
        $('#modify_services').find('.quantity-btn .qty').each(function(){
            disinfectQty = parseInt($(this).attr('data-qty')) + disinfectQty;
        })
        $('.disinfect__quantity').text(disinfectQty);

        var wipeQty = 0;
        $('#modify_services').find('.other-services-wipe').each(function(){
            wipeQty = parseInt($(this).attr('data-disinfect')) + wipeQty;
        })
        $('.wipe__quantity').text(wipeQty);

        var aptTestQty = 0;
        $('#modify_services').find('.other-services-apt-test').each(function(){
            aptTestQty = parseInt($(this).attr('data-disinfect')) + aptTestQty;
        })
        $('.apt__test__quantity').text(aptTestQty);
        
        $('.cart__subtotal').attr('data-subtotal', subTotal);
        $('.cart__subtotal .cart__price').text(accounting.formatMoney(subTotal, "$", 2, ",", "."));

        var jobMin = $('.cart__info').attr('data-min');
        var subTotal = $('.cart__subtotal').attr('data-subtotal');

        var estTotal = (parseFloat(jobMin) + parseFloat(subTotal)).toFixed(2);

        $('.cart_final__total').attr('data-ttl', estTotal);
        $('.cart_final__total').text(accounting.formatMoney(estTotal, "$", 2, ",", "."));        

        $('.cart__order__total').text(accounting.formatMoney(subTotal, "$", 2, ",", "."));

        $('.cart--quote--estimated').addClass('hide');
        $('.cart--quote--default').addClass('hide');
        if(subTotal > 0){
            $('.cart--quote--estimated').removeClass('hide');
            if(subTotal >= 99){
                $('.cart__alert').hide();
            }else{
                $('.cart__alert').show();
            }
        }else{

            document.getElementById("next_step").disabled = true;
            $('.cart--quote--default').removeClass('hide');
        }


    }

    function clearUserSelection(){

        $('#modify_services').find('[data-disinfect]').each(function(){
            $(this).attr('data-disinfect', 0)
        })

        $('#modify_services').find('.quantity-btn .qty').each(function(){
            $(this).attr('data-qty', 0);
            $(this).text(0);
        })

        $('.styledCheckbox').removeClass('sel');
        
        $('.disinfect__quantity').text(0);
        $('.wipe__quantity').text(0);
        $('.apt__test__quantity').text(0);

        $('.disinfect__total').text("");
        $('.disinfect__total').attr('data-disinfecttotal', 0);
        $('.wipe__total').text("");
        $('.wipe__total').attr('data-wipetotal', 0);
        $('.apt__total').text("");
        $('.apt__total').attr('data-apttotal', 0);
        $('.cart__total').text("$0.00");

        $('.cart__subtotal').attr('data-subtotal', 0);
        $('.cart__subtotal .cart__price').text(0);

        $('.cart_final__total').attr('data-ttl', 0);
        $('.cart_final__total').text(0);   
        
        $('.cart--quote--estimated').addClass('hide');
        $('.cart--quote--default').removeClass('hide');

        document.getElementById("next_step").disabled = true;

        EnableInput(false);
        $.post('/shop/product',{     _token: CSRF_TOKEN,
                                            'id': $(this).data("id"),
                                        'action': 'clear'
                                    }, function(result) {
            EnableInput(true);
        }); 
    }


})