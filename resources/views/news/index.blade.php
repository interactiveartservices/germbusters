@extends('layouts.master')

@section('header_scripts')

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">
   	<picture>    	
		<source media="(max-width: 560px)" srcset="/images/news-hdr-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/news-hdr.png 1x, /images/news-hdr@2x.png 2x">	
		<img class="banner-img" src="/images/news-hdr.png" alt="Coronavirus (COVID19) Deep Cleaning Information">
	</picture>
	<div class="bn-text container text-left">
		<h4 class="title text-white">News center</h4>
	</div>

   </div>	   
   <!--End Banner-->

   <div class="content bg-light-gray">
   		<div class="container py-60">

		   	<div class="row main">
		   		<a href="/13-reasons-uv-light-disinfection">		       
			      	<div class="row my-60 d-flex list">
						<div class="pl-30 c1">
							<img class="width-auto" src="/images/news-thumb2.jpg" alt="13 Reasons To Consider UV Light to Help Stop Coronavirus" />					
						</div>
						<div class=" bg-white c2">
							<h1 class="font-raleway d-inline-block text-teal">13 Reasons To Consider UV Light For Disinfection</h1>
							<p class="line-height-1-2 text-dark font-size-18 mt-0">Aug 3, 2020</p>													
						</div>
					</div>
				</a>
		    </div>

		    <div class="row main list pb-60">
		    	<a href="/10-steps-disinfecting-groceries-uv-light">		       
			      	<div class="row d-flex list">
						<div class="pl-30 pr-0 c1">
							 <img class="width-auto" src="/images/news-thumb3.jpg" alt="10 Steps to Disinfect Your Groceries with UV light " />	
						</div>
						<div class="bg-white c2">
							  <h1 class="font-raleway d-inline-block text-teal">10 Steps to Disinfect Your Groceries with UV light </h1>
                              <p class="line-height-1-2 text-dark font-size-18 mt-0">Aug 17, 2020</p>	
						</div>
					</div>
				</a>
		    </div>

            <div class="row main list pb-60">
                <a href="/uv-light-kills-coronavirus">               
                    <div class="row d-flex list">
                        <div class="pl-30 pr-0 c1">
                           	<img class="width-auto" src="/images/news-thumb1.jpg" alt="Introducing GermZapper-300, A UV light Device that Destroys Coronavirus in 3 seconds" />
                        </div>
                        <div class="bg-white c2">
                          	<h1 class="font-raleway d-inline-block text-teal">Introducing GermZapper-300™: A UV light <br>Device that Destroys Coronavirus in 3 seconds</h1>
							<p class="line-height-1-2 text-dark font-size-18 mt-0">August 26, 2020</p>
                        </div>
                    </div>
                </a>
            </div>
	
	</div>




@endsection
@section('footer_scripts')

@endsection

