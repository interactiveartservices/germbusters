@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">

@endsection

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">
   	<picture>    	
		<source media="(max-width: 560px)" srcset="/images/news-hdr-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/news-hdr.png 1x, /images/news-hdr@2x.png 2x">	
		<img class="banner-img" src="/images/news-hdr.png" alt="Coronavirus (COVID19) Deep Cleaning Information">
	</picture>
	<div class="bn-text container text-left">
		<h4 class="title text-white">News center</h4>
	</div>

   </div>	   
   <!--End Banner-->

   <div class="content">
   		<div class="container ">

   			<article itemscope itemtype="http://schema.org/Article">	

		   	<div class="row main pb-60">
		       
		        <div class="txt col-md-8 pr-50 pt-30">
		        	<h6 class="font-size-15 font-weight-light" itemprop="datePublished" >August 26, 2020 </h6>
		        	<br>
		        	<h1 class="text-teal text-transform-normal mt-20" itemprop="headline"> 
						Introducing GermZapper-300™: A UV light <br>
						Device that Destroys Coronavirus in 3 seconds
					</h1>

					<div itemprop="articleBody">
		        	<h2 class="font-size-15 font-weight-normal mt-20" style="font-family:centrale_sans_boldbold_italic!important;color:#444444;">
			        	Florida Start-up Germbusters911 changes the shape of commercial disinfection cleaning by <br> offering everyone next-generation UV light technology with its latest multi-purpose device.
					</h2>
					<p>
			        	<b>Pompano Beach, FL August 26, 2020-</b> Businesses and home-owners who are looking for innovative and effective disinfection solutions, can now purchase the new patent-pending <span class="text-teal">GermZapper-300®</span>, a multi-function UVC disinfection device that's portable enough to hold in your hands yet powerful enough to disinfect a room. 
					</p>
					<p>
			        	The dynamic UVC disinfection solution is the latest innovation in the Germbusters911 innovative UVC light disinfection line. GermZapper 300™ is available with Ozone or a Ozone free version. The handheld, portable germicidal UVC disinfection device is designed to deactivate bacteria, viruses and fungi on surfaces where chemical sprays aren’t appropriate. It is strong enough to disinfect a 645 sqft room in 3 minutes.  
					</p>
					<p>
			        	GermZapper OZ-300™ offers an ozone generating germicidal bulb that helps eradicate foul odor and harmful volatile organic compounds (VOCs) such as sulfides, mercaptans and ammonia. One distinct advantage of ozone is that it can be carried in the air to places where direct UV radiation cannot reach, optimizing its germ killing efficiency as a UV air purification system, providing an additional deodorizing effect.   
					</p>
					<p>
			        	“Today you have carpet cleaning companies buying electrostatic sprayers and claiming they are trained to prevent a highly infectious disease by spraying everything in sight,” Remarks Germbusters911 Partner Scott Ownbey, “People don’t want chemicals sprayed all over their keyboards, papers and other sensitive surfaces, they want a more effective and holistic approach”. 
					</p>
					<p>
			        	*GermZapper-300™ patent-pending induction technology bulb produces a light intensity of 2100uw/cm² at 50cm2 which according to a recent study by Boston University is the amount required in neutralize/kill Covid19 at (22 mJ/cm2) in 3 seconds at a distance of 5 inches. 
					</p>
					<p>
			        	*GermBusters scientifically verifies UV dosage using Dosimeter cards that provide a visual indication of applied UV-C energy up to 100 mJ/cm2, which can be correlated to a dose sufficient to kill pathogens with 99.9% efficiency.
					</p>
					<h2 class="text-teal mt-20 font-size-13">
			        	About GermBusters
					</h2>
					<p><i>
			        	A group of visionary engineers, creative problem solvers, and investors from a variety of industries. Their vision is to use next-generation technology to protect the health of you and your business. Germbusters911 uses a combination of innovative patent-pending UVC light and electrostatic spraying solutions that can be used as  part of your regular cleaning cycle. The UV robots are safe and eliminate human error and are designed to be operated by every-day cleaning staff.
					</i></p>
					<p><i>
			        	Germbusters911 headquarters  is located at 1905 NW 32 Street, Building 5, Pompano Beach, FL 33064. For disinfection services and more, contact their team via phone at 954-761-2480 or via email at hello@germbusters911.com.  For additional information regarding their services, visit the company’s website <span class="text-teal font-weight-bold">www.germbusters911.com.</span></i></p>
			        </div>
			        
		        </div>


		        <div class="col-md-4 text-center text-md-left">
			        <div>
			        	<p class="font-size-15" style="font-family:centrale_sansmedium!important;">Contacts</p><br>
			        	<img src="/images/headshot.jpg" class="width-auto" alt="Scott Ownbey" />
			        	<div class="line-height-1-5 pt-20">
						<div itemprop="author" itemscope itemtype="http://schema.org/Person">
			        		<span itemprop="name">Scott Ownbey</span>
			        	</div>
			        	<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"> 
							<span itemprop="name">Marketing Office</span>
			        	 </div>
			        	   954-761-2480
			        	</div>
			        </div>

			        <div class="media">
			        	<p>Media</p>
			        	<a class="fancybox-media" href="https://youtu.be/VInDq7McTOI"><img src="/images/news-img1.png" class="width-auto img" alt="news" /></a>
			        </div>
			         <div class="media pt-50">

			        	<a class="fancybox-media" title="Download" href="/images/news-img2@2x.png"><img src="/images/news-img2.png" alt="UVC Disinfection on paper" class="width-auto img" itemprop="image" /></a>
			        	<p class="mt-10"><a class="fancybox-media" title="Download" href="/images/news-img2@2x.png"><img src="/images/Icon feather-image.png" class="width-auto" alt="UVC Disinfection on paper" /></a>&nbsp;&nbsp;UVC Disinfection on paper</p>
			        </div>
			         <div class="media pt-30">
			        	<a class="fancybox-media" title="Download" href="/images/news-img3@2x.png"><img src="/images/news-img3.png" alt="UVC Disinfection on paper" class="width-auto img"  /></a>
			        	<p class="mt-10"><a class="fancybox-media" title="Download" href="/images/news-img3@2x.png"><img src="/images/Icon feather-image.png" class="width-auto" alt="UVC Disinfection on paper"/></a>&nbsp;&nbsp;UVC Disinfection on paper</p>
			        </div>
			         <div class="media pt-30">
			        	<a class="fancybox-media" title="Download" href="/images/news-img4@2x.png"><img src="/images/news-img4.png" alt="UVC Disinfection on electronics" class="width-auto img" /></a>
						<p class="mt-10"><a class="fancybox-media" title="Download" href="/images/news-img4@2x.png"><img src="/images/Icon feather-image.png" class="width-auto" alt="UVC Disinfection on electronics"/></a>&nbsp;&nbsp;UVC Disinfection on electronics</p>
			        </div>
				</div>	

		    </div>

		    </article>

		</div>
	
	</div>




@endsection
@section('footer_scripts')

<script type="text/javascript" >
jQuery(document).ready(function($) {

	jQuery('.fancybox').fancybox({		
		padding: 0
	});
	

	jQuery('.fancybox-media').fancybox({
				afterLoad: function () {
					this.title = (this.title) ? '<a href="' + this.href +	'" class="text-white" target="_blank">Download</a> ' : '';
				},
				padding:0,
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					title: {
                		type: 'outside'
            		},
					overlay:{ 
						css: {background:'rgba(0,0,0,0.75)', 'z-index':99998}
					},
					media : {},
					padding:0
				}
			});

   

});
</script>
@endsection

