@extends('layouts.master')

@section('header_scripts')

@endsection

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">
   	<picture>    	
		<source media="(max-width: 560px)" srcset="/images/news-hdr-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/news-hdr.png 1x, /images/news-hdr@2x.png 2x">	
		<img class="banner-img" src="/images/news-hdr.png" alt="Coronavirus (COVID19) Deep Cleaning Information">
	</picture>
	<div class="bn-text container text-left">
		<h4 class="title text-white">News center</h4>
	</div>

   </div>	   
   <!--End Banner-->

   <div class="content">
   		<div class="container">

		<article itemscope itemtype="http://schema.org/Article">	

		   	<div class="row main pb-60 reasons">
		       
		        <div class="txt col-md-12 pr-50 pt-30">

		        	<h6 class="font-size-15 font-weight-light" itemprop="datePublished">August 3, 2020</h6>
		        	<br>
		        	<h1 class="text-teal text-transform-normal mt-20" itemprop="headline"> 
						13 Reasons You Should Consider UV light <br>For Disinfection
 
					</h1>
		        	<h2 class="font-size-15 font-weight-normal mt-20" style="font-family:centrale_sans_boldbold_italic!important;color:#444444;">
			        	In this quick guide you’ll learn 13 reasons to consider UVC Light in your disinfection service.
					</h2>

					<div class="my-30">
						<img src="/images/news-pushcart.jpg" class="w-100" alt="UVC Light in your disinfection service" alt="uvc light push cart" />
					</div>

					 <div class="d-flex">
			        	<img src="/images/headshot-circle.png" class="width-auto" alt="Scott Ownbey" />
			        	<div class="line-height-1-5 pl-20 pt-20 font-weight-medium">
				        	<div itemprop="author" itemscope itemtype="http://schema.org/Person"> Written by<br>
				        	  <span itemprop="name">Scott Ownbey</span>
				        	</div>
				        	<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
								<span itemprop="name" class="text-lgray">Germbusters911</span>
							</div>
						</div>
			        </div>

			   

					<div itemprop="articleBody">
						
					<p class="pt-20">
			        	UVC disinfection is a method that uses short-wavelength ultraviolet light to kill or inactivate pathogens by destroying their DNA, leaving them neutralized or killed.  So if you’re looking to get the most out of your disinfection service and help stop the spread of Covid19  this guide is for you.
					</p>
					<p>
			        	Let’s jump right in.  
					</p>              
					<h2 class="text-teal mt-50">
			        	1. Chemical-Free Disinfection    
					</h2>
					<p>          
			        	UV-C disinfection offers a chemical-free alternative.  There are no toxic chemical or carcinogenic by-products left behind when UV disinfection is used.
					</p>
					<p>
			        	Many EPA N-List approved disinfectants require a cure time which keeps surfaces wet for a period of time.  According to the latest CDC guidelines disinfectant cleaning companies should clean surfaces using soap and water to reduce the number of germs, dirt and impurities on the surface—then use the disinfectant.  Depending on your facility this may not be an option.
					</p>

					<h2 class="text-teal mt-50">
			        	2. UV light disinfection is a highly effective form of disinfection

					</h2>

					<p>The next main advantage of UV light disinfecting is that this disinfection process can make me much more effective than other methods. UV radiation destroys a large number of harmful species. 
					Did you know , for example, that UV light kills the molds and spores? Certain methods of disinfection do not-or do leave a humid atmosphere in which fungi can grow. Since UV disinfection is a dry process, you can be sure it will take care of the existing mold and will prevent its future growth. </p>
					
					<div class="my-30">
						<img src="/images/news-hotel.jpg" class="w-100"  itemprop="image" alt="sanitize hotel" />
					</div>

					<h2 class="text-teal mt-50">3. Sanitizes Electronics & Soft Surfaces</h2>

					<p>All surfaces including keyboards on desks, phones, computers and even food preparation surfaces, will absorb an assured level of disinfection in a certain amount of time as long as the light is not blocked from shining on that surface. </p>

					<p>UV Light Disinfection destroys germs without developing immunity  
					If you’ve followed the reports of antibiotic-resistant bacteria, you know the use of certain conventional antimicrobials and disinfectants has had severe consequences. Developing antibiotic-resistant bacteria is a big concern within the medical community. 
					UV light disinfection is a practical process for destroying bacteria, in comparison to conventional methods of disinfection. The bacteria in question are therefore unable to develop immunity to it. This is a huge plus particularly for hospitals and living facilities that have been supported.
					</p>

					<h2 class="text-teal mt-50">4. There are no toxic chemical or carcinogenic by-products left behind when UV disinfection is used</h2>

					<div class="my-30">
						<img src="/images/news-home.jpg" class="w-100" alt="sanitize home" />
					</div>

					<h2 class="text-teal mt-50">5. Studies prove UVC is effective in inactivating the virus that causes COVID-19</h2>

					<p>A recent study from Boston University and Signify has shown that SARS-CoV-2, the virus that causes COVID-19, can be inactivated with a dose of 22 mJ/cm².  If we use the Germbuster GermZapper 300 for irradiation that would take 3 seconds to inactivate at 6 inches from the surface.  
					</p>

					<p>*UV-C disinfection is best performed by trained UV-C technicians that can coordinate and implement all the parameters above.  
					</p>

					<h2 class="text-teal mt-50">6. UV-C emitting ozone kills airborne coronaviruses</h2>

					<p>UV wavelengths harnessed at 185 nm are used in systems which produce ozone, a proven oxidizer which eliminates odor and has been proven to kill coronaviruses, including Covid-19.  </p>

					<p>Coronaviruses are classified as “enveloped viruses”, which are typically more susceptible to “Physico-chemical challenges”. In other words, they don’t like being exposed to ozone.   Ozone destroys this type of virus by breaking through the outer shell into the core, resulting in damage to the viral RNA. Ozone can also damage the outer shell of the virus in a process called oxidation. Put simply, exposing Coronaviruses to sufficient ozone dose (ppm x time) can result in them being 99% damaged or destroyed.</p>

					<p>GermBuster’s GermZapper OZ-300™ offers an ozone generating UV-C germicidal lamp that helps eradicate foul odor and harmful volatile organic compounds (VOCs) such as sulfides, mercaptans and ammonia. One distinct advantage of ozone is that it can be carried in the air to places where direct UV radiation cannot reach, optimizing its germ killing efficiency as a UV air purification system, providing an additional deodorizing effect.</p>

					<h2 class="text-teal mt-50">7. UV light disinfection light is safe </h2>

					<p>One of the most popular questions we get about disinfection with UV light is, "Is it safe to use? "People tend to equate UV exposure with hazards like sunburn, but the secret to understanding is that UV light is healthy when properly used. When you take the proper care, UV light is less likely to cause damage than the toxic chemicals in cleaning materials.</p>

					<div class="my-30">
						<img src="/images/news-toys.jpg" class="w-100" alt="sanitize toys" />
					</div>    

					<h2 class="text-teal mt-50">8. Portable UV light offers better results</h2>

					<p>Following CDC’s guidance on UV irradiation there are four key variables to consider successful UV-C irradiation: </p>

					<p>• Distance of UV bulbs to surface <br>
						• Removal or organic and inorganic matter from the surface and the bulbs<br>
						• UV-C bulb intensity and *wavelength <br>
						• Time of exposure (dosage)<br>
						• Optimum UV wavelength of 240–280 nm<br>
						• Each target microorganism requires different exposure times. 
						</p>

					<p>Installed lights in rooms in theory seem like a good idea but break many of the  CDC guidelines rules for proper disinfection.  The ability to place UV bulbs in close proximity to high touch areas can not be overstated as a critical step for irradiation.  In addition any debris in line of sight from the bulbs ( including dust on the bulbs) to the surface can drastically affect the germicidal results.  </p>

					<h2 class="text-teal mt-50">9. UV effectiveness can be easily visually verified</h2>

					<p>Measuring system performance of UVGI over time is a critical component of successful implementation. To properly measure the intensity and fluence of a UVGI system, there are two main accessible methods: radiometers and dosimeters.  </p>

					<p>Radiometer <br>
					A radiometer will measure the intensity (and fluence in certain models) of a specified spectrum of electromagnetic radiation. For UVGI applications, the primary concern is typically in the UVC section of the spectrum (200-280nm). The radiometer will consist of a sensor to pick up energy in the desired measurement spectrum and a meter to display the input to the sensor (or output that information to an external component of a monitoring system). Some radiometers have the capacity to measure fluence, which is useful in applications with moving components. </p>

					<p>Dosimeter <br>
					A dosimeter is a photoreactive sticker that will change color to indicate dosage or fluence. It is positioned wherever a measurement of fluence is desired. A good dosimeter will not react outside the desired UV wavelengths it is designed to measure. 
					</p>

					<p>Dosimeters cards or stickers are relatively inexpensive ($3-$10/piece) </p>

					<div class="my-30">
						<img src="/images/news-scientific.jpg" class="w-100" alt="dosimeter cards" />
					</div>    

					<h2 class="text-teal mt-50">10. Cost-effective</h2>

					<p>UV disinfection systems are often smaller and easier to use than conventional systems, with affordable capital and operational costs.</p>

					<h2 class="text-teal mt-50">11. UV is an optimum disinfectant method in kitchens </h2>
   
					<p>UV disinfection doesn’t alter the taste of liquid or food, which can happen with chemical or heat treatments.</p>

					<h2 class="text-teal mt-50">12. UV disinfection systems are often safer </h2>

					<p>UV Disinfection systems eliminate the  the need to produce, store and transport dangerous and potentially toxic chemicals.</p>
    
					<h2 class="text-teal mt-50">13. UV kills more than germs </h2>

					<p>Germicidal lamps are equally effective against bacteria, moulds, spores, bacteria and viruses</p>

					<p>If you're considering adding UV light disinfection to your cleaning plan Consult with a GermBuster’s UVC expert. Meet remotely from your home or one-on-one at your house. During the consultation, we’ll discuss all the UVC service options. </p>
					</div>

		        </div>
		   
		    </div>
	
		</article>

	</div>
</div>


@endsection
@section('footer_scripts')


@endsection

