@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">
@endsection

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">
   	<picture>    	
		<source media="(max-width: 560px)" srcset="/images/news-hdr-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/news-hdr.png 1x, /images/news-hdr@2x.png 2x">	
		<img class="banner-img" src="/images/news-hdr.png" alt="Coronavirus (COVID19) Deep Cleaning Information">
	</picture>
	<div class="bn-text container text-left">
		<h4 class="title text-white">News center</h4>
	</div>

   </div>	   
   <!--End Banner-->

   <div class="content">
   		<div class="container">

		<article itemscope itemtype="http://schema.org/Article">	

		   	<div class="row main pb-60 reasons">
		       
		        <div class="txt col-md-12 pr-50 pt-30">

		        	<h6 class="font-size-15 font-weight-light" itemprop="datePublished">August 17, 2020</h6>
		        	<br>
		        	<h1 class="text-teal text-transform-normal mt-20" itemprop="headline"> 
						10 Steps to Disinfect Your Groceries with UV light 
					</h1>
		        	<h2 class="font-size-15 font-weight-normal" style="font-family:centrale_sans_boldbold_italic!important;color:#444444;">
			        	In this quick guide you’ll learn 10 steps to use UV Light to disinfect your groceries.
					</h2>

					<div class="my-30">
						<a class="fancybox-media" href="https://youtu.be/2c-JDv5EDak"><img src="/images/news-germzapper.png" class="w-100" alt="Disinfect Your Groceries with UV light" /></a>
					</div>

					 <div class="d-flex">
			        	<img src="/images/headshot-circle.png" class="width-auto" alt="Scott Ownbey" />
			        	<div class="line-height-1-5 pl-20 pt-20 font-weight-medium" > 
			        		<div itemprop="author" itemscope itemtype="http://schema.org/Person">Written by<br>
			        	  		<span itemprop="name">Scott Ownbey</span>
			        		</div>
			        	    <div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
							    <span itemprop="name" class="text-lgray">Germbusters911</span>
							 </div>
						</div>
			        </div>


					<div itemprop="articleBody">
						
					<p class="pt-20">
			        	Should you dip your groceries in bleach or drown your groceries in UVC light to prevent exposure to the new coronavirus?
						First let’s look at why you may want to consider disinfecting your groceries in the first place. 
						Even though the most recent guidance from the Centers for Disease Control (CDC) indicates the most important mode of transmission is through close contact from person to person — it may be possible to contract the virus through surfaces.
					</p>
					<p>
			        	In a recent study published March 17, 2020, in The New England Journal of Medicine- the study found that the virus, known officially as SARS-CoV-2, stays on surfaces for various periods of time, depending on the nature of the surface:
					</p>              
					
					<p>          
			        •  Hard surfaces like plastic, steel, countertops, and glass: 72 hours <br>
					• Porous surfaces like cardboard, paper, and fabrics: 24 hours <br>
					• Airborne in droplets released by coughing or sneezing: 3 hours

					</p>
					<p>
			        	At the other hand, there were experts such as Joseph Allen, DSc, MPH, from the T.H. Chan School of Public Health at Harvard University in Boston, who wrote an op-ed in the Washington Post, basically saying: Calm down. He lays out a package handling scenario which shows why the risk of transmission between food packaging and humans in almost all cases remains very small.
					</p>

					<p>Who’s right, then? They both are according to a number of health experts. Dr. Allen is right that if you’re around an infected person, the chance of COVID-19 is far greater than that of a product that might have received some droplets from anyone with symptoms, but VanWingen makes some excellent points about not spreading germs to different surfaces within the house. </p>

					<h2 class="mt-50 font-size-26">
			        	Should you disinfect groceries if you have an Autoimmune Disease
					</h2>
					
					<div class="my-30">
						<img src="/images/news-mike.png" class="w-100" alt="Mike O'Brien" />
					</div>


					<p>People with autoimmune disorders and other health concerns have been more likely to have severe complications after contracting the virus than the general population. Physicians recommend that people with autoimmune disorders adhere to recommendations by the Centers of Disease Control and Prevention.  According to the latest CDC guidelines for grocery shopping they do NOT recommend the use of disinfectants designed for hard surfaces, such as bleach or ammonia, on food packaged in cardboard or plastic wrap. </p>

					<p>So what’s a person supposed to do?
					</p>


					<p><a href="https://bit.ly/3gp5Zug" target="_blank" class="text-dark text-underline">Boston University validates</a> UV-C light disinfection for coronavirus deactivation. A tube lamp rendered the virus 99% inactive in the lab, and could hit nearly 100% depending on the distance and intensity of the UV light.  
					</p>

					<p>Germbusters conducted our own tests with the GermZapper 300®. We found that within 3 seconds at a distance of 6 inches away was enough to deliver the recommended amount of UV light (22mJ/cm2)  to kill SARS-CoV2, the virus that causes COVID19.
					</p>

					<h2 class="text-teal mt-50">10 steps to safely unpack and prepare your groceries for UVC disinfection.</h2>

					<div class="my-30">
						<img src="/images/news-groceries.png" class="w-100"  itemprop="image" alt="prepare your groceries for UVC disinfection" />
					</div>

					<p  class="line-height-2-5 font-size-15" style="font-family:centrale_sans_bold!important;">1. Once you’re home from the grocery store, take care while handling and unpacking your groceries.  <br>
						2. Take the groceries out you want to disinfect and place them a few inches apart from one another.<br>
						3. Place the GermZapper 6 inches away from the groceries and move about 6 inches per second over your items. <br>
						4. Place your UV light down and turn over your groceries to the side that didn’t receive any UV light. <br>
						5. Do not expose your skin to the UVC light.<br>
						6. Do not look into the UVC light when in use.<br>
						7. Always wear protective eyewear. <br>
						8. UVC can turn the outer layer of skin of your fruit and vegetables darker color. This is only the light killing the microbes on  the surfaces and does not in any way damage the inside of your food.<br>
						9. Your groceries should have received an irradiation equal to 22mJ/cm2 which is enough to kill SARS-Cov2. If you want to make certain buy germ meter stickers from <a href="https://uvcdosimeters.com/" target="_blank" class="text-dark text-underline">www.uvcdosimeters.com</a>.<br>
						10. When done wash your hands thoroughly with soap and water.
					</p>

					<p>UVC germicidal  light, when used properly, provides the safest most effective method if disinfection for groceries and any time where harsh chemicals and soaps present a problem.  The GermZapper® has a variety of  applications in many settings, including hospitals, public transportation, offices, retail stores, and supermarkets to name just a few. </p>

					<div class="my-30">
						<img src="/images/news-car-cleaning.png" class="w-100" alt="car cleaning UVC disinfection" />
					</div>

					<p>Priced at over $1700, a hospital-grade UVC disinfection device may at first seem like an expensive option but consider the following.  The average family will spend $40-$50 each month on cleaning supplies or $600 a year plus the average monthly cost of surface cleaners.  According to cleaning habits and household size, surface cleaner costs range anywhere from $10 to $20 per month, or $120 to $240 per year.   In addition a UVC disinfection device is never out of stock and doesn’t leave residue on surfaces.   </p>


					<p>To see and learn more about the GermZapper® please see <a href="https://www.germbusters911.com/uv-light-disinfection-device" class="text-dark text-underline font-weight-bold">our website.</a></p>

					
					</div>

		        </div>
		   
		    </div>
	
		</article>

	</div>
</div>





@endsection
@section('footer_scripts')
<script type="text/javascript" >
jQuery(document).ready(function($) {

	jQuery('.fancybox').fancybox({		
		padding: 0
	});
	

	jQuery('.fancybox-media').fancybox({
				afterLoad: function () {
					this.title = (this.title) ? '<a href="' + this.href +	'" class="text-white">Download</a> ' : '';
				},
				padding:0,
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					title: {
                		type: 'outside'
            		},
					overlay:{ 
						css: {background:'rgba(0,0,0,0.75)', 'z-index':99998}
					},
					media : {},
					padding:0
				}
			});

   

});
</script>

@endsection

