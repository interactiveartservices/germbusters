 <!--Start Header-->
   
<div id="header-2">
   <header class="header">
		<div class="container">
   		
   		
        <div class="row">
        	
            <div class="col-md-3">
            	<a href="/" class="logo"><img src="/images/logo@1x.png" alt=""></a>
            </div>
            
            <div class="col-md-9">
            	
                
                <nav class="menu-2">
            	<ul class="nav wtf-menu">
                	<li class="item-select parent"><a href="/">Home</a>	</li>
					
                    <li class="parent"><a href="#">Services</a>
    					<ul class="submenu">
                            <li class="sub-filler">&nbsp;</li>
                            <li><span class="cursor-pointer" onClick="location.href='/uvc-light'">UVC Light</span></li>          
                            <li><a href="/uv-disinfection-robot">UV Disinfection robot</a> </li>
                            <li><a href="/uv-light-disinfection-device">Hand-held UVC light device</a> </li>
                            <li><a href="/uv-light-disinfection-cart">UVC lamp cart</a> </li>
                            <li><a href="/uvc-hvac">UVC air purifier</a> </li>
                            <li><span class="cursor-pointer" onClick="location.href='/disinfection-services'">Disinfection Services</span></li>
                            <li><a href="/disinfection-services#electrostatic-disinfections">Electrostatic Disinfections</a> </li>
                            <li class="sub-filler">&nbsp;</li>
                        </ul>  
                    </li>
	
                    <li class="parent"><a href="/about-us">About Us</a></li>
                    <li class="parent"><a href="/applications">Applications</a>
                        <ul class="submenu">
                            <li class="sub-filler">&nbsp;</li>       
                            <li><a href="/applications#restaurants">Restaurants</a> </li>
                            <li><a href="/applications#gyms">Gyms</a></li>
                            <li><a href="/applications#casinos">Casinos</a> </li>
                            <li><a href="/applications#schools">Schools</a></li>
                            <li><a href="/applications#hospitals">Hospitals</a> </li>
                            <li><a href="/applications#hospitality">Hospitality</a> </li>
                            <li><a href="/applications#grocery-stores">Grocery Stores</a> </li>
                            <li><a href="/applications#nursing-home">Nursing Home</a> </li>
                            <li class="sub-filler">&nbsp;</li>
                        </ul>  
                    </li>
					
					<li class="parent"><a href="/contact-us">Contact Us</a></li>
                    <li class="parent"><a href="/news" id="news_link">News</a></li>

                    @if(Auth::check())
                       <li class="bg-transparent text-capitalize"><a href="{{ Auth::user()->email == 'affiliateprogram@germbusters911.com' ? '/list/estimates' : '/my-account' }}" class="acct">{{ Auth::user()->firstname. ' '. Auth::user()->lastname }}&nbsp;<img src="/images/Icon material-account-box.png" alt="user-icon" class="width-auto"/></a></li>
                       <li class="parent"><a href="/logout">Sign Out</a></li>
                    @else
                        <li class="parent"><a href="/login">Sign In</a></li>
                        <li class="parent"><a href="/create-account">Sign Up</a></li>
                    @endif                                    
                </ul>
                </nav>
                	
            </div>
            
        </div>
        
        
        </div>	
        <div id="mobile-quote-home" class="tp-caption quote_links customin" style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
            <div class="top-part">                
                <a href="#" id="quote-residential-btn" class="quote-residential-btn qbs selected">
                    <span class="tp1">Residential</span>
                </a>                
                <a href="#" class="qbs deselected quote-commercial-btn" id="quote-commercial-btn">
                    <span class="tp2">Commercial</span>
                </a>
            </div>
            <div class="bottom-part">
                <a id="schedule" class="schedule" href="#" data-toggle="modal" data-aos="fade-up">Get quote &amp; Schedule now</a>
            </div>
        </div>
    </header>

</div>
	
   <!--End Header-->	