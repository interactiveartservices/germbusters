<a href="#0" class="cd-top"></a>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2DQNn31o4aKsciGI9IZEE11X7EyRmlyQ&libraries=geometry"></script>

<script type="text/javascript" src="/js/jquery.js"></script>

<!-- SMOOTH SCROLL -->	
<script type="text/javascript" src="/js/scroll-desktop.js"></script>
<script type="text/javascript" src="/js/scroll-desktop-smooth.js"></script>

<!-- START REVOLUTION SLIDER -->	
<script type="text/javascript" src="/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="/js/jquery.themepunch.tools.min.js"></script>

<!-- BOOTSTRAP -->	
<script type="text/javascript" src="/js/bootstrap.min.js"></script>

<!-- Date Picker and input hover -->
<script type="text/javascript" src="/js/classie.js"></script> 
<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.js"></script>


<!-- Fun Facts Counter -->
<script type="text/javascript" src="/js/counter.js"></script> 


<!-- Welcome Tabs -->	
<script type="text/javascript" src="/js/tabs.js"></script>

<!-- Mobile Menu -->
<script type="text/javascript" src="/js/jquery.mmenu.min.all.js"></script>

<!-- All Carousel -->
<script type="text/javascript" src="/js/owl.carousel.js"></script>
<script type="text/javascript" src="/js/slick.fixed.js"></script>


<script type="text/javascript" src="/js/jquery.fancybox.js"></script> 
<script type="text/javascript" src="/js/jquery.fancybox-media.js"></script>


<!-- All Scripts -->
<script type="text/javascript" src="/js/custom.js"></script>

<script type="text/javascript" src="/js/germbusters.js"></script>

<script type="text/javascript" src="/js/get_quote.js"></script>

<script type="text/javascript">		 
	$("document").ready(function() {

		$("#accept-cookies").click(function(e) {
			e.preventDefault();				
			document.cookie = "accept=1; expires=Fri, 31 Dec 9999 23:59:59 GMT";
			$(".cookie-notify").hide();
		});			
	});				
</script>

<!--<script src="/js/switcher.js"></script>-->