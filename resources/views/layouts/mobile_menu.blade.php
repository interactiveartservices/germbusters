<!-- Mobile Menu Start -->
	<div class="container">
    <div id="page">
			<header class="header">
				<a href="#menu"></a>            
			</header>
			
			<nav id="menu">
				<ul>
					<li class="select"><a href="/">Home</a></li>
					<li><a href="#">Services</a>
						<ul class="submenu">
                            <li class="sub-filler">&nbsp;</li>
                            <li><span class="cursor-pointer" onClick="location.href='/uvc-light'">UVC Light</span></li>             
                            <li><a href="/uv-disinfection-robot">UV Disinfection robot</a> </li>
                            <li><a href="/uv-light-disinfection-device">Hand-held UVC light device</a> </li>
                            <li><a href="/uv-light-disinfection-cart">UVC lamp cart</a> </li>
                            <li><a href="/uvc-hvac">UVC air purifier</a> </li>
                            <li><span class="cursor-pointer" onClick="location.href='/disinfection-services'">Disinfection Services</span></li>
                            <li><a href="/disinfection-services#electrostatic-disinfections">Electrostatic Disinfections</a> </li>
                            <li class="sub-filler">&nbsp;</li>
                        </ul>  
					</li>
                    <li><a href="/about-us">About Us</a></li>
                    <li><a href="/applications">Applications</a>
                     <ul class="submenu">
                            <li class="sub-filler">&nbsp;</li>       
                            <li><a href="/applications#restaurants">Restaurants</a> </li>
                            <li><a href="/applications#gyms">Gyms</a></li>
                            <li><a href="/applications#casinos">Casinos</a> </li>
                            <li><a href="/applications#schools">Schools</a></li>
                            <li><a href="/applications#hospitals">Hospitals</a> </li>
                            <li><a href="/applications#hospitality">Hospitality</a> </li>
                            <li><a href="/applications#grocery-stores">Grocery Stores</a> </li>
                            <li><a href="/applications#nursing-home">Nursing Home</a> </li>
                            <li class="sub-filler">&nbsp;</li>
                        </ul>  
                    </li>
					<li><a href="/contact-us">Contact Us</a></li>
                    <li><a href="/news">News</a></li>
                     @if(Auth::check())
                       <li class="font-weight-bold bg-transparent text-capitalize"><a href="{{ Auth::user()->email == 'affiliateprogram@germbusters911.com' ? '/list/estimates' : '/my-account' }}" class="acct">{{ Auth::user()->firstname. ' '. Auth::user()->lastname }}&nbsp;<img src="/images/Icon material-account-box.png" alt="user-icon" class="width-auto"/></a></li>
                       <li><a href="/logout">Sign Out</a></li>
                    @else
                        <li><a href="/login">Sign In</a></li>
                        <li><a href="/create-account">Sign Up</a></li>
                    @endif                    
                    </li>
					
				</ul>
                
                
            </nav>
		</div>
		</div>
    <!-- Mobile Menu End -->