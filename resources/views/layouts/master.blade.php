<!DOCTYPE html>
<html lang="en" class="no-js" prefix="og: http://ogp.me/ns#">
<head>

@if(app()->environment('production')) 
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-170094609-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-170094609-1');
	</script>

	<!-- Global site tag (gtag.js) - Google Ads: 624090808 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-624090808"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-624090808');
	</script>
@endif

@if (isset($head_title))
    <title>{{$head_title}}</title>
    <meta name="title" content="{{$head_title}}" />  
@else
    <title>Germbusters</title>
    <meta name="title" content="UVC light | Cleaning-Disinfection Services | Germbusters911" />   
@endif

    <meta name="keywords" content="">
@if (isset($meta_desc))
    <meta name="description" content="{{$meta_desc}}" />
@else
    <meta name="description" content="Discover the innovative world of GermBusters911 and disinfect your home or office with UVC light, UV Robots, Electrostatic Spray" />
@endif

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="website">
@if (isset($head_title))
    <meta property="og:title" content="{{$head_title}}" /> 
@else   
	<meta property="og:title" content="UVC light | Cleaning-Disinfection Services | Germbusters911" >
@endif
@if (isset($meta_desc))
    <meta property="og:description" content="{{$meta_desc}}"  />
 @else   
	<meta property="og:description" content="Discover the innovative world of GermBusters911 and disinfect your home or office with UVC light, UV Robots, Electrostatic Spray" >
@endif
	<meta property="og:url" content="https://www.germbuster911.com/" >
@if (isset($meta_img))
	<meta property="og:image" content="{{$meta_img}}" />
@else  
	<meta property="og:image" content="https://www.germbusters911.com/images/germzapper/banner1-home@1x.jpg" />
@endif	
	<meta content="Germbusters911" property="og:site_name">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<link rel="icon" type="image/png" href="/images/favicon.png">
	
    <!--main file-->
	<link href="/css/medical-guide.css" rel="stylesheet" type="text/css">
	
	<link href="/css/all.fa.min.css" rel="stylesheet" type="text/css">

    <!--Medical Guide Icons-->
	<link href="/fonts/medical-guide-icons.css" rel="stylesheet" type="text/css">

    <!-- Default Color-->
	<link href="/css/default-color.css" rel="stylesheet" id="color"  type="text/css">

    <!--bootstrap-->
	<link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
    
    <!--Dropmenu-->
	<link href="/css/dropmenu.css" rel="stylesheet" type="text/css">
    
	<!--Sticky Header-->
	<link href="/css/sticky-header.css" rel="stylesheet" type="text/css">
	
	<!--revolution-->
	<link href="/css/style.css" rel="stylesheet" type="text/css">    
    <link href="/css/settings.css" rel="stylesheet" type="text/css">    
    <link href="/css/extralayers.css" rel="stylesheet" type="text/css">    
   
    <!--Accordion-->
<!--     <link href="/css/accordion.css" rel="stylesheet" type="text/css"> -->
     
    <!--tabs-->
<!-- 	<link href="/css/tabs.css" rel="stylesheet" type="text/css">     -->
   
	<!--Owl Carousel-->
	<link href="/css/slick.css" rel="stylesheet"  type="text/css" />
	<link href="/css/owl.carousel.css" rel="stylesheet" type="text/css">    
    
   <!-- Mobile Menu -->
	<link rel="stylesheet" type="text/css" href="/css/jquery.mmenu.all.css" />

	<!--Germbusters Override-->
	<link href="/css/germbusters.css" rel="stylesheet" type="text/css">

   <!--PreLoader-->
	<link href="/css/loader.css" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>


	@yield('header_scripts')

	<script>
	    window.Laravel = <?php echo json_encode([
	        'csrfToken' => csrf_token(),
	    ]); ?>

        if (window.location.hash === "#_=_"){
            if (history.replaceState) {
                var cleanHref = window.location.href.split("#")[0];

                history.replaceState(null, null, cleanHref);
            } else {
                window.location.hash = "";
            }
        }	   
	</script>
	

@if(app()->environment('production')) 
	<script type="text/javascript"> _linkedin_partner_id = "2427876"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2427876&fmt=gif" /> </noscript>


	<!-- Facebook Pixel Code -->
	<!-- 'https://connect.facebook.net/en_US/fbevents.js'); -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://www.germbusters911.com/js/fbevents.js');
	fbq('init', '3323941860999612'); 
	fbq('track', 'PageView');
	</script>
	<noscript>
	<img height="1" width="1" 
	src="https://www.facebook.com/tr?id=3323941860999612&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->


	<script type="application/ld+json">
	  {
	    "@context": "http://schema.org",
	    "@type": "Organization",
	    "name": "Germbusters911",
	    "url": "https://www.germbusters911.com/",
	    "logo":"https://www.germbusters911.com/images/logo@1x.png",
	    "address": "1905 NW 32 St, Building 5, Pompano Beach, FL 33064",
	    "sameAs": [
	      "https://www.facebook.com/Germbusters911com-104410671286983",
	      "https://www.instagram.com/germbusters911/"
	    ]
	  }
	</script>

@endif

</head>

<body id="{{ $body_id }}" class="{{ $body_class }}">

@if(app()->environment('production')) 
	
	<!-- https://static.leaddyno.com/js -->
	<script type="text/javascript" src="https://www.germbusters911.com/js/leaddyno.js"></script>
	<script>
	  LeadDyno.key = "843d104b056142ebf1d94ae5aef358917e8e9105";
	  LeadDyno.recordVisit();
	  LeadDyno.autoWatch();
	</script>
	
@endif
	

	<div id="wrap">		
		@include('layouts.header')

		@yield('content')
		
		@include('layouts.footer') 
  	</div>

  	@include('layouts.footer_extra')

 	@yield('footer_scripts')


</body>

</html>