
<!--Start Footer-->
   <footer class="footer-light mt-50"  id="footer">

          <div class="footer-nav">
            <div class="container">
        <div id="footer-section-1" class="row">
            
            <div class="col-md-6">
                <a href="/" class="logo footer-logo"><img class="img-responsive" src="/images/footer-logo.png" alt="germbusters"></a>
            </div>
            
            <div class="col-md-6">
                <div class="footer-links text-right">
                    <a href="/privacy-policy">Privacy Policy</a>
                    <a href="/terms-conditions">Terms & Conditions</a>
                    <a href="https://germbusters911.leaddyno.com">Affiliates</a>
                    <a href="/contact-us">Contact Us</a>
                </div>                
            </div>
            
        </div>
            </div>
            
            <a class="back-to-top" href="#."></a>
            
        </div>


         <div class="footer">
            <div class="container">
                <div id="footer-section-2" class="row">
                    
                    <div class="col-md-6">
                        <span class="copyrights">GermBusters provides next-generation disinfection technology and sanitizing solutions to clean and neutralize viruses bacteria, mold and fungi at your home or office. Our solutions are scientifically verified and backed with a no-risk money-back guarantee.</span>
                    </div>
                    
                    <div class="col-md-6 rcol text-right">
                       <h3 id="ip_dyn_phone" class="font-weight-medium">954-462-4000</h3>
                       <p id="ip_dyn_locations" class="font-raleway">We service Fort Lauderdale, Boca Raton, Miami and West Palm Beach</p>
                    </div>
                    
                </div>
            </div>
            
            <a class="back-to-top" href="#."></a>
            
        </div>
        
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-6">
                        <span class="copyrights">Copyright &copy; 2020 Germbusters LLC. All right reserved.</span>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="social-icons">                            
                            <a href="https://www.facebook.com/Germbusters911com-104410671286983" class="fb"><i class="fab fa-facebook-square"></i></a>
                            <a href="https://www.instagram.com/germbusters911/" class="tw"><i class="fab fa-instagram"></i></a>                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <a class="back-to-top" href="#."></a>
            
        </div>
        
   </footer>
   <!--End Footer-->