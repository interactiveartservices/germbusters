 <!--Start PreLoader-->
<div id="preloader">
	<div id="status">&nbsp;</div>
	<div class="loader">
		<h2>Loading...</h2>
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
<!--End PreLoader-->

<div id="preheader" class="d-flex text-white">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 pht">
				<span class="phl"><!-- <span id="ip_dyn_free_quote">For a free quote <b>954-761-2480</b></span>  --><b>For a free quote 877&mdash;75GERMS</b> <span class="csm">Coronavirus (COVID19) Deep Cleaning Information</span></span>
				<span id="ip_dyn_serving" class="phr">Serving South Florida</span>
			</div>
		</div>
	</div>
</div>

@include('layouts.menu')

@include('layouts.mobile_menu')