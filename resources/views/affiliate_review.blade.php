@extends('layouts.master')

@section('header_scripts')
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
<link href="/css/services.css" rel="stylesheet" type="text/css">
<link href="/css/auth/signup.css" rel="stylesheet" type="text/css">
<link href="/css/affiliate.css" rel="stylesheet" type="text/css" />
<style>
    .sign-here {        
        border-top: solid 1px #000;
        padding-top: 10px;
        margin-top: -75px;
        padding-bottom: 75px;
    }
</style>
@endsection
@section('content')
	

	<!--Start Banner-->   
	<div class="sub-banner">  
		<img class="banner-img" src="/images/sub-banner.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
	</div>	   
	<!--End Banner-->

	<!--Start Loader-->
	<div id="loader" class="loader" style="display: none;" >
    	<img src="/images/loading.gif" alt="" />
	</div>
	<!--End Banner-->

	<div class="content">
	  	<div class="container login">
            @include('partials.progresstracker', 
           array('steps'=> [
                    'Estimate', 
                    'Review <span class="mobilehide">Estimate</span>',
                    'Invoice',
                    'Invoice Paid'
                    ], 'progress'=>['is-complete', 'is-complete', 'is-complete', 'active is-complete'] ))          
            <div class="row">
                <div class="col-sm-12 share-alt text-right mt-50 p-0">
                 <!--    <a href="#"><span>Share</span> <img class="img-reponsive" src="/images/share-alt-square.png" /></a> -->
                </div>                    
                <div id="af-review" class="col border mt-20">
                    <div id="af-review-body">
                        <div class="af-review-header">
                            <div class="row d-flex">
                                <div class="col-sm-6">
                                    <img src="images/gm-header.png" class="head-logo" />
                                    <span class="billto">BILL TO</span>
                                    <h4 class="client">John Doe</h4>
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">john@sample.com</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">+1-555-555-5555<br />
                                            <span>300 North Andrews<br />
                                            Fort Lauderdale, FL 33301</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <h1 class="estimate">Invoice</h1>
                                    <span class="invoice-no">Invoice No: 001-1000</span><br />
                                    <div class="ad-box">
                                        <small>ACCOUNT DUE:</small>
                                        <span>$4,550.00</span>
                                    </div>
                                    <div class="payment-methods">
                                        <span class="p1">PAYMENT METHODS</span>
                                        <hr />
                                        <span class="p2">Paypal: </span><span class="p3">payment@germbusters911.com</span><Br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="af-review-content">
                            <div class="row">
                                <div class="col-sm-12 d-flex justify-space-between head-col">
                                    <div class="rcl">Description</div>
                                    <div class="rcr">Total</div>
                                </div>
                                <div class="col-sm-12">
                                    <hr class="af-div" />
                                </div>                                
                                <div class="col-sm-12 d-flex justify-space-between">
                                    <div class="rcl">
                                        <span>General description</span>
                                        <p>Comprehensive Proactive COVID-19 Wipe/Spray/Disinfect all high touch areas using EPA List N approved disinfectant; 
                                            Electrostatic spray  vacant space using EPA List N approved solution. UV Light disinfect keyboard, electronic & office 
                                            Equipment  </p>
                                    </div>
                                    <div class="rcr">&nbsp;</div>
                                </div>
                                <div class="col-sm-12">
                                    <hr class="af-div-mid" />
                                </div>
                                <div class="col-sm-12 d-flex justify-space-between">
                                    <div class="rcl">
                                        <span>Disinfection Cleaning </span>
                                        <p>Comprehensive proactive COVID-19 wipe/spray/disinfect all high touch areas using EPA List N approved. disinfectant; 
                                            wipe/disinfect all tile and finished hard floor surfaces: 5000 sq ft (customer estimate subject to verification) 
                                            @ $.60/sq ft. </p>
                                    </div>
                                    <div class="rcr">$3600</div>
                                </div>
                                <div class="col-sm-12">
                                    <hr class="af-div-mid" />
                                </div>
                                <div class="col-sm-12 d-flex justify-space-between">
                                    <div class="rcl">
                                        <span>Electrostatic Spray </span>
                                        <p>Electrostatic Spray ambient air spaces in vacant office space: 5000 sq ft (customer estimate subject to verification)
                                            @ $.25/sq ft.  </p>
                                    </div>
                                    <div class="rcr">$1400</div>
                                </div>
                                <div class="col-sm-12">
                                    <hr class="af-div-mid" />
                                </div>
                                <div class="col-sm-12 d-flex justify-space-between">
                                    <div class="rcl">
                                        <span>Disclaimer </span>
                                        <p>**GermBusters911 uses only EPA List N approved disinfectant solutions; all technicians wear full PPE equipment; all equipment 
                                            is pre-santized prior to entry at customer premises; COIT follows all EPA/CDC protocols and industry association 
                                            best methods and practices.*** </p>
                                    </div>
                                    <div class="rcr">&nbsp;</div>
                                </div>
                                <div class="col-sm-12 mt-30">
                                    <hr class="af-div" />
                                </div>
                                <div class="col-sm-12 d-flex justify-space-between mt-20">
                                    <div class="rcl">
                                      &nbsp;
                                    </div>
                                    <div class="rcr">
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">SubTotal:</span><span class="st2">$5000.00</span>
                                        </div>
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">Discount:</span><span class="st2">$450.00</span>
                                        </div>
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">Total:</span><span class="st2">$4550.00</span>
                                        </div>
                                        <hr class="af-div" style="margin-left: 18%" />
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">PAID:</span><span class="st2">$0.00</span>
                                        </div>
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">DUE:</span><span class="st2">$4550.00</span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="af-review-footer border">
                    <img src="/images/footer.png"/>
                    <div>
                        <h5>Payment Address</h5>
                        <p>1905 NW 32 Street,Building 5<br />
                            Pompano Beach, FL 33064 </p>
                        <h5>Terms &amp; Condtions</h5>    
                        <p>Net 5 days on all invoices . In addition, Buyer shall pay all sales, use, <br /> 
                            customs, excise or other taxes presently or hereafter payable in regards <br />
                            to this transaction, and Buyer shall reimburse  Germbusters911 for any such <br />
                            taxes or charges paid by GermBusters911.
                        </p>
                    </div>
                </div>

                <div class="af-next text-center pb-60">

                    <canvas id="signature-pad" class="signature-pad" width=700 height=200></canvas>
                    <p class="sign-here">Signature</p>
                    <div>
                        <button id="save">Save</button>
                        <button id="clear">Clear</button>
                      </div>

                    {{-- <span class="font-weight-bold">Order Total $4550.00 USD</span>
                    <div>
                        <label>
                        <input type="checkbox" name="" id="">
                        I agree to Germbuster's <a href="#">Terms &amp; Conditions</a>
                        </label>
                    </div>
                    <button class="btn btn-alt">Place your order</button> --}}
                </div>
            </div>
		</div>
	</div>

	
@endsection

@section('footer_scripts')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script type="text/javascript">
        var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(0, 0, 0)'
        });

        var saveButton = document.getElementById('save');
        var cancelButton = document.getElementById('clear');


        var invoice_id = 216;

        // load saved signature into canvas
        $.get('/api/show_sig/' + invoice_id).done(function(response) {
            signaturePad.fromDataURL(response);
        })        

        // save signature to server
        saveButton.addEventListener('click', function (event) {
            var data = signaturePad.toDataURL('image/png');
            //window.open(data);

            $.post('/api/save_sig', {data: data,  invoice_id: invoice_id}).done(function(r) {
                $("#save, #clear").hide();
                signaturePad.off();
            });
        });

        cancelButton.addEventListener('click', function (event) {
            signaturePad.clear();
        });
    </script>
	{{-- <script src="/js/accounting.min.js" type="text/javascript"></script>
	<script src="/js/library.js" type="text/javascript"></script>
	<script src="/js/location.js" type="text/javascript"></script>
	<script src="/js/auth/signup.js" type="text/javascript" ></script>
	<script src="/js/pages/cleaning.quote.service.js"></script> --}}

@endsection