@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('content')

 <!--Start Banner-->   
<div class="sub-banner">  
	<picture>
		<source media="(max-width: 560px)" srcset="/images/services-banner-device-mobile@2x.png">
		<source media="(min-width: 561px)" srcset="/images/services-banner-device@1x.png 1x, /images/services-banner-device@2x.png 2x">
		<img class="banner-img" src="/images/services-banner-device@1x.png" alt="services">
	</picture>
	<div class="bn-text">
		<img src="/images/uv@2x.png" alt="UV disinfection cart" />
		<h1 style="color:#2A2E30;">UV Light Disinfection Device</h1>
	</div>

</div>	   
<!--End Banner-->

<!--Start Doctor Quote-->
<div class="dr-quote position-relative overflow-hidden bg-none">	
	<div class="visible-lg-block" style="padding:46.56% 0 0 0;position:relative;">
		<iframe src="https://player.vimeo.com/video/424748053?autoplay=1&loop=1&color=ffffff&portrait=0&background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
	</div>
	<script src="https://player.vimeo.com/api/player.js"></script>
	<div class="hidden-lg">
		<img class="banner-uvc-img" src="/images/bg-uvc-banner2-mobile.png" alt="services">		
	</div>
	<div class="bg-svc-vid-gradient"></div>
	<div class="container center-absolute">
		<div class="row">
			<div class="col-md-12">
				<div class="svc-vid w-600 margin-auto">
					<h2 class="quote">Go Beyond Clean</h2>
					<span class="name">Clinical grade disinfection using the power of light</span>
					<ul class="text-left text-white">
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Proven, maintenance-free true UVC germicidal performance</span></li>
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>On-demand operation to fit your needs and cleaning procedures</span></li> 
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Verified Kill Rates up to 99.9%</span></li>
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Affordable, aerosol free disinfection solutions</span></li>
					</ul>
					<a href="#" class="btn btn-yellow" onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
				</div>
			</div>
		</div>
	</div>		
</div>
<!--End Doctor Quote-->   
	
<div class="container-fluid bg-white text-center py-30"> 
	<div class="row">
		<div class="col-sm-12">
			<h6 class="text-teal font-weight-bold">Hire us, or purchase</h6>
			<div class="main-title mt-20"><h2><span>Hand-held</span> UVC light disinfectant device  </h2>
				<small class="font-raleway">Designed to be operated by every-day cleaning staff</small>	
			</div>
			
		</div>
	</div>
</div>     

<div class="bg-black position-relative overflow-hidden">
	<picture class="pic uvdr-pic">		
		<source media="(max-width: 560px)" srcset="/images/BLACK-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/BLACK.png 1x, /images/BLACK@2x.png 2x">
		<img class="banner-img" src="/images/BLACK.png" alt="services bg" style="max-height: 850px;">
	</picture>
	<div class="container position-absolute content uvdr-ca" style="padding-top:15%">		
		<div class="row">
			
			<div class="col-sm-10 svc-items position-relative">
				<img class="width-auto mw-100p uvdd-gz-pic" src="/images/uv-device.png" alt="uv disinfectant device" />

				<ul class="position-absolute text-white ul1" style="top: 20%;">
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>300 Watt output is the world’s strongest <br>Handheld- UVC disinfectant device</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Maximize disinfection by manually placing germ-zapping <br>light where you need it most</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Easy-to-operate, minimal training <br>Required</span></li>
					<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Sanitize an entire room in about 10 min.</span></li>
				</ul>	

				<ul class="position-absolute text-white ul2" style="top:20%; right:-20%;">
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Lightweight frame makes <br>pushing the cart a breeze.</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Industrial- strength, less expensive & <br>
																					more efficient than any other comparable <br>
																					mobile UV decontamination devices </span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>High quality and durable—12 months <br>hassle-free warranty.</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>PROVEN effective—Germ meter cards <br>scientifically verify our results</span></li>
				</ul>	
			</div>

			<div class="col-sm-2">
				
			</div>

			<div class="col-sm-12 text-center position-absolute uvdr-gq" style="bottom: 20%;">
				<a href="#" class="btn btn-yellow mt-0"  onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
			</div>
		</div>
		
	</div>
</div>

<div class="bg-dark-blue text-white">
	<div class=" uvdd kz">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<div class="kz-row d-flex">
					<div class="kz-row-l">
						<h2>Room Bacteria Killer</h2>
						<div class="main-title mb-20">
							<h3 class="text-white">How close to the surface <span class="font-weight-light">do the UVC lamps need to be to kill germs?</span></h3>
						</div>
						<p class="font-raleway line-height-1-2 mb-20">High intensities for a short period or low intensities for a long period <br />
							are fundamentally equal in lethal action on pathogens.</p>
						<ul class="text-left text-white">
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>6 inches = </span>3 seconds</li>
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>3 feet = </span>25-45 seconds</li> 
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>6 feet = </span>2-3 minutes</li>
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>10 feet = </span>10-15 minutes</li>
						</ul>
						<small class="font-italic">Calculations based on (1) 40W with the intensity of approximately 110-130 μw/cm2 at 3.3 feet distance for a dosage of 6.6-7.8 mJ/minute </small>
					</div>
					<div class="kz-row-r">
						<img src="/images/disinfection-device.png" alt="disinfection device" class="mw-100p" />
					</div>
				</div>				
			</div>
		</div>		
	</div>	
</div>

<div class="bg-light-gray py-40">
	<div class="container gz-tiles">
		<div class="row">

			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-right">
				<div class="bg flap">
					<span class="txt">See the Germ-Zapper 300™ <br>In action.</span>
					<a class="btn btn-watch fancybox-media video-icon mt-30"  href="https://www.youtube.com/watch?v=18_uESI8_G0">WATCH NOW</a>
				</div>
				<a class="fancybox-media video-icon"  href="https://www.youtube.com/watch?v=18_uESI8_G0"><img class="gz-img" src="/images/device-lightning.png" alt="device lightning"/></a>
			</div>
	
			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-left">
				<img  class="gz-img" src="/images/device-sofa.png" alt="lightning device sofa"/>
				<div class="bg">
					<span class="txt">Clean soft-surfaces that <br>
					Are sensitive to liquids <br>
					and chemicals.</span>
				</div>
			</div>
		
			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-right">
				<div class="bg flap">
					<span class="txt">Effortless sanitize electronics <br>
					neutralizing 99.9% of all <br>
					pathogens.</span>
				</div>
				<img  class="gz-img" src="/images/device-electronics.png" alt="device electronics" />
			</div>

			<div class="col-sm-12 text-center pb-60">
				<a href="#" class="btn btn-yellow mt-0"  onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
			</div>
			
		</div>
	</div>
</div>

<div class="bg-white">
	<div class="container pt-60">
		<div class="row">
			<div class="col-lg-6">
				<div class="main-title mb-20">
					<h2 class="text-teal-blue"><span class="text-dark">Does UV light kill viruses?</span><br /> Seeing is believing</h2>
				</div>
				<p class="mb-20">UVC dosimeter&trade; cards and stickers are industry standard tools used to 
				scientifically verify if surfaces are getting enough UV-C exposure to kill  your
				targeted pathogens.   They are placed in the treated areas and use a 
				photo-chromatic ink that changes color at various energy levels, which 
				correlates with a log reduction of pathogens.</p>
				<p>The UVC dosimeter&trade; test is offered as an optional test before a disinfection
				service begins.  Our results are scientifically verified-backed with a no-risk guarantee.</p>
			</div>
			<div class="col-lg-6 pl-30 text-center uvc-vid">				
				<a class="fancybox-media video-icon"  href="https://www.youtube.com/watch?v=xhlP3iNgPww"><img class="width-auto mw-430" src="/images/uvc-play.png" alt="UV-C Play Video" /></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 mt-30 font-size-18 font-raleway">
				Germbuster’s UV-C bulbs have been independently tested that our fixtures kill up to<br /> 
				<span class="font-weight-bold">99.9% of the following pathogens at distances up to 9 feet.</span>
			</div>		
		</div>
		<hr style="border-top:3px solid #707070;" />
		<div class="row uvc-list">
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">SARS-CoV</h4>
				<div class="d1">
					<img src="/images/icon-corona.jpg" />
					<span class="">The Corona family of viruses that includes the one that causes COVID-19 — can live on some of the surfaces you probably touch every day</span>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">Influenza</h4>
				<div class="d1">
					<img src="/images/icon-influenza.jpg" />
					<span class="">Both influenza A and B viruses survived for 24-48 hr on hard, nonporous surfaces 
						such as stainless steel and plastic </span>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">MRSA/Staph</h4>
				<div class="d1">
					<img src="/images/icon-staph.jpg" />
					<span class="">Methicillin-resistant Staphylococcus aureus (MRSA) can survive on some surfaces, like towels, razors, furniture, and athletic equipment for hours, days, or even weeks.   </span>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="bg-teal uvdd-gz auto-c pt-60 position-relative">
	<div class="bg-white-filler"></div>
	<div class="container">
		<div class="row">			
			<div class="col-md-6 text-white">
				<!-- <h3 class="font-raleway font-weight-bold my-40 d-inline-block">AutoCharge as it cleans</h3> -->
				<p class="line-height-1-5 font-raleway font-weight-bold font-size-20" style="padding-top: 30%;">
				The Germ-Zapper 300&trade; can be used for 
				hospital-grade disinfection of surfaces in schools,  
				homes, commercial facilities, nursing homes,  
				hospitals, etc.  With 300 watts The Germ Zapper 300&trade;
				is the most powerful hand-held UVC disinfection
				machine on the market. 
				</p>
			</div>
			<div class="col-md-6 p-0">
				<img src="/images/device.png" alt="device disinfection" style="max-width: 400px;" />
			</div>
		</div>
	</div>
</div>
			

<div id="GetQuote" class="bg-white py-50 contact-f">
	<div class="container">
		<div class="row">

			<div class="main-title text-center">
				<h2 class="my-30 font-size-40">Learn more about the Germ-Zapper 300™ <br>hand-held UVC disinfectant device</h2>
				<p class="line-height-1-2">
					Step into the next generation of UV-C disinfection  technology? Please provide your contact details <Br />
					and a GermBuster account specialist will help you take the first step in discovering the power of UV-C.  <br />
					Or you can call 954-761-2480 to talk to an expert now.
				</p>
			</div>

			@include('partials.contact_form')

		</div>
	</div>
</div>

@endsection
@section('footer_scripts')

<script src="/js/library.js" type="text/javascript" ></script>
<script type="text/javascript" >
jQuery(document).ready(function($) {

	jQuery('.fancybox').fancybox({
		padding: 0
	});

	jQuery('.fancybox-media').fancybox({
				padding:0,
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					overlay:{ 
						css: {background:'rgba(0,0,0,0.75)', 'z-index':99998}
					},
					media : {},
					padding:0
				}
			});

   

});
</script>
@endsection

