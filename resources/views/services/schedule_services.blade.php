@extends('layouts.master')

@section('content')
	<link href="/css/services.css" rel="stylesheet" type="text/css">

 	<!--Start Banner-->   
   <div class="sub-banner">  
  		<img class="banner-img" src="/images/sub-banner.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
   </div>	   
   <!--End Banner-->

	<!--Start Loader-->
	<div id="loader" class="loader" style="display: none;" >
    	<img src="/images/loading.gif" alt="" />
	</div>
	<!--End Banner-->

  	<div class="content">
        <div class="container">
			<div class="section-container">
				<div class="col">
					<ul class="progresstracker">
						<li class="progresstracker__step is-complete" id="progresstracker_select_services">
							<span class="progresstracker__link outline-text">
								<span>
									<span class="mobilehide">Modify</span> service
								</span>
							</span>
						</li>
						<li class="progresstracker__step active is-complete" id="progresstracker_scheduling">
							<span>Sched<span class="mobilehide">uling</span></span>
						</li>
						<li class="progresstracker__step" id="progresstracker_guest_information">
							<span>Your Info<span class="mobilehide">rmation</span>
							</span>
						</li>
						<li class="progresstracker__step" id="progresstracker_review_order">
							<span>Review
								<span class="mobilehide"> Your Order</span>
							</span>
						</li>
					</ul>
				</div>
			</div>
				
			<div class="row" id="services-main">

			<div class="col-md-9">

				<script src="//simplybook.me/v2/widget/widget.js"></script>
				<script>

					var widget = new SimplybookWidget({"widget_type":"iframe", "url":"https:\/\/germbusters.simplybook.me","theme":"default","theme_settings":{"timeline_show_end_time":"1","timeline_modern_display":"as_slots","sb_base_color":"#5EC100","display_item_mode":"block","booking_nav_bg_color":"#02adc6","body_bg_color":"#ffffff","dark_font_color":"#494949","light_font_color":"#ffffff","btn_color_1":"#5e7da7","sb_company_label_color":"#ffffff","hide_img_mode":"1","show_sidebar":"1","sb_busy":"#707070","sb_available":"#f3f3f3"},"timeline":"classes_plugin","datepicker":"inline_datepicker","is_rtl":false,"app_config":{"predefined":[]}});
				</script>
<!-- 
				<script src="//simplybook.me/v2/widget/widget.js"></script>
				<script>

					var widget = new SimplybookWidget({"widget_type":"iframe", "url":"https:\/\/germbusters.simplybook.me","theme":"default","theme_settings":{"timeline_show_end_time":"0","timeline_modern_display":"as_slots","sb_base_color":"#02adc6","display_item_mode":"block","booking_nav_bg_color":"#02adc6","body_bg_color":"#ffffff","dark_font_color":"#494949","light_font_color":"#ffffff","btn_color_1":"#5e7da7","sb_company_label_color":"#ffffff","hide_img_mode":"1","show_sidebar":"1","sb_busy":"#c4c4c4","sb_available":"#f3f3f3"},"timeline":"modern","datepicker":"top_calendar","is_rtl":false,"app_config":{"predefined":[]}});
				</script> -->

				<!-- <script src="//simplybook.me/v2/widget/widget.js"></script>
				<script>var widget = new SimplybookWidget({"widget_type":"iframe","url":"https:\/\/germbusters.simplybook.me","theme":"default","theme_settings":{"timeline_show_end_time":"0","timeline_modern_display":"as_slots","sb_base_color":"#33bb60","display_item_mode":"block","booking_nav_bg_color":"#d1e9c6","body_bg_color":"#f7f7f7","dark_font_color":"#494949","light_font_color":"#ffffff","btn_color_1":"#5e7da7","sb_company_label_color":"#ffffff","hide_img_mode":"0","show_sidebar":"1","sb_busy":"#dad2ce","sb_available":"#d3e0f1"},"timeline":"modern","datepicker":"top_calendar","is_rtl":false,"app_config":{"predefined": {
							       "client": {
							         "name": "{{ Auth::check() ? (ucfirst(Auth::user()->firstname). ' '. ucfirst(Auth::user()->lastname)) : '' }}",
							        "email": "{{ Auth::check() ? Auth::user()->email : '' }}",
							        "phone": "{{ Auth::check() ? Auth::user()->phone : '' }}"
							    }
						}
					}});</script> -->

			</div>


			<div class="col-md-3 mt-20">
				@php ($subtotal = floatval($items['total']['subtotal']))

				<div class="cart cart--quote cart--quote--default desktop mb-20 {{ $subtotal <= 0 ? '': 'hide' }}">	
					<div class="cart__header">
						<h3 class="cart__title h5">Your Quote</h3>
					</div>
					<div class="cart__footer">
						<div class="cart__wrap">
							<span class="cart__label">Estimated<br>Total:</span>
							<span class="cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</span>
						</div>
					</div>
				</div>
				<div class="cart cart--quote cart--quote--estimated desktop {{ $subtotal > 0 ? '': 'hide' }}">
					<div class="cart__header">
						<h3 class="cart__title h5">Your Quote</h3>
					</div>
					<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 99 ? 'none': '')  : 'none' }};">
						<div class="cart__wrap">
							<p class="cart__para">
								<span class="icn icn--warning">&nbsp;</span>
								Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$99.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
							</p>
						</div>
					</div>
					<div class="cart__body">
						<div><!---->
							<div class="cart__item-group"><h4 class="cart__item-title h5">DISINFECT</h4>
								<ul class="cart__list">
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Electrostatic Spray</span>
											<span class="cart__price">
												<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
												&nbsp;${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">UVC Disinfect</span>
											<span class="cart__price">
												<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
												&nbsp;${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Wipe Service</span>
											<span class="cart__price">
												<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
												&nbsp;${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">APT Test</span>
											<span class="cart__price">
												<span class="cart__quantity api__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
												&nbsp;${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }} 
											</span>
										</div>
									</li>
								</ul>
								<div class="cart__subtotal" data-subtotal="0">
									<div class="cart__wrap"><span class="cart__label">Subtotal</span>
										<span class="cart__price">
											${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
										</span>
									</div>
								</div>
								<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
									<div class="cart__wrap"><span class="cart__label">Promo</span>
										<span class="cart__price cart__discount">
											- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							</div>
							<!--
								<div class="cart__info" data-min="25">
									<div class="cart__wrap">
										<span class="cart__label">Job Minimum:</span>
										<span class="cart__price">
											${{ number_format(floatval($items['jobmin']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							-->
							<div class="cart__footer">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total cart_final__total">
										${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
									</span>
								</div>
							</div>
							
						</div>
					
					</div>
				</div>

				<div class="promo-area promo-area--mobile hide-med-screen">
				<div class="form__fieldset">
					<div class="form__field">
						<h3 class="promo-area-header h5">Promo Code</h3>
						<div class="row row--2 promo-row">
							<div class="col col--2-3">
							<div class="mobile"><label class="visuallyhidden up-to-medium-only" for="inputPromoCodeMobile">Enter Promo Code</label>
								<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeMobile" name="PromoCode" placeholder="Enter Promo Code" type="text" value="{{ $items['total']['promocode'] }}">
								<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
							</div>
								<div class="desktop">
									<label class="visuallyhidden hide-med-screen" for="inputPromoCodeDesktop">Enter Promo Code</label>
									<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeDesktop" name="PromoCode" placeholder="Enter Code" type="text" value="{{ $items['total']['promocode'] }}">
									<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
								</div>
							</div>
							<span class="col col--1-3">
								<button class="btn btn-caret" id="apply_promo_code">
									<span class="hide-med-screen">Apply</span>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="promo-area hide-med-screen">
				<div class="expand-content">
					<p class="promo-area-subheader">To get the most accurate estimate check all that apply.</p>
				</div>
				<div class="form__filter">
					@php ($prid = '5ece4797eaf5e')
			    	@php ($item = [])
			     	@foreach($items['parking'] as $key => $data)  
                       	@if($data['id'] == $prid)
                       		@php ($item = $data)
      						@break
      					@endif
                   	@endforeach			
                   			
					<div class="form__field">
						<div class="other-services other-services-parking styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkParkingNotNearby" data-id="{{ $prid }}" name="parkingNearby" data-type="parking">
				      	 	<i class="fas fa-check"></i>
				      	 </div>
						<label for="chkParkingNotNearby">{{ $item['name'] }}</label>
					</div>

					@php ($prid = '5ed4ea1f9eeb1')
			    	@php ($item = [])
			     	@foreach($items['parking'] as $key => $data)  
                       	@if($data['id'] == $prid)
                       		@php ($item = $data)
      						@break
      					@endif
                   	@endforeach       

					<div class="form__field">
						<div class="other-services other-services-area styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkThirdFloor" data-id="{{ $prid }}" name="thirdFloor" data-type="parking">
				      	 	<i class="fas fa-check"></i>
				      	</div>
						<label for="chkThirdFloor">{{ $item['name'] }}</label>
					</div>

					@php ($prid = '5ed4e71823c17')
			    	@php ($item = [])
			     	@foreach($items['parking'] as $key => $data)  
                       	@if($data['id'] == $prid)
                       		@php ($item = $data)
      						@break
      					@endif
                   	@endforeach

					<div class="form__field">
						<div class="other-services other-services-driveway styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}"  id="chkGuaranteedParking" data-id="{{ $prid }}" name="guaranteedParking" data-type="parking">
			      	 		<i class="fas fa-check"></i>
			      		</div>
						<label for="chkGuaranteedParking">{{ $item['name'] }}</label>
					</div>
				</div>
				<div class="expand-content"><!----><small><em>(If we need to park across or down the street, or clean above the 2nd floor, portable equipment may be required.)</em></small><!----></div>
			</div>
		</div>

		</div>

		</div>
</div>



<div class="pagination pagination--sticky group" id="bottom-bar">
	<!-- <app-quote-sidebar-mobile><div class="up-to-medium-only">
		<div class="cart cart__mobile cart--quote cart__collapsed" id="cart-container">
			<div class="cart__header cart__mobile-wrap">
				<h4 class="cart__title h5 visuallyhidden">Your Quote</h4>
				<div>
					<a class="cart__edit" id="clear_cart">Clear</a>
				</div>
				</div><div class="cart__body cart--slide cart__closed"></div>
				<div class="cart__footer"><div class="cart__mobile-wrap cart__total">
					<div class="cart__mobile-label">Estimated Total</div>
					<div class="cart__mobile-price">$121.75</div></div>
				</div>
				</div>
				</div>
	</app-quote-sidebar-mobile> -->

	<app-quote-sidebar-mobile>
				
		<div class="cart cart--quote cart--quote--estimated desktop mb-20 p-0" style="display: none;">
					<div class="cart__header p-20">
						<h3 class="cart__title h5">Your Quote</h3>
						<!-- <a class="cart__edit" id="clear_cart" tabindex="0">Clear</a> -->
					</div>
					<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 399 ? 'none': '')  : 'none' }};">
						<div class="cart__wrap">
							<p class="cart__para" style="padding: 20px 15px 0px;">
								<span class="icn icn--warning">&nbsp;</span>
								Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$399.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
							</p>
						</div>
					</div>
					<div class="cart__body">
						<div><!---->
							<div class="cart__item-group m-20"><h4 class="cart__item-title h5">DISINFECT</h4>
								<ul class="cart__list">
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Electrostatic Spray</span>
											<span class="cart__price">
												<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
												&nbsp;
												<span class="disinfect__total">
													${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">UVC Disinfect</span>
											<span class="cart__price">
												<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
												&nbsp;
												<span class="uvc__total">
													${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Wipe Service</span>
											<span class="cart__price">
												<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
												&nbsp;
												<span class="wipe__total">
													${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">APT Test</span>
											<span class="cart__price">
												<span class="cart__quantity apt__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
												&nbsp;
												<span class="apt__total">
													${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
								</ul>
								<div class="cart__subtotal">
									<div class="cart__wrap"><span class="cart__label">Subtotal</span>
										<span class="cart__price">
											${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
										</span>
									</div>
								</div>
								<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
									<div class="cart__wrap"><span class="cart__label">Promo</span>
										<span class="cart__price cart__discount">
											- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							</div>
							
							<div class="cart__footer hide">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total cart_final__total">
										${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
									</span>
								</div>
							</div>

						</div>
					</div>
		</div>

		<div class="up-to-medium-only">
			<div class="cart cart__mobile cart--quote cart__collapsed" id="cart-container">
				<div class="cart__header cart__mobile-wrap">
					<h4 class="cart__title h5 visuallyhidden">Your Quote</h4>
					<!-- <div>
						<a class="cart__edit" id="clear_cart">Clear</a>
					</div> -->
					</div><div class="cart__body cart--slide cart__closed"></div>
					<div class="cart__footer">
						<div class="cart__mobile-wrap py-10">
						<div class="cart__mobile-label">Estimated Total</div>
						<div class="cart__mobile-price cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</div></div>
					</div>
			</div>
		</div>
	</app-quote-sidebar-mobile>
	

	<div class="cart__bottom-bar-footer">
		<div class="cart__openclose--fixed cart__close">
		<div class="cart__openclose-wrap"><div class="cart__openclose cart__close">
			<span class="visuallyhidden">&nbsp;</span>
		</div>
		</div>
		</div>
	</div>
	
	<div class="section-container bottom-bar"><div class="section__page-item d-flex justify-start"><a href="#" onClick="history.go(-1); return false;" class="mt-10 text-dark" id="previous_page" tabindex="0" style="cursor:pointer;"><img src="/images/Icon material-backspace.png" class="mr-10 width-auto" alt="back">BACK</a></div>
		<div class="section__page-item d-flex justify-end">
			<button class="btn btn-alt btn-bottom-bar-continue" id="next_step" tabindex="0"> CONTINUE </button>
		</div>
	</div>
</div>

@endsection


@section('footer_scripts')
	<script type="text/javascript" src="/js/accounting.min.js"></script>
 	<script src="/js/pages/cleaning.quote.service.js"></script>
@endsection