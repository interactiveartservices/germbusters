@extends('layouts.master')

@section('content')
<link href="/css/services.css" rel="stylesheet" type="text/css">

<!--Start Banner-->   
<div class="sub-banner">  
	<img class="banner-img" src="/images/sub-banner.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
</div>	   
<!--End Banner-->

<div class="content">
   
  	<div class="container login">
			
		<div class="section-container">
			<div class="col">
				<ul class="progresstracker">
					<li class="progresstracker__step is-complete" id="progresstracker_select_services">
						<span class="progresstracker__link outline-text">
							<span>
								<span class="mobilehide">Modify</span> service
							</span>
						</span>
					</li>
					<li class="progresstracker__step is-complete" id="progresstracker_scheduling">
						<span>Sched<span class="mobilehide">uling</span></span>
					</li>
					<li class="progresstracker__step is-complete" id="progresstracker_guest_information">
						<span>Your Info<span class="mobilehide">rmation</span>
						</span>
					</li>
					<li class="progresstracker__step active is-complete" id="progresstracker_review_order">
						<span>Review
							<span class="mobilehide"> Your Order</span>
						</span>
					</li>
				</ul>
			</div>
		</div>	
			<div class="row" id="services-main">
				<div class="col-md-9">
					<div class="main">
						<form id="form-review" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/cleaning-quote/update_order') }}">
				       		{{ csrf_field() }}

				       		<input type="hidden" name="action" value="save-information">
				      	</form>

		            	<p class="medium-title">REVIEW YOUR INFORMATION</p>
		            	<p class="txt-small">
		            		Please confirm the details of your order below.  your order is not complete until you agree to our <a href="/terms-conditions" class="font-weight-bold" target="_blank">Terms and Conditions </a> and <a href="/privacy-policy" class="font-weight-bold" target="_blank">Privacy Policy</a>. Click place order and receive your confirmation number.
		            	</p>
						<input id="terms" type="checkbox">&nbsp;&nbsp;<span class="txt-small font-raleway">By checking this box, I agree and concent to Germbusters Terms and Conditions and Privacy Policy. <br> <span class="text-teal font-weight-bold">Please agree to our terms and conditions before placing an order.</span>

						<div class="row info">
							<div class="col-md-8">
								<div class="title">SERVICE INFORMATION <a href="/cleaning-quote/checkout" class="float-right text-underline"><img src="/images/Icon feather-edit-2.png" class="width-auto">  Change</a></div>

								<div class="mt-20">
							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">First Name:</span> {{ $items['buyer']['firstname'] }}</p>
							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">Last Name:</span> {{ $items['buyer']['lastname'] }}</p>

							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">Address:</span>
							 				@if((int)$items['buyer']['addrtype'] == 1)
							 					{{ $items['buyer']['address1'] }}
							 				@else 
							 					{{ $items['buyer']['address2'] }}
							 				@endif
							 		</p>

							 		<!--
								 		<p class="title">Address 2:
								 			<span>{{ $items['buyer']['address2'] }}</span>
								 		</p>
								 	-->

							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">City:</span>
							 			{{ $items['buyer']['city'] }}
							 		</p>
							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">State:</span>
							 			{{ $items['buyer']['state'] }}
							 		</p>
							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">ZIP Code:</span>
							 			{{ $items['buyer']['zipcode'] }}
							 		</p>
							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">Email:</span>
							 			{{ $items['buyer']['email'] }}
							 		</p>
							 		<p class="title"><span class="font-weight-bold w-120 d-inline-block">Phone:</span>
							 			{{ $items['buyer']['phone'] }}
							 		</p>						 	
								</div>

						 		<!-- <div class="title mt-20">APPOINTMENT DATE AND<br> ARRIVAL WINDOW <a href="/cleaning-quote/scheduling" class="float-right"><img src="/images/Icon feather-edit-2.png" class="width-auto">  Change</a></div>
						 		
						 		<div class="mt-20">
						 			<p class="title">Date:<span></span></p>
						 			<p class="title">Arrival Window:<span></span></p>
						 			<p class="title">Service:<span></span></p>
						 		</div>
 -->
						 		<div class="title mt-20">SPECIAL INSTRUCTIONS OR<br> REQUESTS <a href="/cleaning-quote/checkout" class="float-right text-underline"><img src="/images/Icon feather-edit-2.png" class="width-auto">Change</a></div>

						 		<div class="mt-20">{{ $items['buyer']['specialins'] }}</div>
						 	</div>

						 	<div class="col-md-4">
						 		<!-- <div class="box">
						 			<p class="font-weight-bold">MAY WE TEXT YOU?</p>
						 			<input type="checkbox" class="txt-small">  Would you like a text message reminder shortly before your appointment? <br>
						 			<input type="checkbox" class="txt-small">  Would you like to receive offers by text?
						 		</div>
						 		<div class="box mt-20">
						 			<p class="title font-weight-bold"><img src="/images/Icon awesome-map-marker-alt.png" class="width-auto"> GERMBUSTERS</p>
						 			<p class="txt-small line-height-1-2 mt-10"><span class="font-weight-bold" id="ip_loc">GermBuster - Charlotte, NC</span><br>
										<span id="ip_add">2807 Mt Isle Harbor Drive</span><br>
										<span id="ip_phone">704 877 2455</span>
									</p>
						 		</div> -->
						 	</div>
							</div>
						</div>
	        		</div>

	        		<div class="col-md-3 mt-20">
	        			@php ($subtotal = floatval($items['total']['subtotal']))

						<div class="cart cart--quote cart--quote--default desktop mb-20 {{ $subtotal <= 0 ? '': 'hide' }}">	
							<div class="cart__header">
								<h3 class="cart__title h5">Your Quote</h3>
							</div>
							<div class="cart__footer">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</span>
								</div>
							</div>
						</div>
						<div class="cart cart--quote cart--quote--estimated desktop {{ $subtotal > 0 ? '': 'hide' }}"">
							<div class="cart__header">
								<h3 class="cart__title h5">Your Quote</h3>
							</div>
							<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 99 ? 'none': '')  : 'none' }};">
								<div class="cart__wrap">
									<p class="cart__para">
										<span class="icn icn--warning">&nbsp;</span>
										Your order total of <strong><span class="cart__order__total">{{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$99.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
									</p>
								</div>
							</div>
							<div class="cart__body">
								<div><!---->
									<div class="cart__item-group"><h4 class="cart__item-title h5">DISINFECT</h4>
										<ul class="cart__list">
											<li class="cart__item">
												<div class="cart__wrap">
													<span class="cart__label">Electrostatic Spray</span>
													<span class="cart__price">
														<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
														&nbsp;${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
													</span>
												</div>
											</li>
											<li class="cart__item">
												<div class="cart__wrap">
													<span class="cart__label">UVC Disinfect</span>
													<span class="cart__price">
														<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
														&nbsp;${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
													</span>
												</div>
											</li>											
											<li class="cart__item">
												<div class="cart__wrap">
													<span class="cart__label">Wipe Service</span>
													<span class="cart__price">
														<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
														&nbsp;${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
													</span>
												</div>
											</li>
											<li class="cart__item">
												<div class="cart__wrap">
													<span class="cart__label">APT Test</span>
													<span class="cart__price">
														<span class="cart__quantity api__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
														<!-- &nbsp;${{ $items['total']['price']['apttest'] }} -->
														&nbsp;${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }} 
													</span>
												</div>
											</li>
										</ul>
										<div class="cart__subtotal" data-subtotal="0">
											<div class="cart__wrap"><span class="cart__label">Subtotal</span>
												<span class="cart__price">
													${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
												</span>
											</div>
										</div>
										<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
											<div class="cart__wrap"><span class="cart__label">Promo</span>
												<span class="cart__price cart__discount">
													- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
												</span>
											</div>
										</div>
									</div>
									<!--
										<div class="cart__info" data-min="25">
											<div class="cart__wrap">
												<span class="cart__label">Job Minimum:</span>
												<span class="cart__price">
													${{ number_format(floatval($items['jobmin']), 2, '.', ',') }}
												</span>
											</div>
										</div>
									-->
									<div class="cart__footer">
										<div class="cart__wrap">
											<span class="cart__label">Estimated<br>Total:</span>
											<span class="cart__total cart_final__total">
												${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
											</span>
										</div>
									</div>
									
								</div>
							
							</div>
						</div>
						<div class="promo-area promo-area--mobile hide-med-screen">
							<div class="form__fieldset">
							<div class="form__field">
								<h3 class="promo-area-header h5">Promo Code</h3>
								<div class="row row--2 promo-row">
									<div class="col col--2-3">
									<div class="mobile"><label class="visuallyhidden up-to-medium-only" for="inputPromoCodeMobile">Enter Promo Code</label>
										<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeMobile" name="PromoCode" placeholder="Enter Promo Code" type="text" value="{{ $items['total']['promocode'] }}">
										<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
									</div>
										<div class="desktop">
											<label class="visuallyhidden hide-med-screen" for="inputPromoCodeDesktop">Enter Promo Code</label>
											<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeDesktop" name="PromoCode" placeholder="Enter Code" type="text" value="{{ $items['total']['promocode'] }}">
											<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
										</div>
									</div>
									<span class="col col--1-3">
										<button class="btn btn-caret" id="apply_promo_code">
											<span class="hide-med-screen">Apply</span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="promo-area hide-med-screen">
						<div class="expand-content">
							<p class="promo-area-subheader">To get the most accurate estimate check all that apply.</p>
						</div>
						<div class="form__filter">
							@php ($prid = '5ece4797eaf5e')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach		

							<div class="form__field">
								<div class="other-services other-services-parking styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkParkingNotNearby" name="parkingNearby" style="pointer-events: none;">
						      	 	<i class="fas fa-check"></i>
						      	 </div>
								<label for="chkParkingNotNearby">{{ $item['name'] }}</label>
							</div>

							@php ($prid = '5ed4ea1f9eeb1')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach    

							<div class="form__field">
								<div class="other-services other-services-area styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkThirdFloor" name="thirdFloor" style="pointer-events: none;">
						      	 	<i class="fas fa-check"></i>
						      	</div>
								<label for="chkThirdFloor">{{ $item['name'] }}</label>
							</div>

							@php ($prid = '5ed4e71823c17')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

							<div class="form__field">
								<div class="other-services other-services-driveway styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}"  id="chkGuaranteedParking" name="guaranteedParking" style="pointer-events: none;">
					      	 		<i class="fas fa-check"></i>
					      		</div>
								<label for="chkGuaranteedParking">{{ $item['name'] }}</label>
							</div>
						</div>
						<div class="expand-content">
							<!----><small><em>(If we need to park across or down the street, or clean above the 2nd floor, portable equipment may be required.)</em></small><!---->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="pagination pagination--sticky group" id="bottom-bar">

	<app-quote-sidebar-mobile>
				
		<div class="cart cart--quote cart--quote--estimated desktop mb-20 p-0" style="display: none;">
					<div class="cart__header p-20">
						<h3 class="cart__title h5">Your Quote</h3>
						<!-- <a class="cart__edit" id="clear_cart" tabindex="0">Clear</a> -->
					</div>
					<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 399 ? 'none': '')  : 'none' }};">
						<div class="cart__wrap">
							<p class="cart__para" style="padding: 20px 15px 0px;">
								<span class="icn icn--warning">&nbsp;</span>
								Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$399.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
							</p>
						</div>
					</div>
					<div class="cart__body">
						<div><!---->
							<div class="cart__item-group m-20"><h4 class="cart__item-title h5">DISINFECT</h4>
								<ul class="cart__list">
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Electrostatic Spray</span>
											<span class="cart__price">
												<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
												&nbsp;
												<span class="disinfect__total">
													${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">UVC Disinfect</span>
											<span class="cart__price">
												<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
												&nbsp;
												<span class="uvc__total">
													${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Wipe Service</span>
											<span class="cart__price">
												<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
												&nbsp;
												<span class="wipe__total">
													${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">APT Test</span>
											<span class="cart__price">
												<span class="cart__quantity apt__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
												&nbsp;
												<span class="apt__total">
													${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
								</ul>
								<div class="cart__subtotal">
									<div class="cart__wrap"><span class="cart__label">Subtotal</span>
										<span class="cart__price">
											${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
										</span>
									</div>
								</div>
								<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
									<div class="cart__wrap"><span class="cart__label">Promo</span>
										<span class="cart__price cart__discount">
											- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							</div>
							
							<div class="cart__footer hide">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total cart_final__total">
										${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
									</span>
								</div>
							</div>

						</div>
					</div>
		</div>

		<div class="up-to-medium-only">
			<div class="cart cart__mobile cart--quote cart__collapsed" id="cart-container">
				<div class="cart__header cart__mobile-wrap">
					<h4 class="cart__title h5 visuallyhidden">Your Quote</h4>
					<!-- <div>
						<a class="cart__edit" id="clear_cart">Clear</a>
					</div> -->
					</div><div class="cart__body cart--slide cart__closed"></div>
					<div class="cart__footer">
						<div class="cart__mobile-wrap py-10">
						<div class="cart__mobile-label">Estimated Total</div>
						<div class="cart__mobile-price cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</div></div>
					</div>
			</div>
		</div>
	</app-quote-sidebar-mobile>

	<div class="cart__bottom-bar-footer">
		<div class="cart__openclose--fixed cart__close">
		<div class="cart__openclose-wrap"><div class="cart__openclose cart__close">
			<span class="visuallyhidden">&nbsp;</span>
		</div>
		</div>
		</div>
	</div> 
	
	<div class="section-container bottom-bar"><div class="section__page-item d-flex justify-start"><a href="#" onClick="history.go(-1); return false;" class="mt-10 text-dark" id="previous_page" tabindex="0" style="cursor:pointer;"><img src="/images/Icon material-backspace.png" class="mr-10 width-auto" alt="back">BACK</a></div>
		<div class="section__page-item d-flex justify-end"><button class="btn btn-alt btn-bottom-bar-continue" id="next_step" tabindex="0" disabled> CONTINUE </button></div>
	</div>
</div>

@endsection


@section('footer_scripts')
	<script type="text/javascript" src="/js/accounting.min.js"></script>
	<script src="/js/pages/cleaning.quote.service.js"></script>

	<script type="text/javascript">
		$('#review_order').on('click', '.btn-alt', function(e) {
			var $form = $('#form-review');

			$form.get(0).submit();
		});

		$("#terms").click(function(e) {
			if($(this).is(":checked")) {
				$("#next_step").removeAttr("disabled");
			} else {
				$("#next_step").attr("disabled", true);
			}
		})
	</script>
@endsection