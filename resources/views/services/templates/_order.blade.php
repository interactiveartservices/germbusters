<b>Contact Firstname: </b>{{ $order['buyer']['firstname'] }}
<br />
<b>Contact Lastname: </b>{{ $order['buyer']['lastname'] }}
<br />
@if((int)$order['buyer']['addrtype'] == 1)
	<b>Address: </b>{{ $order['buyer']['address1'] }}
@else
	<b>Address: </b>{{ $order['buyer']['address2'] }}
@endif

<br />
<b>City: </b>{{ $order['buyer']['city'] }}
<br />
<b>State: </b>{{ $order['buyer']['state'] }}
<br />
<b>Zip Code: </b>{{ $order['buyer']['zipcode'] }}
<br />
<b>Email: </b>{{ $order['buyer']['email'] }}
<br />
<b>Phone: </b>{{ $order['buyer']['phone'] }}
<br /><br />
<b>SPECIAL INSTRUCTIONS OR REQUESTS:</b>
<br />
{{ $order['buyer']['specialins'] }}
<br />
<br/>
<table>
	<thead style="background-color: #ddd;">
		<th style="width: 250px;">SERVICE</th>
		<th style="width: 100px;text-align: right;">QTY</th>
		<th style="width: 100px;text-align: right;">PRICE</th>
	</thead>
	<tbody>
		@if((int)$order['total']['count']['disinfect'] > 0)
			<tr>
				<td>Electrostatic Spray</td>
				<td style="text-align: right;">{{ $order['total']['count']['disinfect'] }}</td>
				<td style="text-align: right;">${{ number_format(floatval($order['total']['price']['disinfect']), 2, '.', ',') }}</td>
			</tr>
		@endif
		
		@if((int)$order['total']['count']['uvc'] > 0)
			<tr>
				<td>UVC Disinfect</td>
				<td style="text-align: right;">{{ $order['total']['count']['uvc'] }}</td>
				<td style="text-align: right;">${{ number_format(floatval($order['total']['price']['uvc']), 2, '.', ',') }}</td>
			</tr>
		@endif

		@if((int)$order['total']['count']['wipe'] > 0)
			<tr>
				<td>Wipe Service</td>
				<td style="text-align: right;">{{ $order['total']['count']['wipe'] }}</td>
				<td style="text-align: right;">${{ number_format(floatval($order['total']['price']['wipe']), 2, '.', ',') }}</td>
			</tr>
		@endif
		
		@if((int)$order['total']['count']['apttest'] > 0)
			<tr>
				<td>APT Test</td>
				<td style="text-align: right;">{{ $order['total']['count']['apttest'] }}</td>
				<td style="text-align: right;">${{ number_format(floatval($order['total']['price']['apttest']), 2, '.', ',') }}</td>
			</tr>
		@endif
	

		<!--
			<tr>
				<td>JOB MINIMUM</td>
				<td style="text-align: right;"></td>
				<td style="text-align: right;">${{ number_format(floatval($order['jobmin']), 2, '.', ',') }}</td>
			</tr>
		-->

		@php ($prid = '5ece4797eaf5e')
    	@php ($item = [])
		@foreach($order['parking'] as $key => $data)  
			@if($data['id'] == $prid)
           		@php ($item = $data)
					@break
				@endif
       	@endforeach	

		@if((int)$item['ischeck'] == 1)
			<tr>
				<td>No Parking Nearby</td>
				<td style="text-align: right;"></td>
				<td style="text-align: right;">${{ number_format(floatval($item['price']), 2, '.', ',') }}</td>
			</tr>
		@endif

		@php ($prid = '5ed4ea1f9eeb1')
    	@php ($item = [])
		@foreach($order['parking'] as $key => $data)  
			@if($data['id'] == $prid)
           		@php ($item = $data)
					@break
				@endif
       	@endforeach	

		@if((int)$item['ischeck'] == 1)
			<tr>
				<td>Area is on 3rd Floor or Higher</td>
				<td style="text-align: right;"></td>
				<td style="text-align: right;">${{ number_format(floatval($item['price']), 2, '.', ',') }}</td>
			</tr>
		@endif

		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			@php($totalqty = (int)$order['total']['count']['disinfect'] + (int)$order['total']['count']['uvc'] + (int)$order['total']['count']['wipe'] + (int)$order['total']['count']['apttest'])

			<td>SUBTOTAL</td>
			<td style="text-align: right;"><!-- {{ $totalqty }} --></td>
			<td style="text-align: right;">${{ number_format(floatval($order['total']['subtotal']), 2, '.', ',') }}</td>
		</tr>
		
		<tr>
			<td>DISCOUNT</td>
			<td style="text-align: right;"></td>
			<td style="text-align: right;">- ${{ number_format(floatval($order['total']['discount']), 2, '.', ',') }}</td>
		</tr>
		<tr>
			<td><b>TOTAL</b></td>
			<td></td>
			<td style="text-align: right;"><b>${{ number_format(floatval($order['total']['estimated']), 2, '.', ',') }}</b></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>PROMO CODE:</td>
			<td style="text-align: right;"></td>
			<td style="text-align: right;">{{ $order['total']['promocode'] }}</td>
		</tr>
	</tbody>
</table>

<!-- <br />
Date: <b>{{ date('Y-m-d H:i:s') }}</b> -->




