@extends('layouts.master')

@section('content')
	<link href="/css/services.css" rel="stylesheet" type="text/css">
	<link href="/css/auth/signup.css" rel="stylesheet" type="text/css">

	<!--Start Banner-->   
	<div class="sub-banner">  
		<img class="banner-img" src="/images/sub-banner.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
	</div>	   
	<!--End Banner-->

	<!--Start Loader-->
	<div id="loader" class="loader" style="display: none;" >
    	<img src="/images/loading.gif" alt="" />
	</div>
	<!--End Banner-->

	<div class="content">
	  	<div class="container login">
			<div class="section-container">
				<div class="col">
					<ul class="progresstracker">
						<li class="progresstracker__step is-complete" id="progresstracker_select_services">
							<span class="progresstracker__link outline-text">
								<span>
									<span class="mobilehide">Modify</span> service
								</span>
							</span>
						</li>
						<li class="progresstracker__step is-complete" id="progresstracker_scheduling">
							<span>Sched<span class="mobilehide">uling</span></span>
						</li>
						<li class="progresstracker__step active is-complete" id="progresstracker_guest_information">
							<span>Your Info<span class="mobilehide">rmation</span>
							</span>
						</li>
						<li class="progresstracker__step" id="progresstracker_review_order">
							<span>Review
								<span class="mobilehide"> Your Order</span>
							</span>
						</li>
					</ul>
				</div>
			</div>	

			<div class="row" id="services-main">
				<div class="col-md-9 pr-40">
					@if(Auth::guest())
						<div id="main" class="main">
			            	<div class="row">
				                <div class="col-sm-6">
				                    <p class="medium-title">ALREADY HAVE AN ACCOUNT?</p>

				                    <form id="login-form" class="form" role="form" method="POST" action="{{ url('/login') }}">
				                        {{ csrf_field() }}

				                    	<input type="hidden" name="cart" value="1">

				                        <div class="form-row form-row-wide">
				                            <input id="email" name="email" class="input-text" type="email" placeholder="Email Address" autocomplete="email" value="{{ old('email') }}" tabindex="1" required autofocus />
				                            @if ($errors->has('email'))
				                                <span class="help-block">
				                                    <strong>{{ $errors->first('email') }}</strong>
				                                </span>
				                            @endif
				                        </div>

				                        <div class="form-row form-row-wide">
				                            <input id="password" type="password" class="input-text" name="password" placeholder="Password" autocomplete="password" required>
				                            @if ($errors->has('password'))
				                                <span class="help-block">
				                                    <strong>{{ $errors->first('password') }}</strong>
				                                </span>
				                            @endif
				                        </div>

				                        <div>
				                            <a href="#" class="forgot-password text-dark font-size-14 text-underline">Forgot your password?</a>
				                            <button type="submit" class="btn signin" value="Sign in">Sign in</button>
				                        </div>
				                    </form>
				                </div>

				                <div class="col-sm-6 right-side">
				                    <p class="medium-title">CREATE AN ACCOUNT (OPTIONAL)</p>
				                    <p>Save time when scheduling future cleanings by creating an account</p>
				                    <button id="create-account" class="btn mt-20 w-100">Create account</button>
				                </div>
			            	</div>

				            <div class="social-logins text-center">
				                <hr class="separator add-or mt-20">
				                <a class="btn-social" href="{{ url('/auth/facebook/1') }}">
				                    <img src="/images/btn-fb.png" alt="google login" />
				                </a>
				                <a class="btn-social" href="{{ url('/auth/twitter/1') }}">
				                    <img src="/images/btn-twitter.png" alt="google login" />
				                </a>
				                 <a class="btn-social" href="{{ url('/auth/google/1') }}">
				                    <img src="/images/btn-google.png" alt="google login" />
				                </a>
				            </div>
		        		</div>
		        	@endif

	        		@if(Auth::guest())
		        		<div id="guest-acct" class="form guest-acct ">
        					<form id="form-guest" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/cleaning-quote/update_order') }}">
				                {{ csrf_field() }}

				                <input type="hidden" name="action" value="guest-information">

				                <div class="">
				                    <p class="medium-title">OR CONTINUE AS GUEST</p>
				                  	<p class="mt-20">Please enter your information into the form below.</p>
				                </div>
				                 
				                <div class="mt-20">
				                    <div class="row">
				                        <div class="col-md-6 required">
				                            <label>First name*</label>
				                            <input name="fname" type="text" data-delay="300" class="input">
				                        </div>
				                        <div class="col-md-6">
				                            <label>Last name*</label>
				                            <input name="lname" type="text" data-delay="300" class="input">
				                        </div>
				                    </div>

				                    <div class="row">
				                        <div class="col-md-6 required">
				                            <label>Email Address*</label>
				                            <input id="email" name="email" type="text" data-delay="300"  class="input">
				                            <div class="message">&nbsp;</div>
				                        </div>
	
			                         	<div class="col-md-3 required">
			                            	<label>Phone*</label>
                            				<input id="phone" name="phone" type="text" data-delay="300"  class="input">
			                            </div>
				                        <div class="col-md-3">
				                            <label>Alternative Phone</label>
                            				<input id="altphone" name="altphone" type="text" data-delay="300" class="input">
				                        </div>
    
				                    </div>

				                    <div class="row">
				                        <div class="col-md-4 required">
				                            <label>Zip Code*</label>
                           				 	<input id="zipcode" name="zipcode" type="text" data-delay="300" class="input">
				                        </div>
				                        <div class="col-md-6 required">
				                            <label>City*</label>
                            				<input id="city" name="city" type="text" data-delay="300" class="input">
				                        </div>

				                        <div class="col-md-2 required">
				                            <label>State*</label>
                            				<input id="state" name="state" type="text" data-delay="300" class="input">
				                        </div>
				                    </div>

				                    <div class="row">
				                        <div class="col-md-6 required">
				                            <label>Address 1*</label>
                            				<input id="address1" name="address1" type="text" data-delay="300" class="input">
				                        </div>
				                        <div class="col-md-6">
				                            <label>Address 2</label>
                            				<input id="address2" name="address2" type="text" data-delay="300" class="input">
				                        </div>
				                    </div>
				                  
				                    <div class="row">
				                    	<div class="col-md-12">
											<label>Special instructions or Requests</label>
											<textarea data-delay="500" class="input required valid" name="special_instructions" id="special_instructions">{{ $items['buyer']['specialins'] }}</textarea>
										</div>
									</div>

									 <div class="row">
				                    	<div class="col-md-12">
				                    		<div class="styledCheckbox">
					                            <i class="fas fa-check"></i>
					                         </div>
					                         <label class="font-weight-medium">
					                         	Sign up to receive occasional emails <br class="show-mobile"> with promotional offers for future offerings.
					                         </label>
					                         <input id="offers" name="offers" type="hidden" value="0">
										</div>
									</div>
				                </div>
				            </form>
				        </div>
				    @endif

		           	@if(Auth::check())
		           		<form id="form-information" class="form" role="form" method="POST" action="{{ url('/cleaning-quote/update_order') }}">
		           			{{ csrf_field() }}

		           			<input type="hidden" name="action" value="your-information">
		           			<input type="hidden" name="type" value="">

			                <div class="row main-title mb-0">
			                    <h2><span>YOUR</span> INFORMATION</h2>
			                    <hr class="hr-teal">

			                    <div class="form">
									<div class="main">
		            					<p class="medium-title text-uppercase mb-20">WELCOME BACK, {{ Auth::user()->firstname }}</p>
		            					<p> Choose the address for your disinfection cleaning</p>
			                            <select id="select_addresses" name="address" class="mt-20">
			                                <option value="">Please choose one</option>
			                                
			                                @foreach($addresses as $address)
			                                	@if(((int)$address->id == (int)$items['buyer']['addrid']) && ((int)$items['buyer']['addrtype']) == 1)
				                                	<option value="{{ $address->id }}" data-addr="1" selected>
				                                		{{ $address->address1. ' '. $address->city. ', '. $address->state. ' '. $address->zipcode }}
				                                	</option>
				                                @else
				                                	<option value="{{ $address->id }}" data-addr="1">
				                                		{{ $address->address1. ' '. $address->city. ', '. $address->state. ' '. $address->zipcode }}
				                                	</option>
				                                @endif

			                                	@if(trim($address->address2) != '')
			                                		@if((int)$address->id == (int)$items['buyer']['addrid'] && ((int)$items['buyer']['addrtype']) == 2)
				                                		<option value="{{ $address->id }}" data-addr="2" selected> 
				                                			{{ $address->address2. ' '. $address->city. ', '. $address->state. ' '. $address->zipcode }}
				                                		</option>
				                                	@else
				                            			<option value="{{ $address->id }}" data-addr="2">
				                                			{{ $address->address2. ' '. $address->city. ', '. $address->state. ' '. $address->zipcode }}
				                                		</option>
				                                	@endif
			                                	@endif
			                                @endforeach
			                            </select>

			                            <p id="new-address" class="mt-20 text-underline cursor-pointer">Don’t See Your Address?</p>
			                            <p id="enteraddtxt" style="display: none;">Please choose an address from the list above or enter a new address in the form to the location where you need disinfection cleaning services. </p>			                           			                         
									</div>

		            				<p class="medium-title">
		            					<span style="cursor: pointer;">OR ADD A NEW ADDRESS</span>
		            				</p>
		            				<hr>
		        				</div>
		        			</div>

			      			<div class="row special_instructions">
								<label>Special instructions or Requests</label>
								<textarea data-delay="500" class="input required valid" name="special_instructions" id="info_special_instructions">{{ $items['buyer']['specialins'] }}</textarea>
							</div>
		                </form>

						<div id="address-container" class="row form" style="display: none;">
							<form id="form-address" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/cleaning-quote/update_order') }}">
				                {{ csrf_field() }}

				                <input type="hidden" name="action" value="addr-information">

				                <div class="">
				                  	<p>Please enter your information into the form below.</p>
				                </div>
				                 
				                <div class="mt-20">
				                    <div class="row">
				                        <div class="col-md-6 required">
				                            <label>First name*</label>
				               				@if(Auth::check())
						                        <input class="input" type="text" id="fname" name="fname" placeholder="Firstname" data-delay="300" value="{{ Auth::user()->firstname }}" disabled>
						                    @else
						                        <input class="input" type="text" id="fname" name="fname" placeholder="Firstname" data-delay="300" >
						                    @endif
				                        </div>
				                        <div class="col-md-6">
				                            <label>Last name*</label>
						                    @if(Auth::check())
						                        <input class="input" type="text" id="lname" name="lname" placeholder="Lastname" data-delay="300" value="{{ Auth::user()->lastname }}" disabled>
						                    @else
						                        <input class="input" type="text" id="lname" name="lname" placeholder="Lastname" data-delay="300">
						                    @endif
				                        </div>
				                    </div>
				                    
				                    <div class="row">
				                    	<!--
					                        <div class="col-md-6 required">
					                            <label>Email Address*</label>
					                            <input id="email" name="email" type="text" data-delay="300"  class="input">
					                            <div class="message">&nbsp;</div>
					                        </div>
					                     -->
			                         	<div class="col-md-6 required">
			                            	<label>Phone*</label>
	                        				<input id="phone" name="phone" type="text" data-delay="300"  class="input">
			                            </div>
				                        <div class="col-md-6">
				                            <label>Alternative Phone</label>
	                        				<input id="altphone" name="altphone" type="text" data-delay="300" class="input">
				                        </div>

				                    </div>
							
				                    <div class="row">
				                        <div class="col-md-4 required">
				                            <label>Zip Code*</label>
		                   				 	<input id="zipcode" name="zipcode" type="text" data-delay="300" class="input" value="">
				                        </div>
				                        <div class="col-md-6 required">
				                            <label>City*</label>
		                    				<input id="city" name="city" type="text" data-delay="300" class="input" value="">
				                        </div>

				                        <div class="col-md-2 required">
				                            <label>State*</label>
		                    				<input id="state" name="state" type="text" data-delay="300" class="input" value="">
				                        </div>
				                    </div>

				                    <div class="row">
				                        <div class="col-md-6 required">
				                            <label>Address*</label>
		                    				<input id="address1" name="address1" type="text" data-delay="300" class="input" value="">
				                        </div>
				                        <div class="col-md-6">
				                            <label>Address 2</label>
		                    				<input id="address2" name="address2" type="text" data-delay="300" class="input" value="">
				                        </div>
				                    </div>
				                  
				                	<!--    
				                	 	<div class="row">
				                    		<div class="col-md-12">
												<label>Special instructions or Requests</label>
												<textarea data-delay="500" class="input required valid" name="special_instructions" id="special_instructions">{{ $items['buyer']['specialins'] }}</textarea>
											</div>
										</div> 
									-->
				                </div>

				      			<div class="special_instructions">
									<label>Special instructions or Requests</label>
									<textarea data-delay="500" class="input required valid" name="special_instructions" id="addr_special_instructions"">{{ $items['buyer']['specialins'] }}</textarea>
								</div>
				            </form>
						</div>
	                @endif

		           	<div id="create-acct" class="form create-acct" style="display:none;">
		            	@if(Auth::guest())
			                <div class="row create-hdr">
			                    <p class="medium-title">CREATE AN ACCOUNT</p>

			                    <div class="social-logins">
					                <a class="btn-social" href="{{ url('/auth/Facebook') }}">
					                    <img src="/images/btn-fb.png" alt="google login" />
					                </a>
					                <a class="btn-social" href="{{ url('/auth/twitter') }}">
					                    <img src="/images/btn-twitter.png" alt="google login" />
					                </a>
					                 <a class="btn-social" href="{{ url('/auth/google') }}">
					                    <img src="/images/btn-google.png" alt="google login" />
					                </a>
					            </div>

			                    <p class="mt-20">Or  enter your information into the form below.</p>
			                </div>
			            @endif

        				<form id="signup-form" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/signup-account') }}">
        					{{ csrf_field() }}

        					<input type="hidden" name="action" value="signup">
        					<input type="hidden" name="cart" value="1">

			                <div class="row mt-20">
			                    <div class="row">
			                        <div class="col-md-6 required">
			                            <label>First name*</label>
			                            <input name="fname" type="text" data-delay="300" class="input" value="{{ Auth::check() ? Auth::user()->firstname : '' }}">
			                        </div>
			                        <div class="col-md-6 required">
			                            <label>Last name*</label>
			                            <input name="lname" type="text" data-delay="300" class="input" value="{{ Auth::check() ? Auth::user()->lastname : '' }}">
			                        </div>
			                    </div>

			                    <div class="row">
			                        <div class="col-md-6 required">
			                            <label>Email Address*</label>
			                            <input name="email" type="text" data-delay="300"  class="input">
			                            <div class="message">&nbsp;</div>
			                        </div>
			               
		                            <div class="col-md-3 required">
		                                <label>Phone*</label>
		                                <input id="phone" name="phone" type="text" data-delay="300"  class="input">
		                            </div>

		                            <div class="col-md-3">
		                                <label>Alternative Phone</label>
		                                <input id="altphone" name="altphone" type="text" data-delay="300" class="input">
		                            </div>
			          
			                    </div>
			                   
			                    <div class="row">
			                        <div class="col-md-2 required">
			                            <label>Zip Code*</label>
			                            <input name="zipcode" type="text" data-delay="300" class="input">
			                        </div>
			                        <div class="col-md-6 required">
			                            <label>City*</label>
			                            <input name="city" type="text" data-delay="300" class="input">
			                        </div>

			                        <div class="col-md-4 required">
			                            <label>State*</label>
			                            <input name="state" type="text" data-delay="300" class="input">
			                        </div>
			                    </div>

			                     <div class="row">
			                        <div class="col-md-6 required">
			                            <label>Address 1*</label>
			                            <input name="address1" type="text" data-delay="300" class="input">
			                        </div>
			                        <div class="col-md-6">
			                            <label>Address 2</label>
			                            <input name="address2" type="text" data-delay="300" class="input">
			                        </div>
			                    </div>
			                    
			                    @if(Auth::guest())
				                    <div class="row pwd">                
				                        <div class="col-md-6 required">
				                            <label>Password* (must be 8 characters and include a number)</label>
				                            <input type="password" data-delay="300" name="password" id="password" class="input">
				                           	<div class="message">&nbsp;</div>
				                        </div>
				                        <div class="col-md-6 required">
				                            <label>Confirm Password*</label>
				                            <input id="confirm" name="confirm" type="password" data-delay="300" class="input">
				                            <div class="message">&nbsp;</div>
				                        </div>
				                    </div>
				                 @endif

			                    <div class="row special_instructions">
			                    	<div class="col-md-12">
										<label>Special instructions or Requests</label>
										<textarea data-delay="500" class="input required valid" name="special_instructions" id="special_instructions">{{ $items['buyer']['specialins'] }}</textarea>
									</div>
								</div>

								@if(Auth::guest())
				                    <div class="row">
				                        <div class="col-md-12">
				                             <div class="styledCheckbox">
					                            <i class="fas fa-check"></i>
					                         </div>
					                         <label class="font-weight-medium">
					                         	Sign up to receive occasional emails <br class="show-mobile"> with promotional offers for future offerings.
					                         </label>
					                         <input id="offers" name="offers" type="hidden" value="0">
				                        </div>
				                    </div>
				                @endif
			                </div> 
			            </form>
			        </div>
				</div>

				<div class="col-md-3 mt-20">
					@php ($subtotal = floatval($items['total']['subtotal']))

					<div class="cart cart--quote cart--quote--default desktop mb-20 {{ $subtotal <= 0 ? '': 'hide' }}">	
						<div class="cart__header">
							<h3 class="cart__title h5">Your Quote</h3>
						</div>
						<div class="cart__footer">
							<div class="cart__wrap">
								<span class="cart__label">Estimated<br>Total:</span>
								<span class="cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</span>
							</div>
						</div>
					</div>
					<div class="cart cart--quote cart--quote--estimated desktop {{ $subtotal > 0 ? '': 'hide' }}">
						<div class="cart__header">
							<h3 class="cart__title h5">Your Quote</h3>
						</div>
						<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 99 ? 'none': '')  : 'none' }};">
							<div class="cart__wrap">
								<p class="cart__para">
									<span class="icn icn--warning">&nbsp;</span>
									Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$99.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
								</p>
							</div>
						</div>
						<div class="cart__body">
							<div><!---->
								<div class="cart__item-group"><h4 class="cart__item-title h5">DISINFECT</h4>
									<ul class="cart__list">
										<li class="cart__item">
											<div class="cart__wrap">
												<span class="cart__label">Electrostatic Spray</span>
												<span class="cart__price">
													<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
													&nbsp;${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }} 
												</span>
											</div>
										</li>
										<li class="cart__item">
											<div class="cart__wrap">
												<span class="cart__label">UVC Disinfect</span>
												<span class="cart__price">
													<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
													&nbsp;${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
												</span>
											</div>
										</li>
										<li class="cart__item">
											<div class="cart__wrap">
												<span class="cart__label">Wipe Service</span>
												<span class="cart__price">
													<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
													&nbsp;${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
												</span>
											</div>
										</li>
										<li class="cart__item">
											<div class="cart__wrap">
												<span class="cart__label">APT Test</span>
												<span class="cart__price">
													<span class="cart__quantity api__test__quantity">{{ $items['total']['count']['apttest'] }}</span> 
													&nbsp;${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }} 
												</span>
											</div>
										</li>
									</ul>
									<div class="cart__subtotal" data-subtotal="0">
										<div class="cart__wrap"><span class="cart__label">Subtotal</span>
											<span class="cart__price">
												${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
											</span>
										</div>
									</div>
									<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
										<div class="cart__wrap"><span class="cart__label">Promo</span>
											<span class="cart__price cart__discount">
												- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
											</span>
										</div>
									</div>
								</div>
								<!--
									<div class="cart__info" data-min="25">
										<div class="cart__wrap">
											<span class="cart__label">Job Minimum:</span>
											<span class="cart__price">
												${{ number_format(floatval($items['jobmin']), 2, '.', ',') }}
											</span>
										</div>
									</div>
								-->
								<div class="cart__footer">
									<div class="cart__wrap">
										<span class="cart__label">Estimated<br>Total:</span>
										<span class="cart__total cart_final__total">
											${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="promo-area promo-area--mobile hide-med-screen">
						<div class="form__fieldset">
							<div class="form__field">
								<h3 class="promo-area-header h5">Promo Code</h3>
								<div class="row row--2 promo-row">
									<div class="col col--2-3">
										<div class="mobile"><label class="visuallyhidden up-to-medium-only" for="inputPromoCodeMobile">Enter Promo Code</label>
											<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeMobile" name="PromoCode" placeholder="Enter Promo Code" type="text" value="{{ $items['total']['promocode'] }}">
											<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
										</div>
										<div class="desktop">
											<label class="visuallyhidden hide-med-screen" for="inputPromoCodeDesktop">Enter Promo Code</label>
											<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeDesktop" name="PromoCode" placeholder="Enter Code" type="text" value="{{ $items['total']['promocode'] }}">
											<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
										</div>
									</div>
									<span class="col col--1-3">
										<button class="btn btn-caret" id="apply_promo_code">
											<span class="hide-med-screen">Apply</span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="promo-area hide-med-screen">
						<div class="expand-content">
							<p class="promo-area-subheader">To get the most accurate estimate check all that apply.</p>
						</div>
						<div class="form__filter">
							@php ($prid = '5ece4797eaf5e')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach		
						
							<div class="form__field">
								<div class="other-services other-services-parking styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkParkingNotNearby" name="parkingNearby" style="pointer-events: none;">
						      	 	<i class="fas fa-check"></i>
						      	 </div>
								<label for="chkParkingNotNearby">{{ $item['name'] }}</label>
							</div>

							@php ($prid = '5ed4ea1f9eeb1')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach    

							<div class="form__field">
								<div class="other-services other-services-area styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkThirdFloor" name="thirdFloor" style="pointer-events: none;">
						      	 	<i class="fas fa-check"></i>
						      	</div>
								<label for="chkThirdFloor">{{ $item['name'] }}</label>
							</div>

							@php ($prid = '5ed4e71823c17')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

							<div class="form__field">
								<div class="other-services other-services-driveway styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}"  id="chkGuaranteedParking" name="guaranteedParking" style="pointer-events: none;">
					      	 		<i class="fas fa-check"></i>
					      		</div>
								<label for="chkGuaranteedParking">{{ $item['name'] }}</label>
							</div>
						</div>
						<div class="expand-content"><!----><small><em>(If we need to park across or down the street, or clean above the 2nd floor, portable equipment may be required.)</em></small><!----></div>
					</div>
			
				</div>
			</div>
		</div>
	</div>

	<div class="pagination pagination--sticky group" id="bottom-bar">

	<app-quote-sidebar-mobile>
				
		<div class="cart cart--quote cart--quote--estimated desktop mb-20 p-0" style="display: none;">
					<div class="cart__header p-20">
						<h3 class="cart__title h5">Your Quote</h3>
						<!-- <a class="cart__edit" id="clear_cart" tabindex="0">Clear</a> -->
					</div>
					<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 399 ? 'none': '')  : 'none' }};">
						<div class="cart__wrap">
							<p class="cart__para" style="padding: 20px 15px 0px;">
								<span class="icn icn--warning">&nbsp;</span>
								Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$399.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
							</p>
						</div>
					</div>
					<div class="cart__body">
						<div><!---->
							<div class="cart__item-group m-20"><h4 class="cart__item-title h5">DISINFECT</h4>
								<ul class="cart__list">
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Electrostatic Spray</span>
											<span class="cart__price">
												<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
												&nbsp;
												<span class="disinfect__total">
													${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">UVC Disinfect</span>
											<span class="cart__price">
												<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
												&nbsp;
												<span class="uvc__total">
													${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Wipe Service</span>
											<span class="cart__price">
												<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
												&nbsp;
												<span class="wipe__total">
													${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">APT Test</span>
											<span class="cart__price">
												<span class="cart__quantity apt__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
												&nbsp;
												<span class="apt__total">
													${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
								</ul>
								<div class="cart__subtotal">
									<div class="cart__wrap"><span class="cart__label">Subtotal</span>
										<span class="cart__price">
											${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
										</span>
									</div>
								</div>
								<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
									<div class="cart__wrap"><span class="cart__label">Promo</span>
										<span class="cart__price cart__discount">
											- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							</div>
							
							<div class="cart__footer hide">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total cart_final__total">
										${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
									</span>
								</div>
							</div>

						</div>
					</div>
		</div>

		<div class="up-to-medium-only">
			<div class="cart cart__mobile cart--quote cart__collapsed" id="cart-container">
				<div class="cart__header cart__mobile-wrap">
					<h4 class="cart__title h5 visuallyhidden">Your Quote</h4>
					<!-- <div>
						<a class="cart__edit" id="clear_cart">Clear</a>
					</div> -->
					</div><div class="cart__body cart--slide cart__closed"></div>
					<div class="cart__footer">
						<div class="cart__mobile-wrap py-10">
						<div class="cart__mobile-label">Estimated Total</div>
						<div class="cart__mobile-price cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</div></div>
					</div>
			</div>
		</div>
	</app-quote-sidebar-mobile>

	
		<div class="cart__bottom-bar-footer">
			<div class="cart__openclose--fixed cart__close">
				<div class="cart__openclose-wrap">
					<div class="cart__openclose cart__close">
						<span class="visuallyhidden">&nbsp;</span>
					</div>
				</div>
			</div>
		</div>
		<div class="section-container bottom-bar">
			<div class="section__page-item d-flex justify-start"><a href="#" onClick="history.go(-1); return false;" class="mt-10 text-dark" id="previous_page" tabindex="0" style="cursor:pointer;"><img src="/images/Icon material-backspace.png" class="mr-10 width-auto" alt="back">BACK</a></div>
			<div class="section__page-item d-flex justify-end">
				<button class="btn btn-alt btn-bottom-bar-continue" id="next_step" tabindex="0"> CONTINUE </button>
			</div>
		</div>
	</div>
@endsection

@section('footer_scripts')
	<script src="/js/accounting.min.js" type="text/javascript"></script>
	<script src="/js/library.js" type="text/javascript"></script>
	<script src="/js/location.js" type="text/javascript"></script>
	<script src="/js/auth/signup.js" type="text/javascript" ></script>
	<script src="/js/pages/cleaning.quote.service.js"></script>

	<script type="text/javascript">
        $(function() {
        	var $main  = $("#main");
        	var $cracc = $("#create-acct");
        	var $guest = $("#guest-acct");

        	$main.find("#create-account").click(function() {
        		$main.hide();

        		$guest.hide();
        		$cracc.show();
        	});

        	$guest.find('.styledCheckbox').on('click',function() {
		        if($(this).hasClass("sel")){
		            $("#guest-acct #offers").val("0");
		             $(this).removeClass('sel');
		        }else{
		            $("#guest-acct #offers").val("1");
		            $(this).addClass('sel');
		        }
		    });


        	$cracc.find('.styledCheckbox').on('click',function() {
		        if($(this).hasClass("sel")){
		            $("#create-acct #offers").val("0");
		             $(this).removeClass('sel');
		        }else{
		            $("#create-acct #offers").val("1");
		            $(this).addClass('sel');
		        }
		    });

        	$('#new-address').click(function(e) {
        		$("#address-container").show();
        		$("#enteraddtxt").show();

        		var $info = $('#form-information');
        		var $addr = $('#form-address');

        		$addr.find("#addr_special_instructions")
        			 .val($info.find('#info_special_instructions').val());
        		
        		$info.find("#select_addresses").val("");
        		$info.find('.special_instructions').hide();
        	})

			$('#checkout_services').on('click', '.btn-alt', function(e) {
				e.preventDefault();

				@if(Auth::check())
					if($("#address-container").is(":visible")) {
						var $form = $('#form-address');

						var $elem = null;
			        	var iserr = false;

				        $form.removeClass("form-error");
				        // clear all error messages
				        $form.find(".error").html("&nbsp;").removeClass("error");
				        // clear all borderlines except for email 
				        $form.find(".input-error").removeClass("input-error");

				        /* zip code */
				        $elem = $("#form-address input[name='zipcode']");
				        if(isRequired($elem, "Please enter a valid zip code")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        $elem = $form.find("input[name='zipcode']");
				        if(isZipCode($elem)) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* city */
				        $elem = $form.find("input[name='city']");
				        if(isRequired($elem, "Please provide your city")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* state */
				        $elem = $form.find("input[name='state']");
				        if(isRequired($elem, "Please provide your state")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* address 1 */
				        $elem = $form.find("input[name='address1']");
				        if(isRequired($elem, "Please provide your address")) {
				            iserr = !iserr ? true : iserr;   
				        }          

				        /* phone */
				        $elem = $form.find("input[name='phone']");
				        if(isRequired($elem, "Please enter a valid phone number")) {
				            iserr = !iserr ? true : iserr;   
				        } 

				        if(!iserr) {    
				        	$form.get(0).submit();
				        }
				  	} else {
				  		var $form = $('#form-information');

						if($form.find('#select_addresses option:selected').val() !== "") {
							$form.find('input[name="type"]')
								 .val($form.find('#select_addresses option:selected').attr("data-addr"));
							$form.get(0).submit();
						} else {
							alert("Please choose address for your disinfection cleaning.");
						}
				  	}
        		@else
        			if($guest.is(":visible")) {
	        			var $form = $('#form-guest');

			           	var $elem = null;
		        		var iserr = false;

				        $form.removeClass("form-error");
				        // clear all error messages
				        $form.find(".error").html("&nbsp;").removeClass("error");
				        // clear all borderlines except for email 
				        $form.find(".input-error").removeClass("input-error");

				        /* first name */
				        $elem = $form.find("input[name='fname']");
				        if(isRequired($elem, "Please provide your first name")) {
				            iserr = !iserr ? true : iserr;
				        }
				        
				        /* last name */
				        $elem = $form.find("input[name='lname']");
				        if(isRequired($elem, "Please provide your your last name")) {
				            iserr = !iserr ? true : iserr;   
				        }

				        /* zip code */
				        $elem = $form.find("input[name='zipcode']");
				        if(isRequired($elem, "Please enter a valid zip code")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        $elem = $form.find("input[name='zipcode']");
				        if(isZipCode($elem)) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* city */
				        $elem = $form.find("input[name='city']");
				        if(isRequired($elem, "Please provide your city")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* state */
				        $elem = $form.find("input[name='state']");
				        if(isRequired($elem, "Please provide your state")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* address 1 */
				        $elem = $form.find("input[name='address1']");
				        if(isRequired($elem, "Please provide your address")) {
				            iserr = !iserr ? true : iserr;   
				        }          

				        /* phone */
				        $elem = $form.find("input[name='phone']");
				        if(isRequired($elem, "Please enter a valid phone number")) {
				            iserr = !iserr ? true : iserr;   
				        }  

				        /* email */
				        $elem = $form.find("input[name='email']");
				        if(isRequired($elem, "Please provide your email address")) {
				            iserr = !iserr ? true : iserr;         
				        }

				        if(!isEmail($elem , "Please enter a valid email address.")) {
				            iserr = !iserr ? true : iserr;            
				        }

				        if(!iserr) {    
				        	$form.get(0).submit();
				        }
				  	} else {
                      	$('#signup-form').submit();
				  	}

		       	@endif
    		});
        });
    </script>
@endsection