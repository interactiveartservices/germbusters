@extends('layouts.master')

@section('content')
<link href="/css/services.css" rel="stylesheet" type="text/css">

   <!--Start Banner-->   
   <div class="sub-banner">  
  		<img class="banner-img" src="/images/sub-banner-form.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
   </div>	   
   <!--End Banner-->

    <!--Start Loader-->
    <div id="loader" class="loader" style="display: none;" >
        <img src="/images/loading.gif" alt="" />
    </div>


   	<div class="content">
   		<div class="container">
            
			<div class="main-title mt-40 mb-0">   
        		<h2><span>Commercial</span> Deep Disinfectant <span> Cleaning Estimate</span></h2>
    		</div>

		   	<div class="form">

				<div class="row">
		       		<p class="success" id="success" style="display:none;"></p>
		       		<p class="error" id="error" style="display:none;"></p>
		       	</div>

		    	<form name="commercial-form" id="commercial-form" method="post">

					<div class="row border-b">
						<h6 class="mt-20">Select Service</h6>
						<div class="message chk-service">&nbsp;</div>

						<div class="col-md-6 mt-20 p-0">
							<div class="mt-20 required">
								<div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	</div><label>Electrostatic Deep Cleaning Disinfection</label>
						      	<input id="is_deep_disinfection" name="is_deep_disinfection" class="chk" type="hidden" value="0">
					      	</div>

							<div class="mt-20">
						      	<div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	</div><label>Hand wiping service</label>
						      	<input id="is_wiping_service" name="is_wiping_service" type="hidden" class="chk" value="0">
					      	</div>

							<div class="mt-20">
						      	 <div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	 </div><label>UV-C Disinfection</label>
						      	 <input id="is_uv_disinfection" name="is_uv_disinfection" type="hidden" class="chk" value="0">
					      	</div>

						</div>
						<div class="col-md-6">
							<div class=" mt-20">
						      	 <div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	 </div><label> Weekly maintenance plans</label>
						      	 <input id="is_weekly_maintenance" name="is_weekly_maintenance" type="hidden" class="chk" value="0">
					      	</div>

					      	<div class=" mt-20">
						      	 <div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	 </div><label>Monthly maintenance plans</label>
						      	  <input id="is_monthly_maintenance" name="is_monthly_maintenance" type="hidden" class="chk" value="0">
					      	</div>
						</div>
					</div>

					<div class="row border-b">
						<h6>Estimate Square Footage </h6>
						<input type="text" data-delay="300" placeholder="Enter square footage of space to be cleaned" name="square_footage" id="square_footage" class="input half mt-20">
					</div>

					<div class="row border-b">
						<label>Add Promotional Code</label>
						<input type="text" data-delay="300" placeholder="Enter Code" name="promo_code" id="promo_code" class="input half">
					</div>

					<div class="row border-b usr-info">
						<div class="row">
							<div class="col-md-6">
								<label>How Did You Hear About Us?</label>
								<select name="how_did_you_hear" id="how_did_you_hear" class="input">
									<option value="email">Email</option>
									<option value="search_engine">Search Engine</option>
									<option value="advertisement">Advertisement</option>
									<option value="recommendation">Recommendation</option>
									<option value="others">Others</option>
								</select>
							</div>
							<div class="col-md-6"></div>
						</div>

						<div class="row">
							<div class="col-md-6 required">
								<label>Contact First name*</label>
								<input type="text" data-delay="300" name="firstname" id="firstname" class="input">
								 <div class="message">&nbsp;</div>
							</div>
							<div class="col-md-6 required">
								<label>Contact Last name*</label>
								<input type="text" data-delay="300" name="lastname" id="lastname" class="input">
								<div class="message">&nbsp;</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-md-6 required">
                                <label>Company Name</label>
                                <input type="text" data-delay="300" name="company" id="company" class="input">
                                <div class="message">&nbsp;</div>
                            </div>
                            <div class="col-md-6">
                                <label>Locations Number / Name</label>
                                <input type="text" data-delay="300" name="location_number_name" id="location_number_name" class="input">
                            </div>
                        </div>

						<div class="row">
							<div class="col-md-6 required">
								<label>Address*</label>
								<input type="text" data-delay="300" name="address" id="address" class="input">
								<div class="message">&nbsp;</div>
							</div>
							<div class="col-md-6">
								<label>Address2</label>
								<input type="text" data-delay="300" name="address2" id="address2" class="input">
							</div>
						</div>

						<div class="row">
							<div class="col-md-4 required">
								<label>Zip Code*</label>
								<input type="text" data-delay="300" name="zipcode" id="zipcode" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-6 required">
								<label>City*</label>
								<input type="text" data-delay="300" name="city" id="city" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-2 required">
								<label>State*</label>
								<input type="text" data-delay="300" name="state" id="state" class="input">
								<div class="message">&nbsp;</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<label>Number of Locations</label>
								<input type="text" data-delay="300" name="no_of_locations" id="no_of_locations" class="input">
							</div>
							<div class="col-md-6" style="padding:0;">
								<div class="col-md-6 required">
									<label>Phone*</label>
									<input type="text" data-delay="300" name="phone" id="phone" class="input">
									<div class="message">&nbsp;</div>
								</div>
								<div class="col-md-6">
									<label>Alternative Phone</label>
									<input type="text" data-delay="300" name="alt_phone" id="alt_phone" class="input">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6 required">
								<label>Email Address*</label>
								<input type="text" data-delay="300" name="email" id="email" class="input">
								<div class="message">&nbsp;</div>
							</div>
							<div class="col-md-6 required">
								<label>Confirm Email Address*</label>
								<input type="text" data-delay="300" name="confirm_email" id="confirm_email" class="input">
								<div class="message">&nbsp;</div>
							</div>						
						</div>
					</div>

					<div class="row mt-20">
						<label>Special instructions or Requests</label>
						<textarea data-delay="500" class="required valid" name="special_instructions" id="special_instructions"></textarea>
					</div>


					<!--<div class="col-md-2"><input name="btnSubmit" id="btnSubmit" type="submit" value="CONTINUE" class="btn btn-yellow"></div> -->

	            </form>
	                    
			</div>

		</div>
    </div>


    <div class="pagination pagination--sticky group" id="bottom-bar">
	<div class="section-container bottom-bar"><div class="section__page-item d-flex justify-start"></div>
		<div class="section__page-item d-flex justify-end"><button class="btn btn-alt btn-bottom-bar-continue" id="next_step" tabindex="0"> CONTINUE </button></div>
	</div>
	</div>

   <!--End Content-->
@endsection

@section('footer_scripts')
<script type="text/javascript" src="/js/accounting.min.js"></script>
<script src="/js/library.js" type="text/javascript" ></script>
<script src="/js/location.js" type="text/javascript" ></script>
<script type="text/javascript">
    
jQuery(document).ready(function($) {


	 $('.styledCheckbox').on('click',function(){
        if($(this).hasClass("sel")){
            $(this).removeClass('sel');
            $(this).parent().find(".chk").val("0");
        }else{
            $(this).addClass('sel');
            $(this).parent().find(".chk").val("1");
        }
    });

	 function isEmailMatch($email, $conf, message) {
        if($email.val().trim() != $conf.val().trim()) {
            $conf.addClass("input-error");
            $conf.parent().find(".message").addClass("error").html(message);
            return false;                        
        }

        return true;
    }

   $(".btn-alt").click(function(e) {

        e.preventDefault();

       // $('#signup-driver-form #valid-form3').html('Sending <i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending

        var is_deep_disinfection   = $('#is_deep_disinfection').val();
        var is_wiping_service      = $('#is_wiping_service').val(); 
        var is_uv_disinfection     = $('#is_uv_disinfection').val();
        var is_weekly_maintenance  = $('#is_weekly_maintenance').val();
        var is_monthly_maintenance = $('#is_monthly_maintenance').val();
        var square_footage         = $('#square_footage').val();
        var promo_code             = $('#promo_code').val();
        var how_did_you_hear       = $('#how_did_you_hear').val();
        var firstname              = $('#firstname').val();
        var lastname               = $('#lastname').val();
        var company                = $('#company').val();
        var location_number_name   = $('#location_number_name').val();
        var address                = $('#address').val();
        var address2               = $('#address2').val();
        var zipcode                = $('#zipcode').val();
        var city                   = $('#city').val();
        var state                  = $('#state').val();
        var no_of_locations        = $('#no_of_locations').val();
        var phone                  = $('#phone').val();
        var alt_phone              = $('#alt_phone').val();
        var email                  = $('#email').val();
        var special_instructions   = $('#special_instructions').val();
        var CSRF_TOKEN   = $('meta[name="csrf-token"]').attr('content');


        var $elem = null;
        var iserr = false;

        var $frmcommercial = $("#commercial-form");

        $frmcommercial.removeClass("form-error");
        // clear all error messages
        $frmcommercial.find(".error").html("&nbsp;").removeClass("error");
        // clear all borderlines except for email 
        $frmcommercial.find(".input-error").removeClass("input-error");

         if((is_deep_disinfection == "0") && (is_wiping_service == "0") && (is_uv_disinfection == "0") && (is_weekly_maintenance == "0") && (is_monthly_maintenance == "0")){
            $(".chk-service").addClass("error").html("Please select at least one service");
         }

        /* first name */
        $elem = $frmcommercial.find("input[name='firstname']");
        if(isRequired($elem, "Please provide your first name")) {
            iserr = !iserr ? true : iserr;
        }
        
        /* last name */
        $elem = $frmcommercial.find("input[name='lastname']");
        if(isRequired($elem, "Please provide your your last name")) {
            iserr = !iserr ? true : iserr;   
        }           

        /* phone */
        $elem = $frmcommercial.find("input[name='phone']");
        if(isRequired($elem, "Please enter a valid phone number")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* zip code */
        $elem = $frmcommercial.find("input[name='zipcode']");
        if(isRequired($elem, "Please enter a valid zip code")) {
            iserr = !iserr ? true : iserr;   
        }

        if(isZipCode($elem)) {
            iserr = !iserr ? true : iserr;   
        } 


        /* city */
        $elem = $frmcommercial.find("input[name='city']");
        if(isRequired($elem, "Please provide your city")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* state */
        $elem = $frmcommercial.find("input[name='state']");
        if(isRequired($elem, "Please provide your state")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* address 1 */
        $elem = $frmcommercial.find("input[name='address']");
        if(isRequired($elem, "Please provide your address")) {
            iserr = !iserr ? true : iserr;   
        }  

        /* email */
        $elem = $frmcommercial.find("input[name='email']");
        $email1 = $frmcommercial.find("input[name='email']");
        if(isRequired($elem, "Please enter a valid email address.")) {
            iserr = !iserr ? true : iserr;         
        }

        if(!isEmail($elem , "Please enter a valid email address.")) {
            iserr = !iserr ? true : iserr;            
        }

        if($elem.hasClass("email-error")) {
            if(isEmailExists($elem, "A user with this email address already exists.")) {
                iserr = !iserr ? true : iserr;            
            }
        } else {
            $elem.find(".email-error").removeClass("email-error");
        }

         /* email */
        $elem = $frmcommercial.find("input[name='confirm_email']");
        $email2 = $frmcommercial.find("input[name='confirm_email']");
        if(isRequired($elem, "Please confirm your email")) {
            iserr = !iserr ? true : iserr;         
        }

         if(!isEmailMatch($email1, $email2, "Please make sure your confirm email match.")) {
            iserr = !iserr ? true : iserr;         
        }

        if(iserr) {

        	return false;
            // if(!$frmcommercial.hasClass("form-error")) {
            //     $frmcommercial.addClass("form-error");
            // } 
        } else {

            $("#loader").show();
            $(".content").css("background", "#e9e9e9").css("opacity", 0.3);

            // $.get('/api/getlocationbyzip/' + $zipcode.val(), function(data) {
            // //console.log(data);
            //     var zip_ok = check_location(data);

               // if(zip_ok) {
                    // Ajax post data to server
                    $.post('/cleaning-quote/commercial', {
                        'is_deep_disinfection':is_deep_disinfection, 
                        'is_wiping_service':is_wiping_service, 
                        'is_uv_disinfection':is_uv_disinfection, 
                        'is_weekly_maintenance':is_weekly_maintenance, 
                        'is_monthly_maintenance':is_monthly_maintenance, 
                        'square_footage':square_footage, 
                        'promo_code':promo_code, 
                        'how_did_you_hear':how_did_you_hear, 
                        'firstname':firstname, 
                        'lastname':lastname, 
                        'company':company, 
                        'location_number_name':location_number_name,
                        'address':address, 'address2':address2, 
                        'zipcode':zipcode, 
                        'city':city, 
                        'state':state, 
                        'no_of_locations':no_of_locations, 
                        'phone':phone, 
                        'alt_phone':alt_phone, 
                        'email':email, 
                        'special_instructions':special_instructions, 
                        '_token': CSRF_TOKEN
                    })
                    .done(function(response){  
                        // Load json data from server and output message                            
                        if(response.type == 'error') {
                        } else {

                            //alert(response);
                            window.location = "/cleaning-quote/commercial/success/" + response;
                        }
                        }
                    );                    
                //}


            //});
       
        }
               
    });

   
});

</script>
@endsection

