@extends('layouts.master')

@section('content')

   <!--Start Banner-->   
   <div class="sub-banner">  
  		<img class="banner-img" src="/images/sub-banner-form.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
   </div>	   
   <!--End Banner-->

<div class="content">
        
    <div class="container">

    	<h2 class="hdr-txt mt-40">Congratulations!</h2>
    	<hr>

		<div class="main mt-40">

		   	<h2 class="font-weight-bold">Thank You!</h2>
		   	<p class="txt">Your request has been submitted and a GermBuster Account <br>Services Representative will contact you. </p>
			<p class="title">YOUR INFORMATION</p><br>

			  <div class="row">

			 	<div class="col-md-8">
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Square Footage:</span> {{ isset($user->square_footage) ? $user->square_footage : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Contact First Name:</span> {{ isset($user->firstname) ? $user->firstname : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Contact Last Name:</span> {{ isset($user->lastname) ? $user->lastname : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Company Name:</span> {{ isset($user->company) ? $user->company : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Location Number/Name:</span> {{ isset($user->location_number_name) ? $user->location_number_name : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Address 1:</span> {{ isset($user->address1) ? $user->address1 : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Address 2:</span> {{ isset($user->address2) ? $user->address2 : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >City:</span> {{ isset($user->city) ? $user->city : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >State:</span> {{ isset($user->state) ? $user->state : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >ZIP Code:</span> {{ isset($user->zipcode) ? $user->zipcode : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Email:</span> {{ isset($user->email) ? $user->email : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >No. of Location:</span> {{ isset($user->no_of_locations) ? $user->no_of_locations : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Phone:</span> {{ isset($user->phone) ? $user->phone : '' }}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Alt Phone:</span> {{ isset($user->alt_phone) ? $user->alt_phone : ''}}</p>
			 		<p class="font-raleway font-weight-medium"><span class="font-weight-bold d-inline-block" >Services Estimates:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ( isset($user->is_deep_disinfection) && $user->is_deep_disinfection == "1") ? "Electrostatic Deep Cleaning Disinfection" : ""  }}{{ ( isset($user->is_wiping_service) && $user->is_wiping_service == "1") ? ", Hand wiping service" : ""  }}{{ ( isset($user->is_uv_disinfection) && $user->is_uv_disinfection == "1") ? ",UV-C Disinfection" : ""  }}{{ (isset($user->is_weekly_maintenance) && $user->is_weekly_maintenance == "1") ? ", Weekly maintenance plans" : ""  }}{{ (isset($user->is_monthly_maintenance) && $user->is_monthly_maintenance == "1") ? ", Monthly maintenance plans" : ""  }}</p>
			 	</div>

			 	 <div class="col-md-4">
			 		<div class="box mt-20">
			 			<p class="title"><img src="/images/Icon awesome-map-marker-alt.png" class="width-auto"> GERMBUSTERS</p>
			 			<p class="txt-small line-height-1-2 mt-10"><span class="font-weight-bold" id="ip_loc">GermBuster - Charlotte, NC</span><br>
										<span id="ip_add">2807 Mt Isle Harbor Drive</span><br>
										<span id="ip_phone">704 877 2455</span>
						</p>
			 		</div>
			 	</div> 

			 </div>
			 <br>
			 <div class="row">
			 	<div class="col-md-12">
			 		<p class="title">SPECIAL INSTRUCTIONS OR REQUESTS</p>
			 		<p>{{ isset($user->special_instructions) ? $user->special_instructions : '' }}</p>
			 	</div>
			 </div>

		</div>

	</div>
</div>

@endsection

@section('footer_scripts')
@endsection