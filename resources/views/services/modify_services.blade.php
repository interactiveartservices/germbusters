@extends('layouts.master')

@section('content')
	<link href="/css/services.css" rel="stylesheet" type="text/css">

 	<!--Start Banner-->   
   	<div class="sub-banner">  
  		<img class="banner-img" src="/images/sub-banner.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
   	</div>	   
   	<!--End Banner-->

   	<!--Start Loader-->
	<div id="loader" class="loader" style="display: none;" >
	    <img src="/images/loading.gif" alt="" />
	</div>
	<!--End Banner-->

   	<div class="content">
	   <div class="container">
			<div class="section-container">
				<div class="col">
					<ul class="progresstracker">
						<li class="progresstracker__step active is-complete" id="progresstracker_select_services">
							<span class="progresstracker__link outline-text">
								<span>
									<span class="mobilehide">Modify</span> service
								</span>
							</span>
						</li>
						<li class="progresstracker__step" id="progresstracker_scheduling">
							<span>Sched<span class="mobilehide">uling</span></span>
						</li>
						<li class="progresstracker__step" id="progresstracker_guest_information">
							<span>Your Info<span class="mobilehide">rmation</span>
							</span>
						</li>
						<li class="progresstracker__step" id="progresstracker_review_order">
							<span>Review
								<span class="mobilehide"> Your Order</span>
							</span>
						</li>
					</ul>
				</div>
			</div>				
				
			<div class="row" id="services-main">
				<div class="col-md-9 items mt-20">
					<div class="txt-bio">BIO SPRAY RESIDENTIAL DEEP CLEAN DISINFECTION</div>
					<div class="tbl-cont">
						<div class="tbl-cont-scroll">
					<table class="table mb-0 bg-light-gray">
					  <thead>
					    <tr>
					      <th scope="col"></th>
					      <th scope="col">ELECTROSTATIC SPRAY</th>
					      <th scope="col">UVC DISINFECT</th>
					      <th scope="col">ADD WIPE SERVICE</th>
					      <th scope="col">ADD APT TEST</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					    	@php ($prid = '5ed2127b36daa')
					    	@php ($item = [])
					     	@foreach($items['list'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

					      	<th scope="row">Room(s)</th>
						    <td>

						    	<div class="counter quantity-btn">
						    		<button class="counter__btn--decrement quantity sub" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    		<!-- <input data-qty="{{ $item['quantity'] }}" aria-label="Number of vents to clean" class="selector__control form__control quantity qty" id="AirDuct_VentCount" maxlength="5" min="0" name="quantity" pattern="[0-9]*" type="text" value="{{ $item['quantity'] }}"> -->
						    		<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
						    		<button class="counter__btn--increment quantity add" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    	</div>

						      <!-- 	<div class="quantity-btn">
									<button class="quantity sub" data-id="{{ $prid }}" data-type="items">-</button>
										<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
									<button class="quantity add" data-id="{{ $prid }}" data-type="items">+</button>
								</div> -->
							</td>
							<td>
					   		<div class="other-services other-services-uvc styledCheckbox {{ $item['uvc']['isuvc']==1 ? 'sel' : '' }}" id="room_uvc_service" name="room_uvc_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					   		</td>
					     	<td>
					   		<div class="other-services other-services-wipe styledCheckbox {{ $item['wipe']['iswipe']==1 ? 'sel' : '' }}" id="room_wipe_service" name="room_wipe_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					   		</td>
					     	<td>
					     	<div class="other-services other-services-apt-test styledCheckbox {{ $item['apttest']['isapttest']==1 ? 'sel' : '' }}" id="room_apt_test" name="room_apt_test" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					     	</td>
					    </tr>
					    <tr>
					    	@php ($prid = '5ed2129736200')
					    	@php ($item = [])
					     	@foreach($items['list'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

					      	<th scope="row">Staircase</th>
					      	<td>

					      		<div class="counter quantity-btn">
						    		<button class="counter__btn--decrement quantity sub" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    		<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
						    		<button class="counter__btn--increment quantity add" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    	</div>

					      		<!-- <div class="quantity-btn">
									<button class="quantity sub" data-id="{{ $prid }}" data-type="items">-</button>
										<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
									<button class="quantity add" data-id="{{ $prid }}" data-type="items">+</button>
								</div> -->
							</td>
							<td>
					   		<div class="other-services other-services-uvc styledCheckbox {{ $item['uvc']['isuvc']==1 ? 'sel' : '' }}" id="staircase_uvc_service" name="staircase_uvc_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					   		</td>
					      	<td>
					      	 <div class="other-services other-services-wipe styledCheckbox {{ $item['wipe']['iswipe']==1 ? 'sel' : '' }}" id="staircase_wipe_service" name="staircase_wipe_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					      	</td>
					      	<td>
 							<div class="other-services other-services-apt-test styledCheckbox {{ $item['apttest']['isapttest']==1 ? 'sel' : '' }}" id="staircase_apt_test" name="staircase_apt_test" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					      	</td>
					    </tr>
					    <tr>
					    	@php ($prid = '5ed212c91b4a3')
					    	@php ($item = [])
					     	@foreach($items['list'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

					      	<th scope="row">Bath/Laundry</th>
					      	<td>

					      		<div class="counter quantity-btn">
						    		<button class="counter__btn--decrement quantity sub" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    		<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
						    		<button class="counter__btn--increment quantity add" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    	</div>

					      		<!-- <div class="quantity-btn">
									<button class="quantity sub" data-id="{{ $prid }}" data-type="items">-</button>
										<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
									<button class="quantity add" data-id="{{ $prid }}" data-type="items">+</button>
								</div> -->
							</td>
							<td>
					   		<div class="other-services other-services-uvc styledCheckbox {{ $item['uvc']['isuvc']==1 ? 'sel' : '' }}" id="bl_uvc_service" name="bl_uvc_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					   		</td>
					      	<td>
							<div class="other-services other-services-wipe styledCheckbox {{ $item['wipe']['iswipe']==1 ? 'sel' : '' }}" id="bl_wipe_service" name="bl_wipe_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					      	</td>
					      	<td>
					      	<div class="other-services other-services-apt-test styledCheckbox {{ $item['apttest']['isapttest']==1 ? 'sel' : '' }}" id="bl_apt_test" name="bl_apt_test" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					      	</td>
					    </tr>
					    <tr style="border-bottom:1px solid #ddd;">
					    	@php ($prid = '5ed212db3da86')
					    	@php ($item = [])
					     	@foreach($items['list'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

					      	<th scope="row">Entry/Hall</th>
					      	<td>

					      		<div class="counter quantity-btn">
						    		<button class="counter__btn--decrement quantity sub" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    		<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
						    		<button class="counter__btn--increment quantity add" data-id="{{ $prid }}" data-type="items" type="button"></button>
						    	</div>

					      		<!-- <div class="quantity-btn">
									<button class="quantity sub" data-id="{{ $prid }}" data-type="items">-</button>
										<span data-qty="{{ $item['quantity'] }}" class="quantity qty">{{ $item['quantity'] }}</span>
									<button class="quantity add" data-id="{{ $prid }}" data-type="items">+</button>
								</div> -->
						 	</td>
						 	<td>
					   		<div class="other-services other-services-uvc styledCheckbox {{ $item['uvc']['isuvc']==1 ? 'sel' : '' }}" id="eh_uvc_service" name="eh_uvc_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					   		</td>
					      	<td>
					      	<div class="other-services other-services-wipe styledCheckbox {{ $item['wipe']['iswipe']==1 ? 'sel' : '' }}" id="eh_wipe_service" name="eh_wipe_service" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					      	</td>
					      	<td>
					      	<div class="other-services other-services-apt-test styledCheckbox {{$item['apttest']['isapttest']==1 ? 'sel' : '' }}" id="eh_apt_test" name="eh_apt_test" data-type="items" data-id="{{ $prid }}">
					      	 	<i class="fas fa-check m-0"></i>
					      	 </div>
					      	</td>
					    </tr>					     
					  </tbody>
					</table>
						</div>	
					</div>

					<div class="terms  bg-light-gray">
									
						<p class="font-raleway font-size-14">Pricing based on standard room size up to 300 square feet. Minimums may apply.<br>Residential job minimum is <b>$399.00</b></p>
						
						<p class="py-30">

							<a href="/terms-conditions" target="_blank" class="font-raleway text-dark font-weight-bold font-size-14 text-underline">View terms &amp; conditions</a>
							<!-- <input class="tooltip-checkbox ng-untouched ng-pristine ng-valid" type="checkbox" id="tooltipServiceTermsCarpet">
							<app-tooltip class="tooltip__body"><div aria-modal="true" class="content" role="dialog" aria-hidden="true"><button class="tooltip-close" tabindex="-1"><span class="icn icn--x-white"></span><span class="visuallyhidden">Close</span></button><span class="h5 hold-caps">Terms &amp; Conditions</span><p tabindex="-1">Prices are subject to change. One area equals any room up to 300 square feet; combined living areas may be considered as two rooms.Areas exceeding 300 square feet will be priced as additional areas.Baths, halls, staircases, area rugs and large walk-in closets may be priced separately. Sectionals sofas may not be separated. Sofas over 7 ft.and certain fabrics may incur additional charges. Tile cleaning service applies only to Ceramic and Porcelain tiles. Shower stall and tiled countertop cleaning is available at participating locations only.Pre-existing conditions and/or damage may affect pricing or ability to properly complete cleaning. $99 completed job minimum required.Portable equipment may be required for areas above the 2nd floor or if guaranteed adjacent parking is not available. Use of portable equipment may result in a higher required portable job minimum, a portable surcharge or a price increase with necessary price adjustments made at time of service.Contact your local Stanley Steemer for more information.</p></div><div class="arrow"></div></app-tooltip> -->
						
						</p>
					</div>
					
				</div>

			<div class="col-md-3 mt-20">
				@php ($subtotal = floatval($items['total']['subtotal']))

				<div class="cart cart--quote cart--quote--default desktop mb-20 {{ $subtotal <= 0 ? '': 'hide' }}">	
					<div class="cart__header">
						<h3 class="cart__title h5">Your Quote</h3>
						<a class="cart__edit" id="clear_cart" tabindex="0">Clear</a>
					</div>
					<div class="cart__footer">
						<div class="cart__wrap">
							<span class="cart__label">Estimated<br>Total:</span>
							<span class="cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</span>
						</div>
					</div>
				</div>
				
				<div class="cart cart--quote cart--quote--estimated desktop mb-20 {{ $subtotal > 0 ? '': 'hide' }}">
					<div class="cart__header">
						<h3 class="cart__title h5">Your Quote</h3>
						<a class="cart__edit" id="clear_cart" tabindex="0">Clear</a>
					</div>
					<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 399 ? 'none': '')  : 'none' }};">
						<div class="cart__wrap">
							<p class="cart__para">
								<span class="icn icn--warning">&nbsp;</span>
								Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$399.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
							</p>
						</div>
					</div>
					<div class="cart__body">
						<div><!---->
							<div class="cart__item-group"><h4 class="cart__item-title h5">DISINFECT</h4>
								<ul class="cart__list">
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Electrostatic Spray</span>
											<span class="cart__price">
												<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
												&nbsp;
												<span class="disinfect__total">
													${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">UVC Disinfect</span>
											<span class="cart__price">
												<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
												&nbsp;
												<span class="uvc__total">
													${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Wipe Service</span>
											<span class="cart__price">
												<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
												&nbsp;
												<span class="wipe__total">
													${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">APT Test</span>
											<span class="cart__price">
												<span class="cart__quantity apt__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
												&nbsp;
												<span class="apt__total">
													${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
								</ul>
								<div class="cart__subtotal">
									<div class="cart__wrap"><span class="cart__label">Subtotal</span>
										<span class="cart__price">
											${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
										</span>
									</div>
								</div>
								<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
									<div class="cart__wrap"><span class="cart__label">Promo</span>
										<span class="cart__price cart__discount">
											- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
										</span>
									</div>
								</div>	
							</div>
							<!--
								<div class="cart__info">
									<div class="cart__wrap">
										<span class="cart__label">Job Minimum:</span>
										<span class="cart__price">
											${{ number_format(floatval($items['jobmin']), 2, '.', ',') }}
										</span>
									</div>
								</div>
							-->
							<div class="cart__footer">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total cart_final__total">
										${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="promo-area promo-area--mobile hide-med-screen">
					<div class="form__fieldset">
						<div class="form__field">
							<h3 class="promo-area-header h5">Promo Code</h3>
							<div class="row row--2 promo-row">
								<div class="col col--2-3">
								<div class="mobile"><label class="visuallyhidden up-to-medium-only" for="inputPromoCodeMobile">Enter Promo Code</label>
									<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeMobile" name="PromoCode" placeholder="Enter Promo Code" type="text" value="{{ $items['total']['promocode'] }}">
									<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
								</div>
									<div class="desktop">
										<label class="visuallyhidden hide-med-screen" for="inputPromoCodeDesktop">Enter Promo Code</label>
										<input aria-label="Enter Promo Code" class="form__control ng-untouched ng-pristine ng-valid" id="inputPromoCodeDesktop" name="promocode" placeholder="Enter Code" type="text" value="{{ $items['total']['promocode'] }}">
										<span class="alert-text"> The code you have entered is either not valid, expired, or not accepted by this operation. </span>
									</div>
								</div>
								<span class="col col--1-3">
									<button class="btn btn-caret" id="apply_promo_code">
										<span class="hide-med-screen">Apply</span>
									</button>
								</span>
							</div>
						</div>
					</div>
				</div>

				<div class="promo-area">
						<div class="expand-content">
							<p class="promo-area-subheader">To get the most accurate estimate check all that apply.</p>
						</div>
						<div class="form__filter">

							@php ($prid = '5ece4797eaf5e')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach

							<div class="form__field">
								<div class="other-services other-services-parking styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkParkingNotNearby" data-id="{{ $prid }}" name="parkingNearby" data-type="parking">
						      	 	<i class="fas fa-check"></i>
						      	 </div>
								<label for="chkParkingNotNearby">{{ $item['name'] }}</label>
							</div>

							@php ($prid = '5ed4ea1f9eeb1')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach       

							<div class="form__field">
								<div class="other-services other-services-area styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkThirdFloor" data-id="{{ $prid }}" name="thirdFloor" data-type="parking">
						      	 	<i class="fas fa-check"></i>
						      	</div>
								<label for="chkThirdFloor">{{ $item['name'] }}</label>
							</div>

							@php ($prid = '5ed4e71823c17')
					    	@php ($item = [])
					     	@foreach($items['parking'] as $key => $data)  
		                       	@if($data['id'] == $prid)
		                       		@php ($item = $data)
		      						@break
		      					@endif
		                   	@endforeach 

							<div class="form__field">
								<div class="other-services other-services-driveway styledCheckbox {{ $item['ischeck']==1 ? 'sel' : '' }}" id="chkGuaranteedParking" data-type="parking" name="guaranteedParking" data-id="{{ $prid }}">
					      	 		<i class="fas fa-check"></i>
					      		</div>
								<label for="chkGuaranteedParking">{{ $item['name'] }}</label>
							</div>
						</div>
						<div class="expand-content"><!----><small><em appresourcestring="">(If we need to park across or down the street, or clean above the 2nd floor, portable equipment may be required.)</em></small><!----></div>
				</div>
			</div>			
		</div>
		<div class="visible-lg-block">
			<img src="/images/gb-van.png" alt="Germ busters same day Services" style="width:500px">
		</div>
	</div>
</div>

<div class="pagination pagination--sticky group" id="bottom-bar">
 	<app-quote-sidebar-mobile>
				
		<div class="cart cart--quote cart--quote--estimated desktop mb-20 p-0" style="display: none;">
					<div class="cart__header p-20">
						<h3 class="cart__title h5">Your Quote</h3>
						<a class="cart__edit" id="clear_cart" tabindex="0">Clear</a>
					</div>
					<div class="cart__alert" style="display: {{ $subtotal > 0 ? ($subtotal >= 399 ? 'none': '')  : 'none' }};">
						<div class="cart__wrap">
							<p class="cart__para" style="padding: 20px 15px 0px;">
								<span class="icn icn--warning">&nbsp;</span>
								Your order total of <strong><span class="cart__order__total">${{ number_format($subtotal, 2, '.', ',') }}</span></strong> does not meet our required <strong>$399.00</strong> job minimum. You may add items to the order, or continue and be charged the minimum job amount. <!---->
							</p>
						</div>
					</div>
					<div class="cart__body">
						<div><!---->
							<div class="cart__item-group m-20"><h4 class="cart__item-title h5">DISINFECT</h4>
								<ul class="cart__list">
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Electrostatic Spray</span>
											<span class="cart__price">
												<span class="cart__quantity disinfect__quantity">{{ $items['total']['count']['disinfect'] }}</span>
												&nbsp;
												<span class="disinfect__total">
													${{ number_format(floatval($items['total']['price']['disinfect']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">UVC Disinfect</span>
											<span class="cart__price">
												<span class="cart__quantity uvc__quantity">{{ $items['total']['count']['uvc'] }}</span>
												&nbsp;
												<span class="uvc__total">
													${{ number_format(floatval($items['total']['price']['uvc']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">Wipe Service</span>
											<span class="cart__price">
												<span class="cart__quantity wipe__quantity">{{ $items['total']['count']['wipe'] }}</span>
												&nbsp;
												<span class="wipe__total">
													${{ number_format(floatval($items['total']['price']['wipe']), 2, '.', ',') }}
												</span>
											</span>
										</div>
									</li>
									<li class="cart__item">
										<div class="cart__wrap">
											<span class="cart__label">APT Test</span>
											<span class="cart__price">
												<span class="cart__quantity apt__test__quantity">{{ $items['total']['count']['apttest'] }}</span>
												&nbsp;
												<span class="apt__total">
													${{ number_format(floatval($items['total']['price']['apttest']), 2, '.', ',') }}
												</span> 
											</span>
										</div>
									</li>
								</ul>
								<div class="cart__subtotal">
									<div class="cart__wrap"><span class="cart__label">Subtotal</span>
										<span class="cart__price">
											${{ number_format(floatval($items['total']['subtotal']), 2, '.', ',') }}
										</span>
									</div>
								</div>
								<div class="cart__promo {{ floatval($items['total']['discount']) > 0.00 ? '' : 'hide' }}">
									<div class="cart__wrap"><span class="cart__label">Promo</span>
										<span class="cart__price cart__discount">
											- ${{ number_format(floatval($items['total']['discount']), 2, '.', ',') }}
										</span>
									</div>
								</div>	
							</div>
							
							<div class="cart__footer hide">
								<div class="cart__wrap">
									<span class="cart__label">Estimated<br>Total:</span>
									<span class="cart__total cart_final__total">
										${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}
									</span>
								</div>
							</div>

						</div>
					</div>
		</div>

		<div class="up-to-medium-only">
			<div class="cart cart__mobile cart--quote cart__collapsed" id="cart-container">
				<div class="cart__header cart__mobile-wrap">
					<h4 class="cart__title h5 visuallyhidden">Your Quote</h4>
					<!-- <div>
						<a class="cart__edit" id="clear_cart">Clear</a>
					</div> -->
					</div><div class="cart__body cart--slide cart__closed"></div>
					<div class="cart__footer">
						<div class="cart__mobile-wrap py-10">
						<div class="cart__mobile-label">Estimated Total</div>
						<div class="cart__mobile-price cart__total">${{ number_format(floatval($items['total']['estimated']), 2, '.', ',') }}</div></div>
					</div>
			</div>
		</div>
	</app-quote-sidebar-mobile>

	<div class="cart__bottom-bar-footer">
		<div class="cart__openclose--fixed cart__close">
		<div class="cart__openclose-wrap"><div class="cart__openclose cart__close">
			<span class="visuallyhidden">&nbsp;</span>
		</div>
		</div>
		</div>
	</div> 	@php ($subtotal = floatval($items['total']['subtotal']))
	<div class="section-container bottom-bar"><div class="section__page-item d-flex justify-start"><span id="txtjobminimum" class="font-size-14 text-italic {{ ($subtotal >= 399.00) ? 'd-none' : '' }}" >Your order must total <br class="show-mobile"><b>$399</b> and up to proceed</span></div>
		<div class="section__page-item d-flex justify-end">		
			<button class="btn btn-alt btn-bottom-bar-continue" id="next_step" tabindex="0" {{ ($subtotal >= 399.00) ? '' : 'disabled' }}> CONTINUE </button>
		</div>
	</div>
</div>
@endsection

@section('footer_scripts')
	<script type="text/javascript" src="/js/accounting.min.js"></script>
	<script src="/js/pages/cleaning.quote.service.js"></script>
	
	<script type="text/javascript">
		$(window).on('load',function(){
			$('.other-services').each(function(){
				$(this).prop('checked', false);
			});	

/*
			$("#apply_promo_code").click(function(e) {
            e.preventDefault();

             $('#apply_promo_code').html('Apply <i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending
     
             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
             var promocode   = $("input[name='promocode']").val();

            $.post('/modify-services/apply_promocode', {'promocode':promocode, '_token': CSRF_TOKEN}, 

                function(response){  
               
                // Load json data from server and output message    
                if(response.type == 'error') {

                } else {
       
                }
               

            	}, 'json');
        	});		
*/
		});
	</script>
@endsection