@extends('layouts.master')

@section('content')

   <!--Start Banner-->   
   <div class="sub-banner">  
  		<img class="banner-img" src="/images/sub-banner-form.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information"> 
   </div>	   
   <!--End Banner-->

<div class="content">
        
    <div class="container">

    	<h2 class="hdr-txt mt-40">Congratulations!</h2>
    	<hr>

		<div class="main mt-40">

		   	<h2 class="font-weight-bold">Thank You!</h2>
		   	<p class="txt">Your confirmation number is  {{ sprintf('%06d', $order->id) }}.  As a reminder <br>
			You will be contacted by phone or email prior ro your <br> 
			Appointment.. </p>
			<p class="title">YOUR INFORMATION</p><br>

			  <div class="row">

			 	<div class="col-md-8">			 	
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Contact First Name:</span>{{ $order->firstname }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Contact Last Name:</span> {{ $order->lastname }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Address 1:</span> {{ $order->address1 }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Address 1:</span> {{ $order->address2 }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >City:</span> {{ $order->city }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >State:</span> {{ $order->state }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >ZIP Code:</span> {{ $order->zipcode }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Email:</span> {{ $order->email }}
			 		</p> 
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Phone:</span>{{ $order->phone }}
			 		</p>
			 		<p class="font-raleway font-weight-medium">
			 			<span class="font-weight-bold w-180 d-inline-block" >Services Estimates:</span>Disinfection Cleaning
			 		</p>
			 	</div>

			 	<div class="col-md-4">
					<div class="box mt-20">
			 			<p class="title"><img src="/images/Icon awesome-map-marker-alt.png" class="width-auto"> GERMBUSTERS</p>
			 			<p class="txt-small line-height-1-2 mt-10"><span class="font-weight-bold" id="ip_loc">GermBuster - Charlotte, NC</span><br>
										<span id="ip_add">2807 Mt Isle Harbor Drive</span><br>
										<span id="ip_phone">704 877 2455</span>
						</p>

			 		</div>
			 	</div>

			 	<div class="col-md-12">
				 	<p class="title">SPECIAL INSTRUCTIONS OR REQUESTS</p>
				 	<p>{{ $order->special_instructions}}</p>
				 </div>

			 </div>

		</div>

		 <div class="row py-20">
			<div class="col-md-12">
				<p class="title">ORDER #{{ sprintf('%06d', $order->id) }}</p>
			</div>
		</div>


		<div class="main">
			 <div class="row">	
				<div class="col-md-12">
					<!-- <p class="title">APPOINTMENT DATE AND ARRIVAL WINDOW</p>
					<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Date:</span> <span>MAy 12,2020</span></p> 
					<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Arrival Window:</span> <span>8:00-10:00am</span></p> -->
					<p class="font-raleway font-weight-medium"><span class="font-weight-bold w-180 d-inline-block" >Service:</span>
						{{ ((int)$order->total_count_disinfect > 0) ? 'Electrostatic Spray' : '' }}
						{{ ((int)$order->total_count_disinfect > 0) ? '&nbsp;UVC Disinfect' : '' }}
			 			{{ ((int)$order->total_count_wipe > 0) ?  '&nbsp;Wipe Service' : ''  }}
			 			{{ ((int)$order->total_count_apttest > 0) ?  '&nbsp;APT TEST' : ''  }}
			 		</p>
				</div>
			</div>
		</div>	

 		<div class="py-20 font-opensans">
			<div class="col-md-12">
				<p class="title">SERVICES ORDERED</p>
			</div>
			<div class="col-md-12 bg-teal text-white py-10 font-weight-bold">
				<div class="col-xs-7">SERVICE</div>
				<div class="col-xs-2 text-center">QTY</div>
				<div class="col-xs-3 text-center">PRICE</div>
			</div>

			@if((int)$order->total_count_disinfect > 0)
				<div class="col-md-12 py-10">
					<div class="col-xs-7">Electrostatic Spray</div>
					<div class="col-xs-2 text-center">
						{{ $order->total_count_disinfect }}
					</div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($order->total_price_disinfect), 2, '.', ',') }}
					</div>
				</div>
			@endif

			@if((int)$order->total_count_uvc > 0)
				<div class="col-md-12 py-10">
					<div class="col-xs-7">UVC Disinfect</div>
					<div class="col-xs-2 text-center">
						{{ $order->total_count_uvc }}
					</div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($order->total_price_uvc), 2, '.', ',') }}
					</div>
				</div>
			@endif

			@if((int)$order->total_count_wipe > 0)
				<div class="col-md-12 py-10">
					<div class="col-xs-7">Wipe Service</div>
					<div class="col-xs-2 text-center">
						{{ $order->total_count_wipe }}
					</div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($order->total_price_wipe), 2, '.', ',') }}
					</div>
				</div>
			@endif

			@if((int)$order->total_count_apttest > 0)
				<div class="col-md-12 py-10">
					<div class="col-xs-7">APT Test</div>
					<div class="col-xs-2 text-center">
						{{ $order->total_count_apttest }}
					</div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($order->total_price_apttest), 2, '.', ',') }}
					</div>
				</div>
			@endif
		

			<!--
				<div class="col-md-12 py-10 font-weight-bold">
					<div class="col-xs-7">JOB MINIMUM</div>
					<div class="col-xs-2 text-center"></div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($order->jobmin), 2, '.', ',') }}
					</div>
				</div>
			-->

			@php ($prid = '5ece4797eaf5e')
	    	@php ($item = [])
	     	@foreach($items as $key => $data)  
               	@if($data->itemid == $prid)
               		@php ($item = $data)
						@break
					@endif
           	@endforeach	

			@if((int)$item->is_check == 1)
				<div class="col-md-12 py-10">
					<div class="col-xs-7">No Parking Nearby</div>
					<div class="col-xs-2 text-center"></div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($item->price), 2, '.', ',') }}
					</div>
				</div>
			@endif

			@php ($prid = '5ed4ea1f9eeb1')
	    	@php ($item = [])
	     	@foreach($items as $key => $data)  
               	@if($data->itemid == $prid)
               		@php ($item = $data)
						@break
					@endif
           	@endforeach	

			@if((int)$item->is_check == 1)
				<div class="col-md-12 py-10">
					<div class="col-xs-7">Area is on 3rd Floor or Higher</div>
					<div class="col-xs-2 text-center"></div>
					<div class="col-xs-3 text-center">
						${{ number_format(floatval($item->price), 2, '.', ',') }}
					</div>
				</div>
			@endif

			@php($totalqty = $order->total_count_disinfect + $order->total_count_uvc + $order->total_count_wipe + $order->total_count_apttest)

			<div class="col-md-12 bg-gray py-10 font-weight-bold">
				<div class="col-xs-7">SUBTOTAL</div>
				<div class="col-xs-2 text-center"><!-- {{ $totalqty }} --></div>
				<div class="col-xs-3 text-center">
					${{ number_format(floatval($order->subtotal), 2, '.', ',') }}
				</div>
			</div>

			@if((float)$order->discount > 0.00)
				<div class="col-md-12 py-10 font-weight-bold">
					<div class="col-xs-7">PROMO</div>
					<div class="col-xs-2 text-center"><!-- {{ $totalqty }} --></div>
					<div class="col-xs-3 text-center">
						- ${{ number_format(floatval($order->discount), 2, '.', ',') }}
					</div>
				</div>
			@endif
		</div>

		<div class="list-title py-10 font-weight-bold font-opensans font-size-14">
			<div class="col-md-12">						
				<div class="col-sm-7 text-center">
					
				</div>
				<div class="col-xs-12 col-sm-3 text-left">
					<span class="total-header font-size-20">TOTAL</span>		
				</div>
				<div class="col-xs-12 col-sm-2 text-left">
					<span class="total-header text-teal font-size-20">${{ number_format(floatval($order->total), 2, '.', ',') }}</span>		
				</div>
			</div>
		</div>			
	</div>

</div>


@endsection

@section('footer_scripts')
@endsection