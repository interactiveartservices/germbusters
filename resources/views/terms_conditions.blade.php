@extends('layouts.master')

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">  
  		<picture>    	
			<source media="(max-width: 560px)" srcset="/images/sub-banner-form-mobile.png">
			<source media="(min-width: 561px)" srcset="/images/sub-banner-form.jpg 1x, /images/sub-banner-form@2x.jpg 2x">	
			<img class="banner-img" src="/images/sub-banner-form.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information">
		</picture>
   </div>	   
   <!--End Banner-->

   <div class="content">
   		<div class="container ">

		   	<div class="row mt-40 ml-40">
		        <div class="main-title">
		            <h2><span>Terms &</span> Conditions</h2>
		        </div>

		        <div>
		        	<p class="mt-40">
						Germbusters INC.<br>
						TERMS AND CONDITIONS<br>
						EFFECTIVE DATE: May 29, 2020
					</p>
					
					<p>
						THIS DOCUMENT CONTAINS VERY IMPORTANT INFORMATION REGARDING YOUR RIGHTS AND OBLIGATIONS, AS WELL AS CONDITIONS, LIMITATIONS, AND EXCLUSIONS THAT MIGHT APPLY TO YOU. PLEASE READ IT CAREFULLY.  THESE TERMS AND CONDITIONS REQUIRE THE USE OF ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES, RATHER THAN JURY TRIALS OR CLASS ACTIONS.  BY PLACING AN ORDER FOR PRODUCTS OR SERVICES FROM THIS WEBSITE OR OUR NATIONAL headquarters, YOU AFFIRM THAT YOU ARE OF LEGAL AGE TO ENTER INTO THIS AGREEMENT, AND YOU ACCEPT AND ARE BOUND BY THESE TERMS AND CONDITIONS.  YOU AFFIRM THAT IF YOU PLACE AN ORDER ON BEHALF OF AN ORGANIZATION OR COMPANY, YOU HAVE THE LEGAL AUTHORITY TO BIND ANY SUCH ORGANIZATION OR COMPANY TO THESE TERMS AND CONDITIONS.  YOU MAY NOT ORDER, OBTAIN OR RECEIVE PRODUCTS OR SERVICES FROM THIS WEBSITE OR OUR NATIONAL Headquarters IF YOU (A) DO NOT ACCEPT AND AGREE TO THESE TERMS AND CONDITIONS AND OUR PRIVACY POLICY; (B) DO NOT RESIDE IN THE UNITED STATES OR ANY OF ITS TERRITORIES OR POSSESSIONS; (C) ARE AT LEAST 18 YEARS OF AGE OR ARE OF LEGAL AGE TO FORM A BINDING CONTRACT, OR (C) ARE PROHIBITED FROM ACCESSING OR USING THIS WEBSITE OR ANY OF THIS WEBSITE’S CONTENTS, GOODS OR SERVICES BY APPLICABLE LAW. IF YOU DO NOT MEET ALL OF THESE REQUIREMENTS, YOU MUST NOT ACCESS OR USE THE WEBSITE OR OUR NATIONAL CUSTOMER CONTACT CENTER.
					</p>

					<p class="font-weight-bold">Changes To The Terms And Conditions</p>

					<p>
						These terms and conditions apply to the purchase and sale of products and services through www.germbusters.com (the “website”) and headquarters located in pompano beach. these terms and conditions are subject to change by germ busters, inc. (referred to as the “company”, “us”, “we”, or “our” as the context may require) without prior written notice at any time, in our sole discretion. any changes to these terms and conditions will be in effect as of the “last updated date” referenced on the website. you should review these terms and conditions prior to ordering, obtaining or receiving any product or services that are available through this website or our pompano beach headquarters. your continued use of this website or our headquarters or your receipt of products or services after the “last updated date” will constitute your acceptance of and agreement to such changes.
					</p>

					<p class="font-weight-bold">Franchises Are Independently Owned And Operated</p>

					<p>
						While germbusters inc. operates and controls both the website and the pompano beach headquarters, it is not responsible for the operation of the germbusters franchisees that deliver the products and services ordered, obtained or received through the website or the pompano beach headquarters. all germbusters franchisees are independently owned and operated business entities. each germbusters franchisee is solely and independently responsible for its legal and regulatory compliance; for any issues relating to the supply of products or services to you; and for all employment related matters in connection therewith.
					</p>

					<p class="font-weight-bold">Order Acceptance And Cancellation</p>

					<p>
						You agree that orders placed through the website or our pompano beach headquarters constitute an offer to buy the products and services listed in your order pursuant to these terms and conditions. for all orders placed through our pompano beach headquarters, a copy of these terms and conditions will be made available for your review and acceptance immediately following your order via a confirmation email. by proceeding with the delivery of the products and services listed in orders placed through our pompano beach headquarters you are affirmatively accepting these terms and conditions. you may revoke your offer to buy at any time prior to the delivery. all orders must be accepted by us or one of our independently owned and operated germbusters franchisees or we will not be obligated to sell the products or services to you. we may choose not to accept orders at our sole discretion, even after we send you a confirmation email with your order number and details of the items you have ordered.
					</p>
							
					<p class="font-weight-bold">COVID-19 Guidelines</p>


					<p>	
						The health and safety of our Customers; our Employees and the Employees of Germbusters Franchisees is our top priority. During the public health crisis precipitated by COVID-19, we are asking all Customers to make certain representations regarding the nature of the services they have ordered, and the conditions present at the location where services will be provided. Accordingly, by placing an order through the Website or our Pompano Beach Headquarters you are making the following representations:<br><br>

						• The residence, business or building where the services are to be performed has not been occupied—within the 48 hours preceding the date of service—by anyone that:<br>		
						    <span class="ml-20"> • has experienced flu-like symptoms, has been sick or diagnosed with COVID-19 or any other infectious respiratory disease; or</span><br>
						    <span class="ml-20"> • has been in close contact with anyone diagnosed with COVID-19 or any other infectious respiratory disease; or</span><br>
						    <span class="ml-20"> • has been under any voluntary or required self-quarantine or isolation order.</span><br>
						• The services you have ordered are—in your judgement—both urgent and/or necessary for maintaining the safety, sanitation, and essential operation of your residence, business or building and that such services should not be postponed until a later time.
					</p>

					<p class="font-weight-bold">Website & Account Security</p>

					<p>
						We reserve the right to withdraw or amend this website, and any service or material we provide on the website, in our sole discretion without notice. we will not be liable if for any reason all or any part of the website is unavailable at any time or for any period. from time to time, we may restrict access to some parts of the website, or the entire website, to users, including registered users.<br><br>

						You are responsible for:<br><br>

						• Making all arrangements necessary for you to have access to the website.<br>
						• Ensuring that all persons who access the website through your internet connection are aware of these terms and conditions and comply with them.<br><br>

						To access the website or some of the resources it offers, you may be asked to provide certain registration details or other information. it is a condition of your use of the website that all the information you provide on the website is correct, current and complete. you agree that all information you provide to register with this website or otherwise, including but not limited to through the use of any interactive features on the website, is governed by our privacy policy, and you consent to all actions we take with respect to your information consistent with our privacy policy.<br><br>

						If you choose, or are provided with, a username, password or any other piece of information as part of our security procedures, you must treat such information as confidential, and you must not disclose it to any other person or entity. you also acknowledge that your account is personal to you and agree not to provide any other person with access to this website or portions of it using your username, password or other security information. you agree to notify us immediately of any unauthorized access to or use of your username or password or any other breach of security. you also agree to ensure that you exit from your account at the end of each session. you should use particular caution when accessing your account from a public or shared computer so that others are not able to view or record your password or other personal information.<br><br>

						We have the right to disable any user name, password or other identifier, whether chosen by you or provided by us, at any time in our sole discretion for any or no reason, including if, in our opinion, you have violated any provision of these terms and conditions.
					</p>	

					<p class="font-weight-bold">Intellectual Property Rights</p>

					<p>
						The website and its entire contents, features and functionality (including but not limited to all information, software, text, displays, images, video and audio, and the design, selection and arrangement thereof), are owned by the company, its licensors or other providers of such material and are protected by united states and international copyright, trademark, patent, trade secret and other intellectual property or proprietary rights laws.<br><br>

						These Terms And Conditions permit you to use the website for your personal, non-commercial use only. you must not reproduce, distribute, modify, create derivative works of, publicly display, publicly perform, republish, download, store or transmit any of the material on our website, except as follows:<br><br>

						• Your computer may temporarily store copies of such materials in ram incidental to your accessing and viewing those materials. <br> 
						• You may store files that are automatically cached by your web browser for display enhancement purposes.<br>
						• You may print [or download] one copy of a reasonable number of pages of the website for your own personal, non-commercial use and not for further reproduction, publication or distribution.<br>
						• if we provide desktop, mobile or other applications for download, you may download a single copy to your computer or mobile device solely for your Own Personal, Non-Commercial Use, Provided You Agree To Be Bound By Our End User License Agreement For Such Applications.<br><br>

						You Must Not:<br><br>

						• Modify Copies Of Any Materials From This Site.<br>
						• Use Any Illustrations, Photographs, Video Or Audio Sequences Or Any Graphics Separately From The Accompanying Text.<br>
						• Delete Or Alter Any Copyright, Trademark Or Other Proprietary Rights Notices From Copies Of Materials From This Site.<br><br>

						You Must Not Access Or Use For Any Commercial Purposes Any Part Of The Website Or Any Services Or Materials Available Through The Website.<br>
						If You Wish To Make Any Use Of Material On The Website Other Than That Set Out In This Section, Please Address Your Request To: hello@germbusters911.com.<br><br>

						if you print, copy, modify, download or otherwise use or provide any other person with access to any part of the website in breach of the terms and conditions, your right to use the website will cease immediately and you must, at our option, return or destroy any copies of the materials you have made. no right, title or interest in or to the website or any content on the site is transferred to you, and all rights not expressly granted are reserved by the company. any use of the website not expressly permitted by these terms and conditions is a breach of these terms and conditions and may violate copyright, trademark and other laws.
					</p>	

					<p class="font-weight-bold">Trademarks</p>

					<div class="mt-20">
						The Following Trademarks Are Held By Germ Busters, Inc. Go Beyond Clean., Germinator, Sani-Trek,  You May Not Use Such Marks Without The Prior Written Consent Of The Company. All Other Names, Logos, Products, Service Names, Designs And Slogans On This Website Are The Trademarks Of Their Respective Owners.
					</p>

					<p class="font-weight-bold">Prohibited Uses</p>

					<p>
						You May Use The Website Only For Lawful Purposes And In Accordance With These Terms And Conditions. You Agree Not To Use The Website:<br><br>

						• In Any Way That Violates Any Applicable Federal, State, Local Or International Law Or Regulation (Including, Without Limitation, Any Laws Regarding The Export Of Data Or Software To And From The US Or Other Countries).<br>
						• For The Purpose Of Exploiting, Harming Or Attempting To Exploit Or Harm Minors In Any Way By Exposing Them To Inappropriate Content, Asking For Personally Identifiable Information Or Otherwise.<br>
						• To Transmit, Or Procure The Sending Of, Any Advertising Or Promotional Material Without Our Prior Written Consent, Including Any "Junk Mail", "Chain Letter" Or "Spam" Or Any Other Similar Solicitation.<br>
						• To Impersonate Or Attempt To Impersonate The Company, A Company Employee, Another User Or Any Other Person Or Entity (Including, Without Limitation, By Using E-Mail Addresses Or Screen Names Associated With Any Of The Foregoing).<br>
						• To Engage In Any Other Conduct That Restricts Or Inhibits Anyone's Use Or Enjoyment Of The Website, Or Which, As Determined By Us, May Harm The Company Or Users Of The Website Or Expose Them To Liability.<br>
						Additionally, You Agree Not To:<br>
						• Use The Website In Any Manner That Could Disable, Overburden, Damage, Or Impair The Site Or Interfere With Any Other Party's Use Of The Website, Including Their Ability To Engage In Real Time Activities Through The Website.<br>
						• Use Any Robot, Spider Or Other Automatic Device, Process Or Means To Access The Website For Any Purpose, Including Monitoring Or Copying Any Of The Material On The Website.<br>
						• Use Any Manual Process To Monitor Or Copy Any Of The Material On The Website Or For Any Other Unauthorized Purpose Without Our Prior Written Consent.<br>
						• Use Any Device, Software Or Routine That Interferes With The Proper Working Of The Website.<br>
						• Introduce Any Viruses, Trojan Horses, Worms, Logic Bombs Or Other Material Which Is Malicious Or Technologically Harmful.<br>
						• Attempt To Gain Unauthorized Access To, Interfere With, Damage Or Disrupt Any Parts Of The Website, The Server On Which The Website Is Stored, Or Any    Server, Computer Or Database Connected To The Website.<br>
						• Attack The Website Via A Denial-Of-Service Attack Or A Distributed Denial-Of-Service Attack.<br>
						• Otherwise Attempt To Interfere With The Proper Working Of The Website.
					</p>

					<p class="font-weight-bold">User-Generated Contributions</p>

					<p>
						The website may contain message boards, chat rooms, personal web pages or profiles, forums, bulletin boards and other interactive features (collectively, <b>"interactive services"</b>) that allow users to post, submit, publish, display or transmit to other users or other persons (hereinafter, <b>"post"</b>) content or materials (collectively, <b>"user-generated contributions or “ugc”"</b>) on or through the website.<br><br>

						All UGC must comply with the content standards set out in these terms and conditions.<br><br>

						Any UGC you post to the site will be considered non-confidential and non-proprietary. you own the copyright in any original ugc you post. we do not claim any copyrights in ugc. however, by using our website you are granting us and our subsidiaries, affiliates, successors and assigns, a nonexclusive, fully paid, worldwide, perpetual, irrevocable, royalty-free, transferable license (with the right to sublicense through unlimited levels of sublicensees) to use, copy, modify, distribute, publicly display and perform, publish, transmit, remove, retain repurpose, and commercialize ugc you post in any and all media or form of communication whether now existing or hereafter developed, without obtaining additional consent, without restriction, notification, or attribution, and without compensating you in any way, and to authorize others to do the same. for this reason, we ask that you not post any ugc that you do not wish to license to us, including any photographs, videos, confidential information, or product ideas.<br><br>

						You Represent And Warrant That:<br><br>

						• You own or control all rights in and to the ugc and have the right to grant the license granted above to us and each of our respective licensees, successors and assigns.<br>
						• All of your ugc do and will comply with these terms and conditions.<br><br>

						You understand and acknowledge that you are responsible for any ugc you submit or contribute, and you, not the company, have fully responsibility for such content, including its legality, reliability, accuracy and appropriateness.<br>
						we are not responsible, or liable to any third party, for the content or accuracy of any ugc posted by you or any other user of the website.
					</p>

					<p class="font-weight-bold">Monitoring And Enforcement; Termination</p>

					<p>
						We Have The Right To:<br><br>

						• Remove Or Refuse To Post Any UGC For Any Or No Reason In Our Sole Discretion.<br>
						• Take Any Action With Respect To Any User Contribution That We Deem Necessary Or Appropriate In Our Sole Discretion, Including If We Believe That Such User Contribution Violates The Terms And Conditions, Including The Content Standards, Infringes Any Intellectual Property Right Or Other Right Of Any  Person Or Entity, Threatens The Personal Safety Of Users Of The Website Or The Public Or Could Create Liability For The Company.<br>
						• Disclose Your Identity Or Other Information About You To Any Third Party Who Claims That Material Posted By You Violates Their Rights, Including Their Intellectual Property Rights Or Their Right To Privacy.<br>
						• Take Appropriate Legal Action, Including Without Limitation, Referral To Law Enforcement, For Any Illegal Or Unauthorized Use Of The Website.<br>
						• Terminate Or Suspend Your Access To All Or Part Of The Website For Any Or No Reason, Including Without Limitation, Any Violation Of These Terms And Conditions.<br><br>

						Without Limiting The Foregoing, We Have The Right To Fully Cooperate With Any Law Enforcement Authorities Or Court Order Requesting Or Directing Us To Disclose The Identity Or Other Information Of Anyone Posting Any Materials On Or Through The Website. <b>YOU WAIVE AND HOLD HARMLESS THE COMPANY AND EACH OF ITS AFFILIATES, FRANCHSIEES, LICENSEES AND SERVICE PROVIDERS FROM ANY CLAIMS RESULTING FROM ANY ACTION TAKEN BY ANY OF THE FOREGOING PARTIES DURING OR AS A RESULT OF ITS INVESTIGATIONS AND FROM ANY ACTIONS TAKEN AS A CONSEQUENCE OF INVESTIGATIONS BY EITHER SUCH PARTIES OR LAW ENFORCEMENT AUTHORITIES.</b><br><br>

						However, We Do Not Undertake To Review All Material Before It Is Posted On The Website And Cannot Ensure Prompt Removal Of Objectionable Material After It Has Been Posted. Accordingly, We Assume No Liability For Any Action Or Inaction Regarding Transmissions, Communications Or Content Provided By Any User Or Third Party. We Have No Liability Or Responsibility To Anyone For Performance Or Nonperformance Of The Activities Described In This Section.
					</p>

					<p class="font-weight-bold">Content Standards</p>

					<p>
						These content standards apply to any and all UGC and use of Interactive Services. UGC must in their entirety comply with all applicable federal, state, local and international laws and regulations. Without limiting the foregoing, UGC must not:<br><br>

						• Contain any material which is defamatory, obscene, indecent, abusive, offensive, harassing, violent, hateful, inflammatory or otherwise objectionable.<br>
						• Promote sexually explicit or pornographic material, violence, or discrimination based on race, sex, religion, nationality, disability, sexual orientation or age.<br>
						• Infringe any patent, trademark, trade secret, copyright or other intellectual property or other rights of any other person.<br>
						• Violate the legal rights (including the rights of publicity and privacy) of others or contain any material that could give rise to any civil or criminal liability under applicable laws or regulations or that otherwise may be in conflict with these Terms and Conditions and our Privacy Policy.<br>
						• Be likely to deceive any person.<br>
						• Promote any illegal activity, or advocate, promote or assist any unlawful act.<br>
						• Cause annoyance, inconvenience or needless anxiety or be likely to upset, embarrass, alarm or annoy any other person.<br>
						• Impersonate any person or misrepresent your identity or affiliation with any person or organization.<br>
						• Involve commercial activities or sales, such as contests, sweepstakes and other sales promotions, barter or advertising.<br>
						• Give the impression that they emanate from or are endorsed by us or any other person or entity, if this is not the case.
					</p>

					<p class="font-weight-bold">Information Overview</p>
					
					<p>
						The information presented on or through the Website is made available solely for general information purposes. We do not warrant the accuracy, completeness or usefulness of this information. Any reliance you place on such information is strictly at your own risk. We disclaim all liability and responsibility arising from any reliance placed on such materials by you or any other visitor to the Website, or by anyone who may be informed of any of its contents.
						This Website may include content provided by third parties, including materials provided by independently owned and operated GermBuster ® franchise owners. All statements and/or opinions expressed in these materials, and all articles and responses to questions and other content, other than the content provided by the Company, are solely the opinions and the responsibility of the person or entity providing those materials. These materials do not necessarily reflect the opinion of the Company. We are not responsible, or liable to you or any third party, for the content or accuracy of any materials provided by any third parties.
					</p>

					<p class="font-weight-bold">Website modifications</p>
					
					<p>
						We may update the content on this Website from time to time, but its content is not necessarily complete or up-to-date. Any of the material on the Website may be out of date at any given time, and we are under no obligation to update such material.
					</p>

					<p class="font-weight-bold">You & our Website</p>
					
					<p>
						All information we collect on this Website is subject to our Privacy Policy. By using the Website, you consent to all actions taken by us with respect to your information in compliance with the Privacy Policy.
					</p>

					<p class="font-weight-bold">Website Links</p>
					
					<p>
						If the Website contains links to other sites and resources provided by third parties, these links are provided for your convenience only. This includes links contained in advertisements, including banner advertisements and sponsored links. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them. If you decide to access any of the third-party websites linked to this Website, you do so entirely at your own risk and subject to the terms and conditions of use for such websites.

					<p class="font-weight-bold">Job Opportunities</p>

					<p>
						The Website may provide links or other information related to employment opportunities posted by Germbusters Inc. or Germbusters Franchisees. All Germbusters Franchisees are independently owned and operated business entities. Employment Opportunities listed by Germbusters Franchisees are for those Germbusters Franchisees alone and not with Germbusters International, Inc. This means that theGermbusters Franchisee that posted the opportunity is solely responsible for setting the job requirements, all hiring decisions, and all other employment related matters in its business. In these circumstances,GermbustersInternational, Inc. does not receive copies of any application you submit to a Germ busters Franchise; does not control whether you receive an interview or are hired by the Germ busters Franchise; and does not control any of the Germbusters Franchisee’s employment policies and practices. Germbusters Inc. does not employ independent Germbusters Franchisees. If you are hired byGermbusters Franchisee, only that franchisee, and notGermbusters, Inc. will be your employer.
					</p>

					<p class="font-weight-bold">Geographic Restrictions</p>

					<p>
						The owner of the Website is based in the state of Florida in the United States. We provide this Website for use only by persons located in the United States and its territories or possessions. We make no claims that the Website or any of its content is accessible or appropriate outside of the United States. Access to the Website may not be legal by certain persons or in certain countries. If you access the Website from outside the United States, you do so on your own initiative and are responsible for compliance with local laws.
					</p>

					<p class="font-weight-bold">Warranties & Disclaimers</p>

					<p>
						You understand that we cannot and do not guarantee or warrant that files available for downloading from the internet or the Website will be free of viruses or other destructive code. You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for anti-virus protection and accuracy of data input and output, and for maintaining a means external to our site for any reconstruction of any lost data.<br><br>

						WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY WEBSITE LINKED TO IT.
						YOUR USE OF THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE IS AT YOUR OWN RISK. THE WEBSITE, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. NEITHER THE COMPANY NOR ANY PERSON ASSOCIATED WITH THE COMPANY MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY OR AVAILABILITY OF THE WEBSITE. WITHOUT LIMITING THE FOREGOING, NEITHER THE COMPANY NOR ANYONE ASSOCIATED WITH THE COMPANY REPRESENTS OR WARRANTS THAT THE WEBSITE, ITS CONTENT OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE OR UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, THAT OUR SITE OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE WEBSITE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE WEBSITE WILL OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.
						THE COMPANY HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR PARTICULAR PURPOSE.<br>
						THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.
					</p>

					<p class="font-weight-bold">Liability limitations</p>

					<p>
						IN NO EVENT WILL THE COMPANY, ITS AFFILIATES, FRANCHSIEES, LICENSORS, SERVICE PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE WEBSITE OR OUR NATIONAL CUSTOMER CONTACT CENTER, ANY WEBSITES LINKED TO THE WEBSITE, ANY CONTENT ON THE WEBSITE OR SUCH OTHER WEBSITES OR ANY SERVICES OR ITEMS ORDERED, OBTAINED, OR RECIEVED THROUGH THE WEBSITE OR OUR NATIONAL CUSTOMER CONTACT CENTER OR SUCH OTHER WEBSITES, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE.<br><br>

						THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.
					</p>

					<p class="font-weight-bold">Indemnification</p>

					<p>	
						You agree to defend, indemnify and hold harmless the Company, its affiliates, franchisees, licensees and service providers, and its respective officers, directors, employees, contractors, agents, licensors, suppliers, successors and assigns from and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses or fees (including reasonable attorneys' fees) arising out of or relating to your violation of these Terms and Conditions or your use of the Website or our Pompano Beach Headquarters, including, but not limited to, your UGC, any use of the Website's content or the products and services ordered, obtained or received through the Website or our Pompano Beach Headquarters other than as expressly authorized in these Terms and Conditions or your use of any information obtained from the Website.
					</p>

					<p class="font-weight-bold">Governing Law and Jurisdiction</p>

					<p>
						All matters relating to the Website, our Pompano Beach Headquarters, these Terms and Conditions, or our Privacy Policy and any dispute or claim arising therefrom or related thereto (in each case, including non-contractual disputes or claims), shall be governed by and construed in accordance with the internal laws of the State of Florida without giving effect to any choice or conflict of law provision or rule (whether of the State of Florida or any other jurisdiction) that would cause the application of the laws of any jurisdiction other than those of the State of Florida.
					</p>

					<p class="font-weight-bold">Dispute Resolution and Binding Arbitration</p>
					
					<p>					
						DUE TO THE MUTUAL BENEFITS (SUCH AS REDUCED EXPENSE AND INCREASED EFFICIENCY) PROVIDED BY PRIVATE BINDING ARBITRATION, BOTH YOU AND GermBusters INTERNATIONAL, INC. AGREE THAT ANY CLAIM, DISPUTE, AND/OR CONTROVERSY ARISING OUT OF OR RELATING IN ANY WAY TO YOUR USE OF THE WEBSITE OR THE NATIONAL CUSTOMER CONTACT CENTER, ANY PRODUCTS OR SERVICES SOLD, OBTAINED, OR RECIEVED FROM GermBusters INTERNATIONAL, INC. THROUGH THE WEBSITES OR THE NATIONAL CUSTOMER CONTACT CENTER, THESE TERMS AND CONDITIONS, THE PRIVACY POLICY, OR THE SCOPE OR VALIDITY OF THIS ARBITRATION AGREEMENT SHALL BE SUBMITTED TO AND RESOLVED EXCLUSIVELY BY BINDING ARBITRATION, RATHER THAN IN COURT, EXCEPT THAT YOU MAY ASSERT CLAIMS IN SMALL CLAIMS COURT IF YOUR CLAIMS QUALIFY. ALL MATTERS RELATING TO ARBITRATION WILL BE GOVERNED BY THE FEDERAL ARBITRATION ACT, 9 U.S.C. SECTIONS 1‐16, AND NOT BY ANY STATE ARBITRATION LAW.<br><br>

						Any arbitration pursuant to the Terms and Conditions or the Privacy Policy shall be initiated with and conducted by the American Arbitration Association (AAA) in accordance with the AAA's Consumer Arbitration Rules and Supplementary Procedures for Consumer-Related Disputes, both of which may be obtained at http://www.adr.org or by calling (800)778‐7879. Payment of all filing, administration and arbitrator fees will be governed by the AAA's rules. You may choose to have the arbitration conducted by telephone, based on written submissions, or in person in Pompano Beach, Florida. Nothing herein shall prevent Germbusters Inc. or you from obtaining from a temporary restraining order or preliminary injunctive relief to preserve the status quo or to prevent any irreparable harm pending the arbitration of the underlying claim, dispute, and/or controversy.<br><br>

						The arbitration proceedings shall be conducted by a single arbitrator. In addition to any requirements imposed by law, the arbitrator shall be a retired state or federal court judge, or a licensed attorney with arbitration experience and at least ten (10) years’ experience as a lawyer, and shall be subject to disqualification on the same grounds as would apply to a judge of a court in the relevant jurisdiction. The arbitrator shall follow controlling law and issue a decision in writing within forty-five (45) days of the arbitration hearing with a supporting opinion based on applicable law. The decision of the arbitrator (the “Decision”) shall be final, binding, and conclusive on the parties and may be entered in any court of competent jurisdiction; provided, however, that You and Germbusters International, Inc. agree that the Decision may be appealed pursuant to the AAA’s Optional Appellate Arbitration Rules (“Appellate Rules”), which may be obtained at http://www.adr.org or by calling (800)778‐7879. The Decision shall not be considered final until after the time for filing the notice of appeal pursuant to the Appellate Rules has expired. Appeals must be initiated within thirty (30) days of receipt of the Decision, as defined by Rule A-3 of the Appellate Rules, by filing a Notice of Appeal with any AAA office. The single appellate arbitrator, who shall be chosen in the same manner described above, shall review the Decision applying the same standard(s) of review applicable in civil cases in the relevant jurisdiction and shall issue a reasoned award. The appellate arbitrator’s decision shall be final, binding and conclusive on the parties, and may be entered in any court of competent jurisdiction. At either party’s election, such decision and supporting opinion may be appealed to another arbitrator (“appellate arbitrator”), who shall be chosen in the same manner as described above. The appellate arbitrator shall apply to the underlying decision and opinion the same standard for review of civil cases as an appellate court in the relevant jurisdiction and issue a decision in writing with a supporting opinion based on such review and applicable law. The appellate arbitrator’s decision shall be final, binding and conclusive on the parties and may be entered in any court of competent jurisdiction.<br><br>

						WE EACH AGREE THAT ANY DISPUTE RESOLUTION PROCEEDINGS WILL BE CONDUCTED ONLY ON AN INDIVIDUAL BASIS, AND NOT ON A CLASS-WIDE, COLLECTIVE, MULTIPLE‐PARTY, OR PRIVATE ATTORNEY GENERAL BASIS. WE ALSO AGREE THAT ANY ARBITRATION PROCEEDING BETWEEN US MAY NOT BE CONSOLIDATED WITH ANY OTHER ARBITRATION BETWEEN GermBusters INTERNATIONAL, INC. AND ANY OTHER PERSON. YOU AND GermBusters INTERNATIONAL, INC. BOTH UNDERSTAND THAT BY AGREEING TO THIS BINDING ARBITRATION PROVISION, BOTH ARE GIVING UP THEIR RIGHT TO TRIAL BY JURY OF ANY INDIVIDUAL, CLASS-WIDE, COLLECTIVE, MULTIPLE‐PARTY, PRIVATE ATTORNEY GENERAL, OR OTHER CLAIM EITHER MAY HAVE AGAINST THE OTHER, EXCEPT AS EXPRESSLY PROVIDED HEREIN.<br><br>

						Should any term or provision or portion of this arbitration agreement be declared void or unenforceable or deemed in contravention of law, it shall be severed and/or modified and the remainder of this agreement shall be enforceable; provided, however, that if the provision above prohibiting class-wide, collective action, consolidated, or other group arbitration is deemed invalid, then this entire arbitration provision shall be null and void and shall not apply to that dispute, which shall be resolved in a judicial proceeding in Pompano Beach, Florida. In such circumstances, you waive any and all objections to the exercise of jurisdiction over you by such courts and to venue in such courts.<br>
						Limitation on Time to File Claims<br><br>

						ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING TO THESE TERMS AND CONDITIONS OR THE WEBSITE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF ACTION OR CLAIM IS PERMANENTLY BARRED.
					</p>

					<p class="font-weight-bold">Waiver and Sever-ability</p>

					<p>
						No waiver of by the Company of any term or condition set forth in these Terms and Conditions shall be deemed a further or continuing waiver of such term or condition or a waiver of any other term or condition, and any failure of the Company to assert a right or provision under these Terms and Conditions shall not constitute a waiver of such right or provision.<br>
						If any provision of these Terms and Conditions is held by a court or other tribunal of competent jurisdiction to be invalid, illegal or unenforceable for any reason, such provision shall be eliminated or limited to the minimum extent such that the remaining provisions of the Terms and Conditions will continue in full force and effect.
					</p>

					<p class="font-weight-bold">Entire Agreement</p>

					<p>
						These Terms and Conditions and our Privacy Policy constitute the sole and entire agreement between you and Germbusters International, Inc. with respect to the Website and our Pompano Beach Headquarters and supersede all prior and contemporaneous understandings, agreements, representations and warranties, both written and oral, with respect to both the Website and our Pompano Beach Headquarters.
					</p>

					<p class="font-weight-bold">Your Comments and Concerns</p>

					<p>	
						This website is operated by:<br>
						GermBusters INC<br>
						1905 NW 32 street<br>
						Pompano Beach, Fl 33064<br><br>

						All other feedback, comments, requests for technical support and other communications relating to the Website should be directed to: hello@germbusters911.com.<br>
						Thank you for visiting <b>www.germbusters911.com.</b><br><br>


						Copyright © 2020 Germbusters International, Inc. All Rights Reserved. All Germbuster s Franchise Locations are Independently Owned and Operated. Available Services, Pricing, Promotions, and Hours of Operation may vary by location.
					</p>


		        </div>
		    </div>
	
	</div>
</div>



@endsection

