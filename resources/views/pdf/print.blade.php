<!DOCTYPE html>
<html lang="en" class="no-js" prefix="og: http://ogp.me/ns#">
<head>
    <title>Germbusters</title>
    <meta name="title" content="Germbusters" />   
    <style type="text/css">
    /*! CSS Used from: http://germbusters.test/css/medical-guide.css */
body{background:#fff;font-family:'Source Sans Pro', sans-serif;color:#444444;}
*{margin:0px;padding:0px;}
h1{font-weight:900;font-family:'Raleway', sans-serif;font-size:54px;}
h4{font-weight:600;font-size:28px;font-family:'Raleway', sans-serif;}
p{font-family:'Source Sans Pro', sans-serif;font-weight:300;font-size:16px;line-height:27px;}
img{width:100%;}
p{margin:0px;padding:0px;}
/*! CSS Used from: http://germbusters.test/css/bootstrap.css */
html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;}
body{margin:0;}
h1{font-size:2em;margin:0.67em 0;}
small{font-size:80%;}
img{border:0;}
hr{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;height:0;}
@media print{
*,*:before,*:after{background:transparent!important;color:#000!important;-webkit-box-shadow:none!important;box-shadow:none!important;text-shadow:none!important;}
img{page-break-inside:avoid;}
img{max-width:100%!important;}
p{orphans:3;widows:3;}
}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
img{vertical-align:middle;}
hr{margin-top:20px;margin-bottom:20px;border:0;border-top:1px solid #eeeeee;}
small{font-size:85%;}
.text-right{text-align:right;}
.col-sm-6,.col-sm-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px;}
@media (min-width: 768px){
.col-sm-6,.col-sm-12{float:left;}
.col-sm-12{width:100%;}
.col-sm-6{width:50%;}
}
.row:before,.row:after{content:" ";display:table;}
.row:after{clear:both;}

/*! CSS Used from: http://germbusters.test/css/dropmenu.css */
::-webkit-input-placeholder{color:#fff;}
:-moz-placeholder{color:#fff;}
::-moz-placeholder{color:#fff;}
:-ms-input-placeholder{color:#fff;}
/*! CSS Used from: http://germbusters.test/css/germbusters.css */
.text-right{text-align:right;}
.d-flex{display:block;}
.mt-20{margin-top:20px;}
.mt-30{margin-top:30px;}
/*! CSS Used from: http://germbusters.test/css/services.css */
::placeholder{color:#555555;opacity:1;}
small{display:block;font-size:.75em;line-height:1.3;}
/*! CSS Used from: http://germbusters.test/css/affiliate.css */
#af-review-body{background-size:contain;padding:30px 40px 0;font-family:'Poppins', sans-serif;}
.af-review-content{padding-top:50px;}
.head-logo{width:260px;position:relative;}
h1.estimate{font-size:3.5rem;color:#3B96BA;font-family:'Poppins', sans-serif;font-weight:bold;margin:0;line-height:1;}
.invoice-no{font-size:17px;font-weight:500;line-height:1;}
.ad-box{display:inline-flex;background:#3A96BA;border:solid 4px #3A96BA;color:#fff;margin-top:60px;margin-bottom:30px;}
.ad-box small{background:#fff;color:#000;font-weight:500;font-size:10px;}
.ad-box span{font-size:22px;font-weight:600;letter-spacing:-1.5px;}
.billto{width:100%;display:inline-block;margin-top:20px;}
.client{font-size:19px;margin:15px 0;}
.hl{font-size:14px;font-weight:500;margin-bottom:10px;}
.hl .d1{margin-right:15px;}
.hl .d2 span{font-size:14px;margin-top:10px;margin-left:65px;display:inline-block;line-height:1.2;}
.af-div{border-top-color:#6E7071;border-top-width:4px;margin:10px 0 20px;}
.af-div-mid{border-top-color:#F1F1F1;border-top-width:1px;margin:18px 0;}
.rcl{width:70%;font-size:14px;float:left;}
.rcl span{font-weight:600;}
.rcl p{font-weight:400;font-size:12px;line-height:1.5;margin-top:8px;}
.rcr{width:30%;text-align:right;align-self:center;font-weight:600;float:left;}
.head-col{font-weight:700;font-size:14px;}
.st1,.st2{width:50%;}
.af-review-footer h5 {
	font-size: 16px;
	font-weight: 700;
}
.af-review-footer p {
	font-size: 12px;
	font-weight: 500;
	line-height: 1.5;
	margin-top: 10px;
	margin-bottom: 20px;
}
.sign-here {
	border-top: solid 1px #000;
	padding-top: 10px;
	margin-top: -75px;
	padding-bottom: 40px;
	max-width: 300px;
	/* transform: translateX(-50%); */
	left: 30%;
	position: relative;
	z-index: -1;
}
.page-break-after
    {
        page-break-after: always;
        page-break-inside: avoid;
    }

 </style>
</head>

<body id="affiliate_review" class="affiliate_review">

                    <div id="af-review-body">
                        <div class="af-review-header">
                            <div class="row d-flex">
                                
                                <div class="" style="float:left;width:50%;">
                                    <img src="https://www.germbusters911.com/images/gm-header.png" class="head-logo" style="width:200px;" />
                                    <span class="billto">BILL TO</span>
                                    <h4 class="client">{{ isset($details) ? ucfirst($details->firstname) : '' }} &nbsp; {{ isset($details) ? ucfirst($details->lastname) : '' }}</h4>
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">{{ isset($details) ? $details->email : '' }}</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">{{ isset($details) ? $details->phone : '' }}<br />
                                            <span>{{ isset($details) ? $details->address1 : '' }}<br />{{ isset($details) ? $details->city : '' }}, {{ isset($details) ? $details->state : '' }} {{ isset($details) ? $details->zipcode : '' }}</span>
                                        </span>
                                    </div>
                                    
                                    <span class="billto">PREPARED BY</span>
                                    <h4 class="client">{{ isset($details) ? $details->acct_manager : '' }}</h4>

                                    @if( $details->acct_manager == "Bill Wagonseller") 
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">bill@germbusters911.com</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">704-877-2455<br />
                                            <span>2807 Mount Isle Harbor Dr, <br> Charlotte, NC 28214 </span>
                                        </span>
                                    </div>
                                    @else
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">{{ $details->acct_manager == "Mike OBrien" ? 'mike@germbusters911.com' : 'scott@germbusters911.com' }}</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">{{ $details->acct_manager == "Mike OBrien" ? '704-281-5054' : '954-761-2480' }}<br />
                                        <span>1905 NW 32 St, Building 5,<br> Pompano Beach, FL 33064</span>
                                        </span>
                                    </div> 
                                    @endif
                                </div>
                                
                                <div class="text-right" style="float:left;width:50%;">                                    

                                <h1 class="estimate" style="text-transform:capitalize">{{ $invoice_title }}</h1>
                                    <span class="invoice-no">Invoice No: 001-{{ str_pad($details->invoice_id, 4, '0', STR_PAD_LEFT) }}</span><br />
                                    <div class="ad-box" style="width:300px;float:right;">
                                        <small style="display: block;float: left;height: 53px;line-height:53px;width: 130px;text-align: center;">ACCOUNT DUE:</small>
                                        <span style="display:block;float:left;height:53px;line-height:53px;width:145px;text-align:center">${{ number_format(floatval($details->total), 2, '.', ',') }}</span>
                                    </div>
                                 <!--    <div class="payment-methods">
                                        <span class="p1">PAYMENT METHODS</span>
                                        <hr />
                                        <span class="p2">Paypal: </span><span class="p3">payment@germbusters911.com</span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="af-review-content">
                            <div class="row">
                                <div class="head-col">
                                    <div class="rcl">Description</div>
                                    <div class="rcr">Total</div>
                                </div>
                                <div class="" style="float:left;width:100%">
                                    <hr class="af-div" />
                                </div>                                
                                <div class="body-col">
                                    <div class="rcl">
                                        <span>General description</span>
                                        <p>{{ $details->general_desc }}</p>
                                    </div>
                                    <div class="rcr">${{ number_format($details->general_price, 2, '.', ',') }}</div>
                                </div>
                                <div class="" style="float:left;width:100%">
                                    <hr class="af-div-mid" />
                                </div>

                                @if(isset($invoice_items)) 
                                    @foreach($invoice_items as $value) 
                                        <div class="body-col">
                                            <div class="rcl">
                                                <span>{{ $value->title }}</span>
                                                <p>{{ $value->description }}</p>
                                            </div>
                                            <div class="rcr">
                                                ${{ number_format($value->price, 2, '.', ',') }}
                                            </div>
                                        </div>
                                        <div class="" style="float:left;width:100%">
                                            <hr class="af-div-mid" />
                                        </div>
                                    @endforeach
                                @endif

                                <div class="body-col">
                                    <div class="rcl">
                                        <span>Disclaimer </span>
                                        <p>{{ $details->description_disclaimer }}</p>
                                    </div>
                                    <div class="rcr">&nbsp;</div>
                                </div>
                                <div class="mt-30" style="float:left;width:100%">
                                    <hr class="af-div" />
                                </div>
                                <div class="mt-20 totals">
                                    <div class="rcl">
                                        &nbsp;
                                    </div>
                                    <div class="rcr">
                                        <div class="text-right">
                                            <span class="st1">SubTotal: </span><span class="st2">${{ number_format($details->subtotal, 2, '.', ',') }}</span>
                                        </div>
                                        <div class="text-right">
                                            <span class="st1">Discount: </span><span class="st2">${{ number_format($details->discount, 2, '.', ',') }}</span>
                                        </div>
                                        @if($details->promo_code != '')
                                            <div class="text-right">
                                                <span class="st1">Promo: </span><span class="st2">${{ number_format($details->promo, 2, '.', ',') }}</span>
                                            </div>
                                        @endif
                                        <div class="text-right">
                                            <span class="st1">Total: </span><span class="st2">${{ number_format($details->total, 2, '.', ',') }}</span>
                                        </div>
                                        <hr class="af-div" style="margin-left: 30%;" />
                                        <div class="text-right">
                                            <span class="st1">PAID: </span><span class="st2">$0.00</span>
                                        </div>
                                        <div class="text-right">
                                            <span class="st1">DUE: </span><span class="st2">${{ number_format(floatval($details->total), 2, '.', ',') }}</span>
                                        </div>
                                         @if($type != "commercial" && $type != "residential")
                                         <div class="d-flex justify-space-between text-right">
                                            <span class="st1">
                                                {{ $details->brand }} - {{ date_format(date_create($details->date_paid),"m/d - y") }} :
                                            </span><span class="st2">
                                                ${{ number_format(floatval($details->amount), 2, '.', ',') }} 
                                            </span>
                                        </div>
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">Remaining Invoice Balance:</span><span class="st2">
                                                @if(strtolower($details->status) == 'success')
                                                    $0.00
                                                @else
                                                    ${{ number_format($details->total, 2, '.', ',') }}
                                                @endif
                                            </span>
                                        </div>
                                        @endif
                                    </div>
                                </div>                                
                            </div>
                        </div>     
                        <div class="page-break-after">
                        <div class="af-review-footer border" style="margin-top: 40px" >                        
                            <div>
                                <h5>Payment Address</h5>
                                <p>1905 NW 32 Street,Building 5<br />
                                    Pompano Beach, FL 33064 </p>
                                <h5>Terms &amp; Condtions</h5>    
                                <p>{{ $details->terms_conditions }}</p>
                            </div>
                        </div>                   
                        <div style="text-align:center;">
                            <img src="{{$sig}}" class="signature-pad" style="width:700px;height:150px;border:0;outline:0" />                            
                            {{-- <canvas id="signature-pad" class="signature-pad" width=700 height=150></canvas> --}}
                            <p class="sign-here">Signature</p>                            
                        </div>
                    </div>
                    </div>                                                                                               

    </body>

</html>