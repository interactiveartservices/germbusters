@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('content')

 <!--Start Banner-->   
<div class="sub-banner">  
	<picture>
		<source media="(max-width: 560px)" srcset="/images/services-banner-cart-mobile@2x.png">
		<source media="(min-width: 561px)" srcset="/images/services-banner-cart.png 1x, /images/services-banner-cart@2x.png 2x">
		<img class="banner-img" src="/images/services-banner-cart.png" alt="services">
	</picture>
	<div class="bn-text">
		<img src="/images/uv@2x.png" alt="UV disinfection cart" />
		<h1>UV Light Disinfection Cart</h1>
	</div>

</div>	   
<!--End Banner-->

<!--Start Doctor Quote-->
<div class="dr-quote position-relative overflow-hidden bg-none">	
	<div class="visible-lg-block" style="padding:46.56% 0 0 0;position:relative;">
		<iframe src="https://player.vimeo.com/video/424748053?autoplay=1&loop=1&color=ffffff&portrait=0&background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
	</div>
	<script src="https://player.vimeo.com/api/player.js"></script>
	<div class="hidden-lg">
		<img class="banner-uvc-img" src="/images/bg-uvc-banner2-mobile.png" alt="services">		
	</div>
	<div class="bg-svc-vid-gradient"></div>
	<div class="container center-absolute">
		<div class="row">
			<div class="col-md-12">
				<div class="svc-vid w-600 margin-auto">
					<h2 class="quote">Go Beyond Clean</h2>
					<span class="name">Clinical grade disinfection using the power of light</span>
					<ul class="text-left text-white">
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Proven, maintenance-free true UVC germicidal performance</span></li>
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>On-demand operation to fit your needs and cleaning procedures</span></li> 
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Verified Kill Rates up to 99.9%</span></li>
						<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Affordable, aerosol free disinfection solutions</span></li>
					</ul>
					<a href="#" class="btn btn-yellow" onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
				</div>
			</div>
		</div>
	</div>		
</div>
<!--End Doctor Quote-->   
	
<div class="container-fluid bg-white text-center py-30"> 
	<div class="row">
		<div class="col-sm-12">
			<h6 class="text-teal font-weight-bold">Hire us, purchase or lease</h6>
			<div class="main-title mt-20"><h2><span>Manual</span> UVC disinfectant cleaning machine </h2>
				<small class="font-raleway">Designed to be operated by every-day cleaning staff</small>	
			</div>
			
		</div>
	</div>
</div>   

<div class="bg-black position-relative">
	<picture class="pic uvdr-pic">		
		<source media="(max-width: 560px)" srcset="/images/BLACK-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/BLACK.png 1x, /images/BLACK@2x.png 2x">
		<img class="banner-img" src="/images/BLACK.png" alt="services bg" style="max-height: 850px;">
	</picture>
	<div class="container position-absolute content uvdr-ca">		
		<div class="row">
			
			<div class="col-lg-10 svc-items position-relative">
				<img class="width-auto mw-100p" src="/images/uv-cart.png" alt="uv light disinfectant cart" />

				<ul class="position-absolute text-white ul1">
				<li><i class="icon-checkmark4 text-sky mr-20"></i><span>Sanitize an entire room in about 10 minutes  </span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Maximize disinfection by aligning  anti-viral panels to <br>any angled surface from 0-90° degrees</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Easy maneuverability & wheel <br>stability control</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Effortlessly configured adjustable UVC panels <br> to a maximum height of 7’ feet</span></li>
				</ul>	

				<ul class="position-absolute text-white ul2">
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>lightweight frame makes <br>pushing the cart a breeze</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20"></i><span>Protective shield safety blocks <br>UVC rays from cleaning technician</span></li>
					<li><i class="icon-checkmark4 text-sky mr-20"></i><span>600 watts of disinfection power</span></li>
				</ul>	
			</div>

			<div class="col-lg-2">
				
			</div>

			<div class="col-sm-12 text-center">
				<a href="#" class="btn btn-yellow mt-0"  onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
			</div>
		</div>
		
	</div>
</div>


<div class="bg-dark-blue text-white">
	<div class="container kz">
		<div class="row">
			<div class="col-sm-12">
				<div class="kz-row d-flex">
					<div class="kz-row-l">
						<h2>Room Bacteria Killer</h2>
						<div class="main-title mb-20">
							<h3 class="text-white">How close to the surface <span class="font-weight-light">do the UVC lamps need to be to kill germs?</span></h3>
						</div>
						<p class="font-raleway line-height-1-2 mb-20">High intensities for a short period or low intensities for a long period <br />
							are fundamentally equal in lethal action on pathogens.</p>
						<ul class="text-left text-white">
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>6 inches = </span>3 seconds</li>
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>3 feet = </span>25-45 seconds</li> 
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>6 feet = </span>2-3 minutes</li>
							<li><i class="icon-checkmark4 text-sky mr-20"></i><span>10 feet = </span>10-15 minutes</li>
						</ul>
						<small class="font-italic">Calculations based on (1) 40W with the intensity of approximately 110-130 μw/cm2 at 3.3 feet distance for a dosage of 6.6-7.8 mJ/minute </small>
					</div>
					<div class="kz-row-r">
						<img src="/images/cart.png" alt="cart" />
					</div>
				</div>				
			</div>
		</div>		
	</div>	
</div>

<div class="bg-light-gray py-40">
	<div class="container gz-tiles">

		<div class="row">

			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-right">
				<div class="bg flap">
					<span class="txt">Effortlessly adjust germ-zapping <br>
				panels to align to any table surface, <br>
				maximizing disinfection times.</span>
				</div>
				<img src="/images/cart-pushcart.png" alt="push cart" class="gz-img" />
			</div>
	
			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-left">
				<img src="/images/cart-Casino_Cart_V01.png" alt="casino cart" class="gz-img"  />
				<div class="bg flap">
					<span class="txt">300 watt anti-viral panels <br>
				can be adjusted at any angle<br>
				To provide a superior sanitizing <br>
				effect on any surface.</span></div>
			</div>
	
			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-right">
				<div class="bg flap">
					<span class="txt">Sterilizer™ panels can extend to a <br>
				height over 7’ feet too cover <br>
				tall aisle ways. Disinfect chemical <br>
				sensitive surfaces- safety & effectively.</span>
				</div>
				<img src="/images/cart-grocery.png" alt="grocery cart" class="gz-img" />
			</div>

			<div class="col-sm-12 gz-tile d-flex py-40 justify-content-left">
				<img src="/images/cart-gym.png" alt="gym cart" class="gz-img" />
				<div class="bg flap">
					<span class="txt">Neutralize the most dangerous <br>
				pathogens in seconds, including<br>
				MRSA, Infleunza, C.difficile <br>
				SARS- CoV. </span> 
				</div>
			</div>

			<div class="col-sm-12 text-center pb-60">
				<a href="#" class="btn btn-yellow mt-0"  onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
			</div>

		</div>

	</div>
</div>


<div class="bg-white">
	<div class="container pt-60">
		<div class="row">
			<div class="col-lg-6">
				<div class="main-title mb-20">
					<h2 class="text-teal-blue"><span class="text-dark">Does UV-C really work?</span><br /> Seeing is believing</h2>
				</div>
				<p class="mb-20">UVC dosimeter&trade; cards and stickers are industry standard tools used to 
				scientifically verify if surfaces are getting enough UV-C exposure to kill  your
				targeted pathogens.   They are placed in the treated areas and use a 
				photo-chromatic ink that changes color at various energy levels, which 
				correlates with a log reduction of pathogens.</p>
				<p>The UVC dosimeter&trade; test is offered as an optional test before a disinfection
				service begins.  Our results are scientifically verified-backed with a no-risk guarantee.</p>
			</div>
			<div class="col-lg-6 pl-30 text-center uvc-vid">				
				<a class="fancybox-media video-icon"  href="https://www.youtube.com/watch?v=xhlP3iNgPww"><img class="width-auto mw-430" src="/images/uvc-play.png" alt="UV-C Play Video" /></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 mt-30 font-size-18 font-raleway">
				Germbuster’s UV-C bulbs have been independently tested that our fixtures kill up to<br /> 
				<span class="font-weight-bold">99.9% of the following pathogens at distances up to 9 feet.</span>
			</div>		
		</div>
		<hr style="border-top:3px solid #707070;" />
		<div class="row uvc-list">
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">SARS-CoV</h4>
				<div class="d1">
					<img src="/images/icon-corona.jpg" />
					<span class="">The Corona family of viruses that includes the one that causes COVID-19 — can live on some of the surfaces you probably touch every day</span>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">Influenza</h4>
				<div class="d1">
					<img src="/images/icon-influenza.jpg" />
					<span class="">Both influenza A and B viruses survived for 24-48 hr on hard, nonporous surfaces 
						such as stainless steel and plastic </span>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">MRSA/Staph</h4>
				<div class="d1">
					<img src="/images/icon-staph.jpg" />
					<span class="">Methicillin-resistant Staphylococcus aureus (MRSA) can survive on some surfaces, like towels, razors, furniture, and athletic equipment for hours, days, or even weeks.   </span>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="bg-teal uvdd-gz auto-c pt-60 position-relative">
	<div class="bg-white-filler"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 text-white">
				<p class="line-height-1-5 font-raleway font-weight-bold font-size-20" style="padding-top: 20%;">
				The Sterilizer™ can be used for <br>
				hospital-grade disinfection of surfaces in schools, <br>
				homes, commercial facilities, nursing homes, <br>
				hospitals, etc.  Its adjustable 300 watts panels <br>
				can align to any surface providing  a superior disinfection<br>
				cleaning. 

				</p>
			</div>
			<div class="col-md-6 p-0">
				<img src="/images/cart2.png" alt="sterilizer disinfection" style="max-width: 500px;" />
			</div>
		</div>
	</div>
</div>
			

<div id="GetQuote" class="bg-white py-60 contact-f">
	<div class="container">
		<div class="row">

			<div class="main-title text-center">
				<h2 class="my-30 font-size-40">Learn more about Sterilizer™ <br>disinfectant UVC cart</h2>
				<p class="line-height-1-2">
					Step into the next generation of UV-C disinfection  technology? Please provide your contact details <Br />
					and a GermBuster account specialist will help you take the first step in discovering the power of UV-C.  <br />
					Or you can call 954-761-2480 to talk to an expert now.
				</p>
			</div>

			<div class="form">
				<form name="frmContact" id="frmContact" method="post">	
					<input type="hidden" id="frmPage" value="UV Light Disinfection Cart">
					<div class="row">

							<div class="col-md-12">
								<label>Choose your industry</label>
								<select name="industry" id="industry" class="">
									<option value="- Select -">- Select -</option>
									<option value="Accounting &amp; Financial">Accounting &amp; Financial</option>
									<option value="Agriculture">Agriculture</option>
									<option value="Animal &amp; Pet">Animal &amp; Pet</option>
									<option value="Architectural">Architectural</option>
									<option value="Art &amp; Design">Art &amp; Design</option>
									<option value="Attorney &amp; Law">Attorney &amp; Law</option>
									<option value="Automotive">Automotive</option>
									<option value="Bar &amp; Nightclub">Bar &amp; Nightclub</option>
									<option value="Business &amp; Consulting">Business &amp; Consulting</option>
									<option value="Childcare">Childcare</option>
									<option value="Cleaning &amp; Maintenance">Cleaning &amp; Maintenance</option>
									<option value="Communications">Communications</option>
									<option value="Community &amp; Non-Profit">Community &amp; Non-Profit</option>
									<option value="Computer">Computer</option>
									<option value="Construction">Construction</option>
									<option value="Cosmectics &amp; Beauty">Cosmectics &amp; Beauty</option>
									<option value="Dating">Dating</option>
									<option value="Education">Education</option>
									<option value="Entertainment &amp; The Arts">Entertainment &amp; The Arts</option>
									<option value="Environmental">Environmental</option>
									<option value="Fashion">Fashion</option>
									<option value="Floral">Floral</option>
									<option value="Food &amp; Drink">Food &amp; Drink</option>
									<option value="Games &amp; Recreational">Games &amp; Recreational</option>
									<option value="Home Furnishing">Home Furnishing</option>
									<option value="Industiral">Industiral</option>
									<option value="Internet">Internet</option>
									<option value="Landscaping">Landscaping</option>
									<option value="Medical &amp; Pharmaceutical">Medical &amp; Pharmaceutical</option>
									<option value="Photographiy">Photographiy</option>
									<option value="Physical Fitness">Physical Fitness</option>
									<option value="Political">Political</option>
									<option value="Real Estate &amp; Mortgage">Real Estate &amp; Mortgage</option>
									<option value="Religious">Religious</option>
									<option value="Restaurant">Restaurant</option>
									<option value="Retail">Retail</option>
									<option value="Security">Security</option>
									<option value="Spa &amp; Esthetics">Spa &amp; Esthetics</option>
									<option value="Sport">Sport</option>
									<option value="Technology">Technology</option>
									<option value="Travel &amp; Hotel">Travel &amp; Hotel</option>
									<option value="Wedding Service">Wedding Service</option>
								</select>
							</div>

							<div class="col-md-12 mt-20 required">
								<label>First name*</label>
								<input type="text" data-delay="300" name="firstname" id="firstname" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Last name*</label>
								<input type="text" data-delay="300" name="lastname" id="lastname" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12">
								<label>Job Title</label>
								<input type="text" data-delay="300" name="jobtitle" id="jobtitle" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Email Address</label>
								<input type="text" data-delay="300" name="email" id="email" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Phone*</label>
								<input type="text" data-delay="300" name="phone" id="phone" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12">
								<label>What would you like to know?</label>
								<textarea name="message" id="message" class="textarea medium" aria-invalid="false" rows="10" cols="50"></textarea>
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-1 cb">
								<label>
								<input name="notify" type="checkbox" value="I would like to be notified by email of future case studies, white papers, webinars and other educational content" checked="checked" id="notify">
								&nbsp;&nbsp;I would like to be notified by email of future case studies, white papers, webinars and other educational content</label>
							</div>

							<div class="col-md-12 mb-30">
							<div class="g-recaptcha" data-sitekey="6LeAfrMZAAAAAKOCyQ1JkBRMlIXW6PTpzpq8Zusy"></div>
							</div>

					</div>

			    </form>

			    <div><button name="btnGetQuote" id="btnGetQuote">Submit</button></div>		
			</div>		


		</div>
	</div>
</div>

@endsection
@section('footer_scripts')

<script src="/js/library.js" type="text/javascript" ></script>
<script type="text/javascript" >
jQuery(document).ready(function($) {

	jQuery('.fancybox').fancybox({
		padding: 0
	});

	jQuery('.fancybox-media').fancybox({
				padding:0,
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					overlay:{ 
						css: {background:'rgba(0,0,0,0.75)', 'z-index':99998}
					},
					media : {},
					padding:0
				}
			});

   

});
</script>
@endsection

