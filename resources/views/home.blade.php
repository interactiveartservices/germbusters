@extends('layouts.master')

@section('content')
    <div id="home-overlay-main">
        <img src="/images/germzapper/video-overlay@1x.jpg" />
        <div class="visible-lg-block" style="padding:56.25% 0 0 0;position:relative;">
            <iframe id="vid" src="https://player.vimeo.com/video/453268714?autoplay=1&loop=1&color=ffffff&portrait=0&background=1&muted=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        <script src="https://player.vimeo.com/api/player.js"></script>

        <div class="hidden-lg">            
            <div id="home-slider" class="">
                <div class="slide-row slide1">
                  <picture>
                        <source media="(max-width: 600px)" sizes="100vw" srcset="/images/slides/banner1-mobile.png 1x, /images/slides/banner1-mobile@2x.png 2x">
                        <source media="(min-width: 601px)" sizes="100vw" srcset="/images/slides/banner1-tablet.png 1x, /images/slides/banner1-tablet@2x.png 2x">
                        <img class="img-responsive" src="/images/slides/banner1-tablet.png " alt="banner 1">
                  </picture>
                </div>
                <div class="slide-row slide2">
                    <picture>
                          <source media="(max-width: 600px)" sizes="100vw" srcset="/images/slides/banner2-mobile.png 1x, /images/slides/banner2-mobile@2x.png 2x">
                          <source media="(min-width: 601px)" sizes="100vw" srcset="/images/slides/banner2-tablet.png 1x, /images/slides/banner2-tablet@2x.png 2x">
                            <img class="img-responsive" src="/images/slides/banner2-tablet.png " alt="banner 2">
                    </picture>
                </div>
                <div class="slide-row slide3">
                    <picture>
                          <source media="(max-width: 600px)" sizes="100vw" srcset="/images/slides/banner3-mobile.png 1x, /images/slides/banner3-mobile@2x.png 2x">
                          <source media="(min-width: 601px)" sizes="100vw" srcset="/images/slides/banner3-tablet.png 1x, /images/slides/banner3-tablet@2x.png 2x">
                            <img class="img-responsive" src="/images/slides/banner3-tablet.png " alt="banner 3">
                    </picture>
                </div>  
                <div class="slide-row slide4">
                    <picture>
                          <source media="(max-width: 600px)" sizes="100vw" srcset="/images/slides/banner4-mobile.png 1x, /images/slides/banner4-mobile@2x.png 2x">
                          <source media="(min-width: 601px)" sizes="100vw" srcset="/images/slides/banner4-tablet.png 1x, /images/slides/banner4-tablet@2x.png 2x">
                            <img class="img-responsive" src="/images/slides/banner4-tablet.png " alt="banner 4">
                    </picture>
                </div>  
                <div class="slide-row slide5">
                    <picture>
                          <source media="(max-width: 600px)" sizes="100vw" srcset="/images/slides/banner5-mobile.png 1x, /images/slides/banner5-mobile@2x.png 2x">
                          <source media="(min-width: 601px)" sizes="100vw" srcset="/images/slides/banner5-tablet.png 1x, /images/slides/banner5-tablet@2x.png 2x">
                            <img class="img-responsive" src="/images/slides/banner5-tablet.png " alt="banner 5">
                    </picture>
                </div>  
                <div class="slide-row slide6">
                    <picture>
                          <source media="(max-width: 600px)" sizes="100vw" srcset="/images/slides/banner6-mobile.png 1x, /images/slides/banner6-mobile@2x.png 2x">
                          <source media="(min-width: 601px)" sizes="100vw" srcset="/images/slides/banner6-tablet.png 1x, /images/slides/banner6-tablet@2x.png 2x">
                            <img class="img-responsive" src="/images/slides/banner6-tablet.png " alt="banner 6">
                    </picture>
                </div>      
            </div>            
        </div>

        <!--  <div class="visible-lg">
            <a id="audio-toggle" href="#" class="audio-off" >
                <img class="toggle-sound-on width-auto" src="/images/audio-on.png" alt="audio on">
                <img class="toggle-sound-off width-auto" src="/images/audio-off.png" alt="audio off">
            </a>
        </div> -->
                        
    </div>
    
   <!--Start Banner-->
   
   <div class="tp-banner-container overflow-hidden">
        <div class="tp-banner" >
            <ul>    <!-- SLIDE  -->
            
    
    <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
        <!-- MAIN IMAGE -->
        {{-- <div class="tp-caption sfl fadeout fulllscreenvideo tp-videolayer"
            data-x="0"
            data-y="0"
            data-speed="600"
            data-start="1000"
            data-easing="Power4.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeOut"
            data-autoplay="true"
            data-autoplayonlyfirsttime="true"
            data-nextslideatend="true"
            style="z-index: 8"
            data-vimeoid="435170393"
            data-videoattributes="enablejsapi=1&html5=1&hd=1&wmode=opaque&showinfo=0&rel=0"
            data-videocontrols="controls"
            data-videowidth="100%"
            data-videoheight="100%">
        </div> --}}
        <img src="/images/slides/banner1-clear.png" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
        <!-- LAYERS -->

        
        <div class="tp-caption thin_light_27 customin tp-resizeme rs-parallaxlevel-0"
            data-x="0"
            data-y="220" 
            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="500"
            data-start="1400"
            data-easing="Back.easeOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">Protect your customers and employees
        </div>
        
        
        <div class="tp-caption light_bold_60 customin tp-resizeme rs-parallaxlevel-0"
            data-x="0"
            data-y="250" 
            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="500"
            data-start="1400"
            data-easing="Back.easeOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            style=""><h1 style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;font-size:52px;font-weight: 700;margin:0;">Expert Deep Disinfecting<br />
            Cleaning To Fight Coronavirus</h1>
        </div>                    

      <!--   <div class="tp-caption quote_links customin tp-resizeme rs-parallaxlevel-0"
            data-x="0"
            data-y="400" 
            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="500"
            data-start="1800"
            data-easing="Back.easeOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
            <div class="top-part">                
                <a href="#" id="quote-residential-btn" class="qbs selected quote-residential-btn">
                    <span class="tp1">Residential</span>
                </a>                
                <a href="#" class="qbs deselected quote-commercial-btn" id="quote-commercial-btn">
                    <span class="tp2">Commercial</span>
                </a>
            </div>
            <div class="bottom-part">
                <a id="schedule" class="schedule" href="#" data-toggle="modal" data-aos="fade-up">Get quote &amp; Schedule now</a>
            </div>
        </div>                     -->
        
    </li>
    
    
</ul>
<div class="tp-bannertimer"></div>  </div>
</div>


   
   <!--End Banner-->
   
   
   <!--Start Content-->
   <div class="content">


    <div class="container-fluid text-center"> 
        <div class="row">
            <div class="col-sm-12">
                <div class="mt-50 mb-30"><h2>A brighter way to disinfect at <span class="text-teal font-weight-bold">home</span> or work</h2>
                </div>
            </div>
        </div>
    </div>     


    <!-- TRUSTED BY -->
    <div id="trustby">
      <div class="container no-margin no-padding">
        <div class="row trustby-cont">
          <span class="d-block font-raleway text-center">
            TRUSTED BY
          </span>
          <div class="brand-logos">
            <div class=""><img src="/images/brands/us-air-force.png" alt="US Air Force" class="width-auto"></div>
            <div class=""><img src="/images/brands/potter-and-company.png" alt="Potter and Company" class="width-auto"></div>
            <div class=""><img src="/images/brands/womein-in-distress.png" alt="Women in Distress" class="width-auto"></div>
            <div class=""><img src="/images/brands/southern-spirits.png" alt="Southern Spirits" class="width-auto"></div>
            <div class=""><img src="/images/brands/bc.png" alt="bc" class="width-auto"></div>
            <div class=""><img src="/images/brands/lolo-school.png" alt="lolo school" class="width-auto"></div>
            <div class=""><img src="/images/brands/avis.png" alt="avis" class="width-auto"></div>
          </div>
        </div>
      </div>
    </div>
   
   <!--Start Services-->
   <div class="services-two">
            <div class="container">
                <div class="row position-relative">
                    
                  <div class="col-md-6">
                    <div class="main-title">   
                    <h2><span>We got your cleaning service </span><br />down to a science</h2>
                    <p class="line-height-1-2"> ATP testing is industry standard in the healthcare and food manufacturing industry. 
                        Now Germbusters offers everyone an optional ATP test before and after our disinfection 
                        service.  So results are scientifically proven- backed with a no-risk guarantee.  </p>
                   </div>
                        
                        <div class="service-sec">
                            
                            <div class="icon">
                                <img src="/images/ico-corona.png" alt="coronavirus" class="img-responsive" />
                            </div>
                            
                            <div class="detail">
                                <h5 class="text-blue">Coronavirus control</h5>
                                <p>The Corona family of viruses that includes the one that causes COVID-19 &mdash; can live on some of the surfaces you probably touch every day</p>
                            </div>
                            
                        </div>
                        <div class="service-sec">
                            
                            <div class="icon opacity-80">
                                <img src="/images/ico-flu.png" alt="influenza" />
                            </div>
                            
                            <div class="detail">
                                <h5 class="text-blue">Influenza control</h5>
                                <p>Both influenza A and B viruses survived for 24-48 hr on hard, nonporous surfaces such as stainless steel and plastic </p>
                            </div>
                            
                        </div>

                        <div class="service-sec">
                            
                            <div class="icon opacity-60">
                                <img src="/images/ico-staph.png" alt="Staph control" />
                            </div>
                            
                            <div class="detail">
                                <h5 class="text-blue">Staph control</h5>
                                <p>Methicillin-resistant Staphylococcus aureus (MRSA) can survive on some surfaces, like towels, razors, furniture, and athletic equipment for hours, days, or even weeks.  </p>
                            </div>
                            
                        </div>
                        
                    </div>                                        
                 
                    <div id="money-back-guarantee" class="col-md-6 position-relative">
                        <img class="width-auto" src="/images/temp-scanner2.png" alt="temp-scanner" />

                        <div class="mbg">
                            <h2><span>MONEY BACK </span>GUARANTEE <span>100% RISK FREE</span></h2>
                            <p>We’ll leave your home/facility with a hospital 
                                Rating of &gt; 100. Or your money back.</p>
                        </div>
                                            
                    </div>
                    
                    
                    <div class="col-12 foot">
                        ATP testing has been used by hospitals  for years to help them minimize spread of healthcare acquired infections
                    </div>
                </div>
            </div>
        </div>
   <!--End Services-->
   
   
    <!--Start Doctor Quote-->
    <div class="dr-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="quote">Give your customers peace of mind</h2>
                    <span class="name">Our scientifically verified  results can be proudly displayed differentiating <br />
                        your facility as one that goes the extra mile to limit the spread of germs and <br />
                        protect the wellness of your patrons and employees </span>
                </div>
            </div>
        </div>
    </div>
<!--End Doctor Quote-->
   
   <!--Start Welcome-->
    <div class="welcome-two">
        <div class="container">
                        
            <div class="welcome-detail">
                <div class="row">
                    
                    <div class="col-md-5">
                        <img src="images/deep-disinfectant.png" alt="Deep Disinfectant Cleaning Services">
                    </div>
                    
                    <div class="col-md-7">
                        <div class="detail">
                          
                         <div class="main-title">   
                          <h2><span>Commercial </span>Deep Disinfectant <span>Cleaning Services</span></h2>                        
                          <p> Rely on Germbusters licensed and certified technicians to deep clean, sanitize and disinfect your commercial space. 
                          Trust Germbusters to to be your partner with:</p>
                         </div>

                            <ul>
                                <li><span>EPA certified products </span></li>
                                <li><span>Cost-saving maintenance programs</span></li>
                                <li><span>3rd party testing after cleaning</span></li>
                                <li><span>Licensed. Insured. Certified</span></li>
                                <li><span>No-Risk Money back guarantee  </span></li>
                                <li><span>Dedicated GermBuster Account Executive</span></li>
                                <li><span>After-hours/weekend scheduling</span></li>
                                <li><span>Advanced UV-C Sanitization kills 99% germs</span></li>
                            </ul>                        
                            
                        </div>
                    </div>

                   <!--  <div class="text-center">
                       <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#modalGetQuote" data-aos="fade-up">SCHEDULE YOUR DEEP CLEANSING SERVICE TODAY</a>
                    </div> -->

                </div>
            </div>
            
        </div>
    </div>
   <!--End Welcome-->

   <!--Start Services Three-->
   <div class="services-three d-flex">
        <div class="serv-sec">
            <h2><img class="width-auto" src="/images/icon-uvc.png" alt="uv-c" /> UVC Light Disinfection</h2>
            <p class="font-size-18">Germicidal ultraviolet radiation is lethal to a wide range of infectious microorganisms including staph (bacteria),  influenza, bacteria, mold, and virus</p>
            <a href="/uvc-light">learn more</a>
        </div>
        
        <div class="serv-sec serv-sec2">
            <h2><img class="width-auto" src="/images/icon-spray.png" alt="biological spray" /> Electrostatic Disinfection</h2>
            <p class="font-size-18">We use specialty chemical disinfectants that provide long lasting protection against bacteria & viruses by reducing presence & growth of microbe populations by up to 99.99%.</p>
            <a href="/disinfection-services">learn more</a>
        </div>
        
        <div class="serv-sec serv-sec3">
            <h2><img class="width-auto" src="/images/icon-purifier.png" alt="air purifier" /> UVC HVAC</h2>
            <p class="font-size-18">GermBusters air purification systems eliminate more than 99.9% of bacteria, viruses, dust, allergens, chemicals and odors. Go beyond surface clean to create a better environment in your facility.</p>
            <a href="/uvc-hvac">learn more</a>
        </div>
        
        
   </div>
   <div class="clear"></div>
   <!--End Services Three-->

    <!--Start Welcome-->
    <div class="welcome-two dark-back">
        <div class="container">
                 
            <div class="call">
                <div class="row">
                    
                    <div class="col-md-7">
                        <img src="images/gb-van.png" alt="Germ busters same day Services">
                    </div>
                    
                    <div class="col-md-5">
                        <div class="detail">
                          
                         <div class="main-title">
                          <h2><span class="med">When you know its bad & it doesn’t look good </span>  
                          Who you gonna call? </h2>                        
                          <p> When an employee of family member is sick you want to disinfect the environment 
                            immediately.  Our prompt & professional team will eradicate germs in the air 
                            and on surfaces as fast as we can.  We even offer same day service!</p>
                         <hr/>
                         </div>
                         <h6>For same day service</h6>
                         <h5 id="ip_phone">954-761-2480</h5>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
   <!--End Welcome-->


   <!-- <div class="cdc"> 
    <h2><span> CDC recommends clean and disinfect</span> high-touch surfaces daily </h2>
    <a href="#" class="btn btn-yellow" data-toggle="modal" data-target="#modalGetQuote" data-aos="fade-up">LEARN ABOUT DEEP DISCOUNTS FOR WEEKLY CLEANING</a>
   </div>     -->
   
   <!--Start Latest News-->
   <div class="latest-news">
        <div class="container">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="main-title">
                        <h2>Get back to business<span> with GermBusters</span></h2>
                        <p>Whether you’re looking to disinfect a school, gym, office building, bank or any commercial space,
                            GermBusters can ensure the high level of sanitation needed to keep your facility clean. Let 
                            GermBusters be your front line against Infections with antimicrobial products that stop germs 
                            in their tracks and discourage superbugs from breeding.</p>
                    </div>
                </div>
            </div>
            
           
           
           
    <div id="latest-news">
        <div class="container">
          <div class="row">
            <div class="col-md-4 post item">
                    <h6 class="text-center">Office  buildings</h6>
                    <img class="lazyOwl" src="images/gb-office.png" alt="germ busters office buildings">
                    <!--    <div class="detail">
                        <img src="images/news-dr1.jpg" alt="">
                        <h4><a href="news-detail.html">Center for Medical</a></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit eros...</p>
                        <span><i class="icon-clock3"></i> Apr 22, 2016</span>
                        <span class="comment"><a href="news-detail.html"><i class="icon-icons206"></i> 5 Comments</a></span>
                    </div> -->
                </div>
            <div class="col-md-4 post item">
                    <h6 class="text-center">Schools</h6>
                <img class="lazyOwl" src="images/gb-schools.png" alt="germ busters schools">
            </div>
            
            <div class="col-md-4 post item">
                <h6 class="text-center">Sports Facilities</h6>
                <img class="lazyOwl" src="images/gb-sports.png" alt="germ busters sports">
            </div>
                                        
          </div>
        </div>

    </div>
    
    
            
        </div>
   </div>
   <!--End Latest News-->
   
   
   </div>

   @include('partials.get_quote')

</div>


   <!--End Content-->
@endsection

@section('footer_scripts')
<script type="text/javascript">
    
    jQuery(document).ready(function($) {

        setSite();

        function setSite() {
            var site = "desktop";
        
            if (jQuery( window ).width() > 768 && jQuery( window ).width() <= 1024 ) {
                site = "tablet-landscape";			
            } else if(jQuery( window ).width() > 480 && jQuery( window ).width() <= 768 ) {
                site =  "tablet-portrait";			
            } else if(jQuery( window ).width() <= 480 ) {
                site = "mobile";
            }
            jQuery("body").removeClass("desktop tablet-landscape tablet-portrait mobile").addClass(site);
            mutePlayerOnMobile(site);
        }      
        
        jQuery(window).resize(function() {						  		  
			setSite();
	    });	        
           

        jQuery('.tp-banner').show().revolution(
        {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:745,
            hideThumbs:200,
            
            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,
            
            navigationType:"bullet",
            navigationArrows:"solo",
            navigationStyle:"preview4",
            
            touchenabled:"on",
            onHoverStop:"off",
            
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
                                    
                                    parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
                                    
            keyboardNavigation:"off",
            
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,

            soloArrowLeftHalign:"left",
            soloArrowLeftValign:"center",
            soloArrowLeftHOffset:20,
            soloArrowLeftVOffset:0,

            soloArrowRightHalign:"right",
            soloArrowRightValign:"center",
            soloArrowRightHOffset:20,
            soloArrowRightVOffset:0,
                    
            shadow:0,
            fullWidth:"on",
            fullScreen:"off",

            spinner:"spinner4",
            
            stopLoop:"off",
            stopAfterLoops:-1,
            stopAtSlide:-1,

            shuffle:"off",
            
            autoHeight:"off",						
            forceFullWidth:"off",						
                                    
                                    
                                    
            hideThumbsOnMobile:"off",
            hideNavDelayOnMobile:1500,						
            hideBulletsOnMobile:"off",
            hideArrowsOnMobile:"off",
            hideThumbsUnderResolution:0,
            
            hideSliderAtLimit:1025,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            startWithSlide:0,
            videoJsPath:"rs-plugin/videojs/",
            fullScreenOffsetContainer: ""	
        });
       
        var iframe = document.querySelector('iframe');
        var player = new Vimeo.Player(iframe);       

        
        player.on('loaded', hideOverlayImage);

        jQuery('#audio-toggle').click(function (e) {
            e.preventDefault();
            
            if($("#audio-toggle").hasClass("audio-on")) {
                $("#audio-toggle").addClass("audio-off").removeClass("audio-on");                                
                player.setVolume(0);
            } else {
                $("#audio-toggle").addClass("audio-on").removeClass("audio-off");
                player.setVolume(1);
            }            
        });

        function mutePlayerOnMobile(site) {
            var iframe = document.querySelector('iframe');
            var player = new Vimeo.Player(iframe);

            player.ready().then(function() {                    
                if(site == 'desktop') {
                    player.play();
                } else {
                    player.pause();
                }
            });
        }

        function hideOverlayImage() {
            $("#home-overlay-main > img").hide();
        }

        $('#home-slider').slick({
			arrows: false,
			dots:true,
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 1500,			
			
		});

        $('.brand-logos').slick({
            speed: 4000,
            autoplay: true,
            autoplaySpeed: 0,
            centerMode: true,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            infinite: true,
            initialSlide: 1,
            arrows: false,
            buttons: false
        });

                     
    });	//ready

    
    
</script>
    
        
@endsection
