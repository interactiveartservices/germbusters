@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style type="text/css">
	.work-text {
		display: none;
	}
	.visible {
		opacity: 1;
	}	
	.slider-filter {
		color: #222;
	}
	.slider-filter:hover, .slider-filter.active {
		color:#02adc6;
	}
	.work-body .tp-adjust-work-text {
		top: 320px!important;
	}

	.work-body .tp-adjust-work-btn1 {
		top: 470px!important;
	}

	.work-body .tp-adjust-work-btn2 {
		top: 495px!important;
	}

	.qt-text::before, .qt-text::after {
		content: '"';
		font-size: 140px;
		position: absolute;
		font-weight: bold;
		line-height: 50px;
		font-family: 'Open Sans';
	}

	.qt-text::before {		
		top: 0px;
		left: -25px;			
		transform: skewX(-15deg);
	}

	.qt-text::after {
		transform: skewX(-20deg) translateX(20%);
		bottom: -50px;
	}

	.gz-tile2 span {		
		margin-left: 50%;
		transform: translateX(-50%);
	}

	.uvdr-ca {top: 26%; left: 47%;}



	@media all and (max-width: 560px) {
		#models {
			padding: 0 15px;
		}
		.gz-model {
			padding: 0px;
		}

		.qt-text {			
			font-size: 16px !important;
			margin: 0 20px!important;
			letter-spacing: 0px;
		}	
		.qt-text::before, .qt-text::after {
			font-size: 80px;
			display: none;
		}
		.qt-text::before {		
			top: 20px;
		}
		.qt-bt > * {
			font-size: 15px!important;
		}

		.qt-bt > a > img {
			width: 40px;
		}

		.bg-uvddkz {
			margin: 0!important;
		}

		.uvdr-ca {
			position: static;
		}

		.svc-items > img {
			display: none;
		}

		.services .gz-tiles .gz-tile2 .txt {
			font-size: 16px;
			padding: 40px 0;
		}

		.gz-tile .o1 {
			order: 1
		} 

		.gz-tile .o2 {
			order: 2
		}

		
	}

</style>
@endsection

@section('content')
	<div id="home-overlay-main">		
		<div>            
			<div id="home-slider">
				<div class="slide-row slide1 home-slide">
					 <picture>
                        <source media="(max-width: 600px)" sizes="100vw" srcset="/images/germzapper/banner1-home-mobile@1x.jpg 1x, /images/germzapper/banner1-home-mobile@2x.jpg 2x">
                        <source media="(min-width: 601px)" sizes="100vw" srcset="/images/germzapper/banner1-home@1x.jpg 1x, /images/germzapper/banner1-home@2x.jpg 2x">
                        <img class="img-responsive" src="/images/germzapper/banner1-home@1x.jpg " alt="banner 1 home">
                  	</picture>
					<!-- <img src="/images/germzapper/banner1-home@1x.jpg" 
      					alt="banner 1 home" 
      					srcset="/images/germzapper/banner1-home@2x.jpg 2x">		 -->		
				</div>
				<div class="slide-row slide2 home-slide">
					 <picture>
                        <source media="(max-width: 600px)" sizes="100vw" srcset="/images/germzapper/banner2-home-mobile@1x.jpg 1x, /images/germzapper/banner2-home-mobile@2x.jpg 2x">
                        <source media="(min-width: 601px)" sizes="100vw" srcset="/images/germzapper/banner2-home@1x.jpg 1x, /images/germzapper/banner2-home@2x.jpg 2x">
                        <img class="img-responsive" src="/images/germzapper/banner2-home@1x.jpg " alt="banner 2 home">
                  	</picture>			
				</div>				
				<div class="slide-row slide3 work-slide">
					 <picture>
                        <source media="(max-width: 600px)" sizes="100vw" srcset="/images/germzapper/banner1-work-mobile@1x.jpg 1x, /images/germzapper/banner1-work-mobile@2x.jpg 2x">
                        <source media="(min-width: 601px)" sizes="100vw" srcset="/images/germzapper/banner1-work@1x.jpg 1x, /images/germzapper/banner1-work@2x.jpg 2x">
                        <img class="img-responsive" src="/images/germzapper/banner1-work@1x.jpg " alt="banner 1 work">
                  	</picture>
				</div>
				<div class="slide-row slide4 work-slide">
				 	<picture>
                        <source media="(max-width: 600px)" sizes="100vw" srcset="/images/germzapper/banner2-work-mobile@1x.jpg 1x, /images/germzapper/banner2-work-mobile@2x.jpg 2x">
                        <source media="(min-width: 601px)" sizes="100vw" srcset="/images/germzapper/banner2-work@1x.jpg 1x, /images/germzapper/banner2-work@2x.jpg 2x">
                        <img class="img-responsive" src="/images/germzapper/banner2-work@1x.jpg " alt="banner 2 work">
                  	</picture>				
				</div>	
				<div class="slide-row slide4 work-slide">
				 	<picture>
                        <source media="(max-width: 600px)" sizes="100vw" srcset="/images/germzapper/banner3-work-mobile@1x.jpg 1x, /images/germzapper/banner3-work-mobile@2x.jpg 2x">
                        <source media="(min-width: 601px)" sizes="100vw" srcset="/images/germzapper/banner3-work@1x.jpg 1x, /images/germzapper/banner3-work@2x.jpg 2x">
                        <img class="img-responsive" src="/images/germzapper/banner3-work@1x.jpg " alt="banner 3 work">
                  	</picture>				
				</div>				      
			</div>
		</div>
								
	</div>

 <!--Start Banner-->
   
   <div class="tp-banner-container overflow-hidden">
	<div class="tp-banner" >
     <ul>    <!-- SLIDE  -->
            
    
    <li class="home-body" data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">        
		<!-- LAYERS -->
		<img src="/images/slides/banner1-clear.png" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

		
		<div class="tp-caption xbold_white_55 customin tp-resizeme rs-parallaxlevel-0"
			data-x="0"
			data-y="150" 
			data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
			data-speed="500"
			data-start="1400"
			data-easing="Back.easeOut"
			data-splitin="none"
			data-splitout="none"
			data-elementdelay="0.1"
			data-endelementdelay="0.1"
			style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
			<span class="home-text">
			Say goodbye to<br />
			chemical residue</span>			
			<span class="work-text">
				On-demand,<br />
				hospital-grade <br />
				disinfection
			</span>

		</div>

        <div class="tp-caption medium_white_27 customin tp-resizeme rs-parallaxlevel-0 tp-adjust-work-text"
            data-x="0"
            data-y="270" 
            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="500"
            data-start="1400"
            data-easing="Back.easeOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
			style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
			<span class="home-text">
				UV light disinfection offers <br />
				a brighter way to clean <br />
				keyboards, toys, food surfaces, <br />
				even groceries-introducting  <br />
				GermZapper300&trade;
			</span>
			<span class="work-text">
				At 300 watts, GermZapper300&trade;<br />
				is the first commercial use UV <br />
				light disinfection device strong enough  <br />
				to disinfect the ICU but is now  <br />
				available to you.
			</span>
        </div>              

     	<div class="tp-caption customin tp-resizeme rs-parallaxlevel-0 tp-adjust-work-btn1"
            data-x="0"
            data-y="420" 
            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="500"
            data-start="1800"
            data-easing="Back.easeOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
			style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
				<a id="schedule" href="/germzapper-uv-light-disinfection-device/multipurpose-case" class="btn btn-teal sl-linkbtn text-dark" data-aos="fade-up">ORDER NOW</a>
		</div>
	  	
		<div class="tp-caption customin tp-resizeme rs-parallaxlevel-0 tp-adjust-work-btn2"
            data-x="0"
            data-y="445" 
            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
            data-speed="500"
            data-start="1800"
            data-easing="Back.easeOut"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
			style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">				
				<a id="schedule" href="/GermZapper_OZ-300.pdf" class="btn btn-outline sl-linkbtn text-white border-white" data-aos="fade-up" target="_blank">DOWNLOAD SPEC SHEET</a>            
        </div>
        
	</li>
	
	
    
    
</ul>
<div class="tp-bannertimer"></div></div> 
</div>  
   
<!--End Banner-->

<div class="text-center pt-40"> 
	<div class="row m-0">
		<div class="col-sm-12">
			<div class="main-title mb-20">
				<h2>A brighter way to disinfect at 
					<a href="#" id="slider-home" data-filter="home" class="slider-filter font-weight-bold active">home</a> or 
					<a href="#" id="slider-work" data-filter="work" class="slider-filter font-weight-bold">work</a>
				</h2>
			</div>
		</div>
	</div>
</div>     

<!-- TRUSTED BY -->
<div id="trustby">
  <div class="container">
    <div class="row trustby-cont">
      <span class="d-block font-raleway text-center">
        TRUSTED BY
      </span>
      <div class="brand-logos">
        <div class=""><img src="/images/brands/us-air-force.png" alt="US Air Force" class="width-auto"></div>
        <div class=""><img src="/images/brands/potter-and-company.png" alt="Potter and Company" class="width-auto"></div>
        <div class=""><img src="/images/brands/womein-in-distress.png" alt="Women in Distress" class="width-auto"></div>
        <div class=""><img src="/images/brands/southern-spirits.png" alt="Southern Spirits" class="width-auto"></div>
        <div class=""><img src="/images/brands/bc.png" alt="bc" class="width-auto"></div>
        <div class=""><img src="/images/brands/lolo-school.png" alt="lolo school" class="width-auto"></div>
        <div class=""><img src="/images/brands/avis.png" alt="avis" class="width-auto"></div>
      </div>
    </div>
  </div>
</div>

<div id="models">
	<div class="container pb-60">
	    <div class="row text-center">
	    	<div class="col-md-12 main-title mt-20"><h2>GermZapper 300 models</h2></div>
			<div class="col-md-6 pl-0 pr-10 gz-model">
				<div class="font-opensans text-left">
					<a href="/germzapper-uv-light-disinfection-device/handheld-case">
			    		<img src="/images/germzapper/gz-handheld-case.png" alt="GermZapper300 Handheld case" class="width-auto mw-100p">
			    		<div class="my-10  text-dark"><span class="font-weight-semibold line-height-1-5">GermZapper300</span> <span class="float-right">$2295.00</span></div>
			    		<span class="mt-10 text-lgray">Available Ozone + | Ozone free<br>Handheld Case</span>
		    		</a>
		    	</div>
		    </div>
		    <div class="col-md-6 pl-10 pr-0 gz-model">
		    	<div class="font-opensans text-left">
		    		<a href="/germzapper-uv-light-disinfection-device/multipurpose-case">
				    	<img src="/images/germzapper/gz-multiuse-case.png" alt="GermZapper300 Multiuse case" class="width-auto mw-100p">
				    	<div class="text-left my-10 text-dark"><span class="font-weight-semibold line-height-1-5">GermZapper300</span> <span class="float-right">$2395.00</span></div>
				    	<span class="mt-10 text-lgray">Available Ozone + | Ozone free<br>Multipurpose Case</span>
			    	</a>
		    	</div>
	    	</div>
	    	<a class="btn btn-yellow bg-neonteal text-white text-italic font-size-16 w-100">Feel free to touch what you want</a>
	    </div>

	    <div class="row text-center">
			<div class="col-md-4">
				<img src="/images/germzapper/icon-coronavirus.png" class="my-50 width-auto" alt="icon Coronavirus" />
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10">Free from Coronavirus </h4>
				<div class="d1">				
					<span class="">GermZapper300 provides 
					enough UV light to inactivate 
					Coronavirus (SARS-CoV2) in 3 seconds 
					6- inches away.</span>
				</div>
			</div>
			<div class="col-md-4">
				<img src="/images/germzapper/icon-expenses.png" class="my-50 width-auto" alt="icon expensses"/>
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10">Free from monthly expenses </h4>
				<div class="d1">				
					<span class="">No more monthly expenses to buying 
					disinfectant products- that is if they’re in stock.
					Never depend on anyone to sanitize items
					You use everyday.</span>
				</div>
			</div>
			<div class="col-md-4">
				<img src="/images/germzapper/icon-odors.png" class="my-50 width-auto" alt="icon odors" />
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10">Free from musty odors</h4>
				<div class="d1">				
					<span class="">UV light not only kills viruses and germs 
					but is equally effective against 
					bacteria, spores, mold and mildew</span>
				</div>
			</div>
			
		</div>
	<p class="py-60 text-teal text-italic text-center line-height-1-2">GermZapper OZ-300 is a ozone generating germicidal lamp that helps eradicate foul odor and harmful volatile organic <br>
				compounds (VOCs) such as sulfides, mercaptans and ammonia.</p>

</div>
</div>



<div class="bg-light-gray pb-60">
	<div class="container text-center pt-60 pb-40">		
		<div class="row">
			<h4>What you don’t know can kill you</h4>
			<p class="mw-700 line-height-1-2" style="margin:40px auto;">In this side-by-side demonstration the GermZapper goes up against the leading UV wand sold on Amazon.
			The GermZapper turned the germ meter card orange in :3 secs indicating SARS-Cov2 was killed.  
			What you don’t see is 1 hours 45 minutes later the UV wand  never changed the color of the 
			germ meter card, indicating wand killed 0% of any germs.
			 </p>
			
		</div>
		<img class="" src="/images/germzapper/gz-amazon.png" alt="germzapper indicator" />

		<a  href="https://uvcdosimeters.com/uvc-100-dosimeter/" target="_blank" class="btn btn-yellow" style="background: none;border: aliceblue;border: 3px solid #24DBE0;border-radius: 30px;">FIND OUT IF YOUR WAND WORKS</a>
	</div>
</div>

<div class="position-relative">
	<picture class="pic">		
		<source media="(max-width: 768px)" srcset="/images/germzapper/bg-mike-mobile.jpg">		
		<img class="width-100" src="/images/germzapper/bg-mike.png" alt="germzapper mike obrien" />
	</picture>	
	<div class="position-absolute d-flex align-center" style="top: 0;height: 100%;width:100%;background: rgba(50, 189, 209, 0.7);">
		<div class="text-white text-center position-relative" style="z-index: 1;width: 100%;">

			<p class="qt-text font-raleway font-size-46 letter-spacing-2 font-weight-medium line-height-1-2 position-relative" style="margin:0 20%;">
				After a kidney transplant I was at a much higher 
				risk of contracting Coronavirus.  I needed confidence
				I was getting hospital-grade disinfection in my home.
				The GermZapper provides that,..and with the germ cards 
				I can see that its working.
			</p>
			<div class="qt-bt">
				<a class="d-inline-block w-100 my-20 fancybox-media" href="https://youtu.be/2c-JDv5EDak"><img src="/images/play-btn.png" class="width-auto" alt="play video" /></a>
				<span class="font-opensans font-weight-bold font-size-24 mt-10">Mike O'Brien</span><br> 
				<span class="font-opensans font-size-24">Kidney Transplant Survivor</span>
			</div>
		</div>
	</div>	
</div>

<h4 class="pt-60 text-center">Room Bacteria Killer</h4>

<div class="bg-dark-blue text-white bg-uvddkz" style="margin:3% 10% 7%;">
	<div class=" uvdd kz">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-10">
				<div class="kz-row d-flex">
					<div class="kz-row-l">
						<div class="main-title mb-20">
							<h3 class="text-white">How close to the surface <span class="font-weight-light">the GermZapper needs to be to kill 99.9% of all pathogens</span></h3>
						</div>
						<p class="font-raleway line-height-1-2 mb-20">High intensities for a short period or low intensities for a long period <br />
							are fundamentally equal in lethal action on pathogens.</p>
						<ul class="text-left text-white pl-60">
							<li><span>6 inches = </span>3 seconds</li>
							<li><span>3 feet = </span>25-45 seconds</li> 
							<li><span>6 feet = </span>4-5 minutes</li>
							<li><span>10 feet = </span>10-12 minutes</li>
						</ul>
						<small class="font-italic">Calculations based on 300W with the intensity of approximately 110-130 μw/cm2 at 3.3 feet distance for a dosage of 6.6-7.8 mJ/minute </small>
					</div>
					<div class="kz-row-r">
						<img src="/images/disinfection-device.png" alt="disinfection device" class="mw-100p" />
					</div>
				</div>				
			</div>
		</div>		
	</div>	
</div>

<div class="bg-black position-relative overflow-hidden">
	<picture class="pic">		
		<source media="(max-width: 560px)" srcset="/images/germzapper/bg-mike-germzapper-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/germzapper/bg-mike-germzapper.png 1x, /images/germzapper/bg-mike-germzapper@2x.png 2x">
		<img class="banner-img" src="/images/germzapper/bg-mike-germzapper.png" alt="services bg">
	</picture>
	<div class="container position-absolute content uvdr-ca" >		
		<div class="row">
			
			<div class="col-sm-10 svc-items position-relative">
				<img class="width-auto" src="/images/germzapper/uv-light.png" alt="uv disinfectant device"  style="max-width:65%;"/>

				<ul class="position-absolute text-white ul1" style="top: 80%;">
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>300 Watt output is the world’s strongest <br>Handheld- UVC disinfectant device</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>Maximize disinfection by manually placing <br>germ-zapping light where you need it most</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>Easy-to-operate, minimal training <br>required</span></li>
					<li><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>Sanitize an entire room in about 10 min.</span></li>
				</ul>	

				<ul class="position-absolute text-white ul2" style="top:80%; right:-38%;">
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>Purchase with a stand to make effortless<br>room disinfection </span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>Industrial- strength, less expensive & <br>
																					more efficient than any other comparable <br>
																					mobile UV decontamination devices </span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>High quality and durable—12 months <br>hassle-free warranty.</span></li>
					<li class="d-flex"><i class="icon-checkmark4 text-sky mr-20 font-size-20"></i><span>PROVEN effective—Germ meter cards <br>scientifically verify our results</span></li>
				</ul>	
			</div>

			<div class="col-sm-2">
				
			</div>

			
		</div>
		
	</div>
</div>



<div class="bg-light-gray py-40">
	<div class="container gz-tiles pt-40">
		<div class="row">
	
			<div class="col-sm-12 row m-0 p-0 gz-tile d-flex py-40 justify-content-center">
				<div class="col-lg-6 p-0"><img  class="w-100" src="/images/germzapper/sofa.png" alt="lightning device sofa"/></div>
				<div class="bg-teal gz-tile2 col-lg-6 p-0 d-flex align-center">
					<span class="txt">Clean soft-surfaces that <br>
					Are sensitive to liquids <br>
					and chemicals.</span>
				</div>
			</div>
		
			<div class="col-sm-12 row mx-0 my-60 p-0 gz-tile d-flex py-40 justify-content-center">
				<div class="bg-teal gz-tile2 col-lg-6 p-0 d-flex align-center o2">
					<span class="txt">Effortless sanitize electronics <br>
					neutralizing 99.9% of all <br>
					pathogens.</span>
				</div>
				<div class="col-lg-6 p-0 o1"><img  class="w-100" src="/images/germzapper/electronics.png" alt="device electronics" /></div>
			</div>

			<div class="col-sm-12 text-center pb-60">
				<a href="/germzapper-uv-light-disinfection-device/multipurpose-case" class="btn btn-yellow mt-0" style="padding-left: 80px;padding-right:80px;">BUY NOW</a>
			</div>
			
		</div>
	</div>
</div>

<div class="bg-white">
	<div class="container pt-60">
		<div class="row">
			<div class="col-lg-6">
				<div class="main-title mb-20">
					<h2 class="text-teal-blue"><span class="text-dark">Does UV light kill viruses?</span><br /> Seeing is believing</h2>
				</div>
				<p class="mb-20">UVC dosimeter&trade; cards and stickers are industry standard tools used to 
				scientifically verify if surfaces are getting enough UV-C exposure to kill  your
				targeted pathogens.   They are placed in the treated areas and use a 
				photo-chromatic ink that changes color at various energy levels, which 
				correlates with a log reduction of pathogens.</p>
				<p>The UVC dosimeter&trade; test is offered as an optional test before a disinfection
				service begins.  Our results are scientifically verified-backed with a no-risk guarantee.</p>
			</div>
			<div class="col-lg-6 pl-30 text-center uvc-vid">				
				<a class="fancybox-media video-icon"  href="https://www.youtube.com/watch?v=xhlP3iNgPww"><img class="width-auto mw-430" src="/images/uvc-play.png" alt="UV-C Play Video" /></a>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 mt-30 font-size-18 font-raleway">
				Germbuster’s UV-C bulbs have been independently tested that our fixtures kill up to<br /> 
				<span class="font-weight-bold">99.9% of the following pathogens 3 seconds 6 inches away.</span>
			</div>		
		</div>
		<hr style="border-top:3px solid #707070;" />
		<div class="row uvc-list">
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">SARS-CoV</h4>
				<div class="d1">
					<img src="/images/icon-corona.jpg" />
					<span class="">The Corona family of viruses that includes the one that causes COVID-19 — can live on some of the surfaces you probably touch every day</span>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">Influenza</h4>
				<div class="d1">
					<img src="/images/icon-influenza.jpg" />
					<span class="">Both influenza A and B viruses survived for 24-48 hr on hard, nonporous surfaces 
						such as stainless steel and plastic </span>
				</div>
			</div>
			<div class="col-md-4">
				<h4 class="font-size-20 font-raleway font-weight-bold mb-10 d-inline-block">MRSA/Staph</h4>
				<div class="d1">
					<img src="/images/icon-staph.jpg" />
					<span class="">Methicillin-resistant Staphylococcus aureus (MRSA) can survive on some surfaces, like towels, razors, furniture, and athletic equipment for hours, days, or even weeks.   </span>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="bg-teal uvdd-gz auto-c pt-60 position-relative">
	<div class="bg-white-filler"></div>
	<div class="container">
		<div class="row">			
			<div class="col-md-6 text-white">
				<!-- <h3 class="font-raleway font-weight-bold my-40 d-inline-block">AutoCharge as it cleans</h3> -->
				<p class="line-height-1-5 font-raleway font-weight-bold font-size-20" style="padding-top: 30%;">
				The Germ-Zapper 300&trade; can be used for 
				hospital-grade disinfection of surfaces in schools,  
				homes, commercial facilities, nursing homes,  
				hospitals, etc.  With 300 watts The Germ Zapper 300&trade;
				is the most powerful hand-held UVC disinfection
				machine on the market. 
				</p>
			</div>
			<div class="col-md-6 p-0">
				<img src="/images/device.png" alt="device disinfection" style="max-width: 400px;" />
			</div>
		</div>
	</div>
</div>
			

<div id="GetQuote" class="bg-white py-50 contact-f">
	<div class="container">
		<div class="row">

			<div class="main-title text-center">
				<h2 class="my-30 font-size-40">Learn more about the Germ-Zapper 300™ <br>hand-held UVC disinfectant device</h2>
				<p class="line-height-1-2">
					Step into the next generation of UV-C disinfection  technology? Please provide your contact details <Br />
					and a GermBuster account specialist will help you take the first step in discovering the power of UV-C.  <br />
					Or you can call 954-761-2480 to talk to an expert now.
				</p>
			</div>

			<div class="form">
				<form name="frmContact" id="frmContact" method="post">	
					<input type="hidden" id="frmPage" value="UV Light Disinfection Device">
					<div class="row">

							<div class="col-md-12">
								<label>Choose your industry</label>
								<select name="industry" id="industry" class="">
									<option value="- Select -">- Select -</option>
									<option value="Accounting &amp; Financial">Accounting &amp; Financial</option>
									<option value="Agriculture">Agriculture</option>
									<option value="Animal &amp; Pet">Animal &amp; Pet</option>
									<option value="Architectural">Architectural</option>
									<option value="Art &amp; Design">Art &amp; Design</option>
									<option value="Attorney &amp; Law">Attorney &amp; Law</option>
									<option value="Automotive">Automotive</option>
									<option value="Bar &amp; Nightclub">Bar &amp; Nightclub</option>
									<option value="Business &amp; Consulting">Business &amp; Consulting</option>
									<option value="Childcare">Childcare</option>
									<option value="Cleaning &amp; Maintenance">Cleaning &amp; Maintenance</option>
									<option value="Communications">Communications</option>
									<option value="Community &amp; Non-Profit">Community &amp; Non-Profit</option>
									<option value="Computer">Computer</option>
									<option value="Construction">Construction</option>
									<option value="Cosmectics &amp; Beauty">Cosmectics &amp; Beauty</option>
									<option value="Dating">Dating</option>
									<option value="Education">Education</option>
									<option value="Entertainment &amp; The Arts">Entertainment &amp; The Arts</option>
									<option value="Environmental">Environmental</option>
									<option value="Fashion">Fashion</option>
									<option value="Floral">Floral</option>
									<option value="Food &amp; Drink">Food &amp; Drink</option>
									<option value="Games &amp; Recreational">Games &amp; Recreational</option>
									<option value="Home Furnishing">Home Furnishing</option>
									<option value="Industiral">Industiral</option>
									<option value="Internet">Internet</option>
									<option value="Landscaping">Landscaping</option>
									<option value="Medical &amp; Pharmaceutical">Medical &amp; Pharmaceutical</option>
									<option value="Photographiy">Photographiy</option>
									<option value="Physical Fitness">Physical Fitness</option>
									<option value="Political">Political</option>
									<option value="Real Estate &amp; Mortgage">Real Estate &amp; Mortgage</option>
									<option value="Religious">Religious</option>
									<option value="Restaurant">Restaurant</option>
									<option value="Retail">Retail</option>
									<option value="Security">Security</option>
									<option value="Spa &amp; Esthetics">Spa &amp; Esthetics</option>
									<option value="Sport">Sport</option>
									<option value="Technology">Technology</option>
									<option value="Travel &amp; Hotel">Travel &amp; Hotel</option>
									<option value="Wedding Service">Wedding Service</option>
								</select>
							</div>

							<div class="col-md-12 mt-20 required">
								<label>First name*</label>
								<input type="text" data-delay="300" name="firstname" id="firstname" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Last name*</label>
								<input type="text" data-delay="300" name="lastname" id="lastname" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12">
								<label>Job Title</label>
								<input type="text" data-delay="300" name="jobtitle" id="jobtitle" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Email Address</label>
								<input type="text" data-delay="300" name="email" id="email" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Phone*</label>
								<input type="text" data-delay="300" name="phone" id="phone" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12">
								<label>What would you like to know?</label>
								<textarea name="message" id="message" class="textarea medium" aria-invalid="false" rows="10" cols="50"></textarea>
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-1 cb">
								<label>
								<input name="notify" type="checkbox" value="I would like to be notified by email of future case studies, white papers, webinars and other educational content" checked="checked" id="notify">
								&nbsp;&nbsp;I would like to be notified by email of future case studies, white papers, webinars and other educational content</label>
							</div>

							<div class="col-md-12 mb-30">
							<div class="g-recaptcha" data-sitekey="6LeAfrMZAAAAAKOCyQ1JkBRMlIXW6PTpzpq8Zusy"></div>
							</div>

					</div>

			    </form>

			    <div><button name="btnGetQuote" id="btnGetQuote">Submit</button></div>		
			</div>		


		</div>
	</div>
</div>

@endsection
@section('footer_scripts')

<script src="/js/library.js" type="text/javascript" ></script>
<script type="text/javascript" >
jQuery(document).ready(function($) {
	
	jQuery('.fancybox').fancybox({
		padding: 0
	});

	jQuery('.tp-banner').show().revolution(
	{
		dottedOverlay:"none",
		delay:16000,
		startwidth:1170,
		startheight:745,
		hideThumbs:200,
		
		thumbWidth:100,
		thumbHeight:50,
		thumbAmount:5,
		
		navigationType:"bullet",
		navigationArrows:"solo",
		navigationStyle:"preview4",
		
		touchenabled:"on",
		onHoverStop:"off",
		
		swipe_velocity: 0.7,
		swipe_min_touches: 1,
		swipe_max_touches: 1,
		drag_block_vertical: false,
								
								parallax:"mouse",
		parallaxBgFreeze:"on",
		parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
								
		keyboardNavigation:"off",
		
		navigationHAlign:"center",
		navigationVAlign:"bottom",
		navigationHOffset:0,
		navigationVOffset:20,

		soloArrowLeftHalign:"left",
		soloArrowLeftValign:"center",
		soloArrowLeftHOffset:20,
		soloArrowLeftVOffset:0,

		soloArrowRightHalign:"right",
		soloArrowRightValign:"center",
		soloArrowRightHOffset:20,
		soloArrowRightVOffset:0,
				
		shadow:0,
		fullWidth:"on",
		fullScreen:"off",

		spinner:"spinner4",
		
		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,

		shuffle:"off",
		
		autoHeight:"off",						
		forceFullWidth:"off",						
								
								
								
		hideThumbsOnMobile:"off",
		hideNavDelayOnMobile:1500,						
		hideBulletsOnMobile:"off",
		hideArrowsOnMobile:"off",
		hideThumbsUnderResolution:0,
		
		hideSliderAtLimit:1025,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		startWithSlide:0,
		videoJsPath:"rs-plugin/videojs/",
		fullScreenOffsetContainer: ""	
	});

	jQuery('.fancybox-media').fancybox({
		padding:0,
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			overlay:{ 
				css: {background:'rgba(0,0,0,0.75)', 'z-index':99998}
			},
			media : {},
			padding:0
		}
	});

	var slickOpts = {
		arrows: false,
		dots:true,
		autoplay: true,
		autoplaySpeed: 5000,
		speed: 1500,						
	}

	$('#home-slider').slick(slickOpts);


	if (window.location.href.indexOf("work") > -1) {
      $("#slider-home").removeClass("active");
	  $("#slider-work").addClass("active");
	  $('#home-slider').slick('slickFilter', $('.work-slide').parent().parent());	
    }else{
      $('#home-slider').slick('slickFilter', $('.home-slide').parent().parent());	
    }
	


	$(".slider-filter").click(function(e) {

		e.preventDefault();
		var current = $(".slider-filter.active").data("filter");
		var filter = $(this).data("filter");				

		if(current !== filter) {
			$(this).addClass("active").siblings().removeClass("active");

			$('#home-slider').slick('slickUnfilter');
			$('#home-slider').slick('slickFilter', $('.' + filter + '-slide').parent().parent());					
			$("." + filter + "-text").show().siblings().hide();
			$("." + filter + "-text").parents('li').removeClass(current + '-body').addClass(filter + '-body');
			
			// if(filter == 'home') {
			// 	//$('#home-slider').slick(slickOpts);
			// 	$('#home-slider').slick('slickPlay');
			// 	$('#home-slider').show();
			// 	$('#work-slider').slick('slickPause');
			// 	$('#work-slider').hide();
			// 	//$('#work-slider').slick('unslick');
			// } else if(filter == 'work') {
			// 	//$('#work-slider').slick(slickOpts);
			// 	$('#home-slider').slick('slickPause');
			// 	$('#home-slider').hide();
			// 	$('#work-slider').slick('slickPlay');
			// 	$('#work-slider').show();
				
			// 	//$('#home-slider').slick('unslick');

			// }		
		}
		
	});

	$('.brand-logos').slick({
		speed: 4000,
		autoplay: true,
		autoplaySpeed: 0,
		centerMode: true,
		cssEase: 'linear',
		slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		infinite: true,
		initialSlide: 1,
		arrows: false,
		buttons: false
	});


});
</script>
@endsection

