@extends('layouts.master')

@section('content')


<!--Start Banner-->   
<div class="sub-banner">  
	<picture>
		<source media="(max-width: 560px)" srcset="/images/hvac-banner-mobile@2x.png">
		<source media="(min-width: 561px)" srcset="/images/hvac-banner@1x.png 1x, /images/hvac-banner@2x.png 2x">		
		<img class="banner-img" src="/images/hvac-banner@1x.png" alt="uvc hvac">		
	</picture>
	<div class="bn-text">		
		<span class="text-center position-relative">
			<img class="width-auto position-absolute uvc-hvac-img" src="/images/uvc-hvac.png" alt="uvc hvac" />
			<h1 class="m-0">UV-C HVAC</h1>
			<p class="font-raleway font-weight-bold" style="color:#4893C9">According to the EPA the levels of indoor air pollutants are often 2 to 5 times higher than outdoor levels, 
			</p>
		</span>
		
	</div>

</div>	   
<!--End Banner-->

<div class="bg-black position-relative bgbc">
	<picture class="pic">		
		<source media="(max-width: 560px)" srcset="/images/radial-bg-mobile-1@2x.png">
		<source media="(min-width: 561px)" srcset="/images/radial-bg.png 1x, /images/radial-bg@2x.png 2x">
		<img class="banner-img" src="/images/radial-bg.png" alt="radial bg">
	</picture>
	<div class="container center-absolute p-0 content">		
		<div class="row">
			<div class="col-sm-12">
				<div class="txt text-center">
					<h2 class="text-teal pt-40 pb-20">Go Beyond Clean</h2>		
					<span class="font-size-16 font-raleway text-white">A GermBuster UVC disinfection system remains the sole proven, practical and cost effective<br />
						whole-house solution for addressing true microbial safety without impacting odor or taste. </span>			
				</div>
			</div>
			<div class="col-sm-12 pt-20 position-relative">
				<img class="width-auto mw-100p" src="/images/uvc-hvac-equip@1x.png" alt="uvc-hvac" />	
				<ul class="gbc-hvac position-absolute">
					<li>Saves 10-25% in energy</li>
					<li>Destroys surface and airborne microorganisms</li>
					<li>Restores airflow levels to original specifications</li>
					<li>Improves indoor air quality</li>
					<li>Increases HVAC/R equipment life</li>
					<li>Reduces maintenance</li>
				</ul>	
			</div>
		</div>
		
	</div>
</div>

<div class="container-fluid bg-white text-center shadow py-20 position-relative"> 
	<div class="row">
		<div class="col-sm-12">
			<div class="main-title mt-20"><h2><span>Bring your dream of</span> affordable <br>
				clean air living to life</h2>
				<!-- <a href="#" id="quote-residential-btn" class="quote-residential-btn btn btn-outline" data-toggle="modal" data-target="#modalGetQuote" data-aos="fade-up">RESIDENTIAL</a>
				<a href="#" id="quote-commercial-btn" class="quote-commercial-btn btn btn-outline ml-10" href="#" data-target="#modalGetQuote" data-toggle="modal" data-aos="fade-up">COMMERCIAL</a> -->
			</div>
			
		</div>
	</div>
</div>

<div id="applications" class="pt-60 position-relative">	
	<img class="banner-img" src="/images/online-or-inhome.png" alt="online or inhome">
	<div class="txt">
		<h2 class="text-teal">Online or In-home</h2>		
		<p class="">Consult with a GermBuster’s UVC expert. Meet 
			remotely from your home or one-on-one at your 
			house. During the consultation, we’ll discuss all the
			 UVC service options which range from 
			measurement to installation.</p>
	</div>
</div>

<div class="bg-white font-raleway">
	<div class="container pt-50">
		<div class="row">
			<div class="col-lg-8">
				<div class="main-title mb-10"><h2 class="font-size-28"><span>Your life</span> without UVC</h2></div>				
				<p class="line-height-1-5">
					Your HVAC system is more than your home or office air conditioner. The coils deep 
					inside provide an ideal breeding ground for germs and bacteria that build up over time 
					and form Biofilm. A slimy collective of one or more types of microorganisms that:
				</p>
				<ul class="my-20 check line-height-2 font-size-15">
					<li>Reduces indoor air quality</li>
					<li>Makes your HVAC units work harder,</li>
					<li>Negativity affects your family’s health</li>
					<li>Produces unpleasant, harmful odors associated bacterial overgrowth</li>
					<li>Coils contain microbial allergen that  are the cause of allergy symptoms</li>
				</ul>
				<div class="mw-500">
					<div class="ywlu bfc d-flex justify-content-between font-weight-bold font-italic font-size-14">
						<span>Biofilm builds on coils before UVC</span>
						<span>Coils after UVC</span>
					</div>
					<img class="width-auto mw-100p" src="/images/uvc-wo1.png" alt="uvc" />
				</div>				
			</div>
			<div class="col-lg-4 pt-10">
				<p class="wo-quote">
					“We found that flu cases contaminated the air around 
					them with infectious virus just by breathing, without 
					coughing or sneezing
					<span class="rq">&rdquo;</span>
				</p>
				<span class="d-inline-block w-100 text-right mt-20 font-italic font-size-16">Prof. Donald K. Milton</span>
				<img class="wo-girl width-auto mw-100p" src="/images/uvc-wo-girl.png" alt="girl sneeze" />
			</div>
		</div>
	</div>
</div>

<div class="bg-light-gray">
	<div class="container-fluid">
		<div class="row d-flex comm-uvac-cont">
			<div class="cuvac-img">
				<img class="width-auto mw-600" src="/images/commercial-uvc.png" alt="commercial uvc" />
			</div>
			<div class="comm-uvac">
				<div class="main-title mb-10"><h2 class="font-size-28"><span>Commerical</span> UVC HVAC systems</h2></div>
				<p>Rely on Germbusters licensed and certified technicians to deep clean, sanitize and disinfect your commercial space.<br /> 
					Trust Germbusters to to be your partner with:</p>
				<div class="row">
					<ul class="col-md-6 my-20 check line-height-2 font-size-15">
						<li>Improved Indoor air quality</li>
						<li>Less cleaning of coils and ducts </li>
						<li>Extended life of the HVAC system</li>
						<li>Energy savings up to 15% per year </li>						
					</ul>
					<ul class="col-md-6 my-20 check line-height-2 font-size-15">
						<li>Increased productivity decrease absenteeism</li>
						<li>Payback within 24 months (or less) of installation</li>
						<li>Advanced UV-C Sanitization kills 99% germs</li>
						<li>Chemical free</li>						
					</ul>
				</div>
				<a href="/contact-us" class="btn btn-yellow text-uppercase" data-aos="fade-up"> schedule your UVC HVAC consultation today</a>
			</div>
		</div>
	</div>	
</div>

<div class="bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="pt-40 pb-10"><h2 class="text-teal-blue font-weight-extrabold">Get the facts!</h2></div>
				<p class="name line-height-2">Beware companies offering 90 days protection against coronaviruses with one treatment. Get COVID-19 <br />
					cleaning help from the professionals. Find deep cleaning services, sanitation services, and disinfection <br />
					services that fit your needs and budget with 24/7 emergency support. 
				</p>

			<!-- 	<a href="/cleaning-quote/commercial" class="btn btn-teal mt-30 px-30 py-10" data-aos="fade-up">SCHEDULE AN APPOINTMENT</a> -->
			</div>
		</div>
	</div>
</div>

   @include('partials.get_quote')


@endsection