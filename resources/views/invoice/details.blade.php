@extends('layouts.master')

@section('header_scripts')
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
<link href="/css/services.css" rel="stylesheet" type="text/css">
<link href="/css/auth/signup.css" rel="stylesheet" type="text/css">
<link href="/css/affiliate.css" rel="stylesheet" type="text/css" />
<style>
    .form-horizontal .control-label {
        font-size:15px;
    }
    .btn-dark {
        background: #000;
        color: #fff;
        padding: 12px 0;
    }
    .gray-box, .gray-box p {
        font-size: 13px;
    }
    .gray-box p {
        line-height: 1.5;
    }
</style>
@endsection
@section('content')
    <script src="/js/library.js" type="text/javascript"></script>


	<!--Start Loader-->
	<div id="loader" class="loader" style="display: none;" >
    	<img src="/images/loading.gif" alt="" />
	</div>
	<!--End Banner-->

	<div class="content">
	  	<div class="container ">
            @include('partials.progresstracker', 
                array('steps'=> [
                    'Inquiry', 
                    'Review <span class="mobilehide">Estimate</span>',
                    'Invoice',
                    '<span class="mobilehide">Invoice</span> Paid'
                    ], 'progress'=>['is-complete', 'is-complete', 'active is-complete', ''] ))                    

            <form id="details-form" class="form-horizontal font-armour" role="form" method="POST" action="{{ url('/invoice/details') }}">
                <div class="row mt-50">
                    <div class="col-lg-9">
                        <div style="max-width: 520px;margin: 0 auto;">
                            {{ csrf_field() }}

                            <input type="hidden" name="id" value="{{ $id }}">
                            <input type="hidden" name="type" value="{{ $type }}">
                            
                            <div class="form-group">
                                <label for="input-firstname" class="col-sm-3 control-label">First name</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="fname" class="form-control" id="input-firstname" value="{{ $details->firstname }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-lastname" class="col-sm-3 control-label">Last name</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="lname" class="form-control" id="input-lastname" value="{{ $details->lastname }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-phone" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="phone" class="form-control" id="input-phone" value="{{ $details->phone }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-phone" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="address1" class="form-control" id="input-address1" value="{{ $details->address1 }}">
                                    <input type="text" name="address2" class="form-control mt-10" id="input-address2" value="{{ $details->address2 }}">
                                </div>
                            </div>                        
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <div class="checkbox">
                                        <label>
                                            <input name="iscommercial" type="checkbox"> This is a commercial address
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-city" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="city" class="form-control" id="input-city" value="{{ $details->city }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-state" class="col-sm-3 control-label">State/Province</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="state" class="form-control" id="input-state" value="{{ $details->state }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-zipcode" class="col-sm-3 control-label">Zip/Postal Code</label>
                                <div class="col-sm-9 required">
                                    <input type="text" name="zipcode" class="form-control" id="input-zipcode" value="{{ $details->zipcode }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9 required">
                                    <input type="email"  name="email" class="form-control" id="input-email" placeholder="Email" value="{{ $details->email }}">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <p class="mb-10">We'll send your order confirmation here</p>
                                    <button id="continue" class="btn btn-dark btn-default w-100">Continue to Payment</button>
                              </div>
                            </div>
                        </div>
                    </div>               
                    <div class="col-lg-3 bg-gray px-30 py-20 gray-box">
                       @if(isset($details->general_price) && $details->general_price != "0.00")                           
                                <p class="pb-10 font-armour">
                                    <span class=" font-weight-bold">General Price</span><br />
                                    <span> ${{ number_format($details->general_price, 2, '.', ',') }}</span>
                                </p>
                        @endif

                        @if(isset($invoice_items))
                            @foreach($invoice_items as $value) 
                                <p class="pb-10 font-armour">
                                    <span class=" font-weight-bold">{{ $value->title }}</span><br />
                                    <span> ${{ number_format($value->price, 2, '.', ',') }}</span>
                                </p>
                            @endforeach
                        @endif
                        <p class="pb-10 font-armour">
                            <label for="promo-code">Promo code</label><br />
                            <input type="text" name="promo_code" id="promo_code" class="form-control" value="{{ $details->promo_code }}" />
                        </p>
                        <p class="pb-10 font-armour">
                            Tax
                        </p>
                         <div class="d-flex justify-content-between font-armour pt-10">
                            <span>Promo Code</span>
                            <span>${{ number_format($details->promo, 2, '.', ',') }}</span>
                        </div>
                        <div class="d-flex justify-content-between font-armour">
                            <span>Discount</span>
                            <span>${{ number_format($details->discount, 2, '.', ',') }}</span>
                        </div>
                        <div class="d-flex justify-content-between font-armour font-weight-bold font-size-15 pt-10">
                            <span>Total</span>
                            <span>${{ number_format($details->total, 2, '.', ',') }}</span>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="af-next" style="padding-bottom: 10px;">
                    &nbsp;
                </div>
            </div>
		</div>
	</div>

	@include('partials.bottom_bar', array('right_div' => ''))
@endsection

@section('footer_scripts')
	{{-- <script src="/js/accounting.min.js" type="text/javascript"></script>
	<script src="/js/location.js" type="text/javascript"></script>
	<script src="/js/auth/signup.js" type="text/javascript" ></script>
	<script src="/js/pages/cleaning.quote.service.js"></script> --}}

    <script type="text/javascript">
        var $form = $("#details-form");

        $(function() {
            $("#continue").click(function(e) {
                e.preventDefault();

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                var $elem = null;
                var iserr = false;

                /* first name */
                $elem = $form.find("input[name='fname']");
                if(isRequired($elem, "Please provide your first name")) {
                    iserr = !iserr ? true : iserr;
                }

                /* last name */
                $elem = $form.find("input[name='lname']");
                if(isRequired($elem, "Please provide your your last name")) {
                    iserr = !iserr ? true : iserr;   
                }           

                /* phone */
                $elem = $form.find("input[name='phone']");
                if(isRequired($elem, "Please enter a valid phone number")) {
                    iserr = !iserr ? true : iserr;   
                }  

                /* zip code */
                $elem = $form.find("input[name='zipcode']");
                if(isRequired($elem, "Please enter a valid zip code")) {
                    iserr = !iserr ? true : iserr;   
                }  

                /* city */
                $elem = $form.find("input[name='city']");
                if(isRequired($elem, "Please provide your city")) {
                    iserr = !iserr ? true : iserr;   
                }  

                /* state */
                $elem = $form.find("input[name='state']");
                if(isRequired($elem, "Please provide your state")) {
                    iserr = !iserr ? true : iserr;   
                }  

                /* address 1 */
                $elem = $form.find("input[name='address1']");
                if(isRequired($elem, "Please provide your address")) {
                    iserr = !iserr ? true : iserr;   
                }  

                /* email */
                $elem = $form.find("input[name='email']");
                if(isRequired($elem, "Please provide your email address")) {
                    iserr = !iserr ? true : iserr;         
                }

                if(!isEmail($elem , "Please enter a valid email address.")) {
                    iserr = !iserr ? true : iserr;            
                }

                if(!iserr) {
                    if($("#promo_code").val() !== "") {
                        $("#continue").attr("disabled", true);
                        $.post('/invoice/promocode',{     _token: CSRF_TOKEN,
                                                 'promo': $("#promo_code").val(),
                                            }, function(result) {
                            if(isNaN(result)) {
                                e.preventDefault();

                                $form.get(0).submit();
                            } else {
                                alert('The code you have entered is either not valid, expired, or not accepted by this operation.');
                            }

                            $("#continue").removeAttr("disabled");
                        });
                    } else {
                        $form.get(0).submit();
                    } 
                }
            });
        });
    </script>

@endsection