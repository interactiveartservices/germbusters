@extends('layouts.master')

@section('header_scripts')
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
<link href="/css/services.css" rel="stylesheet" type="text/css">
<link href="/css/auth/signup.css" rel="stylesheet" type="text/css">
<link href="/css/affiliate.css" rel="stylesheet" type="text/css" />
<style>
    .form-horizontal .control-label {
        font-size:15px;
    }
    .btn-dark {
        background: #000;
        color: #fff;
        padding: 12px 0;
    }
    .gray-box, .gray-box p {
        font-size: 13px;
    }
    .gray-box p {
        line-height: 1.5;
    }
    .nav-pills > li {	    
	    width: 50%;
    }
    .nav-pills > li > a {	
	    display: block;
	    padding: 10px 0;
        border: solid 1px #000;
    }
    .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
	    color: #ffffff;
	    background-color: #000;
    }
</style>
@endsection
@section('content')
		

	<!--Start Loader-->
	<div id="loader" class="loader" style="display: none;" >
    	<img src="/images/loading.gif" alt="" />
	</div>
	<!--End Banner-->

	<div class="content">
	  	<div class="container ">
            @include('partials.progresstracker', 
                array('steps'=> [
                    'Inquiry', 
                    'Review <span class="mobilehide">Estimate</span>',
                    'Invoice',
                    '<span class="mobilehide">Invoice</span> Paid'
                    ], 'progress'=>['is-complete', 'is-complete', 'active is-complete', ''] ))                    

            <div class="row mt-50">
                <div class="col-lg-9">
                    <div style="max-width: 520px;margin: 0 auto;">
                        <form id="payment-form" class="form-horizontal" action="/invoice/process" method="POST" data-cc-on-file="false" data-stripe-publishable-key="{{ $stripe['pk'] }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="id" value="{{ $id }}">
                            <input type="hidden" name="cardname" value="">
                            <input type="hidden" name="shipaddr" value="">
                            <input type="hidden" name="paymethod" value="creditcard">

                            
                            <div class="form-group credit-card">
                                <div class="col-sm-offset-3 col-sm-9">                              
                                    <ul class="nav nav-pills d-flex text-center">
                                        <li class="active">
                                            <a class="payment-method" href="#" data-method="creditcard">Credit card</a>
                                        </li>
                                       <!--  <li><a class="payment-method" href="#" data-method="paypal">Paypal</a>
                                        </li>  -->                                   
                                    </ul>
                                </div>
                            </div>
                            <div id="credit-container">
                                <div class="form-group credit-card">
                                    <label for="input-cardnumber" class="col-sm-3 control-label">Cardholder Name</label>
                                    <div class="col-sm-9 card-name">
                                        <input type="text" class="form-control" id="input-cardholder" >
                                        <div class="message">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="form-group credit-card">
                                    <label for="input-cardnumber" class="col-sm-3 control-label">Card Number</label>
                                    <div class="col-sm-9 card-number">
                                        <input type="text" class="form-control" id="input-cardnumber">
                                        <div class="message">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="form-group credit-card">
                                    <label for="input-expdate" class="col-sm-3 control-label">Expiration date</label>
                                    <div class="col-sm-9 card-expiry">
                                        <input type="text" class="form-control" id="input-expdate" style="max-width: 100px" placeholder="MM/YYYYY">
                                        <div class="message">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="form-group credit-card">
                                    <label for="input-seccode" class="col-sm-3 control-label">Security Code</label>
                                    <div class="col-sm-9 card-cvc">
                                        <input type="text" class="form-control" id="input-seccode" style="max-width: 100px">
                                        <div class="message">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                <input id="ship-credit" type="checkbox"> Use my shipping address as my billing address
                                            </label>
                                        </div>
                                    </div>
                                </div>       
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">                              
                                        <button id="creditcard-button" type="submit" class="btn btn-dark btn-default w-100">Add Payment Method</button>
                                    </div>
                                </div>
                            </div>
                            <div id="paypal-container" style="display: none;">
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                <input id="ship-paypal" type="checkbox"> Use my shipping address as my billing address
                                            </label>
                                        </div>
                                    </div>
                                </div>   
                                <div id="paypal-button"></div>
                            </div>                                  
                        </form>
                    </div>
                </div>               
                <div class="col-lg-3 bg-gray px-30 py-20 gray-box">
                  @if(isset($details->general_price) && $details->general_price != "0.00")                           
                            <p class="pb-10 font-armour">
                                <span class=" font-weight-bold">General Price</span><br />
                                <span> ${{ number_format($details->general_price, 2, '.', ',') }}</span>
                            </p>
                    @endif
                        
                     @if(isset($invoice_items))
                        @foreach($invoice_items as $value) 
                            <p class="pb-10 font-armour">
                                <span class=" font-weight-bold">{{ $value->title }}</span><br />
                                <span>${{ number_format($value->price, 2, '.', ',') }}</span>
                            </p>
                        @endforeach
                    @endif
                    <p class="pb-10">
                        Tax
                    </p>
                    <div class="d-flex justify-content-between font-armour pt-10">
                        <span>Discount</span>
                        <span>${{ number_format($details->discount, 2, '.', ',') }}</span>
                    </div>
                    <div class="d-flex justify-content-between font-armour pt-10">
                        <span>Promo Code</span>
                        <span>${{ number_format($details->promo, 2, '.', ',') }}</span>
                    </div>
                    <div class="d-flex justify-content-between font-armour font-weight-bold font-size-15 pt-10">
                        <span>Total</span>
                        <span>${{ number_format($details->total, 2, '.', ',') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="af-next" style="padding-bottom: 10px;">
                    &nbsp;
                </div>
            </div>
		</div>
	</div>

	@include('partials.bottom_bar', array('right_div' => ''))
@endsection

@section('footer_scripts')
	{{-- <script src="/js/accounting.min.js" type="text/javascript"></script>
	<script src="/js/library.js" type="text/javascript"></script>
	<script src="/js/location.js" type="text/javascript"></script>
	<script src="/js/auth/signup.js" type="text/javascript" ></script>
	<script src="/js/pages/cleaning.quote.service.js"></script> --}}

    <script type="text/javascript" src="/js/jquery.payment.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="https://www.paypalobjects.com/api/checkout.js"></script>   

    <script type="text/javascript">
        $(function() {
            var $form = $("#payment-form"); 

            function IsValidInput($elem, reqmsg, errmsg)
            {
                if($elem.find("input").val().trim() == "") {
                    $elem.find(".message").addClass("error").html(reqmsg);
                } else {
                    var haserror = true;

                    if($elem.hasClass("card-name") != "") {
                        haserror = false;
                    }

                    if($elem.hasClass("card-number")) {
                        haserror = !$.payment.validateCardNumber($elem.find("input").val());
                    }

                    if($elem.hasClass("card-expiry")) {
                        var expr = $.payment.cardExpiryVal($elem.find("input").val());
                        haserror = !$.payment.validateCardExpiry(expr.month, expr.year);
                    }

                    if($elem.hasClass("card-cvc")) {
                        haserror = !$.payment.validateCardCVC($elem.find("input").val());
                    }

                    if(haserror) {
                        $elem.find(".message").addClass("error").html(errmsg);
                    } else {
                        return true;
                    }
                }

                $elem.find("input").addClass("input-error");

                return false;
            }

            function ProcessPayment(isprocess)
            {
                EnablePaymentButton(!isprocess);    
                EnablePaymentMethod(!isprocess);
                EnableCreditDetails(!isprocess);

                if(!isprocess) {
                    $form.find("#ship-addr").removeAttr("disabled");
                } else {
                    $form.find("#ship-addr").attr("disabled", true);
                }

                if(isprocess) {
                    $("#loader").show();
                } else {
                    $("#loader").hide();
                }
            }

            function EnableCreditDetails(isenable) 
            {
                if(isenable) {
                    $form.find(".credit-card input").removeAttr("disabled");
                } else {
                    $form.find(".credit-card input").attr("disabled", true);
                }
            }

            function EnablePaymentButton(isenable)
            {
                if(isenable) {
                    $form.find("#creditcard-button").removeAttr("disabled");
                    $form.find("#paypal-button").css({pointerEvents: "auto", opacity:"1.0"});
                } else {
                    $form.find("#creditcard-button").attr("disabled", true); 
                    $form.find("#paypal-button").css({pointerEvents: "none", opacity:"0.5"});
                }
            }

            function EnablePaymentMethod(isenable)
            {
                //if(isenable) {
                //    $form.find(".payment-method").css({pointerEvents: "auto", opacity:"1.0"});
                //} else {
                //    $form.find(".payment-method").css({pointerEvents: "none", opacity:"0.8"});
                //}
            }

            $form.find(".payment-method").click(function(e) {
                if($(this).data("method") == "creditcard") {
                    $form.find("#credit-container").show();
                    $form.find("#paypal-container").hide();
                } else {
                    $form.find("#credit-container").hide();
                    $form.find("#paypal-container").show();                    
                }

                $form.find("input[name='paymethod']").val($(this).data("method"));
            });

            $form.bind("submit", function(e) {
                e.preventDefault();
              
                $form.find(".error").html("&nbsp;").removeClass("error");
                $form.find(".input-error").removeClass("input-error");
                $form.removeClass("form-error");

                if(!IsValidInput($form.find(".card-number"), "Card number is required.", "The card number is not a valid credit card number.")) {
                    if(!IsValidInput($form.find(".card-name"), "Cardholder name is required.", "")) {
                        if(!$form.hasClass("form-error")) {
                            $form.addClass("form-error");
                        }
                    }
                }

                if(!IsValidInput($form.find(".card-number"), "Card number is required.", "The card number is not a valid credit card number.")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    }
                }

                if(!IsValidInput($form.find(".card-expiry"), "Card expiry is required.", "Your card's expiration is invalid.")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    }
                }

                if(!IsValidInput($form.find(".card-cvc"), "CVV is required.", "Your card's security code is invalid.")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error"); 
                    }                            
                }

                if($form.hasClass("form-error")) {
                    e.preventDefault();               
                }
            }); 

            $form.on("submit", function(e) { 
                e.preventDefault();

                if(!$form.hasClass("form-error")) {
                    $form.find("input[name='cardname']")
                         .val($form.find(".card-name input").val())
                    $form.find("input[name='shipaddr']")
                         .val($form.find("#ship-credit").is(":checked") ? "1" : "0");

                    ProcessPayment(true);

                    if(!$form.data('cc-on-file')) {
                        Stripe.setPublishableKey($form.data('stripe-publishable-key'));

                        var expr = $.payment.cardExpiryVal($form.find(".card-expiry input").val());
                        var card = {
                                            name: $form.find(".card-name input").val(),
                                          number: $form.find(".card-number input").val(),
                                             cvc: $form.find(".card-cvc input").val(),
                                       exp_month: expr.month,
                                        exp_year: expr.year
                                   }

                        $(this).find(".error").html("&nbsp;").removeClass("error");

                        // create token for customer
                        Stripe.createToken(card, function(status, response) {
                            if (response.error) {
                                switch(response.error.param.toLowerCase()) {
                                    case "number":
                                        $form.find(".card-number").find(".message").addClass("error").html(response.error.message);
                                        break;
                                    case "cvc":
                                        $form.find(".card-cvc").find(".message").addClass("error").html(response.error.message);
                                        break;                                            
                                    case "exp_month":
                                    case "exp_year":
                                        $form.find(".card-expiry").find(".message").addClass("error").html(response.error.message);
                                        break;                                                 
                                    default:
                                        alert(response.error.message);
                                        break;
                                }

                                ProcessPayment(false);
                            } else {
                                // insert the charge token into the form.
                                $form.append("<input type='hidden' name='stripetoken' value='" + response['id'] + "'/>");
                                // submit form
                                $form.get(0).submit();
                            }
                        });
                    }
                }
            }); 

            paypal.Button.render({
                        env:  "{{ $paypal['env'] }}",
                      style: {
                                    label: "paypal",
                                     size: "large",  // small | medium | large | responsive
                                    shape: "rect",        // pill | rect
                                    color: "silver",      // gold | blue | silver | black
                                  tagline: false
                             },
                     client: {
                                   sandbox: "{{ $paypal['sandbox'] }}",
                                production: "{{ $paypal['production'] }}"
                             },
                    payment: function (data, actions) {
                                return actions.payment.create({
                                    transactions: [{
                                                        amount: {
                                                                    total: "{{ $details->total }}",
                                                                 currency: "USD"
                                                                }
                                                  }]
                             });
                },
                onAuthorize: function (data, actions) {
                                return actions.payment.execute().then(function () {
                                            $form.find("input[name='shipaddr']")
                                                 .val($form.find("#ship-paypal").is(":checked") ? "1" : "0");
                                            //ProcessPayment(true);
                                            $form.append("<input type='hidden' name='paypal' value='" + btoa(JSON.stringify(data)) + "'/>");
                                            // submit form
                                            $form.get(0).submit();
                                       });
                             }
            }, "#paypal-button");  
        });
    </script>
@endsection