@extends('layouts.master')
@section('header_scripts')
<link href="/css/affiliate.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')


 <div class="container">
 	<div class="main-title text-center mt-60">
	 	<h1 class="text-teal font-weight-light font-size-40">THANK YOU</h1>
	 	<h3 class="font-weight-bold font-size-28 " >We truly appreciate your business</h3>
	 	<div class="mt-40">
	 		<a class="text-underline" href="/invoice/checkout/paid/{{ $id }}/{{ urlencode(base64_encode($id)) }}">CLick here for the paid invoice</a>
	 	</div>
 	</div>
 </div>

@if(app()->environment('production')) 
<script type="text/javascript" src="https://static.leaddyno.com/js"></script>
<script>
LeadDyno.key = "843d104b056142ebf1d94ae5aef358917e8e9105";
LeadDyno.recordPurchase();
</script>
@endif


@endsection