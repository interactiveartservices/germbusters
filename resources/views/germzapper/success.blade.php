@extends('layouts.master')

@section('header_scripts')
<link href="/css/germzapper.css" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="main-menu-container main-menu-container" style="top: 0px;">
    <div class="container-branding">
        <a href="/" class="logo" title="Germbusters911"><img src="/images/logo@1x.png" alt="logo"></a>
    </div>
    <div class="package-options--nav">
        <ul class="packages-options--nav-list" role="tablist" arial-label="Main menu">
            <li id="menu-step1" class="packages-options--nav-item" tabindex="0" arial-label="battery" role="tab" data-id-selected="false">
                <h2 class="packages-options--nav-title"><span>UV Light Device</span></h2>
            </li>
            <li id="menu-step2" class="packages-options--nav-item" tabindex="0" arial-label="paint" role="tab" data-id-selected="false">
                <h2 class="packages-options--nav-title"><span>Accessories</span></h2>
            </li>
            <li class="packages-options--nav-item packages-options--nav-item_selected" tabindex="0" arial-label="interior" role="tab" data-id-selected="true">
                    <h2 class="packages-options--nav-title"><span>Payment</span></h2>
            </li>
        </ul>
    </div>
    <div class="wrap-container--next wrap-container--item right-nav"></div>
</div>

<div class="container success">
    <div class="main-title text-center mt-60 pt-60">
        <div class="pb-30" style="border-bottom:1px solid #ddd">
            <img src="/images/germzapper/success-check.png" alt="success" class="width-auto" />
            <h1 class="font-weight-medium font-size-22">Congratulations</h1>
            <p class="font-weight-medium" >Estimated  delivery 2-6 weeks</p>
            <p class="line-height-1-2  mt-20 font-size-14 font-centrale-sansbook font-weight-medium text-lgray">Your order requires handcrafted customization. <br>
            Watch your inbox for an update <br>
            For your delivery time.</p>

            <p class="line-height-1-2 mt-20 font-size-14 font-centrale-sansbook font-weight-medium text-lgray">You ordering information will be <br>
            forwarded to your email </p>
        </div>
        <h2 class="mt-40 mb-20 font-weight-medium font-18">
            What Happens Next?
        </h2>

        <div class="">
            <div class="mt-40 col-md-3">
                <div class="icn">
                    <img src="/images/germzapper/Icon material-build.png" alt="Assemble your product at our facility" class="width-auto" />
                </div>
                <span>An engineer will custom <br>
                Assemble your product at our facility<br>
                In Pompano Beach, FL</span>
            </div>
            <div class="mt-40 col-md-3">
                <div class="icn">
                    <img src="/images/germzapper/Icon material-email.png" alt="We will inform you when Your product is ready" class="width-auto" />
                </div>
                <span>We will inform you when<br>
                Your product is ready</span> 
            </div>
            <div class="mt-40 col-md-3">
                <div class="icn">
                    <img src="/images/germzapper/Compound Path_1.png" alt="Your products will be shipped in 3-7 days" class="width-auto" />
                </div>
                <span>Your products will be shipped <br>
                in 3-7 days</span>
            </div>
            <div class="mt-40 col-md-3">
                <div class="icn">
                    <img src="/images/germzapper/Compound Path_2.png" alt="ou will receive an email when the package is delivered" class="width-auto" />
                </div>
                <span>You will receive an email when <br>
                the package is delivered</span>
            </div>
        </div>
    </div>
 </div>

@endsection

@section('footer_scripts')
   
@endsection

