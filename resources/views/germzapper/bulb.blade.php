@extends('layouts.master')

@section('header_scripts')
<link href="/css/germzapper.css" rel="stylesheet" type="text/css">


<script type="application/ld+json">
	  {
	    "@context": "http://schema.org",
	    "@type": "Product",
	    "image": [
      	"http://www.germbusters911.com//images/germzapper/multiuse-case.png",
      	"http://www.germbusters911.com//images/germzapper/handheld-case.png"
     	],
	    "name": "Germzapper {{ $case }}",
	    "description": "GermZapper 300® is portable enough to hold in your hands, yet powerful enough to disinfect a room. Kill 99.99% of all pathogens on surfaces and in the air without the use of harsh chemicals - all in one easy-to-use device.",
	    "aggregateRating":
	    {"@type":"AggregateRating",
	    "ratingValue": "4.5",
	   	"ratingCount": "89"
	     }
	   }
	</script>
@endsection

@section('content')

<input id="casetype" type="hidden" value="{{ $case }}">

<div class="main-menu-container main-menu-container" style="top: 0px;">
	<div class="container-branding">
		<a href="/" class="logo" title="Germbusters911"><img src="/images/logo@1x.png" alt="logo"></a>
	</div>
	<div class="package-options--nav">
		<ul class="packages-options--nav-list" role="tablist" arial-label="Main menu">
			<li class="packages-options--nav-item packages-options--nav-item_selected" tabindex="0" arial-label="battery" role="tab" data-id-selected="true">
				<h2 class="packages-options--nav-title"><span>UV Light Device</span></h2>
			</li>
			<li class="packages-options--nav-item " tabindex="0" arial-label="paint" role="tab" data-id-selected="false">
				<h2 class="packages-options--nav-title"><span>Accessories</span></h2>
			</li>
			<li class="packages-options--nav-item" tabindex="0" arial-label="interior" role="tab" data-id-selected="false">
					<h2 class="packages-options--nav-title"><span>Payment</span></h2>
			</li>
		</ul>
	</div>
	<div class="wrap-container--next wrap-container--item right-nav"></div>
</div>

<div class="germzapper flex-cont">
	<div class="germzapper-container">
		<div class="main-content p-0">
			@if($case == 'handheld-case') 
				<img src="/images/germzapper/handheld-case.png" class="w-100" alt="germzapper handheld case" />
			@else
				<img src="/images/germzapper/multiuse-case.png" class="w-100" alt="germzapper multipupose case"  />
			@endif

			<div class="device-specs">
				<ul class="d-flex font-centrale-sansbook" style="padding:5px 25px">
					<li class="specs--item">
						<span class="specs--value">
						<span class="specs--value-label"><img src="/images/germzapper/uv.png" class="" style="width:35%;" alt="germzapper uv" /></span></span>
					</li>
					<li class="specs--item">        
						<div class="specs--value"><span class="numscroller" data-min='1' data-max='300' data-delay='25' data-increment='30'>300</span><span class="specs--value-label">w</span></div>
						<span class="specs--label"><span>UV Light Intensity</span></span>
					</li>
					<li class="specs--item">
						<div class="specs--value"><span class="numscroller" data-min='1' data-max='257.3' data-delay='25' data-increment='30'>257.3</span><span class="specs--value-label">nm</span></div>
						<span class="specs--label">UV Wavelength</span>
					</li>
					<li class="specs--item br-0">
						<div class="specs--value"><span class="numscroller" data-min='1' data-max='3' data-delay='1' data-increment='1'>3</span><span class="specs--value-label">sec*</span></div>
						<span class="specs--label">Time to Kill 99.9% Pathogens <br>(6-inches) </span>
					</li>
				</ul>
			</div> 

		</div>

		@php ($prid = $case == 'handheld-case' ? '5ece4797eaf5e' : '5f40ea9551b84')
    	@php ($ozone1 = [])
     	@foreach($items['list'] as $key => $data)  
           	@if($data['id'] == $prid)
           		@php ($ozone1 = $data)
					@break
				@endif
       	@endforeach		

		@php ($prid = $case == 'handheld-case' ? '5f3a352595d32' : '5f40eaa2c8bcf')
    	@php ($ozone2 = [])
     	@foreach($items['list'] as $key => $data)  
           	@if($data['id'] == $prid)
           		@php ($ozone2 = $data)
					@break
				@endif
       	@endforeach	

		<div class="right-content gz-content" style="overflow: auto">
			<div class="pp">
				<h5 class="font-weight-medium">Select Your bulb</h5>
				<div class="tds-switch_toggle tds-switch_toggle--label_inside tds-switch_toggle--is_filled savings-toggle__Switch">
					<div class="tds-switch_toggle-container savings-toggle__Switch-container {{ (int)$ozone1['selected'] == 1 ? 'switch_ozone_free' : 'switch_ozone' }}">
						<input class="tds-switch_toggle-checkbox savings-toggle__Switch-checkbox" type="checkbox" id="savingsToggle" checked="">
						<label class="tds-switch_toggle-track savings-toggle__Switch-toggle-track" for="savingsToggle" aria-label="savingsToggle"></label>
						<label class="tds-switch_toggle-btn savings-toggle__Switch-btn" for="savingsToggle" aria-hidden="true">Ozone Free*</label>
						<label class="tds-switch_toggle-btn savings-toggle__Switch-btn" for="savingsToggle" aria-hidden="true">+Ozone</label>
					</div>
				</div>
			
				<p class="font-weight-semibold">All cases have a lifetime guarantee</p>
				<div class="my-15">
					<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>&nbsp;(7)
				</div>

				<div id="btnozonefree" class="btn-ozone text-dark w-100 bulb {{ (int)$ozone1['selected'] == 1 ? 'sel' : '' }}" data-id="{{ $ozone1['id'] }}" data-type="bulb">
					<div class="d-flex w-100" style="flex-direction: row;justify-content: space-between;">
						<span class="font-raleway font-weight-semibold"><p class="text-left font-weight-semibold font-opensans font-size-14">{{ $ozone1['description'] }}</p></span>
						<p class="text-right font-opensans font-size-14">${{ number_format(floatval($ozone1['price']), 2, '.', ',') }}</p>
					</div>
				</div>

				<div id="btnozone" class="btn-ozone text-dark w-100 bulb {{ (int)$ozone2['selected'] == 1 ? 'sel' : '' }}" data-id="{{ $ozone2['id'] }}" data-type="bulb">
					<div class="d-flex w-100" style="flex-direction: row;justify-content: space-between;">
						<span class=""><p class="text-left font-weight-semibold font-opensans font-size-14">{{ $ozone2['description'] }}</p></span>
						<p class="text-right font-opensans font-size-14">${{ number_format(floatval($ozone2['price']), 2, '.', ',') }}</p>
					</div>
				</div>

				<div id="ozone_free_txt" class="pt-10 {{ (int)$ozone1['selected'] == 1 ? '' : 'hide' }}">
					<p class="font-opensans font-size-14 text-left">
					@if($case == 'multipurpose-case') 
						GermZapper 300® mutli-function case is portable enough to hold in your hands, yet powerful enough to disinfect a room.  Kill 99.99% of all pathogens on surfaces and in the air without the use of harsh chemicals - all in one easy-to-use device.   Just turn it on, shine it where you want and  sanitize— a brighter way to clean.  Get yours today.   
					@else
						GermZapper 300® is a handheld, portable germicidal UVC disinfection device designed to deactivate bacteria, viruses and fungi on surfaces where chemical sprays aren’t appropriate and strong enough to disinfect a 645 sqft room in 3 minutes. 
					@endif
				</p>

				<ul class="my-30 font-opensans font-size-14 font-weight-light text-left">
					@if($case == 'multipurpose-case') 
					<li><span class="fa fa-circle"></span> Multi- function case quickly enables room disinfection</li>
					@endif
					<li><span class="fa fa-circle"></span> 300 watts of germicidal UV light</li>
					<li><span class="fa fa-circle"></span> For home or office </li>
					<li><span class="fa fa-circle"></span> 253.7 nm wavelength </li>
					<li><span class="fa fa-circle"></span> Kills 99.9% of all pathogens in 3 seconds at 6 inches </li>
					<li><span class="fa fa-circle"></span> Ozone Free</li>
					<li><span class="fa fa-circle"></span> Safety design</li>
					<li><span class="fa fa-circle"></span> High-quality & durable </li>
 				</ul>


				</div>
				<div id="ozone_txt" class="pt-10 {{ (int)$ozone2['selected'] == 1 ? '' : 'hide' }}">
					@if($case == 'multipurpose-case')
						<p class="font-opensans font-size-14 text-left">GermZapper 300® multi- function case with Ozone is portable enough to hold in your hands, yet powerful enough to disinfect a room.  Kill 99.99% of all pathogens on surfaces and in the air without the use of harsh chemicals - all in one easy-to-use device.   Just turn it on, shine it where you want and sanitize— a brighter way to clean.</p> <br>  

						<p class="font-opensans font-size-14 text-left">Deactivate bacteria, viruses and fungi on surfaces where chemical sprays aren’t appropriate and strong enough to disinfect a 645 sqft room in 3 minutes.</p> <br>  

						<p class="font-opensans font-size-14 text-left">Our ozone generating germicidal lamp helps eradicate foul odor and harmful volatile organic compounds (VOCs) such as sulfides, mercaptans and ammonia.  One distinct advantage of ozone is that it can be carried in the air to places where direct UV radiation cannot reach, optimizing  its germ killing efficiency as a UV air purification system, providing an additional deodorizing effect.</p>
					@else
						<p class="font-opensans font-size-14 text-left">GermZapper OZ300 (+ ozone) is a handheld, portable germicidal UVC disinfection device designed to deactivate bacteria, viruses and fungi on surfaces where chemical sprays aren’t appropriate and strong enough to disinfect a 645 sqft room in 3 minutes. </p> <br>

						<p class="font-opensans font-size-14 text-left">GermZapper OZ-300™ offers a ozone generating germicidal lamp that helps eradicate foul odor and harmful volatile organic compounds (VOCs) such as sulfides, mercaptans and ammonia.  One distinct advantage of ozone is that it can be carried in the air to places where direct UV radiation cannot reach, optimizing  its germ killing efficiency as a UV air purification system, providing an additional deodorizing effect.</p>
					@endif
					
					<ul class="my-30 font-opensans font-size-14 font-weight-light text-left">
						@if($case == 'multipurpose-case') 
						<li><span class="fa fa-circle"></span> Multi- function case quickly enables room disinfection</li>
						@endif
						<li><span class="fa fa-circle"></span> 300 watts of germicidal UV light</li>
						<li><span class="fa fa-circle"></span> For home or office </li>
						<li><span class="fa fa-circle"></span> 253.7 nm wavelength </li>
						<li><span class="fa fa-circle"></span> Kills 99.9% of all pathogens in 3 seconds at 6 inches </li>
						<li><span class="fa fa-circle"></span> Ozone Emitting</li>
						<li><span class="fa fa-circle"></span> Safety design</li>
						<li><span class="fa fa-circle"></span> High-quality & durable </li>
 					</ul>

				</div>

				
			</div>
		</div>
	</div>
</div>


<div class="main-footer-container-3 main-footer-container-3--desktop main-footer-mobile--hidden">
	<div class="finance-container">
		<div class="finance-container--footer">
			<div class="finance-content">				
				<div class="finance-content--item">
					<div class="finance-content--item">
						<div class="financetype-selector font-raleway font-weight-semibold letter-spacing-1">TOTAL</div>
					</div>
					<div id="total_cart" class="financetype-selector font-opensans font-weight-semibold font-size-15 text-teal text-center">
						${{ isset($items) ? number_format(floatval($items['total']), 2, '.', ',') : '0.00' }}
					</div>
				</div>
				
				<div class="delivery font-centrale-sansbook font-size-13">
					<div class="modal-trigger" role="link" tabindex="-1">
						<span class="text-lgray">Each unit is custom assembled in Pompano Beach, Florida</span> &nbsp;<span>Estimated Delivery: 2-6 weeks</span>
					</div>
					<div class="text-right tcs line-height-1-5">
						<a href="/contact-us" class="text-white text-underline">For earlier delivery view availability contact us</a>
					</div>
				</div>

				<div class="finance-content--item">
					<button id="btn-step1" type="button" class="btn btn-next">NEXT</button>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@section('footer_scripts')
	<script src="/js/accounting.min.js" type="text/javascript"></script>
	<script src="/js/pages/germzapper.js"></script>
	<script src="/js/numscroller-1.0.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#savingsToggle").change(function() {
				if($(".tds-switch_toggle-container").hasClass("switch_ozone_free")) {
					$(".tds-switch_toggle-container").removeClass("switch_ozone_free").addClass("switch_ozone");
					$('#btnozone').click();
					$("#ozone_txt").removeClass("hide");
            		$("#ozone_free_txt").addClass("hide");
				} else {
					$(".tds-switch_toggle-container").removeClass("switch_ozone").addClass("switch_ozone_free");
					$('#btnozonefree').click();
					$("#ozone_txt").addClass("hide");
            		$("#ozone_free_txt").removeClass("hide");
				}

				//console.log(this.checked);
			})
			

			$(window).scroll(function() {
				checkScroll();				
			});
			checkScroll();

			function checkScroll() {
				var windowWidth = $(window).width();
				if(windowWidth <=1024) {
					if($(this).scrollTop() > 200) {
						$(".finance-container--footer").fadeIn();
					} else {
						$(".finance-container--footer").fadeOut();
					}
				}
			}
		});
	</script>
@endsection

