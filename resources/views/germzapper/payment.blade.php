@extends('layouts.master')

@section('header_scripts')
<link href="/css/germzapper.css" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="main-menu-container main-menu-container" style="top: 0px;">
    <div class="container-branding">
        <a href="/" class="logo" title="Germbusters911"><img src="/images/logo@1x.png" alt="logo"></a>
    </div>
    <div class="package-options--nav">
        <ul class="packages-options--nav-list" role="tablist" arial-label="Main menu">
            <li id="menu-step1" class="packages-options--nav-item" tabindex="0" arial-label="battery" role="tab" data-id-selected="false">
                <h2 class="packages-options--nav-title"><span>UV Light Device</span></h2>
            </li>
            <li id="menu-step2" class="packages-options--nav-item" tabindex="0" arial-label="paint" role="tab" data-id-selected="false">
                <h2 class="packages-options--nav-title"><span>Accessories</span></h2>
            </li>
            <li class="packages-options--nav-item packages-options--nav-item_selected" tabindex="0" arial-label="interior" role="tab" data-id-selected="true">
                    <h2 class="packages-options--nav-title"><span>Payment</span></h2>
            </li>
        </ul>
    </div>
    <div class="wrap-container--next wrap-container--item right-nav"></div>
</div>


    <form id="payment-form" class="form-horizontal" action="/uv-light-disinfection-device/process" method="POST" data-cc-on-file="false" data-stripe-publishable-key="{{ $stripe['pk'] }}">
        {{ csrf_field() }}

        <input type="hidden" name="cardname" value="">
        <input id="casetype" name="casetype" type="hidden" value="{{ $case }}">

        <div class="germzapper flex-cont">
        	<div  id="gz-payment" class="germzapper-container">
        		<div class="main-content p-0 bg-gray">
        			<div class="form">
                        <div class="mt-20 usr-info">
                            <div class="row">

                                <p class="pb-10">Enter Account Details</p>

                                <div class="col-md-6 required">
                                    <label>First Name</label>
                                    <input id="fname" name="fname" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                                <div class="col-md-6 required">
                                    <label>Last Name</label>
                                    <input id="lname" name="lname" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 required">
                                    <label>Email Address</label>
                                    <input id="email" name="email" type="text" data-delay="300"  class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                             
                                <div class="col-md-6 required">
                                    <label>Phone Number</label>
                                    <input id="phone" name="phone" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                            <div class="row">
                                <p class="font-centrale-sansbook font-size-14 line-height-1-5">By entering my account details above, I agree to be contacted about GermBuster products, including automated texts. <br>  This is not a condition of purchase</p>

                                <p class="py-10">Payment</p>

                                <div class="col-md-12 card-name">
                                    <label>Name on Card</label>
                                    <input id="input-cardholder" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>

                                  <div class="col-md-12 card-number">
                                    <label>Credit Card Number</label>
                                    <input id="input-cardnumber" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-4 card-expiry">
                                    <label>Expiration Date</label>
                                    <input id="input-expdate" type="text" data-delay="300" class="input" placeholder="MM / YYYYY">
                                    <div class="message">&nbsp;</div>
                                </div>
                                <div class="col-md-4 card-cvc">
                                    <label>CVV</label>
                                    <input id="input-seccode" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                                  <div class="col-md-4 required">
                                    <label>Billing Zip Code</label>
                                    <input id="zipcode" name="zipcode" type="text" data-delay="300" class="input">
                                    <div class="message">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
        		</div>

        		<div class="right-content gz-content" style="overflow: auto;">
                    <div class="pp">
        			<h5 class="font-weight-semibold">Your GermZapper 300</h5>
        			<p>Estimated  delivery 2-6 weeks</p>

                    @php ($bulb = [])
                    @foreach($items['list'] as $key => $data) 
                        @if((strtolower($data['type']) == 'bulb') && ((int)$data['selected']) == 1) 
                            @php ($bulb = $data)
                            @break;
                        @endif
                    @endforeach

        			<div class="py-20"> 
                    @if($case == 'multipurpose-case')                         
        				<img src="/images/germzapper/sm-multipurpose.png" class="width-auto" alt="GermZapper handheld-case"  />
                    @else
                        <img src="/images/germzapper/sm-handheld.png" class="width-auto" alt="germzapper multipurpose-case"  />
                    @endif 
                     @foreach($items['list'] as $key => $data) 
                        @if(strtolower($data['description']) == 'stand kit'  && ((int)$data['selected']) == 1)
        				    <img src="/images/germzapper/sm-standkit.png" class="width-auto" alt="germzapper stand kit"  />
                        @endif
                        @if(strtolower($data['description']) == 'carry case' && ((int)$data['selected']) == 1)
        				    <img src="/images/germzapper/sm-case.png" class="width-auto" alt="germzapper case" /> 
                        @endif
                      @endforeach
        			</div>
        			
                    <div class="pb-10 w-100 font-size-20 text-left" style="border-bottom:1px solid #ccc;">
                        <span>Summary</span>
        			</div>

                   
                    <div class="my-20 summary-order text-left">
                        <li>GermZapper 300  <span class="float-right">${{ number_format(floatval($bulb['price']), 2, '.', ',') }}</span></li>
                        <li>{{ $bulb['case-type'] == 'handheld-case' ? 'Handheld case' : 'Multipurpose case' }}<span class="float-right">Included</span></li>
                        <li>{{ $bulb['description'] }} <span class="float-right">Included</span></li>

                        @foreach($items['list'] as $key => $data) 
                            @if((strtolower($data['type']) == 'case') && ((int)$data['selected']) == 1) 
                                <li>{{ $data['description'] }}  <span class="float-right">${{ number_format(floatval($data['price']), 2, '.', ',') }}</span></li>
                            @endif
                        @endforeach

                        <li class="font-weight-bold" style="border-bottom:1px solid #ccc;">Purchase Price <span class="float-right">${{ number_format(floatval($items['total']), 2, '.', ',') }}</span></li>
                        <li class="font-weight-bold">Total Price <span class="float-right">${{ number_format(floatval($items['total']), 2, '.', ',') }}</span></li>
                    </div>

                    <div class="font-centrale-sansbook font-size-14 text-left"><input id="agreement" type="checkbox"> By agreeing to this, I agree to the <a href="/terms-conditions" class="text-dark text-underline font-weight-semibold" target="_blank">Terms of Use</a> and <a href="/privacy-policy" class="text-dark text-underline font-weight-semibold" target="_blank">Privacy Policy</a></div>
        		</div>
            </div>

        	</div>
        </div>

        <div class="main-footer-container-3 main-footer-container-3--desktop main-footer-mobile--hidden">
        	<div class="finance-container">
        		<div class="finance-container--footer">
        			<div class="finance-content">        				
        				<div class="finance-content--item">
                            <div class="finance-content--item">
                                <div class="financetype-selector font-raleway font-weight-semibold letter-spacing-1">TOTAL</div>
                            </div>
        					<div class="financetype-selector font-opensans font-weight-semibold font-size-15 text-teal">
                                ${{ isset($items) ? number_format(floatval($items['total']), 2, '.', ',') : '0.00' }}               
                            </div>
        				</div>
        				
        				<div class="delivery font-centrale-sansbook font-size-13">
        					<div class="modal-trigger" role="link" tabindex="-1">
        						<span class="text-lgray">Each unit is custom assembled in Pompano Beach, Florida</span> &nbsp;<span>Estimated Delivery: 2-6 weeks</span>
        					</div>
        					<div class="text-right line-height-1-5">
        						<a href="/contact-us" class="text-white text-underline">For earlier delivery view availability contact us</a>
        					</div>
        				</div>

        				<div class="finance-content--item">
        					<button id="btn-step3" type="submit" class="btn btn-next" disabled>PLACE ORDER</button>
        				</div>

        			</div>
        		</div>
        	</div>
        </div>
    </form>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="/js/library.js"></script>
    <script type="text/javascript" src="/js/jquery.payment.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

        <script type="text/javascript">
        $(function() {

           $('#menu-step1').on('click', function() {
                location.href = '/germzapper-uv-light-disinfection-device/'.concat($("#casetype").val());
            });

            $('#menu-step2').on('click', function() {
                location.href = '/germzapper-uv-light-disinfection-device/accessories/'.concat($("#casetype").val());
            });

            
            var $form = $("#payment-form"); 

            function IsValidInput($elem, reqmsg, errmsg)
            {
                if($elem.find("input").val().trim() == "") {
                    $elem.find(".message").addClass("error").html(reqmsg);
                } else {
                    var haserror = true;

                    if($elem.hasClass("card-name") != "") {
                        haserror = false;
                    }

                    if($elem.hasClass("card-number")) {
                        haserror = !$.payment.validateCardNumber($elem.find("input").val());
                    }

                    if($elem.hasClass("card-expiry")) {
                        var expr = $.payment.cardExpiryVal($elem.find("input").val());
                        haserror = !$.payment.validateCardExpiry(expr.month, expr.year);
                    } 

                    if($elem.hasClass("card-cvc")) {
                        haserror = !$.payment.validateCardCVC($elem.find("input").val());
                    }

                    if(haserror) {
                        $elem.find(".message").addClass("error").html(errmsg);
                    } else {
                        return true;
                    }
                }

                $elem.find("input").addClass("input-error");

                return false;
            }

            function ProcessPayment(isprocess)
            {
                EnablePaymentButton(!isprocess);    
                EnableCreditDetails(!isprocess);

                if(!isprocess) {
                    $form.find("#ship-addr").removeAttr("disabled");
                } else {
                    $form.find("#ship-addr").attr("disabled", true);
                }

                if(isprocess) {
                    $("#loader").show();
                } else {
                    $("#loader").hide();
                }
            }

            function EnableCreditDetails(isenable) 
            {
                if(isenable) {
                    $form.find(".credit-card input").removeAttr("disabled");
                } else {
                    $form.find(".credit-card input").attr("disabled", true);
                }
            }

            function EnablePaymentButton(isenable)
            {
                if(isenable) {
                    $form.find("#creditcard-button").removeAttr("disabled");
                } else {
                    $form.find("#creditcard-button").attr("disabled", true); 
                }
            }

            $form.bind("submit", function(e) {
                e.preventDefault();
       
                $form.find(".error").html("&nbsp;").removeClass("error");
                $form.find(".input-error").removeClass("input-error");
                $form.removeClass("form-error");

                /* first name */
                $elem = $form.find("input[name='fname']");
                if(isRequired($elem, "Please provide your first name")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    }
                }

                /* last name */
                $elem = $form.find("input[name='lname']");
                if(isRequired($elem, "Please provide your last name")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    } 
                } 

                 /* phone */
                $elem = $form.find("input[name='phone']");
                if(isRequired($elem, "Please enter a valid phone number")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    } 
                }          

                /* email */
                $elem = $form.find("input[name='email']");
                if(isRequired($elem, "Please provide your email address")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    }       
                }

                if(!isEmail($elem , "Please enter a valid email address.")) {
                    iserr = !iserr ? true : iserr;            
                }

                /* zip code */
                $elem = $form.find("input[name='zipcode']");
                if(isRequired($elem, "Please enter a valid zip code")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    } 
                }  

                if(!IsValidInput($form.find(".card-number"), "Card number is required.", "The card number is not a valid credit card number.")) {
                    if(!IsValidInput($form.find(".card-name"), "Cardholder name is required.", "")) {
                        if(!$form.hasClass("form-error")) {
                            $form.addClass("form-error");
                        }
                    }
                }

                if(!IsValidInput($form.find(".card-number"), "Card number is required.", "The card number is not a valid credit card number.")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    }
                }

                if(!IsValidInput($form.find(".card-expiry"), "Card expiry is required.", "Your card's expiration is invalid.")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error");
                    }
                }

                if(!IsValidInput($form.find(".card-cvc"), "CVV is required.", "Your card's security code is invalid.")) {
                    if(!$form.hasClass("form-error")) {
                        $form.addClass("form-error"); 
                    }                            
                }

                if($form.hasClass("form-error")) {
                    e.preventDefault();   

                    $form.find(".input-error").first().focus().scroll();            
                }
            }); 

            $form.on("submit", function(e) { 
                e.preventDefault();

                if(!$form.hasClass("form-error")) {
                    $form.find("input[name='cardname']")
                         .val($form.find(".card-name input").val())

                    ProcessPayment(true);

                    if(!$form.data('cc-on-file')) {
                        Stripe.setPublishableKey($form.data('stripe-publishable-key'));

                        var expr = $.payment.cardExpiryVal($form.find(".card-expiry input").val());
                        var card = {
                                            name: $form.find(".card-name input").val(),
                                          number: $form.find(".card-number input").val(),
                                             cvc: $form.find(".card-cvc input").val(),
                                       exp_month: expr.month,
                                        exp_year: expr.year
                                   }

                        $(this).find(".error").html("&nbsp;").removeClass("error");

                        // create token for customer
                        Stripe.createToken(card, function(status, response) {
                            if (response.error) {
                                switch(response.error.param.toLowerCase()) {
                                    case "number":
                                        $form.find(".card-number").find(".message").addClass("error").html(response.error.message);
                                        break;
                                    case "cvc":
                                        $form.find(".card-cvc").find(".message").addClass("error").html(response.error.message);
                                        break;                                            
                                    case "exp_month":
                                    case "exp_year":
                                        $form.find(".card-expiry").find(".message").addClass("error").html(response.error.message);
                                        break;                                                 
                                    default:
                                        alert(response.error.message);
                                        break;
                                }

                                ProcessPayment(false);
                            } else {
                                // insert the charge token into the form.
                                $form.append("<input type='hidden' name='stripetoken' value='" + response['id'] + "'/>");
                                // submit form
                                $form.get(0).submit();
                            }
                        });
                    }
                }
            }); 


            $("#agreement").click(function(e) {
                if($(this).is(":checked")) {
                    $('#btn-step3').removeAttr("disabled");
                } else {
                    $('#btn-step3').attr("disabled", true);
                }
            })

            $(window).scroll(function() {
				checkScroll();				
			});
			checkScroll();

			function checkScroll() {
				var windowWidth = $(window).width();
				if(windowWidth <=1024) {
					if($(this).scrollTop() > 200) {
						$(".finance-container--footer").fadeIn();
					} else {
						$(".finance-container--footer").fadeOut();
					}
				}
			}
        });
    </script>
@endsection

