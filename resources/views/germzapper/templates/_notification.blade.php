<b>Contact Firstname: </b>{{ $order->firstname }}
<br />
<b>Contact Lastname: </b>{{ $order->lastname }}
<br />
<b>Phone: </b>{{ $order->phone }}
<br />
<b>Zip Code: </b>{{ $order->billing_zipcode }}
<br />
<b>Email: </b>{{ $order->email }}
<br /><br />

@php ($bulb = [])
@foreach($items['list'] as $key => $data) 
    @if((strtolower($data['type']) == 'bulb') && ((int)$data['selected']) == 1) 
        @php ($bulb = $data)
        @break;
    @endif
@endforeach


<table style="width:400px;">
     <tr style="background-color: #ddd;">
        <td><b>Item</b></td>
        <td style="text-align: right;"><b>Price</b></td>
    </tr>
    <tr>
        <td>GermZapper 300</td>
        <td style="text-align: right;">${{ number_format(floatval($bulb['price']), 2, '.', ',') }}</td>
    </tr>
	<tr>
		<td>{{ $bulb['case-type'] == 'handheld-case' ? 'Handheld case' : 'Multiuse case' }}</td>
		<td style="text-align: right;">Included</td>
	</tr>
	<tr>
		<td>{{ $bulb['description'] }}</td>
		<td style="text-align: right;">Included</td>
	</tr>
                        
    @foreach($items['list'] as $key => $data) 
        @if((strtolower($data['type']) == 'case') && ((int)$data['selected']) == 1) 
        	<tr>
            	<td>{{ $data['description'] }}</td>
            	<td style="text-align: right;">${{ number_format(floatval($data['price']), 2, '.', ',') }}</td>
            </tr>
        @endif
    @endforeach

    <tr>
 		<td>Purchase Price</td>
 		<td style="text-align: right;">${{ number_format(floatval($items['total']), 2, '.', ',') }}</td>
    </tr>
   
    <tr>
 		<td><b>Total Price</b></td>
 		<td style="text-align: right;"><b>${{ number_format(floatval($items['total']), 2, '.', ',') }}</b></td>
	</tr>
</table>



