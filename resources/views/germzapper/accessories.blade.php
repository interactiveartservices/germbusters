@extends('layouts.master')

@section('header_scripts')
<link href="/css/germzapper.css" rel="stylesheet" type="text/css">
@endsection

@section('content')

<input id="casetype" type="hidden" value="{{ $case }}">

<div class="main-menu-container main-menu-container" style="top: 0px;">
	<div class="container-branding">
		<a href="/" class="logo" title="Germbusters911"><img src="/images/logo@1x.png" alt="logo"></a>
	</div>
	<div class="package-options--nav">
		<ul class="packages-options--nav-list" role="tablist" arial-label="Main menu">
			<li id="menu-step1" class="packages-options--nav-item" tabindex="0" arial-label="battery" role="tab" data-id-selected="false">
				<h2 class="packages-options--nav-title"><span>UV Light Device</span></h2>
			</li>
			<li class="packages-options--nav-item packages-options--nav-item_selected" tabindex="0" arial-label="paint" role="tab" data-id-selected="true">
				<h2 class="packages-options--nav-title"><span>Accessories</span></h2>
			</li>
			<li class="packages-options--nav-item" tabindex="0" arial-label="interior" role="tab" data-id-selected="false">
					<h2 class="packages-options--nav-title"><span>Payment</span></h2>
			</li>
		</ul>
	</div>
	<div class="wrap-container--next wrap-container--item right-nav"></div>
</div>

<div class="germzapper flex-cont">
	<div class="germzapper-container">
		<div class="main-content p-0">
		
			 <div class="row px-30">
				@php ($prid = '5f3792cd65134')
		    	@php ($acc1 = [])
		     	@foreach($items['list'] as $key => $data)  
                   	@if($data['id'] == $prid)
                   		@php ($acc1 = $data)
  						@break
  					@endif
  				@endforeach	

				@php ($prid = '5f379728eef24')
		    	@php ($acc2 = [])
		     	@foreach($items['list'] as $key => $data)  
                   	@if($data['id'] == $prid)
                   		@php ($acc2 = $data)
  						@break
  					@endif
  				@endforeach	

				<div class="col-xs-12 col-lg-6">
					<img src="/images/germzapper/stand-kit.png" class="w-100" />	
					<div class="my-10">
						<span class="font-weight-semibold line-height-1-5">{{ $acc1['description'] }}</span>
						<span class="float-right">
							${{ number_format(floatval($acc1['price']), 2, '.', ',') }}
						</span>
					</div>
		    		<span class="mt-10 text-lgray">Wheeled stand<br>UV Case mount</span>				
				</div>

				<div class="col-xs-12 col-lg-6">
					<img src="/images/germzapper/carry-case.png" class="w-100" />	
					<div class="my-10">
						<span class="font-weight-semibold line-height-1-5">{{ $acc2['description'] }}</span>
						<span class="float-right">
							${{ number_format(floatval($acc2['price']), 2, '.', ',') }}
						</span>
					</div>		
				</div>
				
			</div>
		</div>

		<div class="right-content gz-content" style="overflow: auto">
			<div class="pp">
				<h5 class="font-weight-medium">Accessories</h5>

				<div class="tds-switch_toggle tds-switch_toggle--label_inside tds-switch_toggle--is_filled savings-toggle__Switch">
					<div class="tds-switch_toggle-container savings-toggle__Switch-container {{ (int)$acc1['selected'] == 1 ? 'switch_stand_kit' : 'switch_carry_case' }}">
						<input class="tds-switch_toggle-checkbox savings-toggle__Switch-checkbox" type="checkbox" id="savingsToggle" checked="">
						<label class="tds-switch_toggle-track savings-toggle__Switch-toggle-track" for="savingsToggle" aria-label="savingsToggle"></label>
						<label class="tds-switch_toggle-btn savings-toggle__Switch-btn" for="savingsToggle" aria-hidden="true">Stand kit</label>
						<label class="tds-switch_toggle-btn savings-toggle__Switch-btn" for="savingsToggle" aria-hidden="true">Carry-case</label>
					</div>
				</div>
				
				<div class="my-15">
					<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>&nbsp;(7)
				</div>
					<div class="btn-ozone text-dark w-100 accessories {{ (int)$acc1['selected'] == 1 ? 'sel' : '' }}" data-id="{{ $acc1['id'] }}" data-type="case" data-accessories="{{ $acc1['description'] }}">
						<div class="d-flex w-100" style="flex-direction: row;justify-content: space-between;">
							<span class="font-raleway font-weight-semibold">
								<p class="text-left font-weight-semibold font-opensans font-size-14">{{ $acc1['description'] }}</p>
							</span>
							<p class="text-right font-opensans font-size-14">
								${{ number_format(floatval($acc1['price']), 2, '.', ',') }}
							</p>
						</div>
					</div>

					<div class="btn-ozone text-dark w-100 accessories {{ (int)$acc2['selected'] == 1 ? 'sel' : '' }}" data-id="{{ $acc2['id'] }}" data-type="case" data-accessories="{{ $acc2['description'] }}">
						<div class="d-flex w-100" style="flex-direction: row;justify-content: space-between;">
							<span class="">
								<p class="text-left font-weight-semibold font-opensans font-size-14">{{ $acc2['description'] }}</p>
							</span>
							<p class="text-right font-opensans font-size-14">
								${{ number_format(floatval($acc2['price']), 2, '.', ',') }}
							</p>
						</div>
					</div>
			
				<p id="acc_details" class="pt-10 text-left">
					<span id="standkit_txt" class="{{ (int)$acc1['selected'] == 1 ? '' : 'hide' }}">
						GermZapper room disinfection stand kit features a 8.5’ folding wheeled base. The stand is a heavy duty, black and chrome-plated steel and aluminum alloy support that features a low base for extra stability, braking wheels, two risers, and a 5/8” baby stud. The legs fold for transport and storage. It is specifically designed to support the GermZapper mount to angle the UV light device in a variety of angles to meet your specific disinfection needs.
					</span>
					<span id="carrycase_txt" class="{{ (int)$acc2['selected'] == 1 ? '' : 'hide' }}">
						GermZapper carry case is the roomiest hand-carry size up to 40% lighter than other polymer cases, GermZapper carry case will lighten the loads of your disinfection routines.  Discover a more effortless user experience and the perfect carry case tailored made for GermZapper UV light device.
					</span>	
				</p> 
			</div> 			
		</div>
	</div>
</div>


<div class="main-footer-container-3 main-footer-container-3--desktop main-footer-mobile--hidden">
	<div class="finance-container">
		<div class="finance-container--footer">
			<div class="finance-content">				
				<div class="finance-content--item">
					<div class="finance-content--item">
						<div class="financetype-selector font-raleway font-weight-semibold letter-spacing-1">TOTAL</div>
					</div>
					<div id="total_cart" class="financetype-selector font-opensans font-weight-semibold font-size-15 text-teal">
						${{ isset($items) ? number_format(floatval($items['total']), 2, '.', ',') : '0.00' }}
					</div>
				</div>
				
				<div class="delivery font-centrale-sansbook font-size-13">
					<div class="modal-trigger" role="link" tabindex="-1">
						<span class="text-lgray">Each unit is custom assembled in Pompano Beach, Florida</span> &nbsp;<span>Estimated Delivery: 2-6 weeks</span>
					</div>
					<div class="text-right line-height-1-5">
						<a href="/contact-us" class="text-white text-underline">For earlier delivery view availability contact us</a>
					</div>
				</div>

				<div class="finance-content--item">
					<button id="btn-step2" type="button" class="btn btn-next">NEXT</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer_scripts')
	<script src="/js/accounting.min.js" type="text/javascript"></script>
	<script src="/js/pages/germzapper.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#savingsToggle").change(function() {
				if($(".tds-switch_toggle-container").hasClass("switch_stand_kit")) {
					$(".tds-switch_toggle-container").removeClass("switch_stand_kit").addClass("switch_carry_case");
					  $("#carrycase_txt").removeClass("hide");
            		  $("#standkit_txt").addClass("hide");
				} else {
					$(".tds-switch_toggle-container").removeClass("switch_carry_case").addClass("switch_stand_kit");
					  $("#carrycase_txt").addClass("hide");
             		  $("#standkit_txt").removeClass("hide");
				}

				//console.log(this.checked);
			});

			$(window).scroll(function() {
				checkScroll();				
			});
			checkScroll();

			function checkScroll() {
				var windowWidth = $(window).width();
				if(windowWidth <=1024) {
					if($(this).scrollTop() > 200) {
						$(".finance-container--footer").fadeIn();
					} else {
						$(".finance-container--footer").fadeOut();
					}
				}
			}
			
		});
	</script>
@endsection

