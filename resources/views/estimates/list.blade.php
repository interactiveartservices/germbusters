@extends('layouts.master')

@section('content')


  <div class="container">
        <div class="form mt-40 mb-60">
                <div class="">
                    <div class="main-title">
                         <h2>
	                         <a href="/list/estimates" class="{{ $type=='estimates' ? 'text-teal font-weight-bold' : 'text-dark font-weight-light' }}">Estimates</a> 
	                         <span class="text-teal font-weight-light">|</span> 
	                         <a href="/list/invoices" class="{{ $type=='invoices' ? 'text-teal font-weight-bold' : 'text-dark font-weight-light' }}">Invoices</a>
	                         <span class="text-teal font-weight-light">|</span> 
	                         <a href="/get-quote-list" class="font-weight-light text-dark" target="_blank">Web Inquires</a>
	                     </h2> 
                    </div>
                </div>

                  <div class="mt-40" style="border-bottom:solid 2px #ccc;">
                    <div class="title">
                    	<span class="">{{ $type=='estimates' ? 'ESTIMATES' : 'INVOICES' }}</span>
                    	@if($type=='estimates')
                        	<span class="add-estimate"><a href="/estimate/create/commercial/0" target="_blank" class="text-dark">ADD NEW ESTIMATE <img src="/images/Icon awesome-plus-square.png" class="width-auto ml-10" /></a></span>
                        @endif
                    </div>

	                <div class="mt-20 table-scroll">
						<div class="table-scroll-width">
							<div class="d-flex font-weight-semibold bg-light-gray p-10 font-size-15" >
								<span class="col-xs-2">NAME</span>
								<span class="col-xs-2">COMPANY</span>
								<span class="col-xs-1">{{ $type=='estimates' ? 'DATE CREATED' : 'SCHEDULED DATE' }}</span>
								<span class="col-xs-1">TYPE</span>
								<span class="col-xs-1">PROMO</span>
								<span class="col-xs-2">ADDRESS</span>
								<span class="col-xs-1">STATUS</span>
								<span class="col-xs-1">ESTIMATE</span>
								<span class="col-xs-1">{{ $type=='estimates' ? 'GENERAL ESTIMATE' : 'PAYMENT DUE' }}</span>
								<span class="col-xs-1"></span>
							</div>

							@foreach($invoices as $invoice)
								<div class="d-flex data font-size-15">
									<span class="col-xs-2">
										@if($type=='estimates')
											<a href="/estimate/create/{{ strtolower($invoice->type) }}/{{ $invoice->id }}" target="_blank" class="text-teal font-weight-semibold">{{ $invoice->name }}</a>
										@else

										@if($invoice->invoice_status == "Paid")
											<a href="/invoice/checkout/paid/{{ $invoice->payment_id }}/{{ urlencode(base64_encode($invoice->payment_id)) }}" target="_blank" class="text-teal font-weight-semibold">{{ $invoice->name }}</a>
											@else
											<a href="/invoice/{{ strtolower($invoice->type) }}/{{ $invoice->id }}/{{ urlencode(base64_encode($invoice->id)) }}" target="_blank" class="text-teal font-weight-semibold">{{ $invoice->name }}</a>
											@endif
											
										@endif    
									</span>
									<span class="col-xs-2" style="word-wrap:break-word;">{{ $invoice->company }}</span>
									<span class="col-xs-1">{{ $type=='estimates' ? $invoice->date_added : $invoice->scheduled_date }}</span>
									<span class="col-xs-1">{{ $invoice->type }}</span>
									<span class="col-xs-1">{{ $invoice->promo_code }}</span>
									<span class="col-xs-2">{{ $invoice->address }}</span>
									<span class="col-xs-1">
									@if($type=='estimates')
										{{ $invoice->status }}
									@else
										<span class="{{ ($invoice->invoice_status=='Not Paid') ? 'text-red' : '' }} p-0">{{ $invoice->invoice_status }}</span>
									@endif
									</span>

									<span class="col-xs-1 estimate" data-estimate="{{ ($type=='estimates' && $invoice->status=='Inquiry') ? $invoice->total : $invoice->invoice_total }}">${{ number_format(floatval($type=='estimates' && $invoice->status=='Inquiry' ? $invoice->total : $invoice->invoice_total), 2, '.', ',') }}</span>
								

									@if($type=='estimates')
										<span class="col-xs-1">
											<div class="styledCheckbox {{ $invoice->generalestimate==1 ? 'sel' : '' }}">
												<i class="fas fa-check m-0"></i>
											</div>
										</span>
										
										<span class="col-xs-1"><a class="delete-service text-red text-underline font-size-13" href="/list/delete/{{ $invoice->type }}/{{ $invoice->id }}">delete</a></span>
									@else
										<?php

										if($invoice->payment_terms == "Due upon receipt"){
											$payment_due = $invoice->payment_terms;
											$due = "0";
										} else{
		
											$start_date = $invoice->scheduled_date; 
											$current_date = strtotime(date('m/d/Y')); 
										if(str_contains($invoice->payment_terms,'days')){
											$terms = str_replace(" days", "", $invoice->payment_terms);
										}else{	
											$terms = str_replace(" day", "", $invoice->payment_terms);
										}
											
											// Get the difference and divide into  
											// total no. seconds 60/60/24 to get  
											// number of days

											$due_date = strtotime(date('Y-m-d', strtotime($start_date . $terms .' days')));	

											$due = ($due_date -  $current_date)/60/60/24;

											if ($due < 0){
												$payment_due = ($due == "-1") ? str_replace("-","",$due) . ' day overdue' : str_replace("-","",$due) . ' days overdue';
											}else{
												$payment_due = ($due == "1") ? $due . ' day' : $due . ' days';
											}

										}

										?> 
										<span class="col-xs-1 {{ $due < 0 ? 'text-red' : '' }}">{{ $payment_due }}</span>

										<span class="col-xs-1"><a class="delete-service text-red text-underline font-size-13" href="/list/delete/{{ $invoice->type }}/{{ $invoice->id }}">delete</a></span>
									@endif

								</div>
							@endforeach
	                	</div>
	                </div>
                    
                </div>

                <div class="row font-weight-semibold p-10 font-size-20" >
	                <span class="col-xs-8 col-md-9">TOTAL</span>	                     
	                <span class="col-xs-4 col-md-3 total_estimate"></span>
	            </div>

        </div>
 </div>


<script type="text/javascript">
        
    jQuery(document).ready(function($) {
    	 $('.styledCheckbox').on('click',function() {
            if($(this).hasClass("sel")){
                $(this).removeClass('sel');
                $(this).parent().find(".chk").val("0");
            }else{
                $(this).addClass('sel');
                $(this).parent().find(".chk").val("1");
            }
   		});

   		var total = 0;
    	$('#estimate_list').find('.estimate').each(function(){
             total =  parseFloat($(this).attr('data-estimate')) + total;
        })
         $('.total_estimate').text('$'+parseFloat(total).toFixed(2));


    });
</script>

@endsection
