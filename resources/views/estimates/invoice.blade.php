@extends('layouts.master')

@section('header_scripts')
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
<link href="/css/services.css" rel="stylesheet" type="text/css">
<link href="/css/affiliate.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    

    <!--Start Loader-->
    <div id="loader" class="loader" style="display: none;" >
        <img src="/images/loading.gif" alt="" />
    </div>
    <!--End Banner-->

    <div class="content">
        <div class="container login">
          @include('partials.progresstracker', 
           array('steps'=> [
                    'Estimate', 
                    'Review <span class="mobilehide">Estimate</span>',
                    'Invoice',
                    'Review <span class="mobilehide">Your Order</span>'
                    ], 'progress'=>['is-complete', 'is-complete', 'active is-complete', ''] ))                     

            <div class="row">  

             {{ csrf_field() }}  
            
                <div class="col-sm-12 share-alt text-right mt-50 p-0">
                   <!--  <a href="mailto:?subject=Germbusters Review Estimate&amp;body=https://www.germbusters911.com/invoice/commercial/{{ $details->id }}"><span>Share</span> <img class="img-reponsive" src="/images/share-alt-square.png" /></a> -->
                     <a href="/print/invoice/{{$type}}/{{$id}}/{{urlencode(base64_encode($id))}}" target="_blank"><span>Print</span></a>
                   <a href="#" data-toggle="modal" data-target="#modalSendEstimate"><span>Share</span> <img class="img-reponsive" src="/images/share-alt-square.png" /></a>
                </div>                    
                <div id="af-review" class="col border mt-20">

                    <div id="af-review-body">
                        <div class="af-review-header">
                            <div class="row d-flex">
                                <div class="col-sm-6">
                                    <img src="/images/gm-header.png" class="head-logo" />
                                    <span class="billto">BILL TO</span>
                                    <h4 class="client">{{ isset($details) ? ucfirst($details->firstname) : '' }} &nbsp; {{ isset($details) ? ucfirst($details->lastname) : '' }}</h4>
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">{{ isset($details) ? $details->email : '' }}</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">{{ isset($details) ? $details->phone : '' }}<br />
                                            <span>{{ isset($details) ? $details->address1 : '' }}<br />{{ isset($details) ? $details->city : '' }}, {{ isset($details) ? $details->state : '' }} {{ isset($details) ? $details->zipcode : '' }}</span>
                                        </span>
                                    </div>

                                    <span class="billto">PREPARED BY</span>
                                    <h4 class="client">{{ isset($details) ? $details->acct_manager : '' }}</h4>

                                     @if( $details->acct_manager == "Bill Wagonseller") 
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">bill@germbusters911.com</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">704-877-2455<br />
                                            <span>2807 Mount Isle Harbor Dr, <br> Charlotte, NC 28214 </span>
                                        </span>
                                    </div>
                                    @else
                                    <div class="d-flex hl">
                                        <span class="d1">E-Mail:</span>
                                        <span class="d2">{{ $details->acct_manager == "Mike OBrien" ? 'mike@germbusters911.com' : 'scott@germbusters911.com' }}</span><br />
                                    </div>
                                    <div class="d-flex hl">
                                        <span class="d1">Phone:</span>
                                        <span class="d2">{{ $details->acct_manager == "Mike OBrien" ? '704-281-5054' : '954-761-2480' }}<br />
                                        <span>1905 NW 32 St, Building 5,<br> Pompano Beach, FL 33064</span>
                                        </span>
                                    </div> 
                                    @endif
                                </div>
                                
                                <div class="col-sm-6 text-right">
                                    <h1 class="estimate">Invoice</h1>
                                    <span class="invoice-no">Invoice No: 001-{{ str_pad($details->invoice_id, 4, '0', STR_PAD_LEFT) }}</span><br />
                                    <div class="ad-box">
                                        <small>ACCOUNT DUE:</small>
                                        <span>${{ number_format(floatval($details->due), 2, '.', ',') }}</span>
                                    </div>
                                   <!--  <div class="payment-methods">
                                        <span class="p1">PAYMENT METHODS</span>
                                        <hr />
                                        <span class="p2">Paypal: </span><span class="p3">payment@germbusters911.com</span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="af-review-content">
                            <div class="row">
                                <div class="col-sm-12 d-flex justify-space-between head-col">
                                    <div class="rcl">Description</div>
                                    <div class="rcr">Total</div>
                                </div>
                                <div class="col-sm-12">
                                    <hr class="af-div" />
                                </div>                                
                                <div class="col-sm-12 d-flex justify-space-between">
                                    <div class="rcl">
                                        <span>General description</span>
                                        <p>{{ $details->general_desc }} </p>
                                    </div>
                                    <div class="rcr">${{ number_format($details->general_price, 2, '.', ',') }}</div>
                                </div>
                                <div class="col-sm-12">
                                    <hr class="af-div-mid" />
                                </div>

                                @if(isset($invoice_items)) 
                                    @foreach($invoice_items as $value) 
                                        <div class="col-sm-12 d-flex justify-space-between">
                                            <div class="rcl">
                                                <span>{{ $value->title }}</span>
                                                <p>{{ $value->description }}</p>
                                            </div>
                                            <div class="rcr">
                                                ${{ number_format($value->price, 2, '.', ',') }}
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <hr class="af-div-mid" />
                                        </div>
                                    @endforeach
                                @endif

                                <div class="col-sm-12 d-flex justify-space-between">
                                    <div class="rcl">
                                        <span>Disclaimer </span>
                                        <p>{{ $details->description_disclaimer }}</p>
                                    </div>
                                    <div class="rcr">&nbsp;</div>
                                </div>
                                <div class="col-sm-12 mt-30">
                                    <hr class="af-div" />
                                </div>
                                <div class="col-sm-12 d-flex justify-space-between mt-20">
                                    <div class="rcl">
                                      &nbsp;
                                    </div>
                                    <div class="rcr">
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">SubTotal:</span><span class="st2">${{ number_format($details->subtotal, 2, '.', ',') }}</span>
                                        </div>
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">Discount:</span><span class="st2">${{ number_format($details->discount, 2, '.', ',') }}</span>
                                        </div>
                                        @if($details->promo_code != '')
                                            <div class="d-flex justify-space-between text-right">
                                                <span class="st1">Promo:</span><span class="st2">${{ number_format($details->promo, 2, '.', ',') }}</span>
                                            </div>
                                        @endif
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">Total:</span><span class="st2">${{ number_format($details->total, 2, '.', ',') }}</span>
                                        </div>
                                        <hr class="af-div" style="margin-left: 18%" />
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">PAID:</span><span class="st2">${{ number_format($details->paid, 2, '.', ',') }}</span>
                                        </div>
                                        <div class="d-flex justify-space-between text-right">
                                            <span class="st1">DUE:</span><span class="st2">${{ number_format(floatval($details->due), 2, '.', ',') }}</span>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>                        
                    </div>                    
                </div>
                <div class="af-review-footer border">
                    <img src="/images/footer.png"/>
                    <div>
                        <h5>Payment Address</h5>
                        <p>1905 NW 32 Street,Building 5<br />
                            Pompano Beach, FL 33064 </p>
                        <h5>Terms &amp; Condtions</h5>    
                        <p>{{ $details->terms_conditions }}</p>
                    </div>
                </div>
                <div class="text-center pb-60 position-relative">

                    <canvas id="signature-pad" class="signature-pad" width=700 height=150></canvas>
                    <p class="sign-here">Signature</p>                    
                </div>

                <div class="af-next text-center border-0 pt-40">                   
                    <button id="pay_cc" class="btn btn-alt">Pay with credit card</button>
               <!--      <button class="btn btn-alt d-block">Pay with check</button> -->
                </div>

                 <div class="af-next text-center pt-20 pb-60 ">                   
                    <button id="pay_check" class="btn btn-alt bg-darkgray text-dark">Pay with check</button>
               <!--      <button class="btn btn-alt d-block">Pay with check</button> -->
                </div>

            </div>
        </div>
    </div>

     <div class="modal" id="modalSendEstimate" tabindex="-1" role="dialog"  data-id="0">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <a href="#">BACK</a> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>

                <div id="" class="modal-body">
                    <h6 class="line-height-1-2 text-center">SEND INVOICE VIA EMAIL</h6>
     
                    <form id="frmSendEstimate" role="form" method="POST" action="{{ url('/estimate/share') }}">
                      
                      <div class="usr-info font-raleway font-size-14 pt-20">
                        <div class="row ">
                            <div class="col-md-6 required">
                                <label class="mb-0">Contact First Name*</label>
                                <input type="text" id="firstname" name="firstname" class="w-100 ml-0">
                            </div>

                            <div class="col-md-6 required">
                                <label class="mb-0">Contact Last Name*</label>
                                <input type="text" id="lastname" name="lastname" class="w-100 ml-0">
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12 required">
                                <label class="mb-0">Email Address*</label>
                                <input type="text" id="email" name="email" class="w-100 ml-0">
                            </div>
                        </div>

                        
                        <div class="row">
                            <div class="col-md-12 required">
                                <label class="mb-0">Subject</label>
                                <input type="text" id="subject" name="subject" class="w-100 ml-0" value="Germbusters911 Invoice- Disinfection Service">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 required">
                                <label class="mb-0">Message</label>
                                <textarea name="message" id="message" class="w-100 ml-0" style="height: 160px;">

Please click this link:
  
{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}</textarea>
                            </div>
                        </div>

                        <div class="row text-center">
                            <a id="sendEstimate" class="py-40 btn btn-yellow" >SEND</a>
                        </div>  

                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

     <div class="modal" id="modalSendEstimateSuccess" tabindex="-1" role="dialog"  data-id="0">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <a href="#">BACK</a> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>

                <div id="" class="modal-body">
                    <h6 class="line-height-1-2 text-center text-teal">SUCCESS</h6>
     
                    <h6 class="line-height-1-2 text-center py-40 font-size-16">INVOICE HAS BEEN SENT.</h6>
                </div>
            </div>
        </div>
    </div>

    @include('partials.bottom_bar', array('right_div' => ''))

@endsection

@section('footer_scripts')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
     <script type="text/javascript">
    
    jQuery(document).ready(function($) {
        var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(0, 0, 0)'
        });

        var services_id = "{{ $details->id }}";

        // load saved signature into canvas
        $.get('/api/show_sig/' + services_id).done(function(response) {
            signaturePad.fromDataURL(response);
        })        

        $("#pay_cc").click(function(e) {
            window.location = "/invoice/details/{{ $type }}/{{ $id }}/{{  urlencode(base64_encode($id)) }}";
        });

         $("#sendEstimate").click(function(e) {
            e.preventDefault();

             $('#sendEstimate').html('SEND <i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending
     
             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");

             var fname     = $("input[name='firstname']").val();
             var lname     = $("input[name='lastname']").val();
             var email     = $("input[name='email']").val();
             var subject   = $("input[name='subject']").val();
             var message   = $("#message").val();

            $.post('/estimate/send', {'fname':fname, 'lname':lname,' email':email, 'subject':subject, 'message':message, '_token': CSRF_TOKEN}, 

                function(response){  
               
                // Load json data from server and output message    
                if(response.type == 'error') {

                } else {

                    $('#firstname').val('');
                    $('#lastname').val('');
                    $('#email').val('');
                    $('#sendEstimate').html('SEND'); 
                    $("#modalSendEstimate").modal("hide"); 
                    $("#modalSendEstimateSuccess").modal("show"); 
        
                }
               

            }, 'json');
        });

    });

</script>

@endsection