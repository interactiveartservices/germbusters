@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/css/services.css" rel="stylesheet" type="text/css">

<style>
    .daterangepicker .calendar-table th, .daterangepicker .calendar-table td {
  padding: 2%;
  font-size: 0.9rem;
  font-family: 'Montserrat';
  font-weight: 300;
}

/*.daterangepicker td.in-range {
  background-color: #FFFAE0;
}*/

.daterangepicker td.active, .daterangepicker td.active:hover {
  background-color: #02adc6;
}

.daterangepicker td.disabled, .daterangepicker option.disabled {
  text-decoration: none;
  background: rgba(2,173,44, 0.1);
}
</style>
@endsection

@section('content')


    <!--Start Loader-->
    <div id="loader" class="loader" style="display: none;" >
        <img src="/images/loading.gif" alt="" />
    </div>


   	<div class="content">
   		<div class="container">
            @include('partials.progresstracker', 
           array('steps'=> [
                    'Estimate', 
                    'Review <span class="mobilehide">Estimate</span>',
                    'Invoice',
                    'Review <span class="mobilehide">Your Order</span>'
                    ], 'progress'=>['active is-complete', '', '', ''] ))                     		

		   	<div class="form">
				<div class="row">
		       		<p class="success" id="success" style="display:none;"></p>
		       		<p class="error" id="error" style="display:none;"></p>
		       	</div>

                <form id="commercial-form" role="form" method="POST" action="{{ url('/estimates-invoices/process') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="type" value="{{ $type }}">

					<div class="row border-b">
						<div class="col-md-6 mt-20 p-0 {{ strtolower($type)=='residential' ? 'hide' : '' }}">
                            <h6 class="mt-20">Select Service</h6>
                            <div class="message chk-service">&nbsp;</div>

							<div class="mt-20 required">
								<div class="styledCheckbox services {{ isset($details->is_deep_disinfection) ? ($details->is_deep_disinfection == 1 ? 'sel' : '') : '' }}">
						      	 	<i class="fas fa-check"></i>
						      	</div>
						      	<label data-description="Electrostatic spray uses a specialized EPA Registered Disinfectant Cleaner that is combined with air and atomized by an electrode inside the sprayer to kill a broad spectrum of microorganisms in order to provide clean, sanitary and healthy environment.">Electrostatic Deep Cleaning Disinfection</label>
						      	<input id="is_deep_disinfection" name="is_deep_disinfection" class="chk" type="hidden" value="{{ isset($details->is_deep_disinfection) ? $details->is_deep_disinfection : 0 }}">
					      	</div>

							<div class="mt-20">
						      	<div class="styledCheckbox services {{ isset($details->is_wiping_service) ? ($details->is_wiping_service == 1 ? 'sel' : '') : '' }}">
						      	 	<i class="fas fa-check"></i>
						      	</div>
						      	<label data-description="Following CDC guidelines we wipe all high-touch areas surfaces before spray and UVC disinfection. If a surface is not cleaned first, the success of the disinfection process can be compromised. Removal of all visible inorganic and organic matter can be as critical as the germicidal activity of the disinfecting agent.">Hand wiping service</label>
						      	<input id="is_wiping_service" name="is_wiping_service" type="hidden" class="chk" value="{{ isset($details->is_wiping_service) ? $details->is_wiping_service : 0 }}">
					      	</div>

							<div class="mt-20">
						      	 <div class="styledCheckbox services {{ isset($details->is_uv_disinfection) ? ($details->is_uv_disinfection == 1 ? 'sel' : '') : '' }}">
						      	 	<i class="fas fa-check"></i>
						      	 </div>
						      	 <label data-description="We calculate the dosage & time of exposure for your surfaces to kill 99.99% of all pathogens. This is an exposure of 50mJ/cm2- 100mJ/cm2- way above the (22mJ/cm2) to kill SARS-CoV-2, the virus that causes COVID-19. Our results will be visually verified for you to see as we'll leave behind germ meter cards that show exposure levels.">UV-C Disinfection</label>
						      	 <input id="is_uv_disinfection" name="is_uv_disinfection" type="hidden" class="chk" value="{{ isset($details->is_uv_disinfection) ? $details->is_uv_disinfection : 0 }}">
					      	</div>
						</div>
                        <div>
                            <div class="col-md-4 success">
                                <div class="box mt-20" style="max-width: 350px;">
                                    <p class="title"><img src="/images/Icon awesome-map-marker-alt.png" class="width-auto"> GERMBUSTERS</p>
                                    <p class="txt-small line-height-1-2 mt-10"><span class="font-weight-bold" id="ip_loc">GermBuster - Charlotte, NC</span><br>
                                                    <span id="ip_add">2807 Mt Isle Harbor Drive</span><br>
                                                    <span id="ip_phone">704 877 2455</span>
                                    </p>
                                </div>
                            </div> 
                        </div>

						<!-- <div class="col-md-6">
							<div class=" mt-20">
						      	 <div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	 </div><label> Weekly maintenance plans</label>
						      	 <input id="is_weekly_maintenance" name="is_weekly_maintenance" type="hidden" class="chk" value="0">
					      	</div>

					      	<div class=" mt-20">
						      	 <div class="styledCheckbox">
						      	 	<i class="fas fa-check"></i>
						      	 </div><label>Monthly maintenance plans</label>
						      	  <input id="is_monthly_maintenance" name="is_monthly_maintenance" type="hidden" class="chk" value="0">
					      	</div>
						</div> -->
					</div>

                    <div class="row border-b {{ strtolower($type)=='residential' ? 'hide' : '' }}">
                       
                        <label class="d-block">Service term</label>
                        <div class="message chk-term">&nbsp;</div>
                        <div class="d-flex mt-20 f-wrap">
                            <div class="">
                                 <div class="styledCheckbox {{ isset($details->is_one_time) ? ($details->is_one_time == 1 ? 'sel' : '') : '' }}">
                                    <i class="fas fa-check"></i>
                                 </div><label>One time</label>
                                 <input id="is_one_time" name="is_one_time" type="hidden" class="chk" value="{{ isset($details->is_one_time) ? $details->is_one_time : 0 }}">
                            </div>

                            <div class="pl-50">
                                 <div class="styledCheckbox {{ isset($details->is_weekly_maintenance) ? ($details->is_weekly_maintenance == 1 ? 'sel' : '') : '' }}">
                                    <i class="fas fa-check"></i>
                                 </div><label>Weekly maintainance plans</label>
                                 <input id="is_weekly_maintenance" name="is_weekly_maintenance" type="hidden" class="chk" value="{{ isset($details->is_weekly_maintenance) ? $details->is_weekly_maintenance : 0 }}">
                            </div>

                            <div class="pl-50">
                                 <div class="styledCheckbox {{ isset($details->is_biweekly) ? ($details->is_biweekly == 1 ? 'sel' : '') : '' }}">
                                    <i class="fas fa-check"></i>
                                 </div><label>Biweekly</label>
                                 <input id="is_biweekly" name="is_biweekly" type="hidden" class="chk" value="{{ isset($details->is_biweekly) ? $details->is_biweekly : 0 }}">
                            </div>

                            <div class="pl-50">
                                 <div class="styledCheckbox {{ isset($details->is_monthly_maintenance) ? ($details->is_monthly_maintenance == 1 ? 'sel' : '') : '' }}">
                                    <i class="fas fa-check"></i>
                                 </div><label>Monthly maintenance plans</label>
                                 <input id="is_monthly_maintenance" name="is_monthly_maintenance" type="hidden" class="chk" value="{{ isset($details->is_monthly_maintenance) ? $details->is_monthly_maintenance : 0 }}">
                            </div>

                            <div class="pl-50">
                                 <div class="styledCheckbox {{ isset($details->is_annual) ? ($details->is_annual == 1 ? 'sel' : '') : '' }}">
                                    <i class="fas fa-check"></i>
                                 </div><label>Annual</label>
                                 <input id="is_annual" name="is_annual" type="hidden" class="chk" value="{{ isset($details->is_annual) ? $details->is_annual : 0 }}">
                            </div>
                        </div>

                        <div class="d-flex mt-50 ml-10 mb-40">                            
                            <div id="es-start" class="d-flex">
                                 <img src="/images/Icon feather-calendar.png" alt="calendar" class="width-auto" />
                                 <span id="startdate_txt" style="position: relative;margin-top: 5px;margin-left: 10px;">{{ isset($details->start_date) ?  date_format(date_create($details->start_date), 'm/d/Y') : 'Start Date' }}</span>
                                 <input type="hidden" id="startdate" name="startdate" value="{{ isset($details->start_date) ?  date_format(date_create($details->start_date), 'm/d/Y') : date('m/d/Y') }}">
                            </div>

                             <div id="es-end" class="pl-50 d-flex" >
                             	 <img src="/images/Icon feather-calendar.png" alt="calendar" class="width-auto" />
                             	  <span id="enddate_txt" style="position: relative;margin-top: 5px;margin-left: 10px;">{{ isset($details->end_date) ?  date_format(date_create($details->end_date), 'm/d/Y') : 'End Date' }}</span>
                                 <input type="hidden" id="enddate" name="enddate" value="{{ isset($details->start_date) ?  date_format(date_create($details->start_date), 'm/d/Y') : date('m/d/Y') }}">
                                 <!--  <label class="ml-10">Start Date - End Date</label><input type="text" name="daterange" id="daterange" value="" /> -->
                            </div>
                        </div>
                    </div>

					<div class="row border-b">
						<h6>Estimate Square Footage </h6>
						<input type="text" data-delay="300" placeholder="Enter square footage of space to be cleaned" name="square_footage" id="square_footage" class="input half mt-20" value="{{ isset($details->square_footage) ? $details->square_footage : '' }}">
					</div>

					<div class="row border-b">
                        <div class="row">
                        <div class="col-md-6">
    						<label>Promo / Affiliate Code</label>
    						<input type="text" data-delay="300" placeholder="Enter Code" name="promo_code" id="promo_code" class="input half" value="{{ isset($details) ? $details->promo_code : '' }}">
                        </div>

                        <div class="col-md-6">
                            <label>Work Order</label>
                            <input type="hidden" name="workorder" value="{{ $workorder }}">
                            <input type="text" data-delay="300" name="promo_codex" id="promo_codex" class="input half" value="{{ $workorder }}" disabled>
                        </div>
                        </div>
					</div>

					<div class="row border-b usr-info">
						<div class="row">
							<div class="col-md-6 required">
								<label>How Did You Hear About Us? *</label>
								<select name="how_did_you_hear" id="how_did_you_hear" class="input" value="">
                    <option value="">Select one</option>

                    @if(isset($details->how_did_you_hear)) {
										<option value="email" {{ strtolower(trim($details->how_did_you_hear)) == 'email' ? 'selected' : '' }}>
											Email
										</option>
										<option value="search_engine" {{ strtolower(trim($details->how_did_you_hear)) == 'search_engine' ? 'selected' : '' }}>
											Search Engine
										</option>
										<option value="advertisement" {{ strtolower(trim($details->how_did_you_hear)) == 'advertisement' ? 'selected' : '' }}>
											Advertisement
										</option>
										<option value="recommendation" {{ strtolower(trim($details->how_did_you_hear)) == 'recommendation' ? 'selected' : '' }}>
											Recommendation
										</option>
										<option value="others" {{ strtolower(trim($details->how_did_you_hear)) == 'others' ? 'selected' : '' }}>
											Others
										</option>
									@else
										<option value="email">Email</option>
										<option value="search_engine">Search Engine</option>
										<option value="advertisement">Advertisement</option>
										<option value="recommendation">Recommendation</option>
										<option value="others">Others</option>
									@endif
								</select>								 
							</div>
							<div class="col-md-6"></div>
						</div>

						<div class="row">
							<div class="col-md-6 required">
								<label>Contact First name*</label>
								<input type="text" data-delay="300" name="firstname" id="firstname" class="input" value="{{ isset($details) ? $details->firstname : '' }}">
								 <div class="message">&nbsp;</div>
							</div>
							<div class="col-md-6 required">
								<label>Contact Last name*</label>
								<input type="text" data-delay="300" name="lastname" id="lastname" class="input" value="{{ isset($details) ? $details->lastname : '' }}">
								<div class="message">&nbsp;</div>
							</div>
						</div>

            <div class="row {{ strtolower($type)=='residential' ? 'hide' : '' }}">
                <div class="col-md-6 required">
                    <label>Company Name</label>
                    <input type="text" data-delay="300" name="company" id="company" class="input" value="{{ isset($details->company) ? $details->company : '' }}">
                     <div class="message">&nbsp;</div>
                </div>
                <div class="col-md-6 required">
                    <label>Location Number/Name</label>
                    <input type="text" data-delay="300" name="location_number_name" id="location_number_name" class="input" value="{{ isset($details->location_number_name) ? $details->location_number_name : '' }}">
                    <div class="message">&nbsp;</div>
                </div>
            </div>

						<div class="row">
							<div class="col-md-6 required">
								<label>Address*</label>
								<input type="text" data-delay="300" name="address1" id="address1" class="input" value="{{ isset($details) ? $details->address1 : '' }}">
								<div class="message">&nbsp;</div>
							</div>
							<div class="col-md-6">
								<label>Address2</label>
								<input type="text" data-delay="300" name="address2" id="address2" class="input" value="{{ isset($details) ? $details->address2 : '' }}">
							</div>
						</div>

						<div class="row">
							<div class="col-md-4 required">
								<label>Zip Code*</label>
								<input type="text" data-delay="300" name="zipcode" id="zipcode" class="input" value="{{ isset($details) ? $details->zipcode : '' }}">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-6 required">
								<label>City*</label>
								<input type="text" data-delay="300" name="city" id="city" class="input" value="{{ isset($details) ? $details->city : '' }}">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-2 required">
								<label>State*</label>
								<input type="text" data-delay="300" name="state" id="state" class="input" value="{{ isset($details) ? $details->state : '' }}">
								<div class="message">&nbsp;</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<label>Email Address*</label>
								<input type="text" data-delay="300" name="email" id="email" class="input" value="{{ isset($details) ? $details->email : '' }}">
							</div>
							<div class="col-md-6" style="padding:0;">
								<div class="col-md-6 required">
									<label>Phone*</label>
									<input type="text" data-delay="300" name="phone" id="phone" class="input" value="{{ isset($details) ? $details->phone : '' }}">
									<div class="message">&nbsp;</div>
								</div>
								<div class="col-md-6">
									<label>Alternative Phone</label>
									<input type="text" data-delay="300" name="alt_phone" id="alt_phone" class="input" value="{{ isset($details) ? $details->alt_phone : '' }}">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6 required">
								<label>Account Manager</label>
								<!-- <input type="text" data-delay="300" name="acct_manager" id="acct_manager" class="input" value="{{ isset($details) ? $details->acct_manager : '' }}"> -->
                  <select name="acct_manager" id="acct_manager" class="input">

                    @if(isset($details->acct_manager)) {
                    <option value="Mike OBrien" {{ ($details->acct_manager == 'Mike OBrien') ? 'selected' : '' }}>
                      Mike OBrien
                    </option>
                    <option value="Bill Wagonseller" {{ ($details->acct_manager == 'Bill Wagonseller') ? 'selected' : '' }}>
                      Bill Wagonseller
                    </option>
                    <option value="Scott Ownbey" {{ ($details->acct_manager == 'Scott Ownbey') ? 'selected' : '' }}>
                      Scott Ownbey
                    </option>
                  @else
                    <option value="Mike OBrien">Mike OBrien</option>
                    <option value="Bill Wagonseller">Bill Wagonseller</option>
                    <option value="Scott Ownbey">Scott Ownbey</option>
                  @endif  
                  </select>

								<div class="message">&nbsp;</div>
							</div>
							<div class="col-md-6 required">
								<label>Payment terms</label>
							<!-- 	<input type="text" data-delay="300" name="payment_terms" id="payment_terms" class="input" value="{{ isset($details) ? $details->payment_terms : '' }}"> -->
									<select name="payment_terms" id="payment_terms" class="input" onchange="updateTerms($(this).val())">
										<option value="Due upon receipt" {{ (isset($details->payment_terms) &&  $details->payment_terms == 'Due upon receipt') ? 'selected' : '' }} >Due upon receipt</option>
										<option value="1 day" {{ (isset($details->payment_terms) && $details->payment_terms == '1 day') ? 'selected' : '' }} >1 day</option>
										<?php for ($num=2; $num<=100; $num++){ echo '<option value="' .$num. ' days"' . ($num==(isset($details->payment_terms) ? $details->payment_terms : '5 days')  ? 'selected="selected"' : ''). ' >' .$num. ' days</option>'; } ?>
									</select>
								<div class="message">&nbsp;</div>
							</div>						
						</div>

                        <div class="row">
                            <div class="col-md-6 required">
                                <label>Scheduled Date</label>
                                <input type="text" data-delay="300" name="scheduled_date" id="scheduled_date" class="input" value="{{ isset($details) ? $details->scheduled_date : '' }}">
                                <div class="message">&nbsp;</div>
                            </div>
                            <div class="col-md-6 required">
                                <label>Scheduled Time</label>

                    					   <select name="scheduled_time" id="scheduled_time" class="input">
                                  <option value="To be determined" {{ (isset($details->scheduled_time) && $details->scheduled_time == 'To be determined') ? 'selected' : '' }}>
                                    To be determined
                                  </option>
                                  <option value="6:00 am" {{ (isset($details->scheduled_time) && $details->scheduled_time == '6:00 am') ? 'selected' : '' }}>
                                   6:00 am
                                  </option>
                                  <option value="6:30 am" {{ (isset($details->scheduled_time) && $details->scheduled_time == '6:30 am') ? 'selected' : '' }}>
                                    6:30 am
                                  </option>
                                  <option value="7:00 am" {{ (isset($details->scheduled_time) && $details->scheduled_time == '7:00 am') ? 'selected' : '' }}>
                                    7:00 am
                                  </option>
                                  <option value="7:30 am" {{ (isset($details->scheduled_time) && $details->scheduled_time == '7:30 am') ? 'selected' : '' }}>
                                    7:30 am
                                  </option>
                                 </select>

                         			<!-- 	@php($dt = DateTime::createFromFormat('H:i', '00:00'))

                         				@for($i=0; $i<48; $i++)
                         					@if(isset($details->scheduled_time))
	                         					@if($details->scheduled_time == $dt->format('H:i'))
	                         						<option value="{{ $dt->format('H:i') }}" selected>{{ $dt->format('h:i A') }}</option>
	                         					@else
	                         						<option value="{{ $dt->format('H:i') }}">{{ $dt->format('h:i A') }}</option>
	                         					@endif
	                         				@else
	                         					<option value="{{ $dt->format('H:i') }}">{{ $dt->format('h:i A') }}</option>
	                         				@endif

                         					@php($dt->add(new DateInterval('PT30M')))
                         				@endfor -->
		
                                <div class="message">&nbsp;</div>
                            </div>                      
                        </div>

                        <div class="row">
                             <div class="col-md-12">
                                <label>General Description</label>
                                <textarea data-delay="500" class="required valid" name="general_desc" id="general_desc">{{ isset($details->general_desc) ? $details->general_desc : 'We’re Germbusters911 The cleaner way to disinfect your home or office. We’re the only disinfection company that uses a combination of EPA Registered Antimicrobial Disinfectant and UVC Germicidal light to offer you superior sanitation coverage that meets the EPA’s criteria for use against SARS-CoV-2 Our patent pending UVC devices kills 99.9% of all pathogens. Technicians wear full PPE equipment and all equipment is pre-santized prior to entry at customer premises. Germbusters follows all EPA/CDC protocols and industry association best methods and practices.' }}</textarea>
                            </div>
                             <div class="col-md-12 mt-20">
                                    <label class="servicePrice">General Price</label>
                                    <input type="text" data-delay="300" id="general_price" name="general_price" class="input half svcprice" value="{{ isset($details->general_price) ? $details->general_price : '0.00' }}">
                                </div>
                        </div>
					       </div>

                       	<div class="row" id="serviceDiv">                  
                        	@if(isset($invoice_items))
   
                            <input type="hidden" name="service_count[]" id="serviceCount" value="{{ count($invoice_items) }}" >

                            @php($counter = 1)

	                            @foreach($invoice_items as $value) 
	                           <div id="serviceNode{{ $counter }}" class="node border-b">
	                					    <div class="mt-20">
	                						       <label class="serviceTitle">Service {{ $counter }}- Title</label>
	                						       &nbsp;<a class="removeService" href="#"></a>
	                                   <input type="text" data-delay="300" name="service_title[]" class="input half svctitle" value="{{ $value->title }}">
	 
	                					    </div>

                                <div class="mt-20">
                                    <label class="serviceDesc">Service {{ $counter }}- Description</label>
                                    <textarea data-delay="500" class="required valid svcdescription" name="service_desc[]">{{ $value->description }}</textarea>
                                </div>

                                <div class="mt-20">
                                    <label class="servicePrice">Service {{ $counter }}- Price</label>
                                    <input type="text" data-delay="300" name="service_price[]" class="input half svcprice" value="{{ $value->price }}">
                                </div>
	                           </div>

	                                @php($counter = $counter + 1)
	                            @endforeach

                        	@else
                 
     						           <input type="hidden" name="service_count[]" id="serviceCount" value="0">
                        	   <div id="serviceNode0" class="node border-b" style="display: none;">
              					       <div class="mt-20">
              						        <label class="serviceTitle">Service 0- Title</label>
              						        &nbsp;<a class="removeService" href="#"></a>
                                  <input type="text" data-delay="300" name="service_title[]" class="input half svctitle " value="">
              					       </div>

                                <div class="mt-20">
                                    <label class="serviceDesc">Service 0- Description</label>
                                    <textarea data-delay="500" class="required valid svcdescription" name="service_desc[]"></textarea>
                                </div>

                                <div class="mt-20">
                                    <label class="servicePrice">Service 0- Price</label>
                                    <input type="text" data-delay="300" name="service_price[]" class="input half svcprice" value="0.00">
                                </div>
                            </div> 

                        @endif
                    </div>

                    <div class="row border-b"> 

                         <div class="mt-20">
                            <label>Add Service</label><a href="#" id="addService"><img src="/images/Icon awesome-plus-square.png" alt="calendar" class="width-auto ml-10" /></a>
                        </div>

                        <div class="mt-20">
                            <label>Description disclaimer</label>
                            <textarea data-delay="500" class="required valid" name="description_disclaimer" id="description_disclaimer">{{ isset($details->description_disclaimer) ? $details->description_disclaimer : 'Full payment is required upon completion of the job.  We accept credit cards & paypal.' }}</textarea>
                        </div>

                        <div class="mt-20 pb-10">
                            <label>Terms and Conditions</label>
                            <input type="hidden" name="tdays" id="tdays" value="{{ isset($details->payment_terms) ? $details->payment_terms : '5 days' }}" />
                            <textarea data-delay="500" class="required valid" name="terms_conditions" id="terms_conditions">{{ isset($details->terms_conditions) ? $details->terms_conditions : 'Net '. (isset($details->payment_terms) ? $details->payment_terms : '5 days'). ' on all invoices . In addition, Buyer shall pay all sales, use, customs, excise or other taxes presently or hereafter payable in regards to this transaction, and Buyer shall reimburse  Germbusters911 for any such taxes or charges paid by GermBusters911.' }}</textarea>
                        </div>
                    </div>

                    <div class="row mt-20 border-b">
                        <label>Add discount</label>
                        <input type="text" data-delay="300" name="discount" id="discount" class="input half" value="{{ isset($details->discount) ? $details->discount : '0.00' }}">
                    </div>
	            </form>

                  <div class="section-container bottom-bar p-0"><div class="section__page-item d-flex justify-start"><a href="#" onClick="history.go(-1); return false;" class="mt-10 text-dark" id="previous_page" tabindex="0" style="cursor:pointer;"><img src="/images/Icon material-backspace.png" class="mr-10 width-auto" alt="back">BACK</a></div>
                        <div class="section__page-item d-flex justify-end">
                            <button class="btn btn-alt btn-green btn-bottom-bar-continue" id="next_step" tabindex="0"> NEXT </button>
                        </div>
                    </div>
			</div>
		</div>
    </div>

@endsection

@section('footer_scripts')

    <script type="text/javascript" src="/js/accounting.min.js"></script>
    <script src="/js/library.js" type="text/javascript" ></script>
    <script src="/js/location.js" type="text/javascript" ></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>    

   <!--End Content-->

    <script type="text/javascript">
        
    jQuery(document).ready(function($) {
    	 $("#commercial-form").find('.styledCheckbox').on('click',function() {
            if($(this).hasClass("sel")){
                $(this).removeClass('sel');
                $(this).parent().find(".chk").val("0");
            }else{
                $(this).addClass('sel');
                $(this).parent().find(".chk").val("1");

           		if($(this).hasClass("services")) {
	            	AddService($(this).parent().find("label").html(), $(this).parent().find("label").data("description"), "0.00");
	            }
            }
        });

    	 function isEmailMatch($email, $conf, message) {
            if($email.val().trim() != $conf.val().trim()) {
                $conf.addClass("input-error");
                $conf.parent().find(".message").addClass("error").html(message);
                return false;                        
            }

            return true;
        }

		$("#estimate").on('click', '#addService', function(e){
            e.preventDefault();

            AddService("", "", "0.00");
         });

		$("#estimate").on('click', '.removeService', function(e){
            e.preventDefault();

            $(this).parent().parent().remove();

        	  $("#serviceCount").val($("#serviceCount").val() - 1); 

			      var cnt = 1;
			      $('#serviceDiv').find('.node').each(function(){

				    $(this).attr('id', "serviceNode" + cnt);
				    $(this).find(".serviceTitle").text("Service" + cnt + "- Title");
	          $(this).find(".serviceDesc").text("Service" + cnt + "- Description");
	          $(this).find(".servicePrice").text("Service" + cnt + "- Price");

				cnt++;
			});



         });

        function AddService(title, description, price) {  	
            var i = $("#serviceCount").val();

             var itm = document.getElementById("serviceNode" + i);
             var clone = itm.cloneNode(true);

             i++;

             clone.id = 'serviceNode' + i;
             itm.after(clone);

            $("#serviceNode" + i).find(".serviceTitle").text("Service" + i + "- Title");
            $("#serviceNode" + i).find(".serviceDesc").text("Service" + i + "- Description");
            $("#serviceNode" + i).find(".servicePrice").text("Service" + i + "- Price");

            $("#serviceNode" + i).find(".svctitle").val(title);
      			$("#serviceNode" + i).find(".svcdescription").val(description);
      			$("#serviceNode" + i).find(".svcprice").val(price);
      			$("#serviceNode" + i).show();

            $("#serviceCount").val(i); 
        }

       // alert($(".serviceTitle").length);
       // alert($("#serviceCount").val());

        @if((int)$id > 0) 

            @if(strtolower($type)=='residential')
                $("#serviceCount").val(0);
            @else
                $("#serviceCount").val($(".serviceTitle").length);
            @endif

        @else
            $("#serviceCount").val(0);
        @endif

       $(".btn-alt").click(function(e) {
            e.preventDefault();

            var is_deep_disinfection   = $('#is_deep_disinfection').val();
            var is_wiping_service      = $('#is_wiping_service').val(); 
            var is_uv_disinfection     = $('#is_uv_disinfection').val();
            var is_one_time            = $('#is_one_time').val();
            var is_weekly_maintenance  = $('#is_weekly_maintenance').val();
            var is_biweekly            = $('#is_biweekly').val();
            var is_monthly_maintenance = $('#is_monthly_maintenance').val();
            var is_annual              = $('#is_annual').val();
            var square_footage         = $('#square_footage').val();
            var promo_code             = $('#promo_code').val();
            var how_did_you_hear       = $('#how_did_you_hear').val();
            var firstname              = $('#firstname').val();
            var lastname               = $('#lastname').val();
            var company                = $('#company').val();
            var location_number_name   = $('#location_number_name').val();
            var address                = $('#address').val();
            var address2               = $('#address2').val();
            var zipcode                = $('#zipcode').val();
            var city                   = $('#city').val();
            var state                  = $('#state').val();
            var no_of_locations        = $('#no_of_locations').val();
            var phone                  = $('#phone').val();
            var alt_phone              = $('#alt_phone').val();
            var email                  = $('#email').val();
            var special_instructions   = $('#special_instructions').val();
            var CSRF_TOKEN             = $('meta[name="csrf-token"]').attr('content');

            var $elem = null;
            var iserr = false;

            var $frmcommercial = $("#commercial-form");

            $frmcommercial.removeClass("form-error");
            // clear all error messages
            $frmcommercial.find(".error").html("&nbsp;").removeClass("error");
            // clear all borderlines except for email 
            $frmcommercial.find(".input-error").removeClass("input-error");

            @if(strtolower($type) == "commercial")
	            if((is_deep_disinfection == "0") && (is_wiping_service == "0") && (is_uv_disinfection == "0")) {
	                $(".chk-service").addClass("error").html("Please select at least one service"); 
	                iserr = !iserr ? true : iserr; 
	                alert("Please select at least one service.");             
	             }

	             if((is_one_time == "0") &&  (is_weekly_maintenance == "0") && (is_biweekly == "0") && (is_monthly_maintenance == "0") && (is_annual == "0")) {
	                $(".chk-term").addClass("error").html("Please select at least one service term"); 
	                iserr = !iserr ? true : iserr;
	                alert("Please select at least one service term.");       
	             }
	        @endif

            /* how did you hear */
            $elem = $("#how_did_you_hear");
            if(isRequired($elem, "Please select how did you hear about us")) {
                iserr = !iserr ? true : iserr;
            }

            /* first name */
            $elem = $frmcommercial.find("input[name='firstname']");
            if(isRequired($elem, "Please provide your first name")) {
                iserr = !iserr ? true : iserr;
            }
            
            /* last name */
            $elem = $frmcommercial.find("input[name='lastname']");
            if(isRequired($elem, "Please provide your your last name")) {
                iserr = !iserr ? true : iserr;   
            }           

            /* phone */
            $elem = $frmcommercial.find("input[name='phone']");
            if(isRequired($elem, "Please enter a valid phone number")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* zip code */
            $elem = $frmcommercial.find("input[name='zipcode']");
            if(isRequired($elem, "Please enter a valid zip code")) {
                iserr = !iserr ? true : iserr;   
            }

            if(isZipCode($elem)) {
                iserr = !iserr ? true : iserr;   
            } 

            /* city */
            $elem = $frmcommercial.find("input[name='city']");
            if(isRequired($elem, "Please provide your city")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* state */
            $elem = $frmcommercial.find("input[name='state']");
            if(isRequired($elem, "Please provide your state")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* address 1 */
            $elem = $frmcommercial.find("input[name='address']");
            if(isRequired($elem, "Please provide your address")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* email */
            $elem = $frmcommercial.find("input[name='email']");
            if(isRequired($elem, "Please enter a valid email address.")) {
                iserr = !iserr ? true : iserr;         
            }

            if(!isEmail($elem , "Please enter a valid email address.")) {
                iserr = !iserr ? true : iserr;            
            }

            if($elem.hasClass("email-error")) {
                if(isEmailExists($elem, "A user with this email address already exists.")) {
                    iserr = !iserr ? true : iserr;            
                }
            } else {
                $elem.find(".email-error").removeClass("email-error");
            }

            if(!iserr) {
            	$("#serviceNode0").remove();

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                if($("#promo_code").val() !== "") {
	                $(".btn-alt").attr("disabled", true);
	                $.post('/invoice/promocode',{     _token: CSRF_TOKEN,
	                                         'promo': $("#promo_code").val(),
	                                    }, function(result) {
	                    if(isNaN(result)) {
	                        $frmcommercial.get(0).submit();
	                    } else {
	                        alert('The Promo/Affiliate Code you have entered is either not valid, expired, or not accepted by this operation.');
	                    }

	                    $(".btn-alt").removeAttr("disabled");
	                }); 
	            } else {
	            	$frmcommercial.get(0).submit();
	            }
            } else{
            	 $('html, body').animate({
                	scrollTop: $(".input-error").first().offset().top-300
            	}, 1000);
            }  

        });
    });

	function updateTerms($this) {

		var updated = $("#terms_conditions").val().replace($("#tdays").val(), $this);

		 $("#terms_conditions").val(updated);
		 $("#tdays").val($this)
	}

		
     $(function() {

          // $('input[name="daterange"]').daterangepicker({
          //   opens: 'left'
          // }, function(start, end, label) {
          //   console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
          // });
          var opts = {
			"singleDatePicker": true,
		    "autoApply": true,
		    "autoUpdateInput": false, 		          
		    "minDate": moment().format("MM/DD/YYYY"),
		    "maxDate": moment().add(1, 'year'),
		    "opens": "center"
		}

		$('#es-start').daterangepicker(opts, function(start, end, label) {
			var sd = start.format('MM/DD/YYYY');
            $('#startdate').val(sd);
            $("#startdate_txt").html(sd);
			//$('#dp-end').data('daterangepicker').setStartDate(start.format('MM/DD/YYYY'));
			$('#es-end').data('daterangepicker').minDate = start;
		});

		$('#es-end').daterangepicker(opts, function(start, end, label) {
			var ed = start.format('MM/DD/YYYY');
            $('#enddate').val(ed);
            $("#enddate_txt").html(ed); 			
			$('#es-start').data('daterangepicker').maxDate = start;
		});

		// $('#es-end').on('show.daterangepicker', function(ev, picker) {

		//   if(COMMON.responsive == 'mobile') {
		//   	$(picker.container).addClass("pushright");
		//   } else {
		// 	$(picker.container).removeClass("pushright");
		//   }

		// });


           $('input[name="scheduled_date"]').daterangepicker(opts, function(start, end, label) {
            var sd = start.format('MM/DD/YYYY');
            $('input[name="scheduled_date"]').val(sd);
            
			
           });
    });



</script>


@endsection


