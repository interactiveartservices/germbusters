@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css">

<style type="text/css">


@font-face {
    font-family: 'centrale_sans_boldbold_italic';
    src: url('/fonts/centrale_sans_bold_italic-webfont.eot');
    src: url('/fonts/centrale_sans_bold_italic-webfont.eot?#iefix') format('embedded-opentype'),
         url('/fonts/centrale_sans_bold_italic-webfont.woff2') format('woff2'),
         url('/fonts/centrale_sans_bold_italic-webfont.woff') format('woff'),
         url('/fonts/centrale_sans_bold_italic-webfont.ttf') format('truetype'),
         url('/fonts/centrale_sans_bold_italic-webfont.svg#centrale_sans_boldbold_italic') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'centrale_sansmedium';
    src: url('/fonts/centralesansmedium-webfont.eot');
    src: url('/fonts/centralesansmedium-webfont.eot?#iefix') format('embedded-opentype'),
         url('/fonts/centralesansmedium-webfont.woff2') format('woff2'),
         url('/fonts/centralesansmedium-webfont.woff') format('woff'),
         url('/fonts/centralesansmedium-webfont.ttf') format('truetype'),
         url('/fonts/centralesansmedium-webfont.svg#centrale_sansmedium') format('svg');
    font-weight: normal;
    font-style: normal;

}


</style>
@endsection

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">
   	<picture>    	
		<source media="(max-width: 560px)" srcset="/images/news-hdr-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/news-hdr.png 1x, /images/news-hdr@2x.png 2x">	
		<img class="banner-img" src="/images/news-hdr.png" alt="Coronavirus (COVID19) Deep Cleaning Information">
	</picture>
	<div class="bn-text container text-left">
		<h4 class="title text-white">News center</h4>
	</div>

   </div>	   
   <!--End Banner-->

   <div class="content">
   		<div class="container ">

		   	<div class="row main pb-60">
		       
		   	<div class="row my-60 d-flex obs-av">
			<div class="col-md-6 pr-0 c1">
				<img src="/images/news-img1.png" alt="" />
			</div>
			<div class="col-md-6 bg-light-gray c2">
				<h3 class="font-raleway font-weight-bold mb-20 d-inline-block">Introducing GermZapper-300, A UV light Device that Destroys C</h3>
				<p class="line-height-1-2">July 20, 2020</p>
				<p class="line-height-1-2 mt-30">The Germinator&trade; uses depth sensors, laser radar and an obstacle avoidance ultrasonic module to get as close to surfaces as possible while navigating complex environments and spaces, maneuvering around objects as necessary to continue it’s disinfecting route.</p>

				<a href="#" class="btn btn-yellow mt-30" onclick="smoothScroll(document.getElementById('GetQuote'))">REQUEST A DEMO - GET A QUOTE</a>
			</div>

		</div>

		    </div>
	
	</div>




@endsection
@section('footer_scripts')

<script type="text/javascript" >
jQuery(document).ready(function($) {

	jQuery('.fancybox').fancybox({		
		padding: 0
	});
	

	jQuery('.fancybox-media').fancybox({
				afterLoad: function () {
					this.title = (this.title) ? '<a href="' + this.href +	'" class="text-white">Download</a> ' : '';
				},
				padding:0,
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					title: {
                		type: 'outside'
            		},
					overlay:{ 
						css: {background:'rgba(0,0,0,0.75)', 'z-index':99998}
					},
					media : {},
					padding:0
				}
			});

   

});
</script>
@endsection

