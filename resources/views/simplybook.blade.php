<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/json-rpc-client.js"></script>

<script type="text/javascript">		 
	$("document").ready(function() {
		var client = new JSONRpcClient({
			'url': 'https://user-api.simplybook.me/admin',
			'headers': {
				'X-Company-Login': 'germbusters',
				'X-Token': '{{ $token }}',
			},
			'onerror': function (error) {
				alert(error);
			}
		});
		var services = client.getBookingsZapier();	

		console.log(services);
	});				
</script>

