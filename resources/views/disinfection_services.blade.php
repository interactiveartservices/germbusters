@extends('layouts.master')

@section('content')


<!--Start Banner-->   
<div class="sub-banner disinfect-svc">  
	<picture>
		<source media="(max-width: 560px)" srcset="/images/electrostatic-banner-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/electrostatic-banner@1x.png 1x, /images/electrostatic-banner@2x.png 2x">
		<img class="banner-img" src="/images/electrostatic-banner@1x.png" alt="electrostatic disinfections services">
	</picture>
	<div class="bn-text">
		<img src="/images/electrostatic@1x.png" alt="electrostatic" />
		<h1>Disinfection Cleaning Services</h1>
	</div>

</div>	   
<!--End Banner-->

<div class="bg-black">
	<div class="container position-relative eds1 p-0">
		<picture>
			<source media="(max-width: 560px)" srcset="/images/electrostatic-disinfection-nozzle-mobile@2x.png">
			<source media="(min-width: 561px)" srcset="/images/electrostatic-disinfection-nozzle.png 1x, /images/electrostatic-disinfection-nozzle@2x.png 2x">
			<img src="/images/electrostatic-disinfection-nozzle.png" alt="electrostatic" />		
		</picture>
		
		
		<div class="row">
			<div class="col-12">
				<div class="txt font-raleway">
					<h2 class="text-teal pb-30">Go beyond clean with powerful disinfectant technology </h2>
					<p class="text-left mb-10 line-height-1-2">Nothing is more important than the health and safety of your customers and employees. Viruses, bacteria and fungi are a constant, unseen threat to that security. Germbusters cleaning and disinfection service gives you and the ones you care for peace of mind you're doing everything you can to keep your people safe.
					</p>
					<p class="text-left mb-10 line-height-1-2">We bring hospital-grade disinfection service for businesses looking to reopen and implement an effective coronavirus cleaning procedure that adheres to CDC guidelines.  No two facilities are alike.  Whether you’re looking to append to your current cleaning procedures at a school district, or clean a Airbnb rental, our specialists will work with you so that we can determine the appropriate level of disinfection and protection to customize a solution for you.
					</p>
					<p class="text-left mb-10 line-height-1-2">Our fast-acting disinfectant formula can inactivate pathogens on the surface in just 8 minutes and bring you back to business within 30 minutes after treatment.	
					</p>					
				</div>
				<span id="esds" class="font-size-16 font-raleway">2X the deep disinfection cleaning power with GermBuster’s <br />
					Evaporator™ electrostatic sprayer and Bio Spray service. </span>
		<!-- 		<a href="/cleaning-quote/commercial" class="btn btn-teal" data-aos="fade-up"><h3 class="font-weight-bold font-size-14">SCHEDULE YOUR DISINFECTION APPOINTMENT</h3></a> -->
			</div>
		</div>
	</div>
</div>

<div class="container-fluid bg-white text-center shadow py-20 position-relative"> 
	<div class="row">
		<div class="col-sm-12">
			<div class="main-title mt-20"><h2><span>Compare atomizing nozzles </span>for yourself</h2>
				<p class="font-raleway font-weight-medium mt-20">Compare GermBusters Evaporator™ electrostatic sprayer vs Victory’s electrostatic  sprayer <br>
The noticeable inequality is due to the amount of atomizing nozzles that deliver electrical charged disinfectant spray</p>	
			</div>
			
		</div>
	</div>
</div>

<div class="bg-light-gray">
	<div class="container py-50">		
		<div class="row font-raleway font-size-15">
			<div class="col-lg-6 px-30">
				<img src="/images/germbusters-evaporator.png" alt="GermBusters Evaporator" />
				<span class="font-weight-bold d-block mt-20 mb-10">GermBuster's Evaporator&trade;</span>
				<span class="font-italic font-weight-semibold d-block my-10">5 Atomizing nozzles</span>
				<ul class="ecomp">
					<li>Superior atomization of cleaning solution</li>
					<li>Maximizes particle wrap around effect</li>
					<li>Hard to reach places receive a more even coat</li>
					<li>Superior anti-microbial surface protection </li>
					<li>Typically deliver a log3 to log5 (99.9%-99.999%) sterility assurance level I</li>						
				</ul>
			</div>
			<div class="col-lg-6 px-30">
				<img src="/images/victory-sprayer.png" alt="Victory Sprayer" />
				<span class="font-weight-bold d-block mt-20 mb-10">Victory Sprayer</span>
				<span class="font-italic font-weight-semibold d-block my-10">3 Atomizing nozzles</span>
				<ul class="ecomp">
					<li>Moderate atomization of cleaning solution</li>
					<li>Particle wrap around effect takes longer</li>
					<li>Hard to reach places receive uneven coat</li>
					<li>Moderate anti-microbial surface protection </li>						
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="bg-white" style="padding-bottom: 10%" id="electrostatic-disinfections">
	<picture>
		<source srcset="/images/dfs@1x.png 1x, /images/dfs@2x.png 2x">
			<img class="width-auto mw-100p" src="/images/dfs@1x.png" alt="How does electrostatic disinfection work" />
	</picture>
	
</div>
<div class="bg-brown disinfect-360">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-5 p-0">				
				<img class="width-auto" src="/images/wiping.png" alt="Wiping" />
			</div>
			<div class="col-sm-7">
				<div class="main-title mt-40 mb-0"><h2><span>GermBuster's</span> 360&deg; Disinfection Service</h2></div>
				<p class="font-raleway font-weight-light line-height-1-5 mt-30">Keep your facility healthier while saving time, money and labor. The revolutionary GermBuster’s <br />
					Evaporator™ electrostatic sprayer delivers pathogen killing solutions to the front and back side <br />
					of surfaces paired with a hand wiping service for superior coverage and better germ protection.</p>	
				<a href="/contact-us" class="btn btn-teal px-20 py-10 mt-20" data-aos="fade-up">GET A QUOTE- 360&deg; DISINFECTION</a>
			</div>						
		</div>
	</div>
</div>

 <!--Start Banner-->   
<div class="gbc position-relative">  
<div class="visible-lg-block cvid" style=""><iframe src="https://player.vimeo.com/video/424477463?autoplay=1&loop=1&color=ffffff&portrait=0&background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<div class="hidden-lg cimg">
	<img class="banner-uvc-img hidden-md" src="/images/gbc-bg-mobile@1x.png" alt="go beyond clean">		
</div>

<div class="container ctext">
	<div class="row">
		<div class="bg-sky-blue-opaque p-40">
			<h2 class="text-white pb-20">Go Beyond Clean</h2>
			<p class=" font-size-18 font-weight-medium line-height-1-2">We’re certified and licensed and ready whenever  you are.<br /> 
				Our EPA registered broad-spectrum disinfectant provides:  
				 </p>
			<ul class="">
				<li>Kills norovirus in just 4 minutes</li>
				<li>Kills 99.9% of bacteria, viruses*, fungi &amp; molds</li>
				<li>Bactericidal, virucidal*, tuberculocidal, and fungicidal**</li>
				<li>Disinfect, sanitize, clean, and deodorize in one step</li>
				<li>Kills, destroys and eliminates household germs</li>
				<li>No rinse required, even on food contact surfaces</li>
				<li>Sanitizes soft surfaces in just 2 minutes</li>
				<li>Non-abrasive and non-corrosive</li>
				<li>Non-flammable – no harmful chemicals</li>
				<li>Heavy duty cleaner and disinfectant</li>
				<li>Ready-to-use formula</li>
				<li>Safe for everyday use</li>
				<li>Formulated without phthalates, propylparaben, butylparaben, formaldehyde, formaldehyde-dnors, or npe’s</li>				
			   
			</ul>

			<a href="/contact-us" class="btn btn-yellow bp" data-aos="fade-up">CONTACT US TODAY - GET A QUOTE</a>
		</div>

	</div>
</div>
</div>

   <!--Start Services-->
   <div class="services-two">
	<div class="container">
		<div class="row position-relative">
			
		  <div class="col-md-6">
			<div class="main-title">   
			<h2><span>We got your cleaning service </span><br />down to a science</h2>
			<p class="line-height-1-2"> ATP testing is industry standard in the healthcare and food manufacturing industry. 
				Now Germbusters offers everyone an optional ATP test before and after our disinfection 
				service.  So results are scientifically proven- backed with a no-risk guarantee.  </p>
		   </div>
				
				<div class="service-sec">
					
					<div class="icon">
						<img src="/images/ico-corona.png" alt="coronavirus" class="img-responsive" />
					</div>
					
					<div class="detail">
						<h5 class="text-blue">Coronavirus control</h5>
						<p>The Corona family of viruses that includes the one that causes COVID-19 &mdash; can live on some of the surfaces you probably touch every day</p>
					</div>
					
				</div>
				<div class="service-sec">
					
					<div class="icon opacity-80">
						<img src="/images/ico-flu.png" alt="influenza" />
					</div>
					
					<div class="detail">
						<h5 class="text-blue">Influenza control</h5>
						<p>Both influenza A and B viruses survived for 24-48 hr on hard, nonporous surfaces such as stainless steel and plastic </p>
					</div>
					
				</div>

				<div class="service-sec">
					
					<div class="icon opacity-60">
						<img src="/images/ico-staph.png" alt="Staph control" />
					</div>
					
					<div class="detail">
						<h5 class="text-blue">Staph control</h5>
						<p>Methicillin-resistant Staphylococcus aureus (MRSA) can survive on some surfaces, like towels, razors, furniture, and athletic equipment for hours, days, or even weeks.  </p>
					</div>
					
				</div>
				
			</div>                                        
		 
			<div class="col-md-6 position-relative">
				<img class="width-auto" src="/images/temp-scanner2.png" alt="temp-scanner" />

				  <div class="mbg">
                            <h2><span>MONEY BACK </span>GUARANTEE <span>100% RISK FREE</span></h2>
                            <p>We’ll leave your home/facility with a hospital 
                                Rating of &gt; 100. Or your money back.</p>
                        </div>
									
			</div>
			
			
			<div class="col-12 foot">
				ATP testing has been used by hospitals  for years to help them minimize spread of healthcare acquired infections
			</div>
		</div>
	</div>
</div>
<!--End Services-->


  <!--Start Doctor Quote-->
    <div class="dr-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="quote">Give your customers peace of mind</h2>
                    <span class="name">Our scientifically verified  results can be proudly displayed differentiating <br />
                        your facility as one that goes the extra mile to limit the spread of germs and <br />
                        protect the wellness of your patrons and employees </span>
                </div>
            </div>
        </div>
    </div>
<!--End Doctor Quote-->

<div class="bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="pt-40 pb-10"><h2 class="text-teal-blue font-weight-extrabold">Get the facts!</h2></div>
				<p class="name line-height-2">Beware companies offering 90 days protection against coronaviruses with one treatment. Get COVID-19 <br />
					cleaning help from the professionals. Find deep cleaning services, sanitation services, and disinfection <br />
					services that fit your needs and budget with 24/7 emergency support. 
				</p>

				<!-- <a href="/cleaning-quote/commercial" class="btn btn-teal mt-30 px-30 py-10" data-aos="fade-up"><h3 class="font-weight-bold font-size-14">SCHEDULE YOUR DISINFECTION APPOINTMENT</h3></a> -->
			</div>
		</div>
	</div>
</div>

@endsection