<div class="modal" id="modalGetQuote" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <!-- <a href="#">BACK</a> -->
          <button type="button" class="close" data-dismiss="modal">&times;</button>
<!--           <h4 class="modal-title">Modal Header</h4> -->
        </div>

            <div id="step1" class="steps modal-body text-center" >

                 <p class="p-bold">BUILD YOUR QUOTE</p>
                <h6><span>We currently service</span> South Florida & Charlotte </h6>

                <p class="p-large">Tell us the zip code of the location in our service area you wish to have service</p>
                <div class="form-fields">
                    <form id="check-zip">
                        <input id="txtZip" class="" type="text" name="txtZip" placeholder="Zip Code" style="width:35%;">
                        <a id="zipcheck" data-step="2" class="step-continue btn btn-gray" style="margin-top:-8px" >CONTINUE</a>                    
                    </form>                    
                    <a href="#" id="my-location"><img class="width-auto" src="/images/icon-location.png" alt="use my location"> Use my location</a>
                </div>
                               
                <div class="link">
                    <a href="#" id="findmyzip">I don't know my zip code</a>
                </div>

            </div>


            <div id="step2" class="steps modal-body hide">
                <p>
                    The health and safety of you and our employees at GermBusters is our top priority. For this reason, <br>
                    we ask that you let us know if has anyone has experienced: <br>
                    • flu-like symptoms or was ill or suffering from <br>
                    • COVID-19 within the last 48 hours at your residence or facility.</p>
                    
                    <br><br>
                    
                    <p>
                    If any of these apply, please make sure to schedule your appointment at least 48 hours after these <br>
                    conditions have been resolved (or resolved. <br>
                    • At your service our technicians wear gloves, booties and will wear a mask. <br>
                    • We clean & disinfect all our equipment with EPA approved disinfectant.  </p>
                
                <div class="text-center">
                    <a href="#" id="step-next" data-step="1" class="step-continue btn btn-yellow">CONTINUE</a>
                </div>
               
            </div>


            <div id="step3" class="steps modal-body text-center hide">
                <h6><span>See if you are in our </span>service area</h6>

                <p>Ener the address below that you with to have disinfected and we’ll look <br> Up your zip code.</p>
                <form id="find-zip">
                    <input type="text" id="txtAddress" name="txtAddress" placeholder="Address" style="width:98%;">
                    <br>
                    <input type="text" id="txtCity" name="txtCity" placeholder="City" style="width:47%;">
                    <input type="text" id="txtState" name="txtState" placeholder="State" style="width:47%;">
                    <a id="zipfind"  data-step="3" class="step-continue btn btn-gray">FIND MY ZIP CODE</a>
                </form>
            </div>

            <div id="step4" class="steps modal-body text-center hide">
                <h6><span>Sorry you're outside our </span> service area.</h6>

                <p class="p-large">We are continually expanding our territory <br>
                And hope to provide you service soon. <br>
                <a href="/contact-us">Contact us</a> with any questions.
                </p>
            </div>

        </div>
      </div>
    </div>

<script type="text/javascript">
        
    @if(App::make('App\Http\Controllers\ShoppingCartController')->IsCart())
        var gq_is_cart = true;        
    @else
        var gq_is_cart = false;
    @endif
                        
</script>