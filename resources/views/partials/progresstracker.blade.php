<div class="section-container row">
				<div class="col">                    
					<ul class="progresstracker ml-0">
						<li class="progresstracker__step {{$progress[0]}}" id="progresstracker_select_services">
							<span class="progresstracker__link outline-text">
								<span>{!! $steps[0] !!}</span>
							</span>
						</li>
						<li class="progresstracker__step  {{$progress[1]}}" id="progresstracker_scheduling">
							<span>{!! $steps[1] !!}</span>
						</li>
						<li class="progresstracker__step  {{$progress[2]}}" id="progresstracker_guest_information">
							<span>{!! $steps[2] !!}</span>
						</li>
						<li class="progresstracker__step  {{$progress[3]}}" id="progresstracker_review_order">
							<span>{!! $steps[3] !!}</span>
						</li>
					</ul>
				</div>
            </div>