<div class="section-container bottom-bar"><div class="section__page-item d-flex justify-start pb-60"><a href="#" onClick="history.go(-1); return false;" class="mt-10 text-dark" id="previous_page" tabindex="0" style="cursor:pointer;"><img src="/images/Icon material-backspace.png" class="mr-10 width-auto" alt="back">BACK</a></div>
		<div class="section__page-item d-flex justify-end">
			{!! $right_div !!}
		</div>
	</div>