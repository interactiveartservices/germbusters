@extends('layouts.master')


<style type="text/css">

.data:nth-of-type(odd){
  background-color: #F8F8F8;
}
  </style>

@section('content')


  <div class="mx-60">
        <div class="form mt-40 mb-60">

                  <div class="">
                   <div class="title">
                    	<span class="">WEB INQUIRES</span>
                    </div>

	                <div class="mt-20 table-scroll">
	                	<div class="table-scroll-width">
		                    <div class="d-flex font-weight-semibold bg-light-gray p-10 font-size-15" >
                            <span style="width:20px;"></span>
		                        <span class="col-xs-1">INDUSTRY</span>
		                        <span class="col-xs-1">NAME</span>
		                        <span class="col-xs-1">JOB TITLE</span>
		                        <span class="col-xs-2">EMAIL</span>
		                        <span class="col-xs-1">PHONE</span>
		                        <span class="col-xs-3">MESSAGE</span>
                            <span class="col-xs-1">PAGE</span>
		                        <span class="col-xs-1">DATE</span>
		                        <span class="col-xs-2">NOTES</span>
		                    </div>
                        <?php $i = 1; ?>
		                    @foreach($quotes as $quote)
		                        <div class="data d-flex font-size-15 pt-10">
                               <span style="width:20px;">{{ $i }}.</span>                               
		                            <span class="col-xs-1">{{ $quote->industry }}</span>
		                            <span class="col-xs-1">{{ $quote->firstname. ' ' .$quote->lastname}}</span>
		                            <span class="col-xs-1">{{ $quote->jobtitle }}</span>
		                            <span class="col-xs-2"><a href="mailto:{{ $quote->email }}" class="text-underline">{{ $quote->email }}</a></span>
		                            <span class="col-xs-1">{{ $quote->phone }}</span>
		                            <span class="col-xs-3">{{ $quote->message }}</span>
                                <span class="col-xs-1">{{ $quote->page }}</span>
      		                			<span class="col-xs-1">{{ date('m-d-Y', strtotime($quote->date_added)) }}</span>  
      		                			<span class="col-xs-2">{{ $quote->notes }}&nbsp;<a href="#" data-id="{{ $quote->id }}" data-notes="{{ $quote->notes }}" data-toggle="modal" data-target="#modalNote" data-aos="fade-up"  class="btnEdit text-underline">edit</a></span>
		                        </div>
                        <?php $i++; ?>
	                    	@endforeach
	                	</div>
	                </div>
                    
                </div>

        </div>
 </div>

<div class="modal" id="modalNote" tabindex="-1" role="dialog" data-id="">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <a href="#">BACK</a> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>
                <div id="step_address" class="steps modal-body text-center form">
                    <h6 class="line-height-1-2">Add/Edit Note</h6>
     
                    <form id="addr-add-form" class="form-horizontal login-form" role="form" method="POST" action="">
                        <span>
                           <textarea name="notes" id="notes" class="w-100"></textarea>
                        </span>                        
                        
                        <a id="btnSubmit" class="py-40 step-continue btn btn-yellow">SUBMIT</a>   
                    </form>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript">
    
  	jQuery(document).ready(function($) {

  		$('.btnEdit').on('click',function() {

            $('#modalNote').attr("data-id", $(this).data('id'));
            $('#notes').val($(this).data('notes'));
            //$('#modalNote').modal("show");
        });


    	$("#btnSubmit").click(function(e) {

            e.preventDefault();


       		$('#btnSubmit').html('Submit<i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending

             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
          

            $.post('/get-quote-list/addnote', {'id': $("#modalNote").data("id"), 'notes': $("#notes").val(), '_token': CSRF_TOKEN},

                 function(response){  
               
                // Load json data from server and output message    
                if(response.type == 'error') {                	
                } else {
					$('#btnSubmit').html('Submit');
                	location.reload();         
                }
               

            }, 'json');
        });
     
   });

    
</script>

@endsection
