@extends('layouts.master')
@section('header_scripts')
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<style>
		.g-recaptcha-response {visibility: hidden}
	</style>
@endsection

@section('content')


<!--Start Banner-->   
<div class="sub-banner">  
	<picture>
		<source media="(max-width: 560px)" srcset="/images/hvac-banner-mobile@2x.png">
		<source media="(min-width: 561px)" srcset="/images/hvac-banner@1x.png 1x, /images/hvac-banner@2x.png 2x">	
		<img class="banner-img" src="/images/hvac-banner@1x.png" alt="uvc hvac">
	</picture>
	<div class="bn-text">
		<h1>Contact Us</h1>
	</div>

</div>	   
<!--End Banner-->

<div class="container-fluid bg-white text-center pt-30 pb-10 shadow mb-30"> 
	<div class="row">
		<div class="col-sm-12">
			<div class="main-title mt-20"><h2><span>Go Beyond </span> Clean</h2>
				<p class="font-raleway">Our vision is to use next-generation technology to <br>
				protect the health of you and your business. 
				</p>	
			</div>
			
		</div>
	</div>
</div>

<div class="container text-center py-30" style="@if(strtolower($location['region']) == 'la')width:1400px; @else width:1000px; @endif"> 
	<div class="row">
		@if(strtolower($location['region']) == 'la')
		<div class="col-sm-4 text-left main-title ">
			<h2 class="mb-20"><span class="">Los</span> Angeles</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3301.6225542074635!2d-118.63991798459719!3d34.15599748057778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c29e662f27bc67%3A0x31e59c28c09c4017!2s23401%20Park%20Sorrento%2C%20Calabasas%2C%20CA%2091302%2C%20USA!5e0!3m2!1sen!2sph!4v1598234028232!5m2!1sen!2sph" width="380" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			<div class="mt-20">
		<!-- 		<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block" >Phone:</span> 310 804 3250</p> -->
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Email:</span> brian@germbustersla911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Web:</span >www.germbustersLA911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Address:</span> 23401 Park Sorrento, Calabasas, CA 91320</p>
			</div>
		</div>
		<div class="col-sm-4 text-left main-title ">
			<h2 class="mb-20">Charlotte</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1648642.3925931286!2d-83.85308923802276!3d36.19076818843458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8856a4c44787bd81%3A0x60f965bd28f7a3!2s2807%20Mt%20Isle%20Harbor%20Dr%2C%20Charlotte%2C%20NC%2028214%2C%20USA!5e0!3m2!1sen!2sph!4v1591597629950!5m2!1sen!2sph" width="380" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			<div class="mt-20">
		<!-- 		<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block" >Phone:</span> 704-877-2455</p> -->
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Email:</span> bill@germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Web:</span >www.germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Address:</span> 2807 Mount Isle Harbor Dr, Charlotte, NC 28214 </p>
			</div>
		</div>
		<div class="col-sm-4 text-left main-title">
			<h2 class="mb-20"><span class=""> Pompano</span> Beach</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.741356742395!2d-80.15159668476807!3d26.270056483408567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d91cd3434dc82f%3A0xe87a461e7d1cf7ca!2s1905%20NW%2032nd%20St%20Building%205%2C%20Pompano%20Beach%2C%20FL%2033064%2C%20USA!5e0!3m2!1sen!2sph!4v1591597968593!5m2!1sen!2sph" width="380" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			<div class="mt-20">
			<!-- 	<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Phone:</span> 954-761-2480</p> -->
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Email:</span> hello@germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Web:</span> www.germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Address:</span> 1905 NW 32 St, Building 5, Pompano Beach, FL 33064</p>
			</div>
		</div>
		@else
		<div class="col-sm-6 text-left main-title ">
			<h2 class="mb-20">Charlotte</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1648642.3925931286!2d-83.85308923802276!3d36.19076818843458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8856a4c44787bd81%3A0x60f965bd28f7a3!2s2807%20Mt%20Isle%20Harbor%20Dr%2C%20Charlotte%2C%20NC%2028214%2C%20USA!5e0!3m2!1sen!2sph!4v1591597629950!5m2!1sen!2sph" width="380" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			<div class="mt-20">
			<!-- 	<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block" >Phone:</span> 704-877-2455</p> -->
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Email:</span> bill@germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Web:</span >www.germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Address:</span> 2807 Mount Isle Harbor Dr, Charlotte, NC 28214 </p>
			</div>
		</div>
		<div class="col-sm-6 text-left main-title">
			<h2 class="mb-20"><span class=""> Pompano</span> Beach</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.741356742395!2d-80.15159668476807!3d26.270056483408567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d91cd3434dc82f%3A0xe87a461e7d1cf7ca!2s1905%20NW%2032nd%20St%20Building%205%2C%20Pompano%20Beach%2C%20FL%2033064%2C%20USA!5e0!3m2!1sen!2sph!4v1591597968593!5m2!1sen!2sph" width="380" height="310" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			<div class="mt-20">
			<!-- 	<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Phone:</span> 954-761-2480</p> -->
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Email:</span> hello@germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Web:</span> www.germbusters911.com</p>
				<p class="font-raleway font-weight-medium font-size-14"><span class="font-weight-bold w-70 d-inline-block">Address:</span> 1905 NW 32 St, Building 5, Pompano Beach, FL 33064</p>
			</div>
		</div>

		@endif
	</div>
</div>     

<div class="bg-gray py-60 contact-f">
	<div class="container">
		<div class="row">

			<div class="main-title text-center">
				<h2 class="my-30 font-size-40">Need a custom cleaning solution?</h2>
				<p class="line-height-1-2">
					Step into the next generation of UV-C disinfection  technology? Please provide your contact details <Br />
					and a GermBuster account specialist will help you take the first step in discovering the power of UV-C.  <br />
					Or you can call 954-761-2480 to talk to an expert now.
				</p>
			</div>

			<div class="form">
				<form name="frmContact" id="frmContact" method="post">	
					<input type="hidden" id="frmPage" value="Contact Us">
					<div class="row">

							<div class="col-md-12">
								<label>Choose your industry</label>
								<select name="industry" id="industry" class="">
									<option value="- Select -">- Select -</option>
									<option value="Accounting &amp; Financial">Accounting &amp; Financial</option>
									<option value="Agriculture">Agriculture</option>
									<option value="Animal &amp; Pet">Animal &amp; Pet</option>
									<option value="Architectural">Architectural</option>
									<option value="Art &amp; Design">Art &amp; Design</option>
									<option value="Attorney &amp; Law">Attorney &amp; Law</option>
									<option value="Automotive">Automotive</option>
									<option value="Bar &amp; Nightclub">Bar &amp; Nightclub</option>
									<option value="Business &amp; Consulting">Business &amp; Consulting</option>
									<option value="Childcare">Childcare</option>
									<option value="Cleaning &amp; Maintenance">Cleaning &amp; Maintenance</option>
									<option value="Communications">Communications</option>
									<option value="Community &amp; Non-Profit">Community &amp; Non-Profit</option>
									<option value="Computer">Computer</option>
									<option value="Construction">Construction</option>
									<option value="Cosmectics &amp; Beauty">Cosmectics &amp; Beauty</option>
									<option value="Dating">Dating</option>
									<option value="Education">Education</option>
									<option value="Entertainment &amp; The Arts">Entertainment &amp; The Arts</option>
									<option value="Environmental">Environmental</option>
									<option value="Fashion">Fashion</option>
									<option value="Floral">Floral</option>
									<option value="Food &amp; Drink">Food &amp; Drink</option>
									<option value="Games &amp; Recreational">Games &amp; Recreational</option>
									<option value="Home Furnishing">Home Furnishing</option>
									<option value="Industiral">Industiral</option>
									<option value="Internet">Internet</option>
									<option value="Landscaping">Landscaping</option>
									<option value="Medical &amp; Pharmaceutical">Medical &amp; Pharmaceutical</option>
									<option value="Photographiy">Photographiy</option>
									<option value="Physical Fitness">Physical Fitness</option>
									<option value="Political">Political</option>
									<option value="Real Estate &amp; Mortgage">Real Estate &amp; Mortgage</option>
									<option value="Religious">Religious</option>
									<option value="Restaurant">Restaurant</option>
									<option value="Retail">Retail</option>
									<option value="Security">Security</option>
									<option value="Spa &amp; Esthetics">Spa &amp; Esthetics</option>
									<option value="Sport">Sport</option>
									<option value="Technology">Technology</option>
									<option value="Travel &amp; Hotel">Travel &amp; Hotel</option>
									<option value="Wedding Service">Wedding Service</option>
								</select>
							</div>

							<div class="col-md-12 mt-20 required">
								<label>First name*</label>
								<input type="text" data-delay="300" name="firstname" id="firstname" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Last name*</label>
								<input type="text" data-delay="300" name="lastname" id="lastname" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12">
								<label>Job Title</label>
								<input type="text" data-delay="300" name="jobtitle" id="jobtitle" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Email Address</label>
								<input type="text" data-delay="300" name="email" id="email" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12 required">
								<label>Phone*</label>
								<input type="text" data-delay="300" name="phone" id="phone" class="input">
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-12">
								<label>What would you like to know?</label>
								<textarea name="message" id="message" class="textarea medium" aria-invalid="false" rows="10" cols="50"></textarea>
								<div class="message">&nbsp;</div>
							</div>

							<div class="col-md-1 cb">
								<label>
								<input name="notify" type="checkbox" value="I would like to be notified by email of future case studies, white papers, webinars and other educational content" checked="checked" id="notify">
								&nbsp;&nbsp;I would like to be notified by email of future case studies, white papers, webinars and other educational content</label>
							</div>

							<div class="col-md-12 mb-30">
							<div class="g-recaptcha" data-sitekey="6LeAfrMZAAAAAKOCyQ1JkBRMlIXW6PTpzpq8Zusy"></div>
							</div>

						</div>

			    </form>

			    <div><button name="btnGetQuote" id="btnGetQuote">Submit</button></div>		
			</div>		
			  


		</div>
	</div>
</div>


@endsection
@section('footer_scripts')
<script src="/js/library.js" type="text/javascript" ></script>
<script type="text/javascript" >
$(document).ready(function() {
    

});
</script>
@endsection

