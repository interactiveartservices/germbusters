@extends('layouts.master')

@section('content')
    <div class="container login">
        <div class="main">
            <div class="row">
                <div class="col-sm-6">
                    <p class="medium-title">ALREADY HAVE AN ACCOUNT?</p>

                    <form class="form" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-row form-row-wide">
                            <input id="email" name="email" class="input-text" type="email" placeholder="Email Address" autocomplete="email" value="{{ old('email') }}" tabindex="1" required autofocus />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-row form-row-wide">
                            <input id="password" type="password" class="input-text" name="password" placeholder="Password" required tabindex="2">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div>
                            <a href="#" data-toggle="modal" data-target="#modalForgotPassword" data-aos="fade-up" class="forgot-password text-dark font-size-14 text-underline">Forgot your password?</a>
                            <button type="submit" class="btn signin" value="Sign in" tabindex="3">Sign in</button>
                        </div>
                    </form>
                </div>

                <div class="col-sm-6 right-side">
                    <p class="medium-title">CREATE AN ACCOUNT ( OPTIONAL)</p>
                    <p>Save time when scheduling future cleanings by creating an account</p>
                    <a href="/create-account" id="create-account" class="btn mt-20 w-100">Create account</a>
                </div>
            </div>

            <div class="social-logins text-center">
                <hr class="separator add-or mt-20">
                <a class="btn-social" href="{{ url('/auth/facebook') }}">
                    <img src="/images/btn-fb.png" alt="google login" />
                </a>
                <a class="btn-social" href="{{ url('/auth/twitter') }}">
                    <img src="/images/btn-twitter.png" alt="google login" />
                </a>
                 <a class="btn-social" href="{{ url('/auth/google') }}">
                    <img src="/images/btn-google.png" alt="google login" />
                </a>

            </div>
        </div>
   
    </div>


    <div class="modal" id="modalForgotPassword" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <!-- <a href="#">BACK</a> -->
          <button type="button" class="close" data-dismiss="modal">&times;</button>
<!--           <h4 class="modal-title">Modal Header</h4> -->
        </div>

            <div id="forgot_password" class="steps modal-body text-center" >

                <h6><span>Forgot</span>  your password</h6>

                <p class="p-large">Enter your email address to get instructions on how to retrieve your password.</p>
                <div class="form-fields mb-50">
                    <form id="check-zip">
                        <input id="email" class="" type="text" name="email" placeholder="Email Address">
                        <a id="sendPassword" class="btn btn-teal mt-0 text-decoration-none">SEND</a>                    
                    </form>                    
                </div>

            </div>

             <div id="forgot_password_send" class="steps modal-body text-center hide" >

                <h6><span>Forgot</span> password</h6>

                <p class="p-large mb-50">Thank You.  An email has been sent to the address you entered with instructions
                For resetting your password.  If you don’t receive this email shortly, please <br>
                Check your junk or spam folder.</p>
            </div>


        </div>
      </div>
    </div>
</div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
    
    $(function() {

          $("#sendPassword").click(function(e) {
            e.preventDefault();

            $('#sendPassword').html('SEND <i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending
     

             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
             var email   = $("#forgot_password input[name='email']").val();

            $.post('/account/forgot_password', {'email':email, '_token': CSRF_TOKEN}, 

                function(response){  
               
                // Load json data from server and output message    
                if(response.type == 'error') {

                } else {

                    $("#forgot_password").addClass("hide");
                    $("#forgot_password_send").removeClass("hide");           
                }
               

            }, 'json');
        });
    });

  </script>
@endsection