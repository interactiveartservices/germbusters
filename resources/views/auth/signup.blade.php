@extends('layouts.master')

@section('content')

    <div class="container login">
        <div class="form create-acct mt-40 ml-40">
        	<form id="signup-form" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/signup-account') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="main-title">
                        <h2><span>Create an</span> account</h2>
                    </div>

                    <div class="social-logins">
                        <a class="btn-social" href="{{ url('/auth/Facebook') }}">
                            <img src="/images/btn-fb.png" alt="google login" />
                        </a>
                        <a class="btn-social" href="{{ url('/auth/twitter') }}">
                            <img src="/images/btn-twitter.png" alt="google login" />
                        </a>
                         <a class="btn-social" href="{{ url('/auth/google') }}">
                            <img src="/images/btn-google.png" alt="google login" />
                        </a>
                    </div>

                    <p class="mt-20">Or enter your information into the form below.</p>
                </div>
                 
                <div class="row mt-20 usr-info">
                    <div class="row">
                        <div class="col-md-6 required">
                            <label>First name*</label>
                            <input id="fname" name="fname" type="text" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                        <div class="col-md-6 required">
                            <label>Last name*</label>
                            <input id="lname" name="lname" type="text" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 required">
                            <label>Email Address*</label>
                            <input id="email" name="email" type="text" data-delay="300"  class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                     
                        <div class="col-md-3 required">
                            <label>Phone*</label>
                            <input id="phone" name="phone" type="text" data-delay="300"  class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                        
                        <div class="col-md-3">
                            <label>Alternative Phone</label>
                            <input id="altphone" name="altphone" type="text" data-delay="300" class="input">
                        </div>
                         
                    </div>

                    <div class="row">
                        <div class="col-md-2 required">
                            <label>Zip Code*</label>
                            <input id="zipcode" name="zipcode" type="text" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                        <div class="col-md-6 required">
                            <label>City*</label>
                            <input id="city" name="city" type="text" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>

                        <div class="col-md-4 required">
                            <label>State*</label>
                            <input id="state" name="state" type="text" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 required">
                            <label>Address 1*</label>
                            <input id="address1" name="address1" type="text" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                        <div class="col-md-6">
                            <label>Address 2</label>
                            <input id="address2" name="address2" type="text" data-delay="300" class="input">
                        </div>
                    </div>

                    <div class="row pwd">                
                        <div class="col-md-6 required">
                            <label>Password* (must be 8 characters and include a number)</label>
                            <input id="password" name="password" type="password" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                        <div class="col-md-6 required">
                            <label>Confirm Password*</label>
                            <input id="confirm" name="confirm" type="password" data-delay="300" class="input">
                            <div class="message">&nbsp;</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mt-20">
                                <div class="styledCheckbox sel">
                                    <i class="fas fa-check"></i>
                                </div><label> Sign up to receive occasional emails <br class="show-mobile"> with promotional offers for future offerings.</label>
                            </div>
                            <input id="offers" name="offers" type="hidden" value="1">
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-yellow border-0" value="0">CREATE ACCOUNT</button>
                        </div>
                    </div>

                </div>

                
            </form>

        </div>
    </div>
@endsection

@section('footer_scripts')
    <script src="/js/library.js" type="text/javascript" ></script>
    <script src="/js/location.js" type="text/javascript" ></script>
    <script src="/js/auth/signup.js" type="text/javascript" ></script>
    <script type="text/javascript">

    $(function() {
        $('.styledCheckbox').on('click',function() {
            if($(this).hasClass("sel")){
                $(this).removeClass('sel');
                $("#offers").val("0");
            }else{
                $(this).addClass('sel');
                $("#offers").val("1");
            }
        });

      });
    </script>
@endsection