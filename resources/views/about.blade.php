@extends('layouts.master')

@section('content')

 <!--Start Banner-->   
   <div class="sub-banner">
   	<picture>    	
		<source media="(max-width: 560px)" srcset="/images/sub-banner-form-mobile.png">
		<source media="(min-width: 561px)" srcset="/images/sub-banner-form.jpg 1x, /images/sub-banner-form@2x.jpg 2x">	
		<img class="banner-img" src="/images/sub-banner-form.jpg" alt="Coronavirus (COVID19) Deep Cleaning Information">
	</picture>
   </div>	   
   <!--End Banner-->

   <div class="content">
   		<div class="container ">

		   	<div class="row main">
		        <div class="main-title">
		            <h1 class="font-weight-bold"><span class="font-weight-light">About</span> Us</h1>
		        </div>

		        <div>
		        	<p>
			        	Who are we? We’re a group of visionary engineers, creative problem solvers, and investors from a variety of industries. Our vision is to use next-generation technology to protect the health of you and your business. We use a combination of next-generation UVC and electrostatic spraying solutions that can be used as a part of a regular part of your cleaning cycle. Our robots are safe and eliminate human error and are designed to be operated by every- day cleaning staff.
					</p>

		        </div>
		    </div>
	
	</div>




@endsection

