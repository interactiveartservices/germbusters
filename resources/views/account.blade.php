@extends('layouts.master')

@section('content')
    <style>
        .loader { position: fixed; margin: 0 auto; left: 55%; transform: translate(-50%, -50%); top: 45%; z-index: 9999999; }
        .message.error { color: red; font-size: small;display: block;margin-top: 5px; }
        .input-error {border: .063rem solid #c10000 !important; border-right: .563rem solid #c10000 !important; margin-bottom:10px!important;*/ }
    </style>

    <div id="loader" class="loader" style="display: none;" >
        <img src="/images/loading.gif" alt="" />
    </div>

    <div class="container">
        <div class="form mt-40 ml-40">
                <div class="">
                    <div class="main-title">
                        <h2>Your Account</h2>
                    </div>

                    <p class="mt-20">Hello <span class="text-capitalize">{{ Auth::user()->firstname. ' '. Auth::user()->lastname }}</span>, <!-- Your next cleaning is scheduled for <span class="font-weight-bold">Tuesday May 14</span> --></p>

                    @if(Auth::user()->provider == '')
                        <p class="mt-20">Change password &nbsp;<img src="/images/arr-change.png" alt="change" class="width-auto cursor-pointer" id="edit-pwd"></p>
                        <div id="div-pwd" class="mt-20 hide">
                                <div class="required">
                                <label>Old password*</label>
                                <input id="password" name="oldpassword" type="password" data-delay="300" class="input w-500" value="">
                                <div class="message">&nbsp;</div>
                            </div>
                            <div class="required">
                                <label>New password* (must be 8 characters and include a number)</label>
                                <input id="password" name="newpassword" type="password" data-delay="300" class="input w-500" value="">
                                <div class="message">&nbsp;</div>
                            </div>
                            <div class="required">
                                <label>Confirm Password*</label>
                                <input id="confirm" name="confirm" type="password" data-delay="300" class="input w-500" value="">
                                <div class="message">&nbsp;</div>
                            </div>

                            <button type="submit" id="updatePassword" class="btn btn-teal" value="">SAVE</button>
                        </div>
                    @endif
                </div>
                 
                <div class="mt-40">
                        <p class="title">YOUR INFORMATION</p>
                        <p class="font-weight-bold font-size-14">EMAIL ADDRESS </p>
                        <div class="row">
                        <div class="col-md-6">
                            <p><span id="emailadd">{{ Auth::user()->email }}</span><img src="/images/Icon feather-edit-2.png" class="width-auto ml-20"><a href="#" id="edit-email" class="d-inline-block ml-10 text-teal text-underline font-size-14">Edit</a></p>
                            <div id="div-email" class="hide">
                                 <input id="email" name="email" type="text" data-delay="300" class="input" value="{{ Auth::user()->email }}">
                                 <button type="submit" id="updateEmail" class="btn btn-teal" value="">SAVE</button>
                            </div>
                         </div>

                            <div class="col-md-6 float-right">If you no longer wish to received email promotions and updates, please</div> 
                           <div class="col-md-6 float-right mt-20"><a href="#" id="unsubscribe" class="text-dark text-underline"> Unsubscribe </a></div>
                        </div>             
                </div>

                 <div class="mt-40">
                    <div class="">
                        <p class="title">PRIMARY ADDRESS</p>
                        <p class="font-weight-bold font-size-14">RESIDENTIAL PRIMARY ADDRESS </p>
                         <div class="mt-20">
                            <div class="d-flex font-weight-semibold">
                                <span class="col-md-4">ADDRESS 1</span>
                                <span class="col-md-4">ADDRESS 2</span>
                                <span class="col-md-2">PHONE</span>
                                <span class="col-md-2">MODIFY</span>
                            </div>

                            <div class="d-flex data">
                                @php ($data = null)
                                @if(isset($addresses))
                                    @foreach($addresses as $key => $address)
                                        @if((int)$address->primary == 1)
                                            @php ($data = $address)
                                        @endif
                                    @endforeach
                                @endif

                                @if(isset($data))
                                    <span class="col-md-4">{{ $data->address1. ' '. $data->city. ', '. $data->state. ' '. $data->zipcode }}</span>
                                    <span class="col-md-4">{{ $data->address2. ' '. $data->city. ', '. $data->state. ' '. $data->zipcode }}</span>
                                    <span class="col-md-2">{{ $data->phone }}</span>
                                    <span class="col-md-2">
                                        <img src="/images/Icon feather-edit-2.png" class="width-auto">
                                        <a href="#" class="edit-modal d-inline-block ml-10 text-teal text-underline font-size-14" data-id="{{ $data->id }}" data-aos="fade-up">
                                            Edit
                                        </a>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-40">
                    <div class="">
                        <p class="title">OTHER ADDRESSES</p>
                         <div class="mt-20">
                            <div class="d-flex font-weight-semibold">
                                <span class="col-md-4">ADDRESS 1</span>
                                <span class="col-md-4">ADDRESS 2</span>
                                <span class="col-md-2">PHONE</span>
                                <span class="col-md-2">MODIFY</span>
                            </div>
                            
                            @if(isset($addresses))
                                @foreach($addresses as $key => $address)
                                    @if((int)$address->primary == 0)
                                        <div class="d-flex data">
                                            <span class="col-md-4">{{ $address->address1. ' '. $address->city. ', '. $address->state. ' '. $address->zipcode }}</span>
                                            <span class="col-md-4">{{ $address->address2. ' '. $address->city. ', '. $address->state. ' '. $address->zipcode }}</span>
                                            <span class="col-md-2">{{ $address->phone}}</span>
                                            <span class="col-md-2">
                                                <img src="/images/Icon feather-edit-2.png" class="width-auto">
                                                <a href="#" class="edit-modal d-inline-block ml-10 text-teal text-underline font-size-14" data-id="{{ $address->id }}" data-aos="fade-up">
                                                    Edit
                                                </a>
                                            </span>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <p class="font-weight-bold font-size-14 py-20">ADD NEW RESIDENTIAL ADDRESS &nbsp;<a href="#" data-toggle="modal" data-target="#modaladdAddress" data-aos="fade-up"><img src="/images/arr-change.png" alt="change" class="width-auto"></a></p>
                        <p class="title">SCHEDULED APPOINTMENTS</p>
                        <p class="small">Recently scheduled appointments may not show up right away.  Please check back</p>
                         <p class="font-weight-bold font-size-14 py-20">SCHEDULED RESIDENTIAL SERVICES
                        <div class="mt-20">
                            <div class="d-flex font-weight-semibold">
                                <span class="col-md-8">ADDRESS</span>
                                <span class="col-md-4">DATE/TIME CREATED</span>
                                <span class="col-md-4">&nbsp;</span>
                            </div>
 
                            @if(isset($orders))
                                @foreach($orders as $key => $order)
                                    <div class="d-flex data">
                                        @if((int)$order->addrtype == 1)
                                            <span class="col-md-8">
                                                {{ $order->address1. ' '. $order->city. ', '. $order->state. ' '. $order->zipcode }}
                                            </span>
                                        @else
                                            <span class="col-md-8">
                                                {{ $order->address2. ' '. $order->city. ', '. $order->state. ' '. $order->zipcode }}
                                            </span>
                                        @endif

                                        <span class="col-md-4">{{ date("m/d/Y h:i a", strtotime($order->date_added)) }}</span>
                                        <span class="col-md-4"><a href="#" class="text-teal text-underline"><a href="#" data-id="{{ $order->id }}" class="order-details text-teal text-underline">View details</a></span>                              
                                    </div>
                                @endforeach
                            @endif

                        </div>
                        <p class="font-weight-bold font-size-14 mt-20">SCHEDULED NEW RESIDENTIAL SERVICES<a href="/cleaning-quote/modify-services"> <img src="/images/arr-change.png" alt="change" class="width-auto"></a></p>
                         
                    </div>
                </div>
        </div>
    </div>

    <div class="modal" id="modaladdAddress" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <a href="#">BACK</a> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>
                <div id="step_address" class="steps modal-body text-center">
                    <p>YOUR ACCOUNT</p>
                    <h6 class="line-height-1-2">Add a New Address</h6>
     
                    <form id="addr-add-form" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/account/address') }}">
                        {{ csrf_field() }}

                        <input type="hidden" id="id" name="id" value="0">

                        @if(Auth::check())
                            <input type="text" id="fname" name="fname" placeholder="Firstname" value="{{ Auth::user()->firstname }}" disabled>
                        @else
                            <input type="text" id="fname" name="fname" placeholder="Firstname">
                        @endif

                        @if(Auth::check())
                            <input type="text" id="lname" name="lname" placeholder="Lastname" value="{{ Auth::user()->lastname }}" disabled>
                        @else
                            <input type="text" id="lname" name="lname" placeholder="Lastname">
                        @endif

                        <span class="required">
                            <input type="text" class="addr-info" id="zipcode" name="zipcode" placeholder="Zip Code" style="width:15%;">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="city" name="city" placeholder="City" style="width:45%;">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="state" name="state" placeholder="State" style="width:28%;">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="address1" name="address1" placeholder="Address 1">
                        </span>
                        <span>
                            <input type="text" class="addr-info" id="address2" name="address2" placeholder="Address 2">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="phone" name="phone" placeholder="Phone">
                        </span>
                        <span>
                            <input type="text" class="addr-info" id="altphone" name="altphone" placeholder="Alternative Phone">
                        </span>
                        <p>
                            Update the phone number below fo future appointment text <br>
                            Reminders. You currently have an appointment scheduled, please <br>
                            Call 954-462-4000 to change your text number.
                        </p>
                        <div class="text-left mt-20 ml-20 w-100">   
                            <input type="text" class="addr-info" id="sms" name="sms" placeholder="SMS Phone">
                        </div>
                        <div class="text-left ml-20 mt-20">
                             <div class="styledCheckbox">
                                <i class="fas fa-check"></i>
                             </div>
                             <label class="font-weight-medium">
                                Make this my primary address
                             </label>
                             <input id="isprimary" name="isprimary" type="hidden" value="0">
                        </div>
                        
                        <a id="new_btn_address" data-step="" class="py-40 step-continue btn btn-yellow" >SAVE & CONTINUE</a>   
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalEditAddress" tabindex="-1" role="dialog"  data-id="0">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <a href="#">BACK</a> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>
                <div id="edit_address" class="steps modal-body text-center">
                    <p>YOUR ACCOUNT</p>
                    <h6 class="line-height-1-2">Edit Address</h6>
     
                    <form id="addr-edit-form" class="form-horizontal login-form" role="form" method="POST" action="{{ url('/account/address') }}">
                        {{ csrf_field() }}

                        <input type="hidden" id="id" name="id" value="0">

                        @if(Auth::check())
                            <input type="text" id="fname" name="fname" placeholder="Firstname" value="{{ Auth::user()->firstname }}" disabled>
                        @else
                            <input type="text" id="fname" name="fname" placeholder="Firstname">
                        @endif

                        @if(Auth::check())
                            <input type="text" id="lname" name="lname" placeholder="Lastname" value="{{ Auth::user()->lastname }}" disabled>
                        @else
                            <input type="text" id="lname" name="lname" placeholder="Lastname">
                        @endif

                        <span class="required">
                            <input type="text" class="addr-info" id="zipcode" name="zipcode" placeholder="Zip Code" style="width:15%;">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="city" name="city" placeholder="City" style="width:45%;">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="state" name="state" placeholder="State" style="width:28%;">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="address1" name="address1" placeholder="Address 1">
                        </span>
                        <span>
                            <input type="text" class="addr-info" id="address2" name="address2" placeholder="Address 2">
                        </span>
                        <span class="required">
                            <input type="text" class="addr-info" id="phone" name="phone" placeholder="Phone">
                        </span>
                        <span>
                            <input type="text" class="addr-info" id="altphone" name="altphone" placeholder="Alternative Phone">
                        </span>
                        <p>
                            Update the phone number below fo future appointment text <br>
                            Reminders. You currently have an appointment scheduled, please <br>
                            Call 954-462-4000 to change your text number.
                        </p>
                        <div class="text-left mt-20 ml-20 w-100">   
                            <input type="text" class="addr-info" id="sms" name="sms" placeholder="SMS Phone">
                        </div>
                        <div class="text-left ml-20 mt-20">
                             <div class="styledCheckbox">
                                <i class="fas fa-check"></i>
                             </div>
                             <label class="font-weight-medium">
                                Make this my primary address
                             </label>
                             <input id="isprimary" name="isprimary" type="hidden" value="0">
                        </div>
                        
                        <a id="edit_btn_address" data-step="" class="py-40 step-continue btn btn-yellow" >SAVE & CONTINUE</a>   
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalServiceDetails" tabindex="-1" role="dialog"  data-id="0">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <a href="#">BACK</a> -->
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!-- <h4 class="modal-title">Modal Header</h4> -->
                </div>
                <div id="view_details" class="py-20 modal-body main-title">
                    <h2 class="line-height-1-2 text-center"><span>SERVICES</span> ORDERED</h2>
             
                    <div class="row py-20 font-opensans font-size-14">
                    <p class="font-weight-bold">CONFIRMATION #<span id="order-confirmation"></span></p>
                        <div class="col-md-12 bg-teal text-white py-10 font-weight-bold">
                            <div class="col-xs-7">SERVICE</div>
                            <div class="col-xs-2 text-center">QTY</div>
                            <div class="col-xs-3 text-center">PRICE</div>
                        </div>

                        <div id="disenfect" class="col-md-12 py-10">
                            <div class="col-xs-7">Electrostatic Spray</div>
                            <div id="disenfect-count" class="col-xs-2 text-center">
                                0
                            </div>
                            <div id="disenfect-price" class="col-xs-3 text-center">
                              $0.00
                            </div>
                        </div>
                      
                        <div id="uvcservice" class="col-md-12 py-10">
                            <div class="col-xs-7">UVC Disinfect</div>
                            <div id="uvcservice-count" class="col-xs-2 text-center">
                              0
                            </div>
                            <div id="uvcservice-price" class="col-xs-3 text-center">
                              $0.00
                            </div>
                        </div>

                        <div id="wipeservice" class="col-md-12 py-10">
                            <div class="col-xs-7">Wipe Service</div>
                            <div id="wipeservice-count" class="col-xs-2 text-center">
                              0
                            </div>
                            <div id="wipeservice-price" class="col-xs-3 text-center">
                              $0.00
                            </div>
                        </div>
                       
                        <div id="apttest" class="col-md-12 py-10">
                            <div class="col-xs-7">APT Test</div>
                            <div id="apttest-count" class="col-xs-2 text-center">
                              0
                            </div>
                            <div id="apttest-price" class="col-xs-3 text-center">
                             $0.00
                            </div>
                        </div>
                                                 
                        <div id="no-parking" class="col-md-12 py-10">
                            <div class="col-xs-7">No Parking Nearby</div>
                            <div class="col-xs-2"></div>
                            <div id="no-parkng-price" class="col-xs-3 text-center">
                               $0.00 
                            </div>
                        </div>
                                          
                        <div id="area-parking" class="col-md-12 py-10">
                            <div class="col-xs-7">Area is on 3rd Floor or Higher</div>
                            <div class="col-xs-2"></div>
                            <div id="area-parking-price" class="col-xs-3 text-center">
                                $0.00 
                            </div>
                        </div>

                         <div class="col-md-12 bg-gray py-10 font-weight-bold">
                            <div class="col-xs-7">SUBTOTAL</div>
                            <div id="subtotal-count" class="col-xs-2 text-center">
                            </div>
                            <div id="subtotal-price"  class="col-xs-3 text-center">
                              $0.00 
                            </div>
                        </div>
                         <div  id="promo" class="col-md-12 py-10 font-weight-bold">
                            <div class="col-xs-7">PROMO</div>
                            <div id="promo-count" class="col-xs-2 text-center">
                            </div>
                            <div id="promo-price" class="col-xs-3 text-center">
                              $0.00 
                            </div>
                        </div>
                    </div>

                    <div class="list-title py-10 font-weight-bold font-opensans font-size-14">
                        <div class="col-md-12">                     
                            <div class="col-sm-7 text-center">
                                
                            </div>
                            <div class="col-xs-12 col-sm-3 text-left">
                                <span class="total-header font-size-20">TOTAL</span>        
                            </div>
                            <div  class="col-xs-12 col-sm-2 text-left">
                                <span id="total_price" class="total-header text-teal font-size-20">$0.00</span>     
                            </div>
                        </div>
                    </div>  

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
    <script src="/js/library.js" type="text/javascript" ></script>
    <script src="/js/location.js" type="text/javascript" ></script>
    <script src="/js/accounting.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    
    jQuery(document).ready(function($) {


        $("#edit-pwd").click(function(e) {
            e.preventDefault();
            $("#div-pwd").removeClass("hide");
        });

        $("#edit-email").click(function(e) {
            e.preventDefault();
            $("#div-email").removeClass("hide");
        });


        $("#updateEmail").click(function(e) {
            e.preventDefault();

             $('#updateEmail').html('SAVE <i class="fa fa-spinner fa-pulse fa-fw"></i>'); // Message displayed in the submit button during the sending
     
             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");
             var email   = $("input[name='email']").val();

            $.post('/account/update_email', {'email':email, '_token': CSRF_TOKEN}, 

                function(response){  
               
                // Load json data from server and output message    
                if(response.type == 'error') {

                } else {

                    $('#updateEmail').html('SAVE'); // Message displayed in the submit button during the sending
                    $("#div-email").addClass("hide");
                    $(".form").find("#emailadd").text(email);             
                }
               

            }, 'json');
        });

        $("#unsubscribe").click(function(e) {
            e.preventDefault();

             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");

            $.post('/account/unsubscribe', {'_token': CSRF_TOKEN}, 

                function(response){  
               
                // Load json data from server and output message    
                if(response.type == 'error') {

                } else {

                    alert('Saved! You will no longer receive email promotions and updates.');        
                }
               

            }, 'json');
        });

        /**** ADD NEW ADDRESS ****/
        var $newaddr = $("#addr-add-form");

        $newaddr.find('.styledCheckbox').on('click',function() {
            if($(this).hasClass("sel")){
                $newaddr.find("input[name='isprimary']").val("0");
                $(this).removeClass('sel');
            }else{
                $newaddr.find("input[name='isprimary']").val("1");
                $(this).addClass('sel');
            }
        });

        $newaddr.find("#new_btn_address").click(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");

            var $elem = null;
            var iserr = false;

            $(this).attr("disabled", true);

            $newaddr.removeClass("form-error");
            // clear all error messages
            $newaddr.find(".error").html("&nbsp;").removeClass("error");
            // clear all borderlines except for email 
            $newaddr.find(".input-error").removeClass("input-error");

            /* zip code */
            $elem = $("#step_address input[name='zipcode']");
            if(isRequired($elem, "Please enter a valid zip code")) {
                iserr = !iserr ? true : iserr;   
            }  

            $elem = $("#step_address input[name='zipcode']");
            if(isZipCode($elem)) {
                iserr = !iserr ? true : iserr;   
            }  

            /* city */
            $elem = $("#step_address input[name='city']");
            if(isRequired($elem, "Please provide your city")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* state */
            $elem = $("#step_address input[name='state']");
            if(isRequired($elem, "Please provide your state")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* address 1 */
            $elem = $("#step_address input[name='address1']");
            if(isRequired($elem, "Please provide your address")) {
                iserr = !iserr ? true : iserr;   
            }          

            /* phone */
            $elem = $("#step_address input[name='phone']");
            if(isRequired($elem, "Please enter a valid phone number")) {
                iserr = !iserr ? true : iserr;   
            } 
 
            $(this).removeAttr("disabled");

            if(!iserr) {
                $newaddr.get(0).submit();
            }
        });

        $('#modaladdAddress').on('shown.bs.modal', function () {
            $newaddr.removeClass("form-error");
            // clear all error messages
            $newaddr.find(".error").html("&nbsp;").removeClass("error");
            // clear all borderlines except for email 
            $newaddr.find(".input-error").removeClass("input-error");

            $newaddr.find("input[class='addr-info']").val("");
            $("#step_address input[name='isprimary']").val("0");

            var $primary = $newaddr.find(".styledCheckbox");

            if($primary.hasClass("sel")) {
                $primary.removeClass("sel");
            }
        });

        /**** EDIT ADDRESS ****/
        var $editaddr = $("#addr-edit-form");

        $editaddr.find('.styledCheckbox').on('click',function() {
            if($(this).hasClass("sel")){
                $editaddr.find("input[name='isprimary']").val("0");
                $(this).removeClass('sel');
            }else{
                $editaddr.find("input[name='isprimary']").val("1");
                $(this).addClass('sel');
            }
        });

        $editaddr.find("#edit_btn_address").click(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");

            var $elem = null;
            var iserr = false;

            $(this).attr("disabled", true);

            $editaddr.removeClass("form-error");
            // clear all error messages
            $editaddr.find(".error").html("&nbsp;").removeClass("error");
            // clear all borderlines except for email 
            $editaddr.find(".input-error").removeClass("input-error");

            /* zip code */
            $elem = $("#edit_address input[name='zipcode']");
            if(isRequired($elem, "Please enter a valid zip code")) {
                iserr = !iserr ? true : iserr;   
            }  

            $elem = $("#edit_address input[name='zipcode']");
            if(isZipCode($elem)) {
                iserr = !iserr ? true : iserr;   
            }  

            /* city */
            $elem = $("#edit_address input[name='city']");
            if(isRequired($elem, "Please provide your city")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* state */
            $elem = $("#edit_address input[name='state']");
            if(isRequired($elem, "Please provide your state")) {
                iserr = !iserr ? true : iserr;   
            }  

            /* address 1 */
            $elem = $("#edit_address input[name='address1']");
            if(isRequired($elem, "Please provide your address")) {
                iserr = !iserr ? true : iserr;   
            }          

            /* phone */
            $elem = $("#edit_address input[name='phone']");
            if(isRequired($elem, "Please enter a valid phone number")) {
                iserr = !iserr ? true : iserr;   
            } 
 
            $(this).removeAttr("disabled");

            if(!iserr) {
                $editaddr.get(0).submit();
            }
        });

        $('.edit-modal').on('click',function() {
            $("#edit_address input[name='id']").val($(this).data('id'));

            $('#modalEditAddress').attr("data-id", $(this).data('id'));
            $('#modalEditAddress').modal("show");
        });

        $('#modalEditAddress').on('shown.bs.modal', function () {     
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content"); 

            $editaddr.removeClass("form-error");
            // clear all error messages
            $editaddr.find(".error").html("&nbsp;").removeClass("error");
            // clear all borderlines except for email 
            $editaddr.find(".input-error").removeClass("input-error");

            $editaddr.find("input[class='addr-info']").val("");
            $("#edit_address input[name='isprimary']").val("0");

            var $primary = $editaddr.find(".styledCheckbox");

            if($primary.hasClass("sel")) {
                $primary.removeClass("sel");
            }

            $.post('/account/getaddress', {     _token: CSRF_TOKEN,
                                                  'id': $(this).attr('data-id')
                                          }, function(result) {
                var o = JSON.parse(result);

                $("#edit_address input[name='zipcode']").val(o.zipcode);
                $("#edit_address input[name='city']").val(o.city);
                $("#edit_address input[name='state']").val(o.state);
                $("#edit_address input[name='address1']").val(o.address1);
                $("#edit_address input[name='address2']").val(o.address2);
                $("#edit_address input[name='phone']").val(o.phone);
                $("#edit_address input[name='altphone']").val(o.alt_phone);
                $("#edit_address input[name='sms']").val(o.sms_phone);

                var $primary = $editaddr.find(".styledCheckbox");
             
                if(parseInt(o.primary) === 1) {
                    $("#edit_address input[name='isprimary']").val("1");
                    $primary.removeClass("sel").addClass("sel");
                } else {
                    $("#edit_address input[name='isprimary']").val("0");
                    $primary.removeClass("sel");                    
                }
            });
        });


        /**** CHANGE PASSWORD ****/
        $("#updatePassword").click(function(e) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content");  

            var $elem = null;
            var iserr = false;

            // clear all error messages
            $("#div-pwd .error").html("&nbsp;").removeClass("error");
            // clear all borderlines except for email 
            $("#div-pwd .input-error").removeClass("input-error");

            /* oldpassword */
            $oldpass = $("#div-pwd input[name='oldpassword']");
            if(isRequired($oldpass, "Please enter a old password.")) {
                iserr = !iserr ? true : iserr;         
            }                

            if(!isPassword($oldpass)) {
                iserr = !iserr ? true : iserr;            
            }     

            /* newpassword */
            $newpass = $("#div-pwd input[name='newpassword']");
            if(isRequired($newpass, "Please enter a new password.")) {
                iserr = !iserr ? true : iserr;         
            }                

            if(!isPassword($newpass)) {
                iserr = !iserr ? true : iserr;            
            }           

            /* confirm password */
            $conf = $("#div-pwd input[name='confirm']");
            if(isRequired($conf, "Please confirm the password.")) {
                iserr = !iserr ? true : iserr;         
            }    

            /* confirm password */
            $conf = $("#div-pwd input[name='confirm']");
            if(isRequired($conf, "Please confirm the password.")) {
                iserr = !iserr ? true : iserr;         
            }                

            if(!isPasswordMatch($newpass, $conf, "Please make sure your passwords match.")) {
                iserr = !iserr ? true : iserr;         
            }

            if(!iserr) {
                $.post('/account/updatepassword', {        _token: CSRF_TOKEN,
                                                    "oldpass": $oldpass.val(),
                                                    "newpass": $newpass.val()
                                          }, function(result) {
                    if(result === "") {
                        $("#div-pwd").removeClass("hide").addClass("hide");
                    } else {
                        alert(result);
                    }
                                          
                });
            }
        });

        /**** ORDERS ****/
        $(".order-details").click(function(e) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr("content"); 

            $("#order-confirmation").text("");

            $("#disenfect-count").text("0");
            $("#disenfect-price").text("$0.00");
            $("#disenfect").hide();

            $("#uvcservice-count").text("0");
            $("#uvcservice-price").text("$0.00");
            $("#uvcservice").hide();

            $("#wipeservice-count").text("0");
            $("#wipeservice-price").text("$0.00");
            $("#wipeservice").hide();

            $("#apttest-count").text("0");
            $("#apttest-price").text("$0.00");
            $("#apttest").hide();

            // $("#subtotal-count").text("0");
            $("#subtotal-price").text("$0.00");

            $("#promo-price").text("$0.00");
            $("#promo").hide();

            $("#no-parkng-price").text("$0.00");
            $("#no-parking").hide();

            $("#area-parking-price").text("$0.00");
            $("#area-parking").hide();

            $("#total_price").text("$0.00");

            $("#loader").show();
            $.post('/account/getorder', {        _token: CSRF_TOKEN,
                                                   "id": $(this).attr("data-id")
                                          }, function(result) {
                var o = JSON.parse(result);

                $("#order-confirmation").text(o.order.id);

                if(parseInt(o.order.total_count_disinfect) > 0) {
                    $("#disenfect").show();
                    $("#disenfect-count").text(o.order.total_count_disinfect);
                    $("#disenfect-price").text(accounting.formatMoney(o.order.total_price_disinfect, "$", 2, ",", "."));
                } else {
                    $("#disenfect").hide();
                }

                if(parseInt(o.order.total_count_wipe) > 0) {
                    $("#wipeservice").show();
                    $("#wipeservice-count").text(o.order.total_count_wipe);
                    $("#wipeservice-price").text(accounting.formatMoney(o.order.total_price_wipe, "$", 2, ",", "."));
                } else {
                    $("#wipeservice").hide();
                }

                if(parseInt(o.order.total_count_uvc) > 0) {
                    $("#uvcservice").show();
                    $("#uvcservice-count").text(o.order.total_count_uvc);
                    $("#uvcservice-price").text(accounting.formatMoney(o.order.total_price_uvc, "$", 2, ",", "."));
                } else {
                    $("#uvcservice").hide();
                }

                if(parseInt(o.order.total_count_apttest) > 0) {
                    $("#apttest").show();
                    $("#apttest-count").text(o.order.total_count_apttest);
                    $("#apttest-price").text(accounting.formatMoney(o.order.total_price_apttest, "$", 2, ",", "."));
                } else {
                    $("#apttest").hide();
                }

                var totalqty = parseInt(o.order.total_count_disinfect) + parseInt(o.order.total_count_uvc) + parseInt(o.order.total_count_wipe) + parseInt(o.order.total_count_apttest);

                // $("#subtotal-count").text(totalqty);
                $("#subtotal-price").text(accounting.formatMoney(o.order.subtotal, "$", 2, ",", "."));

                if(parseFloat(o.order.discount) > 0.00) {
                    $("#promo").show();
                    $("#promo-price").text(accounting.formatMoney(o.order.discount, "- $", 2, ",", "."));
                } else {
                    $("#promo").hide();
                }

                var prid = '5ece4797eaf5e';
                var item = null;

                o.items.forEach(function(data){
                    if(data.itemid === prid) {
                        item = data;
                        return false;
                    }
                });
            
                if(parseInt(item.is_check) == 1) {
                    $("#no-parking").show();
                    $("#no-parkng-price").text(accounting.formatMoney(item.price, "$", 2, ",", "."));
                } else {
                    $("#no-parking").hide();
                }

                var prid = '5ed4ea1f9eeb1';
                var item = null;

                o.items.forEach(function(data){
                    if(data.itemid === prid) {
                        item = data;
                        return false;
                    }
                });
        
                if(parseInt(item.is_check) == 1) {
                    $("#area-parking").show();
                    $("#area-parking-price").text(accounting.formatMoney(item.price, "$", 2, ",", "."));
                } else {
                    $("#area-parking").hide();
                }

                $("#total_price").text(accounting.formatMoney(o.order.total, "$", 2, ",", "."));
                $("#loader").hide();
            });

            $("#modalServiceDetails").modal("show"); 
        })
    });

    </script>
@endsection