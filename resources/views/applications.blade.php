@extends('layouts.master')

@section('content')

 <!--Start Banner-->   
<div class="sub-banner position-relative overflow-hidden" style="height:565px;width:100%;">  
<div class="visible-lg-block" style="padding:56.28% 0 0 0;position: relative;top:-50%" ><iframe src="https://player.vimeo.com/video/425350858?autoplay=1&loop=1&color=ffffff&portrait=0&background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<div class="hidden-lg">
		<img class="banner-uvc-img hidden-md" src="/images/bg-application-banner-mobile.jpg" alt="GermBusters provides next-generation disinfection">		
	</div>
<div class="container center-absolute mw-900">
		<div class="row">
			<div class="col-sm-12 d-flex applications-banner-text text-white">
				<div>
					<h1 class="font-weight-bold pr-50 text-white mt-0">Applications</h1>
				</div>
				<div class="margin-auto font-raleway font-size-15 line-height-1-5">
					GermBusters provides next-generation disinfection UVC light and electrostatic
					 sanitizing solutions to clean and neutralize viruses bacteria, mold and fungi at your
					 home or office.  Our products and services are scientifically verified and backed with 
					a no-risk money-back guarantee.
				</div>
			</div>
		</div>
	</div>		
</div>

<div class="container-fluid bg-white text-center pt-30 pb-10 shadow mb-30"> 
	<div class="row">
		<div class="col-sm-12">
			<div class="main-title mt-20"><h2><span>Get back to business</span> with GermBusters</h2>
				<p class="font-raleway">Whether you’re looking to disinfect a school, gym, office building, bank or any commercial <br>
											space, GermBusters can ensure the high level of sanitation needed to keep your facility clean. <br>
											Let GermBusters be your front line against Infections with antimicrobial products that stop <br>
											germs in their tracks and discourage superbugs from breeding.
				</p>	
			</div>
			
		</div>
	</div>
</div>     

<div class="container">

	<div id="restaurants" class="row p-30 position-relative">
		<div class="col-sm-12 ">
			<img src="/images/applications-restaurants.jpg" alt="applications restaurants" />
			<div class="txt">
				<h2>Restaurants</h2>
				<hr>
				<p>Aerobic bacteria was found on 100 percent of the ketchup bottles and 
				83 percent of the menus. E.coli—a type of coliform bacteria—was 
				found on 4 percent of ketchup bottles and 8 percent of menus.  
				Protect your customers and your hard earned reputation from these 
				bacteria with GernBusters Sani-Trek™UV-C cart and PathoGenator 
				AI UV-C robot. Our UV-C solutions kill 99.9% of dangerous pathogens
				 without the use of chemicals.  Disinfect hard to reach surfaces, air and 
				equipment in your dining and prep areas to eliminate germs and keep 
				your customers and staff healthy.</p>
			</div>
		</div>
	</div>

	<div id="gyms" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-gyms.jpg" alt="applications gyms" />
			<div class="txt">
				<h2>Gyms</h2>
				<hr>
				<p>What happens when your gym goes viral in the wrong way? Today’s 
				exercise trends are creating more opportunities for the spread of 
				harmful pathogens like like MRSA and Norovirus. Set your athletic 
				club apart and give your customers confidence and peace of mind 
				that they are training in a safe and clean environment.    
				GermBusters Sani-Trek™ UV-C cart and PathoGenator  UV-C robot are 
				user friendly and are designed to be incorporated into your every-day 
				cleaning services and give you the ability to disinfect a large 
				area to clinical standards.</p>
			</div>
		</div>
	</div>

	<div id="casinos" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-casinos.jpg" alt="applications casinos" />
			<div class="txt left">
				<h2>Casinos</h2>
				<hr>
				<p>Mobile and robotic UVC cleaning can help stop Norovirus outbreaks 
				from spreading on high touch surfaces such as slot machines and 
				gaming tables. Customized UVC chip holders in conjunction with 
				electrostatic spray services in bathrooms and outdoor seating areas, 
				along with custom UVC HVAC installations can significantly reduce 
				the spread of highly infectious pathogens potentially spread in casinos.
				</p>
			</div>
		</div>
	</div>

	<div id="schools" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-schools.jpg" alt="applications schools" />
			<div class="txt left top">
				<h2>Schools</h2>
				<hr>
				<p>Every school year, elementary school students contract and spread 
				anywhere from 8 to 10 cold and/or flu cases. The result? 60 million 
				lost school days and $4.2 billion of lost annual income. Germbusters 
				Manual and Robotic UVC disinfection solutions units can help stop 
				flu and Norovirus outbreaks from spreading on high touch surfaces.
				Furthermore, a combination of Germbusters UVC and electrostatic
				 cleaning offers a 360° cleaning approach that has proven to 
				drastically reduce student and teacher illness rates allowing them 
				to spend more time on what matters most.
				</p>
			</div>
		</div>
	</div>

	<div id="hospitals" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-hospitals.jpg" alt="applications hospitals" />
			<div class="txt top">
				<h2>Hospitals</h2>
				<hr>
				<p>Each HAI costs a facility approximately $25,000 in re-treatment, fines 
				and other associated costs. And while every medical facility combats 
				HAI’s not two facilities are alike.  Improve patient outcomes and save 
				valuable time and money for your work with the benefit of full-spectrum 
				of customized disinfection services including:
				• Smart AI auto-piloted UVC robots
				• Manual driven UV carts 
				• Electrostatic spraying & hand wiping service
				• UVC fixtures installation- clean on demand
				</p>
			</div>
		</div>
	</div>

	<div id="hospitality" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-hospitality.jpg" alt="applications hospitality" />
			<div class="txt top">
				<h2>Hospitality</h2>
				<hr>
				<p> 
				Go beyond clean.  Harvest the power of UV-C and reduce the use of 
				harsh industrial chemicals to keep the rooms fresh and safe. 
				Housekeeping staff can overlook basic hygienic practices. 
				GermBusters Sani-Trek™ UV-C cart and PathoGenator AI UV-C 
				robot are user friendly and are designed to be incorporated into 
				your every-day cleaning services.   Differentiate your Hotel as a 
				destination  that goes the extra mile to limit the spread of 
				pathogens and protect the wellness your patrons and employees. 
				</p>
			</div>
		</div>
	</div>

	<div id="grocery-stores" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-grocery.jpg" alt="applications grocery" />
			<div class="txt left">
				<h2>Grocery Stores</h2>
				<hr>
				<p>Grocery stores present challenging high-touch surfaces areas that 
				combined with food amplify and limit disinfection options- until now.  
				Germbusters Sani-Trek™ cart is a unique patent pending unidirectional
				 wall of UV-C light that allows cleaning crews to 99.9% of germs along 
				stocked isles, end-caps and point-of-purchase displays, without the 
				use of chemicals.   Differentiate your store as one that goes the extra 
				mile to limit the spread of pathogens and protect the wellness your 
				patrons and employees. 
				</p>
			</div>
		</div>
	</div>

	<div id="nursing-home" class="row p-30 position-relative">
		<div class="col-sm-12">
			<img src="/images/applications-nursing.jpg" alt="applications nursing" />
			<div class="txt top">
				<h2>Nursing home</h2>
				<hr>
				<p>You work hard to clean and disinfect your facility to prevent infection 
				and keep patients and residents healthy. But one important 
				component is often missing from their cleaning routine: soft surfaces. 
				Pathogens can survive on fabrics for up to 2.5 months and can then
				 be transferred to the hands of staff, patients and residents.   
				Germbusters Manual and Robotic UVC disinfection solutions reduce I
				nfection and the rates of hospitalization for infection keeping your 
				residents, visitors and employees healthy.
 				</p>
			</div>
		</div>
	</div>


</div>    

@endsection